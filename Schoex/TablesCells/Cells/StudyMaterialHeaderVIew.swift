//
//  StudyMaterialHeaderVIew.swift
//  Schoex
//
//  Created by shoaib uddin on 01/05/2020.
//  Copyright © 2020 3rdHub Technologies. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

protocol StudyMaterialHeaderVIewDelegate: class {
    func setSelectedSubject(cell: StudyMaterialHeaderVIew, index: Int )
}



class StudyMaterialHeaderVIew: UITableViewHeaderFooterView {

    var subjects: [SubjectsModel] = [];
    var selectedSubject: Int = 0;
    weak var delegate: StudyMaterialHeaderVIewDelegate?
    
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var countLbl: UILabel!
    @IBOutlet weak var headingLbl: UILabel!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    func setData(subjects: [SubjectsModel], selectedSubject: String ){
        
        
        self.subjects = subjects;
        let heading = Language.getTranslationOf(findKey: "Subjects", defaultWord: "Subjects");
        
        print(selectedSubject)
        if(selectedSubject == ""){
            setButtonTitle(choice: heading);
        }else{
            
            if let i = Int(selectedSubject){
                
                let t = self.subjects.first { (item) -> Bool in
                    return item.id == i;
                }
                
                
                
//                let t = self.subjects[i]
                if let r = t, let title = r.subjectTitle{
                    setButtonTitle(choice: title);
                }
                
            }
            
        }
        
        
    }
    
    func setButtonTitle(choice: String){
        self.menuBtn.setTitle("\(choice) - \(self.subjects.count)", for: .normal)
    }
    
    
    @IBAction func openSubjectPicker(sender: UIButton){
        
        let title = Language.getTranslationOf(findKey: "Subjects", defaultWord: "Subjects");
        let rows = self.subjects.map { (item) -> String in
            return item.subjectTitle ?? ""
        }
        
        ActionSheetStringPicker.show(withTitle: title, rows: rows, initialSelection: selectedSubject, doneBlock: { (picker, index, data) in
            
            //
            let choice = self.subjects[index];
            self.setButtonTitle(choice: choice.subjectTitle ?? "");
            self.delegate?.setSelectedSubject(cell: self, index: index)
            
            
        }, cancel: { (picker) in
            //
            
        }, origin: nil)
        
    }
    
    

}
