//
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import FTPopOverMenu_Swift
import SBFramework

class HomeworkCell: UITableViewCell {
    
    @IBOutlet weak var HomeworkTitle: SolLabelView!
    @IBOutlet weak var HomeworkDate: SolLabelView!
    @IBOutlet weak var HomeworkSubDate: SolLabelView!
    @IBOutlet weak var HomeEvalDate: SolLabelView!
    
    @IBOutlet weak var cellBtn: SolImageView!
    
    var navController : UINavigationController?

    var item : HomeworkModel! {
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func updateUI (){
        HomeworkTitle!.textNotNull = item.homeworkTitle!
        HomeworkDate!.textNotNull = "\(Language.getTranslationOf(findKey: "date",defaultWord: "Date")) : \(item.homeworkDate!)"
        
        HomeworkSubDate!.textNotNull = "\(Language.getTranslationOf(findKey: "SubmissionDate",defaultWord: "Submission Date")) : \(item.homeworkSubmissionDate!)"
        HomeEvalDate!.textNotNull = "\(Language.getTranslationOf(findKey: "EvaluationDate",defaultWord: "Evaluation Date")) : \(item.homeworkEvaluationDate!)"

        if Utils.isUserHavePermission(checkThisModules: ["Homework.Download"]){
            if (self.item.homeworkFile! != Utils.Nil && self.item.homeworkFile! != "") {
                cellBtn!.isHidden = false
                let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.openMenu (_:)))
                cellBtn!.isUserInteractionEnabled = true
                cellBtn!.addGestureRecognizer(gesture)
                
            }else{
                cellBtn!.isHidden = true
            }
        }else{
            cellBtn!.isHidden = true
        }
    }
    
    @objc func openMenu(_ sender:UITapGestureRecognizer){
        
        let config = FTConfiguration.shared
//        config.backgoundTintColor = UIColor.white
//        config.textColor = UIColor.black
//        config.borderColor = UIColor.lightGray
        config.menuSeparatorColor = UIColor.lightGray
        config.menuRowHeight = 40
        config.cornerRadius = 6
        
        var menuItems = [String]();
        print(Language.getTranslationOf(findAndDefaultKey:"Download"));
        menuItems.append(Language.getTranslationOf(findAndDefaultKey:"Download"))
        
        FTPopOverMenu.showForSender(sender: self.cellBtn,
                                    with: menuItems,
                                    done: { (selectedIndex) -> () in
                                        if selectedIndex == 0{
                                            if self.item.homeworkTitle! != Utils.Nil {
                                                // Download
                                                print("\(Constants.getModRewriteBaseUrl())/homeworks/download/\(self.item.id!)")
                                                print("\(self.item.homeworkTitle!) - \(self.item.homeworkFile!)")
                                                
                                                Utils.downloadFile(viewController: (self.navController?.visibleViewController)!, fileDownloadUrl: "\(Constants.getModRewriteBaseUrl())/homeworks/download/\(self.item.id!)", fileName: "\(self.item.homeworkTitle!) - \(self.item.homeworkFile!)")
                                            }
                                            
                                        }
                                        
        }) {
            
        }
        
    }
}

