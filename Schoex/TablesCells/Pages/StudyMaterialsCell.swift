//
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework


@objc protocol StudyMaterialsCellDelegate {
    func callDetailView(cell: StudyMaterialsCell, item: StudyMaterialsModel )
    func openLink(cell: StudyMaterialsCell, links: [String:Any]);
}

class StudyMaterialsCell: UITableViewCell {
    
    @IBOutlet weak var cell_material_title: UILabel!
    @IBOutlet weak var cell_material_description: UILabel!
    @IBOutlet weak var cell_subject: UILabel!
    @IBOutlet weak var cell_classes: UILabel!
    @IBOutlet weak var cell_btn_download: SolCircleButton!
    @IBOutlet var linkBtns: [UIButton]!
    @IBOutlet weak var subjectlbl: UILabel!
    @IBOutlet weak var classlbl: UILabel!
    
    var links: [[String:Any]] = [[String:Any]]();
    
    
    @IBOutlet weak var backgroundCardView: UIView!
    var navController : UINavigationController?
    
    weak var delegate: StudyMaterialsCellDelegate?

    var item : StudyMaterialsModel! {
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func updateUI (){
        
        subjectlbl.text = Language.getTranslationOf(findKey: "Subjects", defaultWord: "Subjects")
        classlbl.text = Language.getTranslationOf(findKey: "Classes", defaultWord: "Classes")
        
        
        for btn in linkBtns{
            btn.setBackgroundColor(color: .clear, forState: .normal)
            btn.setTitleColor(.white, for: .normal)
        }
        
        
        cell_material_title!.textNotNull = item.material_title!
        cell_material_description!.textNotNull = item.material_description!.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil).replacingOccurrences(of: "&[^;]+;", with: "", options:.regularExpression, range: nil)
        
        cell_subject!.textNotNull = item.subject!
        cell_classes!.textNotNull = item.classes!
        
        print(item.material_file!);
        self.links.removeAll();
        for btn in linkBtns{
            btn.tag = -1;
        }
        
        if let files = item.material_file {
            self.links = files;
            cell_btn_download!.isHidden = false
            
            let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.downloadFile (_:)))
            cell_btn_download.addGestureRecognizer(gesture)
            
            
            // find urls of types
            // loop all files and check if a video is available
            
            for index in 0...( files.count - 1 ) {
                let file = files[index];
                let btn = linkBtns[index];
                if let str = file["ext"]{
                    self.links.append(file)
                    btn.setTitle( str as! String , for: .normal)
                    btn.setBackgroundColor(color: .systemBlue, forState: .normal);
                    btn.tag = index;
                }
                
            }
            
            
            
            
            
            
            
            
            
            
        }else{
            cell_btn_download!.isHidden = true
        }
        
        
        
        
        
        
        
    }
    
    
    @IBAction func openLink(sender: UIButton){
        
        let index = sender.tag;
        if(index != -1){
            self.delegate?.openLink(cell: self, links: self.links[index])
            
            
//            if let link = self.links[index]["link"], let url = URL(string: link as! String) {
//                print(url);
////                UIApplication.shared.open(url)
//                self.delegate?.openLink(cell: self, links: url)
//            }
            
        }
        
        
        
        
    }
    
    
    
    
    
    
    @objc func downloadFile(_ sender:UITapGestureRecognizer){
        
        delegate?.callDetailView(cell: self, item: item);
        
//        Utils.downloadFile(viewController: (navController?.visibleViewController)!,fileDownloadUrl: "\(Constants.getModRewriteBaseUrl())/materials/download/\(item.id!)", fileName: "\(item.material_title!) - \(item.material_file!)")
    }
}

