//
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework

class LibraryBooksCell: UITableViewCell {
    
    @IBOutlet weak var cell_bookName: UILabel!
    @IBOutlet weak var cell_bookType: UILabel!
    @IBOutlet weak var cell_bookAuthor: UILabel!
    @IBOutlet weak var cell_btn_download: SolCircleButton!
    @IBOutlet weak var backgroundCardView: UIView!
    @IBOutlet weak var bookAuthorLbl: UILabel!
    var navController : UINavigationController?

    var item : LibraryBooksModel! {
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func updateUI (){
        
        bookAuthorLbl.text = Language.getTranslationOf(findKey: "bookAuthor", defaultWord: "Book Author");
        
        cell_bookName!.textNotNull = item.bookName!
        if item.bookType! == "electronic"{
            cell_bookType!.textNotNull = Language.getTranslationOf(findKey: "electronicBook",defaultWord: "Electronic Book")
        } else if item.bookType! == "traditional"{
            cell_bookType!.textNotNull = Language.getTranslationOf(findKey: "traditionalBook",defaultWord: "Traditional Book")
        }
        cell_bookAuthor!.textNotNull = item.bookAuthor!
        
        let bookPrice = item.bookPrice!
        if !bookPrice.isEmpty{
            cell_bookType!.textNotNull = "\(cell_bookType!.text!) - \(Language.getTranslationOf(findKey: "bookPrice",defaultWord: "Book Price")) : \(bookPrice)"
        }
        
        if Utils.isUserHavePermission(checkThisModules: ["Library.Download"]){
            if item.bookFile! != Utils.Nil {
                cell_btn_download!.isHidden = false
                
                let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.downloadFile (_:)))
                cell_btn_download.addGestureRecognizer(gesture)
            }else{
                cell_btn_download!.isHidden = true
            }
        }else{
            cell_btn_download!.isHidden = true
        }
        
    }
    
    @objc func downloadFile(_ sender:UITapGestureRecognizer){
        Utils.downloadFile(viewController: (navController?.visibleViewController)!,fileDownloadUrl: "\(Constants.getModRewriteBaseUrl())/library/download/\(item.id!)", fileName: "\(item.bookName!) - \(item.bookFile!)")
    }
}

