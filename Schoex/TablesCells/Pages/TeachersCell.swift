//
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework
import Alamofire

class TeachersCell: UITableViewCell {
    
    @IBOutlet weak var cell_fullName: UILabel!
    @IBOutlet weak var cell_username: UILabel!
    @IBOutlet weak var cell_email: UILabel!
    @IBOutlet weak var cell_btn_leaderboard: SolCircleButton!
    @IBOutlet weak var backgroundCardView: UIView!
    @IBOutlet weak var cell_user_image: UIImageView!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var usernameLbl: UILabel!
    
    var item : TeachersModel! {
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func updateUI (){
        
        emailLbl.text = Language.getTranslationOf(findKey: "email", defaultWord: "Email");
        usernameLbl.text = Language.getTranslationOf(findKey: "username", defaultWord: "Username");
        
        cell_fullName!.textNotNull = item.fullName!
        cell_username!.textNotNull = item.username!
        cell_email!.textNotNull = item.email!

        if Utils.isUserHavePermission(checkThisModules: ["teachers.teacLeaderBoard"]){
            let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.markAsLeaderboard (_:)))
            cell_btn_leaderboard!.isUserInteractionEnabled = true
            cell_btn_leaderboard!.addGestureRecognizer(gesture)
        }else{
            Utils.showMsg(msg: Language.getTranslationOf(findKey: "no_access",defaultWord: "You have no access to do this, please contact adminstrator"))
        }
        
        let photoManager: PhotoManager = PhotoManager()
        _ = photoManager.retrieveImage(for: SBFramework.strip(st: "\(Constants.getModRewriteBaseUrl())/dashboard/profileImage/\(item.id ?? 0)"), imageView: self.cell_user_image)
        
    }
    
    @objc func markAsLeaderboard(_ sender:UITapGestureRecognizer){
        Utils.makeLeaderboard(currentLeaderBoardMsg: item.isLeaderBoard!,leaderboardLink: "\(Constants.getModRewriteBaseUrl())/teachers/leaderBoard/\(item.id!)")
    }
    
}

