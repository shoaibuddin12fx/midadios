//
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import Alamofire
import SBFramework

class AssignmentsAnswersCell: UITableViewCell {
    
    @IBOutlet weak var cell_notes: SolLabelView!
    @IBOutlet weak var cell_time: SolLabelView!
    @IBOutlet weak var cell_class: SolLabelView!
    @IBOutlet weak var cell_username: SolLabelView!
    @IBOutlet weak var cell_user_image: UIImageView!
    @IBOutlet weak var cell_download_btn: SolCircleButton!
    @IBOutlet weak var backgroundCard: SolCardView!
    @IBOutlet weak var studentLbl: UILabel!
    @IBOutlet weak var classLbl: UILabel!
    @IBOutlet weak var timeAppliedLbl: UILabel!
    @IBOutlet weak var notesLbl: UILabel!
    
    
    var navController : UINavigationController?

    var item : AssignmentsAnswersModel! {
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func updateUI (){
        
        
        studentLbl.text = Language.getTranslationOf(findKey: "student", defaultWord: "Student")
        classLbl.text = Language.getTranslationOf(findKey: "class", defaultWord: "Class")
        timeAppliedLbl.text = Language.getTranslationOf(findKey: "timeApplied", defaultWord: "Time Applied")
        notesLbl.text = Language.getTranslationOf(findKey: "Notes", defaultWord: "Notes")
        
        cell_time!.textNotNull = item.Time!
        cell_class!.textNotNull = item.ClassName!
        cell_notes!.textNotNull = item.Notes!
        cell_username!.textNotNull = item.FullName!
        
        let photoManager: PhotoManager = PhotoManager()
        _ = photoManager.retrieveImage(for: SBFramework.strip(st: "\(Constants.getModRewriteBaseUrl())/dashboard/profileImage/\(item.userID ?? 0)"), imageView: self.cell_user_image)
        
        if item.AnswerFile! != Utils.Nil {
            cell_download_btn!.isHidden = false
            
            let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.downloadFile (_:)))
            cell_download_btn.addGestureRecognizer(gesture)
        }else{
            cell_download_btn!.isHidden = true
        }
    }
    
    @objc func downloadFile(_ sender:UITapGestureRecognizer){
        Utils.downloadFile(viewController: (navController?.visibleViewController)!, fileDownloadUrl: "\(Constants.getModRewriteBaseUrl())/assignments/downloadAnswer/\(item.id!)", fileName: "\(item.FullName!) - \(item.AnswerFile!)")
    }
}

