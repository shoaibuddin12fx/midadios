//
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/17/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit

class ClassesCell: UITableViewCell {
    
    @IBOutlet weak var cellClassName: UILabel!

    @IBOutlet weak var cellDormitoriesLabel: UILabel!
    @IBOutlet weak var cellDormitoriesName: UILabel!
    @IBOutlet weak var backgroundCardView: UIView!
    @IBOutlet weak var dormitorieslbl: UILabel!
    
    var classItem : ClassesModel! {
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func updateUI (){
        cellClassName!.textNotNull = classItem.className!
        cellDormitoriesName!.textNotNull = classItem.dormitoryName!
        dormitorieslbl.text = Language.getTranslationOf(findKey: "Dormitories", defaultWord: "Dormitories")
    }
}
