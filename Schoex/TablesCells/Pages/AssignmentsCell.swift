//
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import FTPopOverMenu_Swift
import SBFramework

class AssignmentsCell: UITableViewCell {
    
    @IBOutlet weak var cell_AssignTitle: UILabel!
    @IBOutlet weak var cell_AssignDescription: UILabel!
    @IBOutlet weak var cell_AssignDeadLine: UILabel!
    @IBOutlet weak var backgroundCardView: UIView!
    @IBOutlet weak var assignDeadLine: UILabel!
    @IBOutlet weak var cell_btn_menu: SolImageView!
    
    var navController : UINavigationController?

    var item : AssignmentsModel! {
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func updateUI (){
        assignDeadLine.text = Language.getTranslationOf(findKey: "AssignmentDeadline", defaultWord: "Assignment Dead Line")
        cell_AssignTitle!.textNotNull = item.AssignTitle!
        cell_AssignDescription!.textNotNull = item.AssignDescription!
        cell_AssignDeadLine!.textNotNull = item.AssignDeadLine!

        if Utils.isUserHavePermission(checkThisModules: ["Assignments.Download"]){
            if (self.item.AssignFile! != Utils.Nil && self.item.AssignFile! != "") {
                cell_btn_menu!.isHidden = false
                let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.openMenu (_:)))
                cell_btn_menu!.isUserInteractionEnabled = true
                cell_btn_menu!.addGestureRecognizer(gesture)
            }else{
                cell_btn_menu!.isHidden = true
            }
        }else{
            cell_btn_menu!.isHidden = true
        }
        
    }
    
    @objc func openMenu(_ sender:UITapGestureRecognizer){
        
        let config = FTConfiguration.shared
//        config.backgoundTintColor = UIColor.white
        config.borderColor = UIColor.lightGray
        config.menuSeparatorColor = UIColor.lightGray
        config.menuRowHeight = 40
        config.cornerRadius = 6
        
        var menuItems = [String]()
        var downloadEn = false
        var viewAnswersEn = false
        
        if Utils.isUserHavePermission(checkThisModules: ["Assignments.Download"]){
            menuItems.append(Language.getTranslationOf(findAndDefaultKey:"Download"))
            downloadEn = true
        }else{
            downloadEn = false
        }
        if Utils.isUserHavePermission(checkThisModules: ["Assignments.viewAnswers"]){
            menuItems.append(Language.getTranslationOf(findAndDefaultKey:"View Answers"))
            viewAnswersEn = true
        }else{
            viewAnswersEn = false
        }
        self.setActionsListeners(menuItems: menuItems,downloadEnabled:downloadEn,viewAnswersEnabled:viewAnswersEn)
    }
    func setActionsListeners(menuItems : [String], downloadEnabled : Bool, viewAnswersEnabled : Bool) {
        
        FTPopOverMenu.showForSender(sender: self.cell_btn_menu,
                                    with: menuItems,
                                    done: { (selectedIndex) -> () in
                                        if(downloadEnabled && viewAnswersEnabled){
                                            if selectedIndex == 0{
                                                if self.item.AssignFile! != Utils.Nil {
                                                    // Download
                                                    Utils.downloadFile(viewController: (self.navController?.visibleViewController)!, fileDownloadUrl: "\(Constants.getModRewriteBaseUrl())/assignments/download/\(self.item.id!)", fileName: "\(self.item.AssignTitle!) - \(self.item.AssignFile!)")
                                                }
                                                
                                            }else if selectedIndex == 1 {
                                                let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                                                
                                                let nextVC = storyboard.instantiateViewController(withIdentifier: "AssignmentsAnswersPageStoryboard") as! AssignmentsAnswersPage
                                                
                                                nextVC.assignmentID = self.item.id!
                                                
                                                self.navController!.pushViewController(nextVC, animated: true)
                                            }
                                        }else{
                                            if downloadEnabled {
                                                if self.item.AssignFile! != Utils.Nil {
                                                    // Download
                                                    Utils.downloadFile(viewController: (self.navController?.visibleViewController)!, fileDownloadUrl: "\(Constants.getModRewriteBaseUrl())/assignments/download/\(self.item.id!)", fileName: "\(self.item.AssignTitle!) - \(self.item.AssignFile!)")
                                                }
                                                
                                            }else if viewAnswersEnabled {
                                                let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                                                
                                                let nextVC = storyboard.instantiateViewController(withIdentifier: "AssignmentsAnswersPageStoryboard") as! AssignmentsAnswersPage
                                                
                                                nextVC.assignmentID = self.item.id!
                                                
                                                self.navController!.pushViewController(nextVC, animated: true)
                                            }
                                        }
        }) {
            
        }
    }
}

