//
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit

class HostelsCell: UITableViewCell {
    
    @IBOutlet weak var cell_hostelTitle: UILabel!
    @IBOutlet weak var cell_hostelType: UILabel!
    @IBOutlet weak var cell_hostelAddress : UILabel!
    @IBOutlet weak var cell_hostelNotes: UILabel!
    @IBOutlet weak var backgroundCardView: UIView!
    @IBOutlet weak var addresslbl: UILabel!
    @IBOutlet weak var noteslbl: UILabel!
    
    
    var item : HostelsModel! {
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func updateUI (){
        cell_hostelTitle!.textNotNull = item.hostelTitle!
        cell_hostelType!.translatedText = item.hostelType!
        cell_hostelAddress!.textNotNull = item.hostelAddress!
        cell_hostelNotes!.textNotNull = item.hostelNotes!
        addresslbl.text = Language.getTranslationOf(findKey: "Address", defaultWord: "Address")
        noteslbl.text = Language.getTranslationOf(findKey: "Notes", defaultWord: "Notes")

    }
}

