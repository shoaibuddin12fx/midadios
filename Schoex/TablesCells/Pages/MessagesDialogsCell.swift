//
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team.
//  Copyright © 2017 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework
import Alamofire

class MessagesDialogsCell: UITableViewCell {
    
    var navController : UINavigationController?
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userFullname: SolLabelView!
    @IBOutlet weak var latestMessageDate: SolLabelView!
    @IBOutlet weak var latestMessageContent: SolLabelView!
    
    @IBOutlet weak var newMessageIndicator: UIView!
    
    var item : MessagesDialogModel! {
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func updateUI (){
       
        userFullname.textNotNull = item.fullName ?? "NA"
        latestMessageContent.textNotNull = item.lastMessage ?? "NA"
        latestMessageDate.textNotNull = item.lastMessageDate ?? "NA"
        
        
        let photoManager: PhotoManager = PhotoManager()
        _ = photoManager.retrieveImage(for: SBFramework.strip(st: "\(Constants.getModRewriteBaseUrl())/dashboard/profileImage/\(item.userId ?? "")"), imageView: self.userImage)
        
        if item.messageStatus == "1"{
            newMessageIndicator.isHidden = false
            newMessageIndicator.layer.cornerRadius = self.newMessageIndicator.frame.size.width / 2
            newMessageIndicator.clipsToBounds = true
        }else{
            newMessageIndicator.isHidden = true
        }
        
        
    }
    
}

