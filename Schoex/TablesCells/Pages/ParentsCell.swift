//
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import Alamofire
import SBFramework

class ParentsCell: UITableViewCell {
    
    @IBOutlet weak var cell_fullName: UILabel!
    @IBOutlet weak var cell_username: UILabel!
    @IBOutlet weak var cell_email: UILabel!
    @IBOutlet weak var backgroundCardView: UIView!
    @IBOutlet weak var cell_user_image: UIImageView!
    
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var usernameLbl: UILabel!
    
    var item : ParentsModel! {
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func updateUI (){
        
        emailLbl.text = Language.getTranslationOf(findKey: "email", defaultWord: "Email");
        usernameLbl.text = Language.getTranslationOf(findKey: "username", defaultWord: "Username");
        
        cell_fullName!.textNotNull = item.fullName!
        cell_username!.textNotNull = item.username!
        cell_email!.textNotNull = item.email!

        let photoManager: PhotoManager = PhotoManager()
        _ = photoManager.retrieveImage(for: SBFramework.strip(st: "\(Constants.getModRewriteBaseUrl())/dashboard/profileImage/\(item.id ?? 0)"), imageView: self.cell_user_image)
        
        
    }
}

