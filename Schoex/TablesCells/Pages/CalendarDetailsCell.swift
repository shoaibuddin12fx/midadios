//
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit

class CalendarDetailsCell: UITableViewCell {
    
    @IBOutlet weak var cell_pageTitle: UILabel!
    @IBOutlet weak var cell_pageContent: UILabel!
    @IBOutlet weak var backgroundCardView: UIView!
    
    var item : CalendarEventModel! {
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func updateUI (){
        cell_pageTitle!.textNotNull = item.title ?? "NA"
        
        if item.allDay == "1"{
            cell_pageContent!.isHidden = false
            cell_pageContent!.textNotNull = Language.getTranslationOf(findAndDefaultKey: "All Day")
        }else{
            cell_pageContent!.isHidden = true
        }
        
        
    }
    

}

