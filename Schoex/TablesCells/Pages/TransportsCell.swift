//
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework

class TransportsCell: UITableViewCell {
    
    @IBOutlet weak var cell_transportTitle: UILabel!
    @IBOutlet weak var cell_transportDescription: UILabel!
    @IBOutlet weak var cell_transportFee: SolLabelView!
    @IBOutlet weak var fareLbl: UILabel!
    
    @IBOutlet weak var backgroundCardView: UIView!
    
    var item : TransportsModel! {
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func updateUI (){
        cell_transportTitle!.textNotNull = item.transportTitle!
        cell_transportDescription!.textNotNull = item.transportDescription!
        fareLbl.text = Language.getTranslationOf(findKey: "Fare", defaultWord: "Fare")
        
        let fare = item.transportFare!
        if !fare.isEmpty{
            cell_transportFee!.text = "\(fare)"
        }
    }
}

