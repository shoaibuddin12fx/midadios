//
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework

class InvoicesCell: UITableViewCell {
    
    @IBOutlet weak var cell_paymentTitle: UILabel!
    @IBOutlet weak var cell_paymentDescription: UILabel!
    @IBOutlet weak var cell_fullName: UILabel!
    
    // Date - Due date
    @IBOutlet weak var cell_date_due_title: SolLabelView!
    @IBOutlet weak var cell_paymentDate: UILabel!

    // Discounted - paid
    @IBOutlet weak var cellDicountedPaidTitle: SolLabelView!
    @IBOutlet weak var cellDiscountedPaid: SolLabelView!
    
    // Status
    @IBOutlet weak var cellStatus: SolLabelView!
    @IBOutlet weak var cellStatusCon: SolSideButton!
    
    // Total
    @IBOutlet weak var cellTotalAmount: SolLabelView!
    @IBOutlet weak var cellTotalAmountCon: SolSideButton!
    @IBOutlet weak var cell_student_title: SolLabelView!
    
    @IBOutlet weak var studentLbl: UILabel!
    @IBOutlet weak var dueDateLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    
    
    
    var item : InvoicesModel! {
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func updateUI (){
        
        studentLbl.text = Language.getTranslationOf(findKey: "student", defaultWord: "Student")
        dueDateLbl.text = Language.getTranslationOf(findKey: "dateDueDate", defaultWord: "Date - Due Date")
        amountLbl.text = Language.getTranslationOf(findKey: "paidAmount", defaultWord: "Paid Amount")
        
        cell_paymentTitle!.textNotNull = item.paymentTitle!
        cell_paymentDescription!.textNotNull = item.paymentDescription!
        cell_fullName!.textNotNull = item.fullName!
        cell_paymentDate!.textNotNull = item.paymentDate!

        cell_student_title.translatedText = "Student"
        //========================== Payment Amount ===================//
        if let payAmount = item.paymentAmount{
            if !payAmount.isEmpty{
                cellTotalAmountCon!.isHidden = false
                cellTotalAmount!.text = "\(Language.getTranslationOf(findAndDefaultKey:"Amount")) : \(attachCurrency(amount : payAmount))"
            }else{
                cellTotalAmountCon!.isHidden = true
            }
        }
        
        
        //========================== Dicounted - Paid Amount ===================//
        let paidAmount = item.paidAmount!
        let dicountedAmount = item.dicountedAmount!

        cellDicountedPaidTitle!.text = "\(Language.getTranslationOf(findAndDefaultKey:"Discouted Amount")) - \(Language.getTranslationOf(findAndDefaultKey:"Paid Amount"))"
        
        if Utils.App_IsRtl{
            if !paidAmount.isEmpty && !dicountedAmount.isEmpty{
                cellDiscountedPaid!.text = "\(attachCurrency(amount : paidAmount)) - \(attachCurrency(amount : dicountedAmount))"
            }
        }else{
            if !paidAmount.isEmpty && !dicountedAmount.isEmpty{
                cellDiscountedPaid!.text = "\(attachCurrency(amount : dicountedAmount)) - \(attachCurrency(amount : paidAmount))"
            }
        }
        
        //========================== Date - Due Date ===================//
        cell_date_due_title!.text = "\(Language.getTranslationOf(findAndDefaultKey:"Date")) - \(Language.getTranslationOf(findAndDefaultKey:"Due Date"))"
        if Utils.App_IsRtl{
            cell_paymentDate!.text = "\(item.dueDate ?? "NA") - \(item.paymentDate ?? "NA")"
        }else{
            cell_paymentDate!.text = "\(item.paymentDate ?? "NA") - \(item.dueDate ?? "NA")"
        }
        //========================== Payment Status ===================//
        if item.paymentStatusText != Utils.Nil{
            if item.paymentStatusText! == "Partially Paid" {
                cellStatus!.text = Language.getTranslationOf(findKey: "ppaid",defaultWord: "Partially Paid")
            }else {
                cellStatus!.translatedText = item.paymentStatusText!
            }
            cellTotalAmountCon!.isHidden = false
        }else{
            cellStatus!.text = ""
            cellTotalAmountCon!.isHidden = true
        }
        
    }
    func attachCurrency(amount : String) -> String {
        if let currency = item.paymentCurrency{
            return "\(currency) \(amount)"
        }else{
            return "\(amount)"
        }
    }
}

