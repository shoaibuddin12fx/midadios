//
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit

class SubjectsCell: UITableViewCell {
    
    @IBOutlet weak var cell_subjectTitle: UILabel!
    @IBOutlet weak var cell_passGrade: UILabel!
    @IBOutlet weak var cell_finalGrade: UILabel!
    @IBOutlet weak var backgroundCardView: UIView!
    @IBOutlet weak var passGradeLbl: UILabel!
    @IBOutlet weak var finalGradeLbl: UILabel!
    
    var item : SubjectsModel! {
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func updateUI (){
        cell_subjectTitle!.textNotNull = item.subjectTitle!
        cell_passGrade!.textNotNull = item.passGrade!
        cell_finalGrade!.textNotNull = item.finalGrade!
        passGradeLbl.text = Language.getTranslationOf(findKey: "passGrade", defaultWord: "Pass Grade");
        finalGradeLbl.text = Language.getTranslationOf(findKey: "finalGrade", defaultWord: "Final Grade");

    }
}

