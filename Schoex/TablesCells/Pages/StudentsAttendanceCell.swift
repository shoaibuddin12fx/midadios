//
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit

class StudentsAttendanceCell: UITableViewCell {
    
    @IBOutlet weak var cellDateSubject: UILabel!
    @IBOutlet weak var cellAttendance: UILabel!
    @IBOutlet weak var backgroundCardView: UIView!
    @IBOutlet weak var attendanceLbl: UILabel!
    
    var item : StudentsAttendanceModel! {
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func updateUI (){
        
        attendanceLbl.text = Language.getTranslationOf(findKey: "Attendance", defaultWord: "Attendance");
        
        let mSubjectName = item.subjectName
        if mSubjectName != Utils.Nil{
            cellDateSubject!.textNotNull = " \(item.date!) - \(mSubjectName!)"
        }else{
            cellDateSubject!.textNotNull = " \(item.date!)"
        }
        cellAttendance!.textNotNull = item.statusName!
    }
}

