//
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit

class NewsCell: UITableViewCell {
    
    @IBOutlet weak var cellNewsTitle: UILabel!
    @IBOutlet weak var cellNewsContent: UILabel!
    @IBOutlet weak var cellNewsDate: UILabel!
    @IBOutlet weak var backgroundCardView: UIView!
    @IBOutlet weak var dataLbl: UILabel!
    
    var item : NewsModel! {
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func updateUI (){
        cellNewsTitle!.textNotNull = item.newsTitle!
        cellNewsContent!.textNotNull = item.newsText!
        cellNewsDate!.textNotNull = item.newsDate!
        self.dataLbl.text = Language.getTranslationOf(findKey: "Date", defaultWord: "Date");

    }
}

