//
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework

class ClassScheduleDetailsCell: UITableViewCell {
    
    
    @IBOutlet weak var cellSubjectName: SolLabelView!
    @IBOutlet weak var cellStartTime: SolLabelView!
    @IBOutlet weak var cellEndTime: SolLabelView!
    @IBOutlet weak var backgroundCardView: UIView!
    @IBOutlet weak var startTimeLbl: UILabel!
    @IBOutlet weak var endTimeLbl: UILabel!
    @IBOutlet weak var backgroundCardFirstView: UIView!
    @IBOutlet weak var backgroundCardSecondView: UIView!
    
    
    
    var item : ClassesSchModel! {
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func updateUI (){
        cellSubjectName!.textNotNull = item.subjectId!
        cellStartTime!.textNotNull = item.startPeriod!
        cellEndTime!.textNotNull = item.endPeriod!
        
        startTimeLbl.text = Language.getTranslationOf(findKey: "startTime", defaultWord: "Start Time")
        endTimeLbl.text = Language.getTranslationOf(findKey: "endTime", defaultWord: "End Time")
        
        if(Utils.App_IsRtl){
            cellSubjectName.semanticContentAttribute = .forceRightToLeft;
            backgroundCardFirstView.semanticContentAttribute = .forceRightToLeft;
            backgroundCardSecondView.semanticContentAttribute = .forceRightToLeft;
        }else{
            cellSubjectName.semanticContentAttribute = .forceLeftToRight;
            backgroundCardFirstView.semanticContentAttribute = .forceLeftToRight;
            backgroundCardSecondView.semanticContentAttribute = .forceLeftToRight;
        }
        
    }
}

