//
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework

class StudentsMarksheetCell: UITableViewCell {
    
    @IBOutlet weak var cell_exam_name: SolLabelView!
    @IBOutlet weak var cell_exam_average_points: SolLabelView!
    @IBOutlet weak var cell_exam_marks: SolLabelView!
    @IBOutlet weak var backgroundCardView: UIView!
    @IBOutlet weak var averagePointsLbl: UILabel!
    @IBOutlet weak var examMarksLbl: UILabel!
    
    var navController : UINavigationController?

    var item : StudentsMarksheetModel! {
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func updateUI (){
        averagePointsLbl.text = Language.getTranslationOf(findKey: "AveragePoints", defaultWord: "AveragePoints")
        examMarksLbl.text = Language.getTranslationOf(findKey: "examMarks", defaultWord: "examMarks")
        cell_exam_name.textNotNull = item.title!
        cell_exam_average_points.textNotNull = "\(item.pointsAvg!)"
        cell_exam_marks.textNotNull = "\(item.totalMarks!)"
    }
}

