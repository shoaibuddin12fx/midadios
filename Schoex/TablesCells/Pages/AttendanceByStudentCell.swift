//
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/17/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit

class AttendanceByStudentCell: UITableViewCell {
    
    @IBOutlet weak var cellDate: UILabel!
    @IBOutlet weak var cellAttendance: UILabel!
    @IBOutlet weak var backgroundCardView: UIView!
    @IBOutlet weak var attendanceLbl: UILabel!
    
    var item : AttendanceByParentModel! {
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func updateUI (){
        
        attendanceLbl.text = Language.getTranslationOf(findKey: "Attendance", defaultWord: "Attendance")
        
        if item.subject != Utils.Nil{
            cellDate!.textNotNull = " \(item.date!) - \(item.subject!)"
        }else{
            cellDate!.textNotNull = " \(item.date!)"
        }
        
        cellAttendance.text = item.statusName

    }
}
