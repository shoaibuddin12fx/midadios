//
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/17/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework

class CalendarItemCell: UITableViewCell {
    
    
    @IBOutlet weak var title: SolLabelView!
    
    @IBOutlet weak var item1_con: UIView!
    @IBOutlet weak var item1_subtitle: SolLabelView!
    
    @IBOutlet weak var item2_con: UIView!
    @IBOutlet weak var item2_subtitle: SolLabelView!
    
    @IBOutlet weak var item3_con: UIView!
    @IBOutlet weak var item3_subtitle: SolLabelView!
    
    @IBOutlet weak var item4_con: UIView!
    @IBOutlet weak var item4_subtitle: SolLabelView!
    
    @IBOutlet weak var seeMoreBtn: UIButton!
    
    @IBOutlet weak var backgroundCardView: UIView!
    var navController : UINavigationController?

    var blockState : CalendarBlockStateModel.blockState?
    var index : Int?
    
    var item : CalendarBlockDay! {
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func updateUI (){
        
        // Set Day Name
        if let dayNameString = item.dayName{
            if index == 0 {
                // Toady
                title.textNotNull = "\(Language.getTranslationOf(findAndDefaultKey:"Today")), \(dayNameString)"
            }else if index == 1{
                // Tomorrow
                title.textNotNull = "\(Language.getTranslationOf(findAndDefaultKey:"Tomorrow")), \(dayNameString)"
            }else{
                title.textNotNull = dayNameString
            }
        }
        
        if blockState == .loading {
            seeMoreBtn.setTitle(Language.getTranslationOf(findAndDefaultKey:"Loading"), for:UIControlState.normal)
            item1_con.isHidden = true
            item2_con.isHidden = true
            item3_con.isHidden = true
            item4_con.isHidden = true
        }else if blockState == .failed {
            seeMoreBtn.setTitle(Language.getTranslationOf(findAndDefaultKey:"Error loading dates"), for:UIControlState.normal)

            item1_con.isHidden = true
            item2_con.isHidden = true
            item3_con.isHidden = true
            item4_con.isHidden = true
        }else if blockState == .success {
            item1_con.isHidden = false
            item2_con.isHidden = false
            item3_con.isHidden = false
            item4_con.isHidden = false
            
            // Loop on events
            
            if(item.dayEvents.count == 0){
                // No events
                seeMoreBtn.isHidden = false
                item1_con.isHidden = true
                item2_con.isHidden = true
                item3_con.isHidden = true
                item4_con.isHidden = true
                
                seeMoreBtn.setTitle(Language.getTranslationOf(findAndDefaultKey:"No Events"), for:UIControlState.normal)

            }else{
                // There events
                if item.dayEvents.count == 1{
                    // One event
                    seeMoreBtn.isHidden = true
                    item2_con.isHidden = true
                    item3_con.isHidden = true
                    item4_con.isHidden = true
                    
                    // Set items
                    item1_subtitle.text = item.dayEvents[0].title
                    
                    
                }else if item.dayEvents.count == 2{
                    // Two event
                    seeMoreBtn.isHidden = true
                    item3_con.isHidden = true
                    item4_con.isHidden = true
                    
                    // Set items
                    item1_subtitle.text = item.dayEvents[0].title
                    item2_subtitle.text = item.dayEvents[1].title
                    
                }else if item.dayEvents.count == 3{
                    // Three event
                    seeMoreBtn.isHidden = true
                    item4_con.isHidden = true
                    
                    // Set items
                    item1_subtitle.text = item.dayEvents[0].title
                    item2_subtitle.text = item.dayEvents[1].title
                    item3_subtitle.text = item.dayEvents[2].title
                    
                }else if item.dayEvents.count == 4{
                    // Four event
                    seeMoreBtn.isHidden = true
                    
                    // Set items
                    item1_subtitle.text = item.dayEvents[0].title
                    item2_subtitle.text = item.dayEvents[1].title
                    item3_subtitle.text = item.dayEvents[2].title
                    item4_subtitle.text = item.dayEvents[3].title
                    
                }else{
                    // More than four event
                    seeMoreBtn.isHidden = false
                    
                    // Set items
                    item1_subtitle.text = item.dayEvents[0].title
                    item2_subtitle.text = item.dayEvents[1].title
                    item3_subtitle.text = item.dayEvents[2].title
                    item4_subtitle.text = item.dayEvents[3].title
                    
                    seeMoreBtn.setTitle(Language.getTranslationOf(findAndDefaultKey:"See more today dates"), for:UIControlState.normal)

                    
                    let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.openMoreEvents (_:)))
                    seeMoreBtn.addGestureRecognizer(gesture)
                    
                }
            }
            
        }
        
    }
    
    
    @objc func openMoreEvents(_ sender:UITapGestureRecognizer){
        if let dateSelected = item.dayName{
            let replacedDate = dateSelected.replacingOccurrences(of: "/", with: "-")
            let nextVC = navController!.storyboard?.instantiateViewController(withIdentifier: ("CalendarDetailsPageStoryboard")) as! CalendarDetailsPage
            nextVC.selectedDate = replacedDate
            navController!.pushViewController(nextVC, animated: true)
        }
    }
}
