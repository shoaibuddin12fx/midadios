//
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework

class ExamsCell: UITableViewCell {
    
    @IBOutlet weak var cell_examTitle: UILabel!
    @IBOutlet weak var cell_examDescription: UILabel!
    @IBOutlet weak var cell_examDate: UILabel!
    @IBOutlet weak var backgroundCardView: UIView!
    @IBOutlet weak var examDeadLine: UILabel!
    @IBOutlet weak var cell_btn_marksheet: SolCircleButton!
    var item : ExamsModel! {
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func updateUI (){
        examDeadLine.text = Language.getTranslationOf(findKey: "examDeadline", defaultWord: "Exam Dead Line");
        cell_examTitle!.textNotNull = item.examTitle!
        cell_examDescription!.textNotNull = item.examDescription!
        cell_examDate!.textNotNull = item.examDate!
    }
}

