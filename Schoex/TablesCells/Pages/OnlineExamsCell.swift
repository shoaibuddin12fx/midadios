//
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework

class OnlineExamsCell: UITableViewCell {
    
    @IBOutlet weak var cell_examTitle: UILabel!
    @IBOutlet weak var cell_examDescription: UILabel!
    @IBOutlet weak var cell_ExamEndDate: UILabel!
    @IBOutlet weak var backgroundCardView: UIView!
    @IBOutlet weak var cell_btn_marksheet: SolCircleButton!
    @IBOutlet weak var examDeadline: UILabel!
    var navController : UINavigationController?

    var item : OnlineExamsModel! {
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func updateUI (){
        
        examDeadline.text = Language.getTranslationOf(findKey: "examDeadline", defaultWord: "Exam Dead Line")
        cell_examTitle!.textNotNull = item.examTitle!
        cell_examDescription!.textNotNull = item.examDescription!
        cell_ExamEndDate!.textNotNull = item.ExamEndDate!
        
        if Utils.isUserHavePermission(checkThisModules: ["onlineExams.showMarks"]){
            cell_btn_marksheet!.isHidden = false
            let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.openMarksheet (_:)))
            cell_btn_marksheet!.isUserInteractionEnabled = true
            cell_btn_marksheet!.addGestureRecognizer(gesture)
        }else{
            cell_btn_marksheet!.isHidden = true
        }
        
    }
    
    @objc func openMarksheet(_ sender:UITapGestureRecognizer){
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let nextVC = storyboard.instantiateViewController(withIdentifier: "OnlineExamsMarksPageStoryboard") as! OnlineExamsMarksPage
        
        nextVC.examID = self.item.id!
        
        self.navController!.pushViewController(nextVC, animated: true)
        
    }
}

