//
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit

class StaticPagesCell: UITableViewCell {
    
    @IBOutlet weak var cell_pageTitle: UILabel!
    @IBOutlet weak var cell_pageContent: UILabel!
    @IBOutlet weak var backgroundCardView: UIView!
    
    var item : StaticPagesModel! {
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func updateUI (){
        cell_pageTitle!.textNotNull = item.pageTitle!
        cell_pageContent!.textNotNull = item.pageContent!.html2String
        
    }
    

}

