//
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework
import Alamofire

class StudentsCell: UITableViewCell {
    
    var navController : UINavigationController?
    @IBOutlet weak var cell_fullName: UILabel!
    @IBOutlet weak var cell_username: UILabel!
    @IBOutlet weak var cell_email: UILabel!
    @IBOutlet weak var cell_user_image: UIImageView!
    @IBOutlet weak var cell_studentClass: UILabel!
    @IBOutlet weak var backgroundCardView: UIView!
    @IBOutlet weak var cell_btn_attendance: SolCircleButton!
    @IBOutlet weak var cell_btn_marksheet: SolCircleButton!
    @IBOutlet weak var cell_btn_leaderboard: SolCircleButton!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var usernameLbl: UILabel!
    
    var item : StudentsModel! {
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func updateUI (){
        
        emailLbl.text = Language.getTranslationOf(findKey: "email", defaultWord: "Email");
        usernameLbl.text = Language.getTranslationOf(findKey: "username", defaultWord: "Username");
        
        cell_fullName!.textNotNull = item.fullName!
        cell_username!.textNotNull = item.username!
        cell_email!.textNotNull = item.email!
        cell_studentClass!.textNotNull = item.studentClass!
        
        let photoManager: PhotoManager = PhotoManager()
        _ = photoManager.retrieveImage(for: SBFramework.strip(st: "\(Constants.getModRewriteBaseUrl())/dashboard/profileImage/\(item.id ?? 0)"), imageView: self.cell_user_image)
        
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.openAttendance (_:)))
        cell_btn_attendance!.isUserInteractionEnabled = true
        cell_btn_attendance!.addGestureRecognizer(gesture)
        
        let gesture2 = UITapGestureRecognizer(target: self, action:  #selector (self.openMarksheet (_:)))
        cell_btn_marksheet!.isUserInteractionEnabled = true
        cell_btn_marksheet!.addGestureRecognizer(gesture2)
        
        if (Utils.LoggedInUser.LoggedInUserRole == Utils.AppRoles.Admin || Utils.LoggedInUser.LoggedInUserRole == Utils.AppRoles.Teacher) {
            let gesture3 = UITapGestureRecognizer(target: self, action:  #selector (self.markAsLeaderboard (_:)))
            cell_btn_leaderboard!.isUserInteractionEnabled = true
            cell_btn_leaderboard!.addGestureRecognizer(gesture3)
        }else{
            cell_btn_leaderboard.isHidden = true
        }
        
    }
    
    @objc func openAttendance(_ sender:UITapGestureRecognizer){
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        let nextVC = storyboard.instantiateViewController(withIdentifier: "StudentsAttendancePageStoryboard") as! StudentsAttendancePage
        
        nextVC.studentID = self.item.id!
        
        self.navController!.pushViewController(nextVC, animated: true)
        
    }
    
    @objc func openMarksheet(_ sender:UITapGestureRecognizer){
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        let nextVC = storyboard.instantiateViewController(withIdentifier: "StudentsMarksheetPageStoryboard") as! StudentsMarksheetPage
        
        nextVC.studentId = self.item.id!
        
        self.navController!.pushViewController(nextVC, animated: true)
        
    }
    
    @objc func markAsLeaderboard(_ sender:UITapGestureRecognizer){
        Utils.makeLeaderboard(currentLeaderBoardMsg: item.isLeaderBoard!,leaderboardLink: "\(Constants.getModRewriteBaseUrl())/students/leaderBoard/\(item.id!)")
    }
}

