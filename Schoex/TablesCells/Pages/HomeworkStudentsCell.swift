//
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework
import Alamofire

class HomeworkStudentsCell: UITableViewCell {
    
    
    @IBOutlet weak var userPhoto: UIImageView!
    @IBOutlet weak var userName: SolLabelView!
    @IBOutlet weak var userStatus: SolLabelView!
    @IBOutlet weak var actionBtn: UIButton!
    var navController : UINavigationController?

    var item : HomeworkStudentModel! {
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func updateUI (){
        if let userNameString = item.name{
            userName.textNotNull = userNameString
        }
        
        
        if(item.isApplied){
            userStatus.textNotNull = Language.getTranslationOf(findKey: "applied",defaultWord: "Applied")
            actionBtn.setTitle(Language.getTranslationOf(findKey: "notApplied",defaultWord: "Not yet Applied"), for: .normal)
            actionBtn.backgroundColor =  UIColor(hexString: "#ffF3364B")
            
        }else{
            userStatus.textNotNull = Language.getTranslationOf(findKey: "notApplied",defaultWord: "Not yet Applied")
            actionBtn.setTitle(Language.getTranslationOf(findKey: "applied",defaultWord: "Applied"), for: .normal)
            actionBtn.backgroundColor =  UIColor(hexString: "#ff27916F")
            
        }
    }
    
    
    
}


extension UIColor {
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format:"#%06x", rgb)
    }
}

