//
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import AlignedCollectionViewFlowLayout
import TangramKit
import SBFramework


class AttendanceVacationControlCell: UITableViewCell {
    
    //============== Views Declare =================//
    let viewStudentName = UILabel()
    let viewStudentRollID = UILabel()
    let viewLineSperator = UIView()
    let viewAttendanceStatusTitle = UILabel()
    let viewAttendanceStatusValue = UILabel()
    let viewVacationStatusTitle = UILabel()
    let viewVacationStatusValue = UILabel()
    let viewAttendanceStatusFlowlayout = UILabel()
    let viewVacationStatusFlowlayout = UILabel()
    var vacationButtonsFlow = TGFlowLayout()
    var attendanceButtonsFlow = TGFlowLayout()
    
    //============== Variables Declare =================//
    var attendanceButtonsSpecs  = [flowLayoutButton]()
    var vacationButtonsSpecs  = [flowLayoutButton]()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        separatorInset = .zero
        backgroundColor = .none
        
        initFunc()
        
    }
    
    var item : AttendanceControlModel! {
        didSet {
            self.updateUI()
        }
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layout()
    }
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        self.contentView.pin.width(size.width)
        layout()
        return self.contentView.frame.size
    }
    fileprivate func layout() {
        if Utils.App_IsRtl{
            self.contentView.flex.layoutDirection(.rtl).layout(mode: .adjustHeight)
        }else{
            self.contentView.flex.layout(mode: .adjustHeight)
        }
    }
    
    func initFunc()  {
        //============ Declare attendance buttons =================//
        attendanceButtonsSpecs.append(flowLayoutButton(buttonTitle: Language.getTranslationOf(findAndDefaultKey:"Present"),buttonTag: 1))
        attendanceButtonsSpecs.append(flowLayoutButton(buttonTitle: Language.getTranslationOf(findAndDefaultKey:"Absent"),buttonTag: 0))
        attendanceButtonsSpecs.append(flowLayoutButton(buttonTitle: Language.getTranslationOf(findAndDefaultKey:"Late"),buttonTag: 2))
        attendanceButtonsSpecs.append(flowLayoutButton(buttonTitle: Language.getTranslationOf(findKey: "LateExecuse",defaultWord: "Late with excuse"),buttonTag: 3))
        attendanceButtonsSpecs.append(flowLayoutButton(buttonTitle: Language.getTranslationOf(findAndDefaultKey:"Early Dismissal"),buttonTag: 4))
        attendanceButtonsFlow = createButtonsFlowLayout(
            buttons: attendanceButtonsSpecs,
            buttonsBackgroundColorNormal: "x_boxes_menu_text_background_normal",
            buttonsBackgroundColorPressed: "x_boxes_menu_text_background_pressed", actionSelector: #selector(changeAttendanceStatus))
        
        
        //============ Declare vacation buttons =================//
        
        
        vacationButtonsSpecs.append(flowLayoutButton(buttonTitle: Language.getTranslationOf(findAndDefaultKey: "Accepted Vacation"),buttonTag: 1))
        vacationButtonsSpecs.append(flowLayoutButton(buttonTitle: Language.getTranslationOf(findAndDefaultKey:"Rejected Vacation"),buttonTag: 0))
        vacationButtonsFlow = createButtonsFlowLayout(
            buttons: vacationButtonsSpecs,
            buttonsBackgroundColorNormal: "x_boxes_menu_icon_background_normal",
            buttonsBackgroundColorPressed: "x_boxes_menu_icon_background_pressed", actionSelector: #selector(changeVacationStatus))
        
        
        
        //========== Customize View ================//
        // Student Name
        viewStudentName.text = "NA"
        viewStudentName.setStyle(style: Fonts.labelStyle.HeaderTitle)
        
        // Student Roll id
        viewStudentRollID.setStyle(style: Fonts.labelStyle.HeaderSubTitle)
        
        // Line sperator
        viewLineSperator.frame.size.height = 2
        viewLineSperator.backgroundColor = SolColors.getColorByName(colorName: "x_divider_color")
        
        // Containers of status
        viewVacationStatusTitle.text = Language.getTranslationOf(findKey: "confirmVacation",defaultWord: "Vacation requested")
        viewVacationStatusTitle.setStyle(style: Fonts.labelStyle.DetailsTitle)
        
        
        viewVacationStatusValue.setStyle(style: Fonts.labelStyle.DetailsSubTitle)
        
        
        viewAttendanceStatusTitle.text = Language.getTranslationOf(findKey: "Attendance",defaultWord: "Attendance")
        viewAttendanceStatusTitle.setStyle(style: Fonts.labelStyle.DetailsTitle)
        
        viewAttendanceStatusValue.text = "NA"
        viewAttendanceStatusValue.setStyle(style: Fonts.labelStyle.DetailsSubTitle)
        
        //========== Set Views ======================//
        let viewContainer = SolCardView()
        viewContainer.initView()
        let cellHalfWidth = (self.bounds.width / 2) - 14
        
        viewContainer.flex.marginLeft(14).marginRight(14).marginTop(8).marginBottom(8).addItem().padding(10).define({ (flex) in
            flex.addItem().direction(.column).define({ (flex) in
                
                flex.addItem(viewStudentName)
                flex.addItem(viewStudentRollID).paddingBottom(10)
                flex.addItem(viewLineSperator)
                
                flex.addItem().direction(.row).paddingTop(8).paddingBottom(18).define({ (flex) in
                    
                    flex.addItem().width(cellHalfWidth).direction(.column).define({ (flex) in
                        flex.addItem(viewAttendanceStatusTitle)
                        flex.addItem(viewAttendanceStatusValue)
                    })
                    
                    flex.addItem().width(cellHalfWidth).direction(.column).define({ (flex) in
                        flex.addItem(viewVacationStatusTitle)
                        flex.addItem(viewVacationStatusValue)
                    })
                    
                })
                
                flex.addItem(vacationButtonsFlow).paddingBottom(8)
                
                flex.addItem(attendanceButtonsFlow).paddingBottom(8)
            })
        })
        
        contentView.addSubview(viewContainer)
    }
    
    //======================= FlowLayout Creators Functions ================//
    func createButtonsFlowLayout(buttons : [flowLayoutButton], buttonsBackgroundColorNormal :String, buttonsBackgroundColorPressed : String, actionSelector : Selector) -> TGFlowLayout{
        
        let flowLayout = TGFlowLayout(.vert, arrangedCount: 0)
        flowLayout.tg_width.equal(.fill)
        flowLayout.tg_height.equal(.wrap)
        flowLayout.tg_autoArrange = true
        flowLayout.tg_space = 10
        flowLayout.tg_gravity = TGGravity.horz.fill
        flowLayout.tg_layoutAnimationWithDuration(0.2)
        
        for buttonElement in buttons{
            let mButton = UIButton()
            mButton.setTitle(buttonElement.buttonTitle,for:.normal)
            mButton.titleLabel?.customFont(fontName: Fonts.FontRobotoCondensed_Regular, fontSize: 15)
            mButton.tag = buttonElement.buttonTag
            
            mButton.titleLabel?.textColor = SolColors.getColorByName(colorName: "x_boxes_menu_text")
            
            mButton.layer.cornerRadius = 5;
            
            mButton.titleEdgeInsets = UIEdgeInsets(top: 5,left: 5,bottom: 5,right: 5)
            mButton.addTarget(self,action:actionSelector, for:.touchUpInside )
            mButton.tg_width.equal(.wrap, increment:15).min(80)
            mButton.tg_height.equal(.wrap, increment:13)
            
            Utils.setButtonBackgroundColors(
                targetButton: mButton,
                normalState: buttonsBackgroundColorNormal,
                pressedState: buttonsBackgroundColorPressed)
            
            flowLayout.addSubview(mButton)
        }
        return flowLayout
    }
    
    
    class flowLayoutButton{
        let buttonTitle : String
        let buttonTag : Int
        
        init(buttonTitle : String , buttonTag : Int) {
            self.buttonTitle = buttonTitle
            self.buttonTag = buttonTag
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func isThereVacation() -> Bool {
        return item.vacation
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func updateUI (){
        
        // Set user data
        self.viewStudentName.text = item.StudentName
        self.viewStudentRollID.text = "\(Language.getTranslationOf(findKey: "Roll ID",defaultWord: "Roll ID")) :  \(item.StudentRollId)"
        
        changeButtonState(isEnabled: true,buttonsHolder: attendanceButtonsFlow,buttonTag: 0)
        changeButtonState(isEnabled: true,buttonsHolder: attendanceButtonsFlow,buttonTag: 1)
        changeButtonState(isEnabled: true,buttonsHolder: attendanceButtonsFlow,buttonTag: 2)
        changeButtonState(isEnabled: true,buttonsHolder: attendanceButtonsFlow,buttonTag: 3)
        changeButtonState(isEnabled: true,buttonsHolder: attendanceButtonsFlow,buttonTag: 4)
        
        // Set attendance status
        if let status = item.Status{
            self.viewAttendanceStatusValue.text = status
        }else{
            self.viewAttendanceStatusValue.text = "NA"
        }
        
        // Set vacation status
        if(item.vacationStat != Utils.Nil){
            
            if(item.vacationStat == "-1"){
                viewVacationStatusValue.text = "NA"
            }else if(item.vacationStat == "1"){
                viewVacationStatusValue.text = Language.getTranslationOf(findKey: "AcceptedVacation",defaultWord: "Accepted Vacation")
                
                changeButtonState(isEnabled: false,buttonsHolder: attendanceButtonsFlow,buttonTag: 0)
                changeButtonState(isEnabled: false,buttonsHolder: attendanceButtonsFlow,buttonTag: 1)
                changeButtonState(isEnabled: false,buttonsHolder: attendanceButtonsFlow,buttonTag: 2)
                changeButtonState(isEnabled: false,buttonsHolder: attendanceButtonsFlow,buttonTag: 3)
                changeButtonState(isEnabled: false,buttonsHolder: attendanceButtonsFlow,buttonTag: 4)
                viewAttendanceStatusValue.text = "NA"
                
            }else if(item.vacationStat == "0"){
                viewVacationStatusValue.text = Language.getTranslationOf(findKey: "RejectedVacation",defaultWord: "Rejected Vacation")
            }else{
                viewVacationStatusValue.text = "NA"
            }
        }else{
            viewVacationStatusValue.text = "NA"
        }
        
        
    }
    
    @objc func changeAttendanceStatus(sender:UIButton!)
    {
        let statusName:String = (sender.titleLabel?.text)!
        self.viewAttendanceStatusValue.text = statusName
        self.viewAttendanceStatusValue.flex.markDirty()
        item.StatusId = AttendanceControlPage.statusIdentifiersReversed[statusName]!
        item.updateStatus()
    }
    
    @objc func changeVacationStatus(sender:UIButton!)
    {
        let vacationName:String = (sender.titleLabel?.text)!
        self.viewVacationStatusValue.text = vacationName
        self.viewVacationStatusValue.flex.markDirty()
        item.vacationStat = String(sender.tag)
        if(sender.tag == 1){
            // Accepted Vacation
            changeButtonState(isEnabled: false,buttonsHolder: attendanceButtonsFlow,buttonTag: 0)
            changeButtonState(isEnabled: false,buttonsHolder: attendanceButtonsFlow,buttonTag: 1)
            changeButtonState(isEnabled: false,buttonsHolder: attendanceButtonsFlow,buttonTag: 2)
            changeButtonState(isEnabled: false,buttonsHolder: attendanceButtonsFlow,buttonTag: 3)
            changeButtonState(isEnabled: false,buttonsHolder: attendanceButtonsFlow,buttonTag: 4)
            viewAttendanceStatusValue.text = "NA"
        }else if (sender.tag == 0){
            // Rejected Vacation
            changeButtonState(isEnabled: true,buttonsHolder: attendanceButtonsFlow,buttonTag: 0)
            changeButtonState(isEnabled: true,buttonsHolder: attendanceButtonsFlow,buttonTag: 1)
            changeButtonState(isEnabled: true,buttonsHolder: attendanceButtonsFlow,buttonTag: 2)
            changeButtonState(isEnabled: true,buttonsHolder: attendanceButtonsFlow,buttonTag: 3)
            changeButtonState(isEnabled: true,buttonsHolder: attendanceButtonsFlow,buttonTag: 4)
        }
    }
    
    func changeButtonState(isEnabled:Bool , buttonsHolder:TGFlowLayout , buttonTag:Int){
        if(isEnabled){
            buttonsHolder.viewWithTag(buttonTag)?.isUserInteractionEnabled = true
            buttonsHolder.viewWithTag(buttonTag)?.alpha = 1
        }else{
            buttonsHolder.viewWithTag(buttonTag)?.isUserInteractionEnabled = false
            buttonsHolder.viewWithTag(buttonTag)?.alpha = 0.5
        }
    }
    
}

