//
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/17/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit

class AttendanceByParentChooseCell: UITableViewCell {
    
    @IBOutlet weak var cellStudentName: UILabel!
    @IBOutlet weak var backgroundCardView: UIView!
    
    var item : AttendanceByParentChooseModel! {
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func updateUI (){
        cellStudentName.text = item.name
    }
}
