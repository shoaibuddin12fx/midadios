//
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit

class EventsCell: UITableViewCell {
    
    @IBOutlet weak var cell_eventTitle: UILabel!
    @IBOutlet weak var cell_eventDescription: UILabel!
    @IBOutlet weak var cell_eventDate: UILabel!
    @IBOutlet weak var cell_enentPlace: UILabel!
    @IBOutlet weak var backgroundCardView: UIView!
    @IBOutlet weak var eventslbl: UILabel!
    @IBOutlet weak var datelbl: UILabel!
    
    var item : EventsModel! {
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func updateUI (){
        cell_eventTitle!.textNotNull = item.eventTitle!
        cell_eventDescription!.textNotNull = item.eventDescription!.html2String
        cell_eventDate!.textNotNull = item.eventDate!
        cell_enentPlace!.textNotNull = item.enentPlace
        eventslbl.text = Language.getTranslationOf(findKey: "eventPlace", defaultWord: "Event Place")
        datelbl.text = Language.getTranslationOf(findKey: "Date", defaultWord: "Date")

    }
}

