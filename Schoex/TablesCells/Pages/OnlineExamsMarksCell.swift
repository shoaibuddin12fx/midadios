//
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework
import Alamofire

class OnlineExamsMarksCell: UITableViewCell {
    

    @IBOutlet weak var cell_user_name: SolLabelView!
    @IBOutlet weak var cell_grade_status: SolLabelView!
    @IBOutlet weak var cell_grade: SolLabelView!
    @IBOutlet weak var cell_date: SolLabelView!
    @IBOutlet weak var cell_user_image: UIImageView!
    @IBOutlet weak var backgroundCardView: UIView!
    @IBOutlet weak var gradeLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    var examDegreeSuccess : Int = Utils.NilInt

    var item : OnlineExamsMarksModel! {
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
        
    func updateUI (){
        
        gradeLbl.text = Language.getTranslationOf(findKey: "Grade", defaultWord: "Grade")
        dateLbl.text = Language.getTranslationOf(findKey: "Date", defaultWord: "Date")
        
        cell_user_name!.textNotNull = item.FullName!
        cell_date!.textNotNull = item.examDate!
        if(item.examGrade == Utils.NilInt){
            cell_grade!.textNotNull = "Not completed exam"
            cell_grade_status!.textNotNull = "Not completed exam"
        }else{
            cell_grade!.textNotNull = "\(item.examGrade!)"
            if(item.examGrade! != Utils.NilInt && examDegreeSuccess != Utils.NilInt){
                if examDegreeSuccess > item.examGrade! {
                    cell_grade_status!.textNotNull = "Failed"
                }else{
                    cell_grade_status!.textNotNull = "Passed"
                }
            }
            
        }
        
        let photoManager: PhotoManager = PhotoManager()
        _ = photoManager.retrieveImage(for: SBFramework.strip(st: "\(Constants.getModRewriteBaseUrl())/dashboard/profileImage/\(item.studentId ?? 0)"), imageView: self.cell_user_image)
        
    }
}

