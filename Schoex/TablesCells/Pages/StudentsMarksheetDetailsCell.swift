//
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit

import TangramKit
import SBFramework

class StudentsMarksheetDetailsCell: UITableViewCell {
    
    let viewSubjectName = UILabel()
    let viewGrade = UILabel()
    let viewStatus = UILabel()
    let viewLineSperator = UIView()
    let viewCommentsHead = UILabel()
    let viewComments = UILabel()
    let viewLineSperator2 = UIView()

    let viewContainer = SolCardView()
    var marksColsDrawn = false
    
    var passGradeView : UIView = UIView()
    var finalGradeView : UIView = UIView()
    var totalGradeView : UIView = UIView()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        separatorInset = .zero
        backgroundColor = .none
        
        self.initFunc(cell: self)

        
    }
    
    var item : StudentsMarksheetDetailsModel! {
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layout()
    }
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        self.contentView.pin.width(size.width)
        layout()
        return self.contentView.frame.size
    }
    fileprivate func layout() {
        self.contentView.flex.layout(mode: .adjustHeight)
    }
    
    func initFunc(cell : UITableViewCell)  {
        
        // Subject Name
        viewSubjectName.applyBoxMainStyle(BiggerSizeMode: true)
        viewSubjectName.changeFontSize(size: 23)
        
        // Grade Name
        viewGrade.applyBoxSubStyle(BiggerSizeMode: false)
        viewGrade.changeFontSize(size: 14)

        // Status Name
        viewStatus.applyBoxSubStyle(BiggerSizeMode: false)
        viewStatus.changeFontSize(size: 14)

        // Line sperator
        viewLineSperator.frame.size.height = 2
        viewLineSperator.backgroundColor = SolColors.getColorByName(colorName: "x_divider_color" )

        // Comments head
        viewCommentsHead.translatedText = Language.getTranslationOf(findAndDefaultKey:"Comments")
        viewCommentsHead.applyBoxMainStyle(BiggerSizeMode: true)
        
        // Comments head
        viewComments.applyBoxSubStyle(BiggerSizeMode: false)
        
        // Line sperator 2
        viewLineSperator2.frame.size.height = 2
        viewLineSperator2.backgroundColor = SolColors.getColorByName(colorName: "x_divider_color")
        
        //========== Set Views ======================//
        viewContainer.initView()
        viewContainer.flex.marginLeft(15).marginRight(15).marginTop(8).marginBottom(8).addItem().padding(10).define({ (flex) in
            flex.addItem().direction(.column).define({ (flex) in
                
                flex.addItem(viewSubjectName)
                flex.addItem(viewGrade)
                flex.addItem(viewStatus).paddingBottom(10)
                flex.addItem(viewLineSperator)
                
                flex.addItem(viewCommentsHead)
                flex.addItem(viewComments).paddingBottom(10)
                flex.addItem(viewLineSperator2)
                
                

            })
        })
        
        contentView.addSubview(viewContainer)
    }
    
    
    func updateUI (){
        if !marksColsDrawn {
            passGradeView = getMarkView(gradeNumber: item.passGrade!,gradeTitle: Language.getTranslationOf(findAndDefaultKey:"Pass Grade"))
            finalGradeView = getMarkView(gradeNumber: item.finalGrade!,gradeTitle: Language.getTranslationOf(findAndDefaultKey:"Final Grade"))
            totalGradeView = getMarkView(gradeNumber: item.TotalMark!,gradeTitle: Language.getTranslationOf(findAndDefaultKey:"Exam Marks"))
            
            self.viewContainer.flex.addItem(passGradeView)
            self.viewContainer.flex.addItem(finalGradeView)
            self.viewContainer.flex.addItem(totalGradeView)
            
            for (grade_title,grade_number) in item.MarksColsMap {
                
                self.viewContainer.flex.addItem(getMarkView(gradeNumber: grade_number,gradeTitle: grade_title))
            }
            marksColsDrawn = true
        }
        
        viewSubjectName.text = item.subjectName
        viewGrade.text = "\(Language.getTranslationOf(findAndDefaultKey:"gradePoint")): \(item.Grade!)"
        if item.markComments != ""{
            viewComments.text = item.markComments
        }else{
            viewComments.translatedText = "No Comments"
        }
        viewStatus.text = "\(Language.getTranslationOf(findAndDefaultKey:"Status")): \(item.examState!)"
        
        (passGradeView.viewWithTag(1) as! UILabel).text = item.passGrade
        (finalGradeView.viewWithTag(1) as! UILabel).text = item.finalGrade
        (totalGradeView.viewWithTag(1) as! UILabel).text = item.TotalMark
        
    }
    
    func getMarkView(gradeNumber : String,gradeTitle : String) -> UIView {
        let mView = UIView()
        
        let mark = UILabel()
        mark.applyBoxMainStyle(BiggerSizeMode: true)
        mark.tag = 1
        let markTitle = UILabel()
        markTitle.applyBoxSubStyle(BiggerSizeMode: true)
        
        mark.text = gradeNumber
        markTitle.text = gradeTitle
        
        mView.flex.justifyContent(.spaceBetween).padding(12).direction(.row).define({ (flex) in
            flex.addItem(markTitle)
            flex.addItem(mark)
        })
        return mView
    }
    
}

