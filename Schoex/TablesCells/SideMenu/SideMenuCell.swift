//
//  SideMenuCell.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/22/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework

class SideMenuCell: UITableViewCell {

    @IBOutlet weak var cellReferenceTitle: UILabel!
    @IBOutlet weak var cellReferenceImage: UIImageView!
    @IBOutlet weak var stackView: UIStackView!
    
    
    var referenceItem : SideMenuReferenceModel! {
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
    }
    
    override func prepareForReuse() {
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func updateUI (){
        
        
        
        if let title = referenceItem.itemTitle {
            cellReferenceTitle!.translatedText = title
        }
        
        if cellReferenceImage!.tag != 0{
            cellReferenceImage!.image = (UIImage(named : referenceItem.itemImage!))?.tintImage(color : SolColors.getColor(tag: cellReferenceImage!.tag)!)
        }else{
            cellReferenceImage!.image = UIImage(named : referenceItem.itemImage!)
        }
        
        if(Utils.App_IsRtl){
            stackView.semanticContentAttribute = .forceRightToLeft
            cellReferenceTitle.semanticContentAttribute = .forceRightToLeft
            
        }else{
            stackView.semanticContentAttribute = .forceLeftToRight
            cellReferenceTitle.semanticContentAttribute = .forceLeftToRight
        }
        
        
        
        
    }
    


}
