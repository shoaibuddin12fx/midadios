//
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/22/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class UserModel: NSObject {
    
    var id:String?
    var name:String?
    var email:String?
    var role:String?
    var username:String?
    
    init(id:String,
         name:String,
         email:String,
         role:String,
         username:String) {
        
        self.id = id
        self.name = name
        self.email = email
        self.role = role
        self.username = username
    }
}
