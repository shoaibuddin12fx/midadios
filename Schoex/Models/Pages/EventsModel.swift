//
//  StudyMaterialsModel.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/22/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class EventsModel: NSObject {
    var id:Int?
    var eventTitle:String?
    var eventDescription:String?
    var eventDate:String?
    var eventFor:String?
    var enentPlace:String

    
    init(id:Int,
         eventTitle:String,
         eventDescription:String,
         eventDate:String,
         eventFor:String,
         enentPlace:String) {
        
        self.id = id
        self.eventTitle = eventTitle
        self.eventDescription = eventDescription
        self.eventDate = eventDate
        self.eventFor = eventFor
        self.enentPlace = enentPlace
    }
}
