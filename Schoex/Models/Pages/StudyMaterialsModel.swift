//
//  StudyMaterialsModel.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/22/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class StudyMaterialsModel: NSObject {
    var id:Int?
    var subjectId:Int?
    var subject:String?
    var material_title:String?
    var material_description:String?
    var material_file:[[String:Any]]?
    var classes:String?

    
    init(id:Int,
         subjectId:Int,
         subject:String,
         material_title:String,
         material_description:String,
         material_file:[[String:Any]],
         classes:String) {
        
        self.id = id
        self.subjectId = subjectId
        self.subject = subject
        self.material_title = material_title
        self.material_description = material_description
        self.material_file = material_file
        self.classes = classes
    }
}
