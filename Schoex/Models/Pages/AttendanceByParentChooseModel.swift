//
//  ClassesModel.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/17/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class AttendanceByParentChooseModel: NSObject {
    var name:String?
    var studentRollId:String?
    var attendanceArray : [AttendanceByParentModel]?
    
    init(name:String,studentRollId:String,attendanceArray:[AttendanceByParentModel]) {
        self.name = name
        self.studentRollId = studentRollId
        self.attendanceArray = attendanceArray

    }
}
