//
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class StudentsAttendanceModel: NSObject {
    var id:Int?
    var subjectId:String?
    var subjectName:String?
    var studentId:Int?
    var date:String?
    var status:Int?
    var statusName:String?

    
    init(id:Int,studentId:Int,subjectId:String,date : String,status : Int,subjectName:String,statusName : String) {
        self.id = id
        self.subjectId = subjectId
        self.subjectName = subjectName
        self.studentId = studentId
        self.date = date
        self.status = status
        self.statusName = statusName
    }
}
