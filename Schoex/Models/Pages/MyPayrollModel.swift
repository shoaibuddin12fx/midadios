//
//  StudyMaterialsModel.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/22/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class MyPayrollModel: NSObject {
    var id:Int?
    var pay_month:Int?
    var pay_year:Int?
    var salary_type:String?

    init(id:Int,
         pay_month:Int,
         pay_year:Int,
         salary_type:String) {
        
        self.id = id
        self.pay_month = pay_month
        self.pay_year = pay_year
        self.salary_type = salary_type
    }
}
