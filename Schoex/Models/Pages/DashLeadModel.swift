//
//  NewsModel.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class DashLeadModel: NSObject {
    var id:Int?
    var name:String?
    var msg:String?

    
    init(id:Int,name:String,msg:String) {
        self.id = id
        self.name = name
        self.msg = msg
    }
}
