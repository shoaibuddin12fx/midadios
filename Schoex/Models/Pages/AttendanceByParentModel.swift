//
//  ClassesModel.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/17/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class AttendanceByParentModel: NSObject {
    var date:String?
    var status:Int?
    var subject:String?
    var statusName:String?

    
    init(date:String,status:Int,subject:String,statusName:String) {
        self.date = date
        self.status = status
        self.subject = subject
        self.statusName = statusName

    }
}
