//
//  StudyMaterialsModel.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/22/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class OnlineExamsModel: NSObject {
    var id:Int?
    var examTitle:String?
    var examDescription:String?
    var ExamEndDate:String?

    
    init(id:Int,
         examTitle:String,
         examDescription:String,
         ExamEndDate:String) {
        
        self.id = id
        self.examTitle = examTitle
        self.examDescription = examDescription
        self.ExamEndDate = ExamEndDate

    }
}
