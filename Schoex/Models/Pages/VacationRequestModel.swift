//
//  VacationRequestModel.swift
//  Schoex
//
//  Created by Mohamed Selim Refaat on 10/18/19.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class VacationRequestModel: NSObject {

    var date:String?
    var dow:String?
    var status:String?
    var timestamp:String?

    init(date:String,
         dow:String,
         status:String,
         timestamp:String) {
        
        self.date = date
        self.dow = dow
        self.status = status
        self.timestamp = timestamp
    }
}
