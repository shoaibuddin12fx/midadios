//
//  StudyMaterialsModel.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/22/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class MyPayrollDetailsModel: NSObject {

    var currency_symbol:String?

    var id:Int?
    var hour_count:String?
    var hour_overtime:String?
    var pay_amount:String?
    var pay_by_userid:Int?
    var pay_comments:String?
    var pay_method:String?
    var pay_month:String?
    var pay_year:String?
    var salary_type:String?
    var salary_value:String?

    
    init(currency_symbol:String, id:Int,
         hour_count:String,
         hour_overtime:String,
         pay_amount:String,
         pay_by_userid:Int,
         pay_comments:String,
         pay_method:String,
         pay_month:String,
         pay_year:String,
         salary_type:String,
         salary_value:String) {
        
        self.currency_symbol = currency_symbol
        self.id = id
        self.hour_count = hour_count
        self.hour_overtime = hour_overtime
        self.pay_amount = pay_amount
        self.pay_by_userid = pay_by_userid
        self.pay_comments = pay_comments
        self.pay_method = pay_method
        self.pay_month = pay_month
        self.pay_year = pay_year
        self.salary_type = salary_type
        self.salary_value = salary_value

    }
}
