//
//  StudyMaterialsModel.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/22/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class PhotoModel: NSObject {
    var id:Int?
    var albumId:String?
    var mediaType:Int?
    var mediaURL:String?
    var mediaURLThumb:String?
    var mediaTitle:String
    var mediaDescription:String
    var mediaDate:String
    var mediaIsVideo:Bool = false

    
    init(id:Int,
         albumId:String,
         mediaType:Int,
         mediaURL:String,
         mediaURLThumb:String,
         mediaTitle:String,
         mediaDescription:String,
         mediaDate:String) {
        
        self.id = id
        self.albumId = albumId
        self.mediaType = mediaType
        self.mediaURL = mediaURL
        self.mediaURLThumb = mediaURLThumb
        self.mediaTitle = mediaTitle
        self.mediaDescription = mediaDescription
        self.mediaDate = mediaDate
    }
}
