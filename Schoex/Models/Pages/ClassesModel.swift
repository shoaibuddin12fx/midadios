//
//  ClassesModel.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/17/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class ClassesModel: NSObject {
    var id:Int?
    var className:String?
    var classTeacher:String?
    var dormitoryName:String?

    
    init(id:Int,className:String,classTeacher:String,dormitoryName:String) {
        self.id = id
        self.className = className
        self.classTeacher = classTeacher
        self.dormitoryName = dormitoryName
    }
}
