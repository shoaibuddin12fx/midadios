//
//  StudyMaterialsModel.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/22/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class StaffAttendanceControlModel: NSObject {
    
    let TeacherName:String;
    var Status:String?;
    var StatusId:Int;
    let TeacherId:Int;
    var vacation:Bool;
    var vacationStat:String;
    
    
    init(TeacherName : String,Status : Int,TeacherId : Int,vacation : Bool,vacationStat : String) {
        
        self.TeacherName = TeacherName
        self.StatusId = Status
        self.Status = StaffAttendanceControlPage.statusIdentifiers[self.StatusId]
        self.TeacherId = TeacherId
        self.vacation = vacation
        self.vacationStat = vacationStat
        
    }
    func updateStatus() {
        self.Status = StaffAttendanceControlPage.statusIdentifiers[self.StatusId]
    }

}
