//
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/22/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class CalendarBlockDay: NSObject {
    
    var dayName:String?
    var dayEvents:[CalendarEventModel] = [CalendarEventModel]()
    var startDayOfBlock:String?

    
    init(dayName:String,
         dayEvents:[CalendarEventModel],
         startDayOfBlock : String) {
        
        self.dayName = dayName
        self.dayEvents = dayEvents
        self.startDayOfBlock = startDayOfBlock
    }
}
