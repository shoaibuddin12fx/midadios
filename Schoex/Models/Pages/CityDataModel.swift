//
//  CityDataModel.swift
//  Schoex
//
//  Created by shoaib uddin on 01/05/2020.
//  Copyright © 2020 3rdHub Technologies. All rights reserved.
//

import Foundation

class CityDataModel: NSObject {

    var id:Int?
    var city:String?

    init(id:Int, city:String) {
        self.id = id
        self.city = city
    }
    
}
