//
//  StudyMaterialsModel.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/22/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class HomeworkModel: NSObject {
    
    var id:String?
    var homeworkTitle:String?
    var homeworkDescription:String?
    var homeworkFile:String?
    var homeworkDate:String?
    var homeworkSubmissionDate:String?
    var homeworkEvaluationDate:String?
    var subject:String?
    var classes:String?
    var sections:String?


    init(id:String,
         homeworkTitle:String,
         homeworkDescription:String,
         homeworkFile:String,
         homeworkDate:String,
         homeworkSubmissionDate:String,
         homeworkEvaluationDate:String,
         subject:String,
         classes:String,
         sections:String) {
        
        self.id = id
        self.homeworkTitle = homeworkTitle
        self.homeworkDescription = homeworkDescription
        self.homeworkFile = homeworkFile
        self.homeworkDate = homeworkDate
        self.homeworkSubmissionDate = homeworkSubmissionDate
        self.homeworkEvaluationDate = homeworkEvaluationDate
        self.subject = subject
        self.classes = classes
        self.sections = sections

    }
}
