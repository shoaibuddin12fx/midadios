//
//  NewsModel.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class LoggedUserModel: NSObject {
    var LoggedInUserId:Int?
    var LoggedInUserFullName:String?
    var LoggedInUserUsername:String?
    var LoggedInUserRole:Utils.AppRoles?

    
    init(LoggedInUserId:Int,LoggedInUserFullName:String,LoggedInUserUsername:String,LoggedInUserRole:Utils.AppRoles){
        
        self.LoggedInUserId = LoggedInUserId
        self.LoggedInUserFullName = LoggedInUserFullName
        self.LoggedInUserUsername = LoggedInUserUsername
        self.LoggedInUserRole = LoggedInUserRole
    }
    
    override init() {
        
    }
}
