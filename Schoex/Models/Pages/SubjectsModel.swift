//
//  StudyMaterialsModel.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/22/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class SubjectsModel: NSObject {
    var id:Int?
    var subjectTitle:String?
    var className:String?
    var teacherName:String?
    var teacherId:String?
    var passGrade:String?
    var finalGrade:String?

    
    init(id:Int,
         subjectTitle:String,
         className:String,
         teacherName:String,
         teacherId:String,
         passGrade:String,
         finalGrade:String) {
        
        self.id = id
        self.subjectTitle = subjectTitle
        self.className = className
        self.teacherName = teacherName
        self.teacherId = teacherId
        self.passGrade = passGrade
        self.finalGrade = finalGrade
    }
}
