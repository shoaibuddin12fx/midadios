//
//  StudyMaterialsModel.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/22/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class InvoicesModel: NSObject {
    var id:Int?
    var fullName:String?
    var paymentTitle:String?
    var paymentDescription:String?
    var paymentAmount:String?
    var paymentDate:String?
    var dueDate:String?
    var paidAmount:String?
    var dicountedAmount:String?
    var paymentStatus:Int?
    var paymentStatusText:String?
    var paymentCurrency:String?

    init(id:Int,
         fullName:String,
         paymentTitle:String,
         paymentDescription:String,
         paymentAmount:String,
         paymentDate:String,
         dueDate:String,
         dicountedAmount : String,
         paidAmount:String,
         paymentStatus:Int,
         paymentCurrency:String) {
        
        self.id = id
        self.fullName = fullName
        self.paymentTitle = paymentTitle
        self.paymentDescription = paymentDescription
        self.paymentAmount = paymentAmount
        self.paymentDate = paymentDate
        self.dueDate = dueDate
        self.paidAmount = paidAmount
        self.dicountedAmount = dicountedAmount
        self.paymentStatus = paymentStatus
        self.paymentCurrency = paymentCurrency
    }
}
