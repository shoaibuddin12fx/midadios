//
//  StudyMaterialsModel.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/22/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class StudentsMarksheetModel: NSObject {
    var id:Int?
    var title:String?
    var totalMarks:String?
    var pointsAvg:String?
    var detailsArray:[StudentsMarksheetDetailsModel] = [StudentsMarksheetDetailsModel]()

    
    init(id:Int,
         title:String,
         totalMarks:String,
         pointsAvg:String,
         detailsArray:[StudentsMarksheetDetailsModel]) {
        
        self.id = id
        self.title = title
        self.totalMarks = totalMarks
        self.pointsAvg = pointsAvg
        self.detailsArray = detailsArray
    }
}
