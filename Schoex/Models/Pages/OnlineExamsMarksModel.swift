//
//  StudyMaterialsModel.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/22/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class OnlineExamsMarksModel: NSObject {
    var id:Int?
    var studentId:Int?
    var examGrade:Int?
    var examDate:String?
    var FullName:String?

    
    init(id:Int,
         studentId:Int,
         examGrade:Int,
         examDate:String,
         FullName:String) {
        
        self.id = id
        self.studentId = studentId
        self.examGrade = examGrade
        self.examDate = examDate
        self.FullName = FullName
    }
}
