//
//  StudyMaterialsModel.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/22/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class AssignmentsModel: NSObject {
    var id:Int?
    var AssignTitle:String?
    var AssignDescription:String?
    var AssignDeadLine:String?
    var AssignFile:String?


    
    init(id:Int,
         AssignTitle:String,
         AssignDescription:String,
         AssignDeadLine:String,
         AssignFile:String) {
        
        self.id = id
        self.AssignTitle = AssignTitle
        self.AssignDescription = AssignDescription
        self.AssignDeadLine = AssignDeadLine
        self.AssignFile = AssignFile
    }
}
