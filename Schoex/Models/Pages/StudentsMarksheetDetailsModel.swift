//
//  StudyMaterialsModel.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/22/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class StudentsMarksheetDetailsModel: NSObject {
    var id:Int?
    var subjectName:String?
    var examState:String?
    var examMark:Int?
    var attendanceMark:String?
    var markComments:String?
    var passGrade:String?
    var finalGrade:String?
    var Grade:String?
    var TotalMark:String?
    var MarksColsMap:[String : String] = [String : String]()

    
    init(id:Int,
         subjectName:String,
         examState:String,
         examMark:Int,
         attendanceMark:String,markComments:String,passGrade:String,finalGrade:String,Grade:String ){
        
        self.id = id
        self.subjectName = subjectName
        self.examState = examState
        self.examMark = examMark
        self.attendanceMark = attendanceMark
        self.markComments = markComments
        self.passGrade = passGrade
        self.finalGrade = finalGrade
        self.Grade = Grade
    }
    
    override init(){
       
    }
}
