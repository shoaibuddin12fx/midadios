//
//  NewsModel.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class ClassesSchModel: NSObject {
    var id:Int?
    var dayName:String?
    var sectionId:Int?
    var subjectId:String?
    var startPeriod:String?
    var endPeriod:String?

    
    init(id:Int,dayName:String,sectionId:Int,subjectId:String,startPeriod : String,endPeriod : String) {
        self.id = id
        self.dayName = dayName
        self.sectionId = sectionId
        self.subjectId = subjectId
        self.startPeriod = startPeriod
        self.endPeriod = endPeriod
    }
}
