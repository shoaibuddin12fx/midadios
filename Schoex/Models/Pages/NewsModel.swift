//
//  NewsModel.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class NewsModel: NSObject {
    var id:Int?
    var newsTitle:String?
    var newsText:String?
    var newsFor:String?
    var newsDate:String?

    
    init(id:Int,newsTitle:String,newsText:String,newsFor:String,newsDate : String) {
        self.id = id
        self.newsTitle = newsTitle
        self.newsText = newsText
        self.newsFor = newsFor
        self.newsDate = newsDate
    }
}
