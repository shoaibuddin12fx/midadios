//
//  SchoolDataModel.swift
//  Schoex
//
//  Created by shoaib uddin on 01/05/2020.
//  Copyright © 2020 3rdHub Technologies. All rights reserved.
//

import Foundation

class SchoolDataModel: NSObject {

    var id:Int?
    var city_id:Int?
    var name:String?
    var url:String?

    init(id:Int, city_id:Int, name:String, url:String) {
        self.id = id
        self.city_id = city_id
        self.name = name
        self.url = url
    }
    
}
