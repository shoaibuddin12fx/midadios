//
//  StudyMaterialsModel.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/22/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class ExamsModel: NSObject {
    var id:Int?
    var examTitle:String?
    var examDescription:String?
    var examDate:String?
    
    init(id:Int,
         examTitle:String,
         examDescription:String,
         examDate:String) {
        
        self.id = id
        self.examTitle = examTitle
        self.examDescription = examDescription
        self.examDate = examDate
    }
}
