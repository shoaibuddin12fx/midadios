//
//  StudyMaterialsModel.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/22/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class HomeworkStudentModel: NSObject {
    var id:String?
    var name:String?
    var homeworkID:String?
    var isApplied : Bool = false
    
    init(id:String,name:String,homeworkID:String,isApplied:Bool) {
        self.id = id
        self.name = name
        self.homeworkID = homeworkID
        self.isApplied = isApplied
    }
}
