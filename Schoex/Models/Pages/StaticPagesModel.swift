//
//  StudyMaterialsModel.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/22/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class StaticPagesModel: NSObject {
    var id:Int?
    var pageTitle:String?
    var pageContent:String?
    var pageContentOriginal:String?
    var pageActive:Int?
    
    init(id:Int,
         pageTitle:String,
         pageContent:String,
         pageContentOriginal:String,
         pageActive:Int) {
        
        self.id = id
        self.pageTitle = pageTitle
        self.pageContent = pageContent
        self.pageContentOriginal = pageContentOriginal
        self.pageActive = pageActive

    }
}
