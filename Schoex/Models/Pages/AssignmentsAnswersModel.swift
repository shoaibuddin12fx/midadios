//
//  StudyMaterialsModel.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/22/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class AssignmentsAnswersModel: NSObject {
    var id:Int?
    var Notes:String?
    var Time:String?
    var FullName:String?
    var ClassName:String?
    var AnswerFile:String?
    var userID:Int?


    
    init(id:Int,
         Notes:String,
         Time:String,
         FullName:String,
         ClassName:String,
         userID:Int,
         AnswerFile:String ) {
        
        self.id = id
        self.Notes = Notes
        self.Time = Time
        self.FullName = FullName
        self.ClassName = ClassName
        self.AnswerFile = AnswerFile
        self.userID = userID

    }
}
