//
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/22/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class CalendarBlockStateModel: NSObject {

    var lastDay:String?
    var state:blockState?
    
    enum blockState {
        case loading
        case success
        case failed
    }
    
    init(lastDay:String,
         state:blockState) {
        
        self.lastDay = lastDay
        self.state = state
        
    }
}
