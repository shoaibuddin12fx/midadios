//
//  VacationModel.swift
//  Schoex
//
//  Created by Mohamed Selim Refaat on 10/18/19.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class VacationModel: NSObject {
    var id:Int?
    var vacDate:String?
    var acceptedVacation:Int?

    
    init(id:Int,
         vacDate:String,
         acceptedVacation:Int) {
        
        self.id = id
        self.vacDate = vacDate
        self.acceptedVacation = acceptedVacation
    }
}
