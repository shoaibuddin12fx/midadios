//
//  StudyMaterialsModel.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/22/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class TransportsModel: NSObject {
    var id:Int?
    var transportTitle:String?
    var transportDescription:String?
    var transportFare:String?

    
    init(id:Int,
         transportTitle:String,
         transportDescription:String,
         transportFare:String) {
        
        self.id = id
        self.transportTitle = transportTitle
        self.transportDescription = transportDescription
        self.transportFare = transportFare
    }
}
