//
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/22/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class CalendarEventModel: NSObject {
    var id:String?
    var title:String?
    var start:String?
    var url:String?
    var backgroundColor:String?
    var textColor:String?
    var allDay:String?

    
    init(id:String,
         title:String,
         start:String,
         url:String,
         backgroundColor:String,
         textColor:String,
         allDay:String) {
        
        self.id = id
        self.title = title
        self.start = start
        self.url = url
        self.backgroundColor = backgroundColor
        self.textColor = textColor
        self.allDay = allDay
    }
}
