//
//  VacationApproveModel.swift
//  Schoex
//
//  Created by Mohamed Selim Refaat on 10/18/19.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class VacationApproveModel: NSObject {
    var id:Int?
    var userid:Int?
    var fullName:String?
    var email:String?
    var role:String?
    var vacDate:String?

    init(id:Int,
         userid:Int,
         fullName:String,
         email:String,
         role:String,
         vacDate:String) {
        
        self.id = id
        self.userid = userid
        self.fullName = fullName
        self.email = email
        self.role = role
        self.vacDate = vacDate
    }
}
