//
//  StudyMaterialsModel.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/22/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class AlbumsModel: NSObject {
    var id:Int?
    var albumTitle:String?
    var albumDescription:String?
    var albumImage:String?
    var albumParent:String?

    
    init(id:Int,
         albumTitle:String,
         albumDescription:String,
         albumImage:String,
         albumParent:String) {
        
        self.id = id
        self.albumTitle = albumTitle
        self.albumDescription = albumDescription
        self.albumImage = albumImage
        self.albumParent = albumParent
    }
}
