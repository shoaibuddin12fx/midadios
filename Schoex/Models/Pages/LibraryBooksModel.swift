//
//  StudyMaterialsModel.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/22/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class LibraryBooksModel: NSObject {
    var id:Int?
    var bookName:String?
    var bookAuthor:String?
    var bookType:String?
    var bookPrice:String?
    var bookFile:String?

    
    init(id:Int,
         bookName:String,
         bookAuthor:String,
         bookType:String,
         bookPrice:String,
         bookFile:String) {
        
        self.id = id
        self.bookName = bookName
        self.bookAuthor = bookAuthor
        self.bookType = bookType
        self.bookPrice = bookPrice
        self.bookFile = bookFile
    }
}
