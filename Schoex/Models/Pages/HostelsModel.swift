//
//  StudyMaterialsModel.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/22/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class HostelsModel: NSObject {
    var id:Int?
    var hostelTitle:String?
    var hostelType:String?
    var hostelAddress:String?
    var hostelManager:String?
    var hostelNotes:String?

    
    init(id:Int,
         hostelTitle:String,
         hostelType:String,
         hostelAddress:String,
         hostelManager:String,
         hostelNotes:String) {
        
        self.id = id
        self.hostelTitle = hostelTitle
        self.hostelType = hostelType
        self.hostelAddress = hostelAddress
        self.hostelManager = hostelManager
        self.hostelNotes = hostelNotes
    }
}
