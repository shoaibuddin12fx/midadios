//
//  StudyMaterialsModel.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/22/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class NewsDashModel: NSObject {
    var id:Int?
    var title:String?
    var type:String?
    var start:String?
    var isNews:Bool?

    init(id:Int,
         title:String,
         type:String,
         start:String) {
        
        self.id = id
        self.title = title
        self.type = type
        self.start = start
        self.isNews = type == "news"
    }
}
