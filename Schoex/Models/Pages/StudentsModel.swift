//
//  StudyMaterialsModel.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/22/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class StudentsModel: NSObject {
    var id:Int?
    var username:String?
    var fullName:String?
    var email:String?
    var studentClass:String?
    var isLeaderBoard:String?

    
    init(id:Int,
         username:String,
         fullName:String,
         email:String,
         studentClass:String,
         isLeaderBoard:String) {
        
        self.id = id
        self.username = username
        self.fullName = fullName
        self.email = email
        self.studentClass = studentClass
        self.isLeaderBoard = isLeaderBoard
    }
}
