//
//  StudyMaterialsModel.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/22/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class AttendanceControlModel: NSObject {
    
    let StudentName:String;
    var Status:String?;
    var StatusId:Int;
    let StudentRollId:String;
    let StudentId:Int;
    var vacation:Bool;
    var vacationStat:String;
    
    
    init(StudentName : String,Status : Int,StudentRollId : String,StudentId : Int,vacation : Bool,vacationStat : String) {
        
        self.StudentName = StudentName
        self.StatusId = Status
        self.Status = AttendanceControlPage.statusIdentifiers[self.StatusId]
        self.StudentRollId = StudentRollId
        self.StudentId = StudentId
        self.vacation = vacation
        self.vacationStat = vacationStat
        
    }
    func updateStatus() {
        self.Status = AttendanceControlPage.statusIdentifiers[self.StatusId]
    }

}
