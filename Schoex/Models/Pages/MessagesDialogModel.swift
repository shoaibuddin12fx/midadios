//
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/22/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class MessagesDialogModel: NSObject {
    
    var id:String?
    var lastMessageDate:String?
    var lastMessage:String?
    var userId:String?
    var fullName:String?
    var messageStatus:String?
    
    init(id:String,
         lastMessageDate:String,
         lastMessage:String,
         userId:String,
         fullName:String,
         messageStatus:String) {
        
        self.id = id
        self.lastMessageDate = lastMessageDate
        self.lastMessage = lastMessage
        self.userId = userId
        self.fullName = fullName
        self.messageStatus = messageStatus
    }
}
