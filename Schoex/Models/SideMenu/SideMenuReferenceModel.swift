//
//  SideMenuReferenceModel.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/22/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit

class SideMenuReferenceModel: NSObject {
    var id:Int?
    var itemTitle:String?
    var itemImage:String?
    var itemStoryboard:String?

    
    init(id:Int,itemTitle:String,itemImage:String,itemStoryboard:String) {
        self.id = id
        self.itemTitle = itemTitle
        self.itemImage = itemImage
        self.itemStoryboard = itemStoryboard
    }
}
