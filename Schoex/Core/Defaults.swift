//
//  Defaults.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 10/24/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework

class Defaults {
    
    static func setupDefaultWidgetStatus(widget : WidgetCreator)  {
        widget.initStatusViews()

        widget.loadingViewText?.applyDashSubStyle(BiggerSizeMode: true)
        widget.errorViewText?.applyDashSubStyle(BiggerSizeMode: true)
        widget.emptyViewText?.applyDashSubStyle(BiggerSizeMode: true)
        
    }
}
