//
//  ViewLayout.swift
//  EduopusKit
//
//  Created by Mohamed Selim Refaat on 5/17/19.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation
import UIKit

/**
 USE UIVIEW AS CELL AND REVERSE
 
 SOURCE : https://medium.com/anysuggestion/how-to-share-layout-code-between-uiview-and-uitableviewcell-subclasses-929441a9752a
 
 --> AS CELL
 itemViewType4.tableView.register(TableViewCell<ItemViewType5>.self, forCellReuseIdentifier: "view5")
 let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "view5", for: indexPath)
 
 --> AS UIVIEW
 let view5 = View<ItemViewType5>(frame: .zero)
 
 **/

protocol ViewLayout {
    
    func layout(on view: UIView)
    
    init()
    
}
