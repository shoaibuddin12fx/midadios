//
//  ViewGenerator.swift
//  EduopusKit
//
//  Created by Mohamed Selim Refaat on 5/17/19.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation
import UIKit

/**
 USE UIVIEW AS CELL AND REVERSE
 
 SOURCE : https://medium.com/anysuggestion/how-to-share-layout-code-between-uiview-and-uitableviewcell-subclasses-929441a9752a
 
 --> AS CELL
 itemViewType4.tableView.register(TableViewCell<ItemViewType5>.self, forCellReuseIdentifier: "view5")
 let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "view5", for: indexPath)
 
 --> AS UIVIEW
 let view5 = View<ItemViewType5>(frame: .zero)
 
 **/

class ViewConverter<Layout : ViewLayout> : UIView {
    
    public let layout: Layout
    
    public override init(frame: CGRect) {
        self.layout = Layout()
        super.init(frame: frame)
        layout.layout(on: self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class TableViewCell<Layout : ViewLayout> : UITableViewCell {
    
    public let layout: Layout
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        self.layout = Layout()
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        layout.layout(on: contentView) // important!
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
