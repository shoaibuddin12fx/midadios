
import Foundation
import UIKit
import SnapKit

class TableViewLayout: UIView {
    
    //============================================ Variables ======================================//
    
    // Handlers
    var shouldSetupConstraints = true
    
    // Views
    let tableView = UITableView()

    //============================================ Creators ======================================//
    func createTable(delegate : UITableViewDelegate, dataSource : UITableViewDataSource) {
        tableView.delegate = delegate
        tableView.dataSource = dataSource
        tableView.separatorColor = UIColor.clear
        tableView.backgroundColor = .clear
        tableView.layer.backgroundColor = UIColor.clear.cgColor

        self.addSubview(tableView)
    }
   
    
    override func updateConstraints() {
        if(shouldSetupConstraints) {
            // AutoLayout constraints
            
            tableView.snp.makeConstraints { make in
                make.edges.equalToSuperview()
            }
            
            shouldSetupConstraints = false
        }
        super.updateConstraints()
    }
    
    
    //============================================ Initialization ======================================//
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

}
