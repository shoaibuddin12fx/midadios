
import UIKit
import UserNotifications
import SwiftyJSON
import Firebase
import SBFramework
import Fabric
import Crashlytics
import IQKeyboardManagerSwift

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate ,UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    var navigationController : UINavigationController?
    var appWasActive : Bool = false

    var notificationTitle : String?
    var notificationWhere : String?
    var notificationId : String?
    
    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        
        IQKeyboardManager.shared.enable = true

        // Register Fabric & Firebase
        Fabric.with([Crashlytics.self])
        FirebaseApp.configure()

        if #available(iOS 13.0, *) {
            // In iOS 13 setup is done in SceneDelegate
        } else {
            let window = UIWindow(frame: UIScreen.main.bounds)
            self.window = window

               
            let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewcontroller:UIViewController = mainstoryboard.instantiateViewController(withIdentifier: "MainNavControllerStoryboard") as! UINavigationController
            window.rootViewController = newViewcontroller
            
        }
        
        
        // Register defaults
        navigationController = application.windows[0].rootViewController as? UINavigationController
        let attributes = [NSAttributedString.Key.font: UIFont(name: "Cairo-Regular", size: 17)!]
        navigationController?.navigationBar.titleTextAttributes = attributes
        SolColors.colorsDictionary = Colors().colorsDictionary
        SolBools.x_gen_back_is_image = Bools.x_gen_back_is_image
        SolBools.x_login_back_is_image = Bools.x_login_back_is_image
        SolUtils.dsNPEnabled = false
        
        let defaults = UserDefaults.standard
        defaults.set(Constants.BASE_URL, forKey: "aa")
        defaults.set(Constants.LICENCE_CODE, forKey: "ab")
        defaults.set(Constants.VERSION_CODE, forKey: "ac");
        
        let notFirstOpenKey = "notFirstOpen"
        let notFirstOpen = UserDefaults.standard.bool(forKey: notFirstOpenKey)
        if notFirstOpen == false {
            UserDefaults.standard.set(["ar"], forKey: "AppleLanguages")
            UserDefaults.standard.set(true, forKey: notFirstOpenKey)
        }
        
        //NotificationCenter.default.addObserver(self, selector: #selector(applicationWillResignActive(notification:)), name: NSNotification.Name.UIApplicationWillResignActive, object: UIApplication.shared)
        
        //Messaging.messaging().delegate = self
        
        
        // Register notifications
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
                (granted, error) in
                print("Permission granted: \(granted)")
                
                guard granted else { return }
                self.getNotificationSettings()
            }
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            
        }
        application.registerForRemoteNotifications()
        

        return true
    }
    
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            
        }
    }
    
    
    
    // this fires after notification come when app in forground without tap
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
     
        completionHandler([.alert, .badge, .sound])
    }
    
    // this fire when tap notification when forground or background
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let userInfo = response.notification.request.content.userInfo

        
        //====== where ======//
        if let whereStr = userInfo["where"] {
            self.notificationWhere = whereStr as? String
        }
        //====== id ======//
        if let idStr = userInfo["id"] {
            self.notificationId = idStr as? String
        }
        
        if let rootViewController = self.window!.rootViewController as? UINavigationController {
            
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let nextVC = storyboard.instantiateViewController(withIdentifier: "SplashScreenStoryboard") as! SplashScreen
            rootViewController.pushViewController(nextVC, animated: true)

        }
        
        completionHandler()
    }

    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // Print message ID.
        NSLog("notification rec 1")
        
        // Print full message.
        //NSLog(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // Print message ID.
        NSLog("notification rec 2")

        // Print full message.
        //NSLog(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }

    
    
    
    
    /*@objc private func applicationWillResignActive(notification: NSNotification) {
        appWasActive = true
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {}
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        completionHandler(UIBackgroundFetchResult.newData)
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {}
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {}
 */
}


/*extension AppDelegate : MessagingDelegate {
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {}
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {}
}*/
