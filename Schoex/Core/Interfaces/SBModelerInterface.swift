//
//  ModelerInterface.swift
//  Eduopus
//
//  Created by Mohamed Selim Refaat on 5/28/19.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

protocol SBModelerInterface {
    mutating func onModelingStarted(tag : String)
    mutating func onModelingSucceeded(tag : String, modeledData : Any)
    mutating func onModelingFailed(tag : String, error : String)
}
