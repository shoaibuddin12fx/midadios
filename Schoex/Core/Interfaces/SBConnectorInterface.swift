//
//  ConnectorInterface.swift
//  Eduopus
//
//  Created by Mohamed Selim Refaat on 5/28/19.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation
protocol SBConnectorInterface {
    func onRequestStarted(tag : String)
    func onRequestSucceeded(tag : String, response : Any)
    func onRequestFailed(tag : String, exception : Error)
    func onPreparingUrl(tag : String, lastLoadedPage : Int) -> String
}
