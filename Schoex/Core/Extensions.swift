//
//  Extensions.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/28/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation
import UIKit
import SBFramework

extension String {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: Data(utf8),
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}


extension UIButton {
    func setBackgroundColor(color: UIColor, forState: UIControlState) {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        UIGraphicsGetCurrentContext()!.setFillColor(color.cgColor)
        UIGraphicsGetCurrentContext()!.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.clipsToBounds = true
        self.setBackgroundImage(colorImage, for: forState)
    }
}

extension UIView{
    func asCircle(){
        self.layer.cornerRadius = self.frame.width / 2;
        self.layer.masksToBounds = true
    }
}

extension UILabel {
    
    var textNotNull: String {
        get { return self.text! }
        set {
            if !newValue.isEmpty {
                self.text = newValue
            }else{
                self.text = "NA"
            }
        }
    }
    
    var translatedText: String {
        get { return self.text! }
        set {
            if !newValue.isEmpty {
                self.text = Language.getTranslationOf(findKey: newValue,defaultWord: newValue)
            }else{
                self.text = "NA"
            }
        }
    }
    
    func setLineSpacing(lineSpacing: CGFloat = 0.0, lineHeightMultiple: CGFloat = 0.0) {
        
        guard let labelText = self.text else { return }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.lineHeightMultiple = lineHeightMultiple
        
        let attributedString:NSMutableAttributedString
        if let labelattributedText = self.attributedText {
            attributedString = NSMutableAttributedString(attributedString: labelattributedText)
        } else {
            attributedString = NSMutableAttributedString(string: labelText)
        }
        
        // Line spacing attribute
        attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        
        self.attributedText = attributedString
    }
    
    func applyBaseStyle() {
        self.numberOfLines = 0
        self.lineBreakMode = .byWordWrapping
    }
    
    //============== DASH STYLES ====================//
    func changeFontSize(size : Int) {
        self.font = UIFont(name: self.font.fontName, size: CGFloat(size))
    }
    
    //============== DASH STYLES ====================//
    func applyDashHeaderStyle(BiggerSizeMode : Bool) {
        applyBaseStyle()
        self.textColor = SolColors.getColorByName(colorName: "x_dash_header_text")
        if BiggerSizeMode {
            self.font = UIFont(name: "Cairo-Bold", size: 20)
        }else{
            self.font = UIFont(name: "Cairo-Bold", size: 18)
        }
    }
    
    func applyDashMainStyle(BiggerSizeMode : Bool) {
        applyBaseStyle()
        self.textColor = SolColors.getColorByName(colorName: "x_dash_main_text")
        if BiggerSizeMode {
            self.font = UIFont(name: "Cairo-Bold", size: 18)
        }else{
            self.font = UIFont(name: "Cairo-Bold", size: 16)
        }
    }
    
    func applyDashSubStyle(BiggerSizeMode : Bool) {
        applyBaseStyle()
        self.textColor = SolColors.getColorByName(colorName: "x_dash_sub_text")
        if BiggerSizeMode {
            self.font = UIFont(name: "Cairo-Regular", size: 16)
        }else{
            self.font = UIFont(name: "Cairo-Regular", size: 14)
        }
    }
    
    //============== GEN STYLES ====================//
    func applyGenMainStyle(BiggerSizeMode : Bool) {
        applyBaseStyle()
        self.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
        if BiggerSizeMode {
            self.font = UIFont(name: "Cairo-Bold", size: 18)
        }else{
            self.font = UIFont(name: "Cairo-Bold", size: 16)
        }
    }
    
    func applyGenSubStyle(BiggerSizeMode : Bool) {
        applyBaseStyle()
        self.textColor = SolColors.getColorByName(colorName: "x_gen_sub_title_text")
        if BiggerSizeMode {
            self.font = UIFont(name: "Cairo-Regular", size: 16)
        }else{
            self.font = UIFont(name: "Cairo-Regular", size: 14)
        }
    }
    
    //============== GEN STYLES ====================//
    func applyBoxMainStyle(BiggerSizeMode : Bool) {
        applyBaseStyle()
        self.textColor = SolColors.getColorByName(colorName: "x_boxes_main_text")
        if BiggerSizeMode {
            self.font = UIFont(name: "Cairo-Bold", size: 18)
        }else{
            self.font = UIFont(name: "Cairo-Bold", size: 16)
        }
    }
    
    func applyBoxSubStyle(BiggerSizeMode : Bool) {
        applyBaseStyle()
        self.textColor = SolColors.getColorByName(colorName: "x_boxes_sub_title_text")
        if BiggerSizeMode {
            self.font = UIFont(name: "Cairo-Regular", size: 16)
        }else{
            self.font = UIFont(name: "Cairo-Regular", size: 14)
        }
    }
}

extension UIViewController {
    func hideNavigationBar(){
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
    }
    
    func showNavigationBar() {
        // Show the navigation bar on other view controllers
        if self.navigationController != nil{
            self.navigationController?.setNavigationBarHidden(false, animated: true)
        }
    }
    
}
extension UINavigationController {
    override open var childViewControllerForStatusBarStyle: UIViewController? {
        return self.topViewController
    }
}

extension UInt64 {
    func megabytes() -> UInt64 {
        return self * 1024 * 1024
    }
}


extension UIImage {
    func resizeWithPercent(percentage: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: size.width * percentage, height: size.height * percentage)))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
    func resizeWithHeight(height: CGFloat) -> UIImage? {
        let imageView = UIImageView(
            frame: CGRect(
                origin: .zero,
                size: CGSize(
                    width: CGFloat(ceil(height/size.height * size.width)),
                    height: height
                )
            )
        )
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
    
    func tintImage (color : UIColor) -> UIImage {
        let tintedImage = self.tint(color)
        return tintedImage
    }
    
    func tint(_ tintColor: UIColor?) -> UIImage {
        guard let tintColor = tintColor else { return self }
        return modifiedImage { context, rect in
            context.setBlendMode(.multiply)
            context.clip(to: rect, mask: self.cgImage!)
            tintColor.setFill()
            context.fill(rect)
        }
    }
    
    private func modifiedImage( draw: (CGContext, CGRect) -> ()) -> UIImage {
        
        // using scale correctly preserves retina images
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        defer { UIGraphicsEndImageContext() }
        guard let context = UIGraphicsGetCurrentContext() else { return self }
        
        // correctly rotate image
        context.translateBy(x: 0, y: size.height)
        context.scaleBy(x: 1.0, y: -1.0)
        
        let rect = CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height)
        
        draw(context, rect)
        
        guard let newImage = UIGraphicsGetImageFromCurrentImageContext() else { return self }
        return newImage
    }
    
}
