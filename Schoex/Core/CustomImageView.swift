//
//  CustomImageView.swift
//  Eduopus
//
//  Created by Mohamed Selim Refaat on 12/28/18.
//  Copyright © 2018 Solutionsbricks. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage
import Alamofire
import Kingfisher


class CustomImageView: UIImageView {
    
    private var _image: UIImage!
    override var image: UIImage! {
        get {
            return self._image
        }
        set {
            self._image = newValue
        }
    }
}
