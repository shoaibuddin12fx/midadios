//
//
//  Created by SolutionsBricks Mobile Development Team on 9/15/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Alamofire
import UIKit
import Kingfisher

class PhotoManager {
    
    //======= Images Handler ==========//
    
    func retrieveImage(for urlString: String, imageView : UIImageView) {
        if (urlString != nil && imageView != nil){
            var strippedURL = urlString
            if let qmRange = urlString.range(of: "?")?.lowerBound {
                strippedURL = String(urlString[..<qmRange])
            }
            if let url = URL(string: urlString) {
                let resource = ImageResource(downloadURL: url, cacheKey: strippedURL)
                
                imageView.kf.indicatorType = .activity
                imageView.kf.setImage(with: resource)
                imageView.layer.masksToBounds = false
                imageView.layer.cornerRadius = imageView.frame.size.width / 2
                imageView.clipsToBounds = true
            }
        }
    }
    
    func retrieveImage(for urlString: String, completion: @escaping (UIImage) -> Void) {
        
        var strippedURL = urlString
        if let qmRange = urlString.range(of: "?")?.lowerBound {
            strippedURL = String(urlString[..<qmRange])
        }
        
        let url = URL(string: urlString)!
        
        
        let resource = ImageResource(downloadURL: url, cacheKey: strippedURL)

        
        UIImageView().kf.setImage(with: resource, completionHandler: {
            (image, error, cacheType, imageUrl) in
            if let imageResult = image {
                completion(imageResult)
            }
        })
        
    }
}

