//
//  Utils.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/15/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation
import SwiftyJSON
import UIKit
import Oops
import Toaster
import FileBrowser
import SwiftyJSON
import SBFramework
import Firebase

class Utils {
    // ==================== App & User Prefs Variables =========================//
    static var AppToken : String? = ""
    static var LoggedInUser : LoggedUserModel = LoggedUserModel()
    static var activatedModules = [String]()
    static var userPermissionsModules = [String]()

    static var _siteTitle: String?
    static var siteTitle : String {
        set {
            _siteTitle = newValue
            UserDefaults.standard.set(newValue, forKey: "siteTitle")
        }
        get {
            if let siteTitleValue = _siteTitle {
                return siteTitleValue
            }else{
                if let siteTitle = UserDefaults.standard.object(forKey: "siteTitle") as? String {
                    _siteTitle = siteTitle
                    return _siteTitle!
                }else{
                    return "My School"
                }
            }
        }
    }
    
    static var _loginUsername: String?
    static var loginUsername : String {
        set {
            _loginUsername = newValue
            UserDefaults.standard.set(newValue, forKey: "LogUserLoginUsername")
        }
        get {
            if let logUsername = _loginUsername {
                return logUsername
            }else{
                if let savedUsername = UserDefaults.standard.object(forKey: "LogUserLoginUsername") as? String {
                    _loginUsername = savedUsername
                    return _loginUsername!
                }else{
                    return Utils.Nil
                }
            }
        }
    }
    
    static var _loginPassword: String?
    static var loginPassword : String {
        set {
            _loginPassword = newValue
            UserDefaults.standard.set(newValue, forKey: "LogUserLoginPassword")
        }
        get {
            if let logPassword = _loginPassword {
                return logPassword
            }else{
                if let savedPassword = UserDefaults.standard.object(forKey: "LogUserLoginPassword") as? String{
                    _loginPassword = savedPassword
                    return _loginPassword!
                }else{
                    return Utils.Nil
                }
            }
        }
    }
    static var App_CalendarType : String = "gregorian"
    static var App_DateFormat : String = "dd-mm-yyyy"
    static var App_AttendanceModelIsClass : Bool = false
    static var App_IsSectionsEnabled : Bool = false
    static var App_IsRtl : Bool = false
    static var App_NotificationsRefreshInsideMessage : Float = 5000 // Milleseconds
    static var App_NotificationsRefreshOutsideMessage : Float = 900000 // Milleseconds


    
    enum AppRoles : String {
        case Admin = "Admin"
        case Teacher = "Teacher"
        case Student = "Student"
        case Parent = "Parent"
        case Accountant = "Accountant"
    }
    
    static var Nil : String = "NA"
    static var NilInt : Int = -1

    static func getDictValueOf(sourceInput:JSON, itemKey:String) -> [[String: Any]] {
        
        let itemValue = sourceInput[itemKey];
        let obj = itemValue.rawValue;
        let v = obj as! [[String:Any]]
        return v;
        
        
    }
    
    static func getStringValueOf(sourceInput:JSON, itemKey:String) -> String {
        if let itemValue = sourceInput[itemKey].string {
            return itemValue
        }else{
            if sourceInput[itemKey].number != nil {
                if let itemValue = sourceInput[itemKey].number {
                    return "\(itemValue)"
                }else{
                    return "NA"
                }
            }else{
                return "NA"
            }
        }
    }
    
    static func getStringValueOrNilOf(sourceInput:JSON, itemKey:String) -> String {
        if let itemValue = sourceInput[itemKey].string {
            return itemValue
        }else{
            if sourceInput[itemKey].number != nil {
                if let itemValue = sourceInput[itemKey].number {
                    return "\(itemValue)"
                }else{
                    return Nil
                }
            }else{
                return Nil
            }
        }
    }
    
    static func getIntValueOf(sourceInput:JSON, itemKey:String) -> Int {
        if sourceInput[itemKey].null == nil {
            if let intItemValue = sourceInput[itemKey].int {
                return intItemValue
            }else if let intItemValue = sourceInput[itemKey].string {
                if(Int(intItemValue) != nil){
                    return Int(intItemValue)!
                }else{
                    return 0
                }
            }else{
                return 0
            }
        }else{
            return 0
        }
    }
    
    static func getIntValueOrNilOf(sourceInput:JSON, itemKey:String) -> Int {
        if sourceInput[itemKey].null == nil {
            if let intItemValue = sourceInput[itemKey].int{
                return intItemValue
            }else if let intItemValue = sourceInput[itemKey].string {
                if(Int(intItemValue) != nil){
                    return Int(intItemValue)!
                }else{
                    return NilInt
                }
            }else{
                return NilInt
            }
        }else{
            return NilInt
        }
    }
    
    static func getBooleanValueOf(sourceInput:JSON, itemKey:String) -> Bool {
        if let itemValue = sourceInput[itemKey].bool {
            return itemValue
        }else{
            return false
        }
    }
    
    
    static func downloadFile(viewController : UIViewController,fileDownloadUrl : String,fileName : String){
        
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let fileURL = documentsURL.appendingPathComponent(fileName)
            
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        
        Utils.showMsg(msg: Language.getTranslationOf(findAndDefaultKey:"Downloading"))
        
        print("download")
         print(destination)
         print(SBFramework.strip(st:fileDownloadUrl))
         
        
        SBFramework.download(SBFramework.strip(st:fileDownloadUrl), to: destination).response { response in
            
            if response.error == nil, let imagePath = response.destinationURL?.path {
                print(imagePath)
                Utils.showMsg(msg: Language.getTranslationOf(findAndDefaultKey:"File Downloaded"))
                viewController.present(FileBrowser(), animated: true, completion: nil)
            }else{
                Utils.showMsg(msg: Language.getTranslationOf(findAndDefaultKey:"Error Occurred"))
            }
        }
    }
    
    static func makeLeaderboard(currentLeaderBoardMsg : String, leaderboardLink : String) {
        
        
        
        let configuration = Oops.Configuration(
            titleFont: UIFont(name: "RobotoCondensed-Bold", size: 20)!,
            textFont: UIFont(name: "RobotoCondensed-Bold", size: 14)!,
            buttonFont: UIFont(name: "RobotoCondensed-Bold", size: 14)!,
            showCloseButton: false,
            dynamicAnimatorActive: false
        )
        let alert = Oops(configuration: configuration)
        let txt1 = alert.addTextField(Language.getTranslationOf(findKey: "leaderBoardMessage",defaultWord: "Enter leaderboard message"))
        if currentLeaderBoardMsg != Utils.Nil && currentLeaderBoardMsg != ""{
            txt1.text = currentLeaderBoardMsg
        }
        alert.addButton("OK") {
            if txt1.text != nil && txt1.text != ""{
                let parameters: Parameters = [ "isLeaderBoard": txt1.text! ]
                
                SBFramework.request(leaderboardLink, method: .post,parameters: parameters).responseJSON { response in
                    switch response.result {
                    case .success(let value):
                        
                        Utils.parseUpdateResponse(jsonResponse: JSON(value))
                        
                    case .failure(let error):
                        print(error)
                        Utils.showMsg(msg: Language.getTranslationOf(findKey: "errorOccurred",defaultWord: "Error Occurred"))
                    }
                    
                }
            }
        }
        alert.addButton("Close") {
            alert.hideView()
        }
        alert.show(.editor, title: Language.getTranslationOf(findKey: "leaderBoardMessage",defaultWord: "Enter leaderboard message"), detail: "")
    }
    
    static func parseUpdateResponse(jsonResponse : JSON){

        if (jsonResponse["status"].null == nil){
            if jsonResponse["status"] == "success"{
                Utils.showMsg(msg: "Updates Saved")
            }else{
                Utils.showMsg(msg: "Error Occurred")
            }
        }else{
            Utils.showMsg(msg: "Error Occurred")
        }
    }
    static func parseUpdateResponseByMessage(jsonResponse : JSON){

          if (jsonResponse["status"].null == nil){
              if jsonResponse["status"] == "success"{
                Utils.showMsg(msg: jsonResponse["message"].string ?? "Error Occurred")
              }else{
                  Utils.showMsg(msg: "Error Occurred")
              }
          }else{
              Utils.showMsg(msg: "Error Occurred")
          }
      }
    static func showMsg(msg:String){
        Toast(text: msg).show()
    }
    
    static func getTranslationOf(findKey : String,defaultValue : String) -> String {
        return defaultValue
    }
    
    static func setButtonBackgroundColors(targetButton : UIButton,normalState : String , pressedState : String){
        targetButton.setBackgroundColor(color: SolColors.getColorByName(colorName: normalState)!, forState: UIControlState.normal)
        targetButton.setBackgroundColor(color: SolColors.getColorByName(colorName: pressedState)!, forState: UIControlState.highlighted)
    }
    
    static func getColor(hexColor:String)->UIColor{
        var redInt:uint = 0
        var greenInt:uint = 0
        var blueInt:uint = 0
        var range = NSMakeRange(0, 2)
           
        Scanner(string: (hexColor as NSString).substring(with: range)).scanHexInt32(&redInt)
        range.location = 2
        Scanner(string: (hexColor as NSString).substring(with: range)).scanHexInt32(&greenInt)
        range.location = 4
        Scanner(string: (hexColor as NSString).substring(with: range)).scanHexInt32(&blueInt)

        return UIColor(red: (CGFloat(redInt)/255.0), green: (CGFloat(greenInt)/255.0), blue: (CGFloat(blueInt)/255.0), alpha: 1)
    }
       
    //===================== App & User Prefs ===========================//
    static func saveLoggedInUser(loggedInUser : LoggedUserModel) {
        let defaults = UserDefaults.standard
        defaults.set(loggedInUser.LoggedInUserId, forKey: "LogUserId")
        defaults.set(loggedInUser.LoggedInUserFullName, forKey: "LogUserFullName")
        defaults.set(loggedInUser.LoggedInUserUsername, forKey: "LogUserUsername")
        defaults.set(loggedInUser.LoggedInUserRole?.rawValue, forKey: "LogUserRole")

        self.LoggedInUser = loggedInUser
    }
    
    static func getLoggedInUser() -> LoggedUserModel {
        let defaults = UserDefaults.standard
        
        let userId = defaults.integer(forKey: "LogUserId")
        let userFullname = defaults.object(forKey: "LogUserFullName") as? String
        let userUsername = defaults.object(forKey: "LogUserUsername") as? String
        let userRoleRaw = defaults.object(forKey: "LogUserRole") as? String
        
        var userRole = AppRoles.Student
        if userRoleRaw == "Admin"{
            userRole = AppRoles.Admin
        }else if userRoleRaw == "Teacher"{
            userRole = AppRoles.Teacher
        }else if userRoleRaw == "Student"{
            userRole = AppRoles.Student
        }else if userRoleRaw == "Parent"{
            userRole = AppRoles.Parent
        }else if userRoleRaw == "Accountant"{
            userRole = AppRoles.Accountant
        }
        
        if userId != 0 && userFullname != nil && userUsername != nil {
            
            self.LoggedInUser = LoggedUserModel(
                LoggedInUserId: userId,
                LoggedInUserFullName: userFullname!,
                LoggedInUserUsername: userUsername!,
                LoggedInUserRole : userRole)
            
            return self.LoggedInUser
        }else{
            return LoggedUserModel()
        }
    }
    static func removeLoggedInUser (){
        self.LoggedInUser = LoggedUserModel()
        self.loginUsername = Utils.Nil
        self.loginPassword = Utils.Nil
        
        UserDefaults.standard.removeObject(forKey: "LogUserId")
        UserDefaults.standard.removeObject(forKey: "LogUserFullName")
        UserDefaults.standard.removeObject(forKey: "LogUserUsername")
        UserDefaults.standard.removeObject(forKey: "LogUserRole")
        UserDefaults.standard.removeObject(forKey: "LogUserLoginUsername")
        UserDefaults.standard.removeObject(forKey: "LogUserLoginPassword")
        
    }
    
    static func saveNotificationsTimes(notificationsRefreshInsideMessageInSeconds : Float,notificationsRefreshOutsideMessageInSeconds : Float) {
        let defaults = UserDefaults.standard
        defaults.set(notificationsRefreshInsideMessageInSeconds, forKey: "NotRefreshIn")
        defaults.set(notificationsRefreshOutsideMessageInSeconds, forKey: "NotRefreshOut")
        
        self.App_NotificationsRefreshInsideMessage = notificationsRefreshInsideMessageInSeconds
        self.App_NotificationsRefreshOutsideMessage = notificationsRefreshOutsideMessageInSeconds

    }
    
    static func loadNotificationsTimes() {
        let defaults = UserDefaults.standard
        
        let notificationsRefreshInsideMessageInSeconds = defaults.float(forKey: "NotRefreshIn")
        let notificationsRefreshOutsideMessageInSeconds = defaults.float(forKey: "NotRefreshOut")
        
        if notificationsRefreshInsideMessageInSeconds != 0 && notificationsRefreshOutsideMessageInSeconds != 0{
            self.App_NotificationsRefreshInsideMessage = notificationsRefreshInsideMessageInSeconds
            self.App_NotificationsRefreshOutsideMessage = notificationsRefreshOutsideMessageInSeconds
        }
    }
    
    static func isModuleActivated(moduleName : String) -> Bool {
        if self.activatedModules.count == 0{
            if let savedActiveMods = UserDefaults.standard.object(forKey: "activatedModules") as? [String] {
                self.activatedModules = savedActiveMods
            }
        }

        if self.activatedModules.count > 0{
            if self.activatedModules.contains(moduleName) {
                return true
            }else{
                return false
            }
        }else{
            return false
        }
    }
    
    static func isUserHavePermission(checkThisModules : [String]) -> Bool {
        if self.userPermissionsModules.count == 0{
            if let savedActiveMods = UserDefaults.standard.object(forKey: "userCustomPerm") as? [String] {
                self.userPermissionsModules = savedActiveMods
            }
        }

        if self.userPermissionsModules.count > 0{
            for checkModule in checkThisModules{
                if self.userPermissionsModules.contains(checkModule) {
                    return true
                }
            }
        }
        return false
    }
    
    static func checkFailureCause(error : Error, callback : RenewTokenCallback,navigationController : UINavigationController,tokenRenewTried : Bool){
        let nsError = error as? NSError
        if nsError?.code == -1009 {
            Utils.showMsg(msg: error.localizedDescription)
            callback.afterRenewToken(success: false)
        }else{
            if tokenRenewTried{
                Utils.showMsg(msg: "Error occurred")
            }else{
                renewToken(callback : callback,navigationController : navigationController)
            }
        }

    }
    
    
    
    static func renewToken(callback : RenewTokenCallback, navigationController : UINavigationController){
        let url = "\(Constants.getModRewriteBaseUrl())/auth/authenticate"
        
        
        var paramFcmToken = ""
        if let fcmToken = InstanceID.instanceID().token() {
            paramFcmToken = fcmToken
        }
        
        
        let parameters: Parameters = [
            "username":  Utils.loginUsername,
            "password":  Utils.loginPassword,
            "android_token":  paramFcmToken
        ]
        
        SBFramework.request(url, method: .post,parameters: parameters).responseJSON { response in
            switch response.result {
            case .success(let value):
                
                //======== Parse result ==========//
                let json = JSON(value)
                print(json)
                if let errorOcc = json["error"].string {
                    if errorOcc == "invalid_credentials"{
                        Utils.showMsg(msg: "Credentials changed, please login again")
                        Utils.removeLoggedInUser()
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                        let nextVC = storyboard.instantiateViewController(withIdentifier: "LoginPageStoryboard")
                        navigationController.pushViewController(nextVC, animated: true)
                    }else{
                        Utils.showMsg(msg: "Error occurred")
                        callback.afterRenewToken(success: false)
                    }
                }
                
                if let tokenValue = json["token"].string {
                    let defaults = UserDefaults.standard
                    Utils.AppToken = tokenValue
                    defaults.set(tokenValue, forKey: "adr")
                    SolUtils.AppToken = tokenValue
                    defaults.set(Constants.BASE_URL, forKey: "aa")
                    defaults.set(Constants.LICENCE_CODE, forKey: "ab")
                    defaults.set(Constants.VERSION_CODE, forKey: "ac")
                    
                    callback.afterRenewToken(success: true)
                } else {
                    callback.afterRenewToken(success: false)
                }
                
            case .failure(_):
                callback.afterRenewToken(success: false)
            }
            
        }
    }
    
    static func getBackIcon () -> UIImage {
        if Utils.App_IsRtl {
            return UIImage(named: "icn_back_rtl")!
        }else{
            return UIImage(named: "icn_back")!
        }
    }
    
    
    
    static func openNotification(storyboard : UIStoryboard, navigationController : UINavigationController,dashboardJsonData : JSON) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        if appDelegate.notificationWhere != nil{
            if appDelegate.notificationWhere == "newsboard"{
                if Utils.isUserHavePermission(checkThisModules: ["newsboard.list","newsboard.View"]){
                    if appDelegate.notificationId != nil{
                        
                        let nextVC = storyboard.instantiateViewController(withIdentifier: "WebViewerPageStoryboard") as! WebViewerPage
                        nextVC.newsId = Int(appDelegate.notificationId!)
                        nextVC.backToSplash = true
                        navigationController.pushViewController(nextVC, animated: true)
                        
                    }else{
                        let nextVC = storyboard.instantiateViewController(withIdentifier: "NewsPageStoryboard") as! NewsPage
                        navigationController.pushViewController(nextVC, animated: true)
                    }
                }else{
                    Utils.openDashboard(storyboard : storyboard,navigationController : navigationController,dashboardJsonData : dashboardJsonData)
                }
               
            }else if appDelegate.notificationWhere == "events"{
                if Utils.isUserHavePermission(checkThisModules: ["events.list","events.View"]){
                    if appDelegate.notificationId != nil{
                        
                        let nextVC = storyboard.instantiateViewController(withIdentifier: "WebViewerPageStoryboard") as! WebViewerPage
                        nextVC.eventsId = Int(appDelegate.notificationId!)
                        nextVC.backToSplash = true
                        navigationController.pushViewController(nextVC, animated: true)
                        
                    }else{
                        let nextVC = storyboard.instantiateViewController(withIdentifier: "EventsPageStoryboard") as! EventsPage
                        navigationController.pushViewController(nextVC, animated: true)
                    }
                }else{
                    Utils.openDashboard(storyboard : storyboard,navigationController : navigationController,dashboardJsonData : dashboardJsonData)
                }
                
            }else if appDelegate.notificationWhere == "messages"{
              
                let nextVC = storyboard.instantiateViewController(withIdentifier: "MessagesPageStoryboard") as! MessagesPage
                nextVC.openThisMessageID = appDelegate.notificationId
                navigationController.pushViewController(nextVC, animated: true)
                
            }else if appDelegate.notificationWhere == "classschedule"{
                
                if Utils.isUserHavePermission(checkThisModules: ["classSch.list"]){

                    let nextVC = storyboard.instantiateViewController(withIdentifier: "ClassScheduleChoosePageStoryboard") as! ClassScheduleChoosePage
                    navigationController.pushViewController(nextVC, animated: true)
                    
                }else{
                    Utils.openDashboard(storyboard : storyboard,navigationController : navigationController,dashboardJsonData : dashboardJsonData)
                }
                
            }else if appDelegate.notificationWhere == "attendance"{
                
                if Utils.isUserHavePermission(checkThisModules: ["Attendance.takeAttendance"]){
                    if Utils.LoggedInUser.LoggedInUserRole == Utils.AppRoles.Student{
                        
                        let nextVC = storyboard.instantiateViewController(withIdentifier: "AttendanceByStudentPageStoryboard") as! AttendanceByStudentPage
                        navigationController.pushViewController(nextVC, animated: true)
                        
                    }else if Utils.LoggedInUser.LoggedInUserRole == Utils.AppRoles.Parent {
                        
                        let nextVC = storyboard.instantiateViewController(withIdentifier: "AttendanceByParentChoosePageStoryboard") as! AttendanceByParentChoosePage
                        navigationController.pushViewController(nextVC, animated: true)
                        
                    }else{
                        
                        let nextVC = storyboard.instantiateViewController(withIdentifier: "AttendancePageStoryboard") as! AttendancePage
                        navigationController.pushViewController(nextVC, animated: true)
                    }
                }else{
                    Utils.openDashboard(storyboard : storyboard,navigationController : navigationController,dashboardJsonData : dashboardJsonData)
                }
               
            }else if appDelegate.notificationWhere == "material"{
                
                if Utils.isUserHavePermission(checkThisModules: ["studyMaterial.list"]){
                    let nextVC = storyboard.instantiateViewController(withIdentifier: "StudyMaterialsPageStoryboard") as! StudyMaterialsPage
                    navigationController.pushViewController(nextVC, animated: true)
                    
                }else{
                    Utils.openDashboard(storyboard : storyboard,navigationController : navigationController,dashboardJsonData : dashboardJsonData)
                }
                
            }else if appDelegate.notificationWhere == "assignment"{
                
                if Utils.isUserHavePermission(checkThisModules: ["Assignments.list"]){
                    let nextVC = storyboard.instantiateViewController(withIdentifier: "AssignmentsPageStoryboard") as! AssignmentsPage
                    navigationController.pushViewController(nextVC, animated: true)
                    
                }else{
                    Utils.openDashboard(storyboard : storyboard,navigationController : navigationController,dashboardJsonData : dashboardJsonData)
                }
                
                
            }else if appDelegate.notificationWhere == "exams"{
                
                if Utils.isUserHavePermission(checkThisModules: ["examsList.list","examsList.View","examsList.showMarks"]){
                    let nextVC = storyboard.instantiateViewController(withIdentifier: "ExamsPageStoryboard") as! ExamsPage
                    navigationController.pushViewController(nextVC, animated: true)
                }else{
                    Utils.openDashboard(storyboard : storyboard,navigationController : navigationController,dashboardJsonData : dashboardJsonData)
                }
                
                
            }else if appDelegate.notificationWhere == "exams_marks"{
                
                let nextVC = storyboard.instantiateViewController(withIdentifier: "ExamsPageStoryboard") as! ExamsPage
                navigationController.pushViewController(nextVC, animated: true)
                
            }else if appDelegate.notificationWhere == "online_exams"{
                
                
                if Utils.isUserHavePermission(checkThisModules: ["onlineExams.list"]){
                    let nextVC = storyboard.instantiateViewController(withIdentifier: "OnlineExamsPageStoryboard") as! OnlineExamsPage
                    navigationController.pushViewController(nextVC, animated: true)
                }else{
                    Utils.openDashboard(storyboard : storyboard,navigationController : navigationController,dashboardJsonData : dashboardJsonData)
                }
                
                
            }else if appDelegate.notificationWhere == "invoice"{
                
                if Utils.isUserHavePermission(checkThisModules: ["Invoices.list","Invoices.View"]){
                    let nextVC = storyboard.instantiateViewController(withIdentifier: "InvoiceDetailsPageStoryboard") as! InvoiceDetailsPage
                    nextVC.invoiceID =  appDelegate.notificationId!
                    navigationController.pushViewController(nextVC, animated: true)
                }else{
                    Utils.openDashboard(storyboard : storyboard,navigationController : navigationController,dashboardJsonData : dashboardJsonData)
                }
                
                
            }else if appDelegate.notificationWhere == "homework"{
                
                if Utils.isUserHavePermission(checkThisModules: ["Homework.list", "Homework.View"]){
                    let nextVC = storyboard.instantiateViewController(withIdentifier: "HomeworkDetailsPageStoryboard") as! HomeworkDetailsPage
                    nextVC.homeworkID =  appDelegate.notificationId!
                    navigationController.pushViewController(nextVC, animated: true)
                }else{
                    Utils.openDashboard(storyboard : storyboard,navigationController : navigationController,dashboardJsonData : dashboardJsonData)
                }
                
                
            }else if appDelegate.notificationWhere == "mob_notif"{
                Utils.openDashboard(storyboard : storyboard,navigationController : navigationController,dashboardJsonData : dashboardJsonData)
            }else{
                Utils.openDashboard(storyboard : storyboard,navigationController : navigationController,dashboardJsonData : dashboardJsonData)
            }
            self.clearNotificationDelegateVars()
        }
    }
    
    static func clearNotificationDelegateVars() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.notificationTitle = nil
        appDelegate.notificationWhere = nil
        appDelegate.notificationId = nil
    }
    
    
    static func openDashboard(storyboard : UIStoryboard, navigationController : UINavigationController,dashboardJsonData : JSON) {
        if dashboardJsonData.null == nil && !dashboardJsonData.isEmpty{
            let nextVC = storyboard.instantiateViewController(withIdentifier: "DashboardPageStoryboard") as! DashboardPage
            nextVC.dashboardJsonData = dashboardJsonData
            navigationController.pushViewController(nextVC, animated: true)
        }else{
            let nextVC = storyboard.instantiateViewController(withIdentifier: "SplashScreenStoryboard") as! SplashScreen
            navigationController.pushViewController(nextVC, animated: true)
        }
        
    }
    
}

protocol RenewTokenCallback {
    func afterRenewToken(success : Bool)
}


