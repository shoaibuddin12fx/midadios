//
//  CustomButton.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 10/10/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import TransitionButton

class CustomButton: UIView {
    
    @IBOutlet weak var transitionBtn: TransitionButton!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
