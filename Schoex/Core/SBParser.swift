//
//  SBParser.swift
//  Eduopus
//
//  Created by Mohamed Selim Refaat on 5/28/19.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation
import SwiftyJSON

class SBParser {
    
    //===================================== Converters =================================//
    static func convertToJson(data : Any) -> JSON {
        let json = JSON(data)
        return json
    }
    
    static func convertToJsonDictionary(data : Any) -> [String : JSON] {
        let json = JSON(data)
        if json.dictionary != nil{
            return json.dictionary ?? [String : JSON]()
        }
        return [String : JSON]()
    }
    
    static func convertToArray(data : Any) -> [JSON] {
        let json = JSON(data)
        if json.array != nil{
            return json.array ?? [JSON]()
        }
        return [JSON]()
    }
    
    //===================================== Json Array processors =================================//
    static func getJsonArray(attrHolder : JSON , attrName : String) -> [JSON] {
         if (attrHolder.null == nil ) {
            if let value = attrHolder[attrName].array {
                return value
            }
         }
        return [JSON]()
    }
 
    static func getJsonArray(attrHolder : JSON) -> [JSON] {
        if (attrHolder.null == nil ) {
            if let value = attrHolder.array {
                return value
            }
        }
        return [JSON]()
    }
    
    static func getJsonArray(attrHolder : [String : JSON]? , attrName : String) -> [JSON] {
        if (attrHolder != nil ) {
            if let value = attrHolder?[attrName]?.array {
                return value
            }
        }
        return [JSON]()
    }
    
    static func getJsonArrayFormatAsString(attrHolder : [String : JSON]? , attrName : String) -> [JSON] {
        if (attrHolder != nil ) {
            if let attrString = attrHolder![attrName]?.string {
                
                if let dataFromString = attrString.data(using: String.Encoding.utf8, allowLossyConversion: false) {
                    do {
                        let value = try JSON(data: dataFromString)
                        return value.array ?? [JSON]()
                    } catch {
                        return [JSON]()
                    }
                }
                
            }
        }
        return [JSON]()
    }
    
    
    static func getJsonArrayFormatAsString(attrHolder : JSON , attrName : String) -> [JSON] {
        if (attrHolder.null == nil ) {
            
            if let attrString = attrHolder[attrName].string {
                
                if let dataFromString = attrString.data(using: String.Encoding.utf8, allowLossyConversion: false) {
                    do {
                        let value = try JSON(data: dataFromString)
                        return value.array ?? [JSON]()
                    } catch {
                        return [JSON]()
                    }
                }
                
            }
        }
        return [JSON]()
    }
    
    
    //===================================== Dictionary processors =================================//
    
    static func getJsonDictionary(attrHolder : JSON , attrName : String) -> [String : JSON]? {
        if (attrHolder.null == nil ) {
            if let value = attrHolder[attrName].dictionary {
                return value
            }
        }
        return nil
    }
    
    static func getJsonDictionary(attrHolder : JSON) -> [String : JSON]? {
        if (attrHolder.null == nil ) {
            if let value = attrHolder.dictionary {
                return value
            }
        }
        return nil
    }
    
    static func getJsonDictionaryFormatAsString(attrHolder : JSON , attrName : String) -> [String : JSON]? {
        if (attrHolder.null == nil ) {
            
            if let attrString = attrHolder[attrName].string {
                
                if let dataFromString = attrString.data(using: String.Encoding.utf8, allowLossyConversion: false) {
                    do {
                        let value = try JSON(data: dataFromString)
                        return value.dictionary ?? nil
                    } catch {
                        return nil
                    }
                }
                
            }
        }
        return nil
    }
    
    static func getJsonDictionaryFormatAsString(attrHolder : [String : JSON]? , attrName : String) -> [String : JSON]? {
        if (attrHolder != nil ) {
            if let attrString = attrHolder![attrName]?.string {
                
                if let dataFromString = attrString.data(using: String.Encoding.utf8, allowLossyConversion: false) {
                    do {
                        let value = try JSON(data: dataFromString)
                        return value.dictionary ?? nil
                    } catch {
                        return nil
                    }
                }
                
            }
        }
        return nil
    }
    
    //===================================== Integer processors =================================//
    
    static func getInt(attrHolder : JSON , attrName : String) -> Int {
        if (attrHolder.null == nil ) {
            if let value = attrHolder[attrName].int {
                return value
            }else if let stringItemValue = attrHolder[attrName].string {
                if(Int(stringItemValue) != nil){
                    return Int(stringItemValue)!
                }
            }
        }
        return 0
    }
    
    static func getInt(attrHolder : JSON) -> Int {
        if (attrHolder.null == nil ) {
            if let value = attrHolder.int {
                return value
            }else if let stringItemValue = attrHolder.string {
                if(Int(stringItemValue) != nil){
                    return Int(stringItemValue)!
                }
            }
        }
        return 0
    }
    
    static func getInt(attrHolder : [String : JSON]? , attrName : String) -> Int {
        if (attrHolder != nil ) {
            if let value = attrHolder?[attrName]?.int {
                return value
            }else if let stringItemValue = attrHolder?[attrName]?.string {
                if(Int(stringItemValue) != nil){
                    return Int(stringItemValue)!
                }
            }
        }
        return 0
    }
    
    //===================================== String processors =================================//

    static func getString(attrHolder : JSON , attrName : String) -> String {
        if (attrHolder.null == nil ) {
            if let value = attrHolder[attrName].string {
                return value
            }else if let intItemValue = attrHolder[attrName].int {
                return String(intItemValue)
            }
        }
        return ""
    }
    
    static func getString(attrHolder : JSON) -> String {
        if (attrHolder.null == nil ) {
            if let value = attrHolder.string {
                return value
            }else if let intItemValue = attrHolder.int {
                return String(intItemValue)
            }
        }
        return ""
    }
    
    
    static func getString(attrHolder : [String : JSON]? , attrName : String) -> String {
        if (attrHolder != nil ) {
            if let value = attrHolder?[attrName]?.string {
                return value
            }else if let intItemValue = attrHolder?[attrName]?.int {
                return String(intItemValue)
            }
        }
        return ""
    }
    
    //===================================== Boolean processors =================================//

    static func getBoolean(attrHolder : JSON , attrName : String) -> Bool {
        if (attrHolder.null == nil ) {
            if let value = attrHolder[attrName].bool {
                return value
            }
        }
        return false
    }
    
    static func getBoolean(attrHolder : JSON) -> Bool {
        if (attrHolder.null == nil ) {
            if let value = attrHolder.bool {
                return value
            }
        }
        return false
    }
    
    static func getBoolean(attrHolder : [String : JSON]? , attrName : String) -> Bool {
        if (attrHolder != nil ) {
            if let value = attrHolder?[attrName]?.bool {
                return value
            }
        }
        return false
    }
    
    //===================================== Float processors =================================//

    static func getFloat(attrHolder : JSON , attrName : String) -> Float {
        if (attrHolder.null == nil ) {
            if let value = attrHolder[attrName].float {
                return value
            }
        }
        return 0.0
    }
    
    static func getFloat(attrHolder : JSON) -> Float {
        if (attrHolder.null == nil ) {
            if let value = attrHolder.float {
                return value
            }
        }
        return 0.0
    }
    
    static func getFloat(attrHolder : [String : JSON]? , attrName : String) -> Float {
        if (attrHolder != nil ) {
            if let value = attrHolder?[attrName]?.float {
                return value
            }
        }
        return 0.0
    }
    
    //===================================== Double processors =================================//

    static func getDouble(attrHolder : JSON , attrName : String) -> Double {
        if (attrHolder.null == nil ) {
            let value = attrHolder[attrName].doubleValue
            return value
        }
        return 0.0
    }
    
    static func getDouble(attrHolder : JSON) -> Double {
        if (attrHolder.null == nil ) {
            let value = attrHolder.doubleValue
            return value
            
        }
        return 0.0
    }
    
    static func getDouble(attrHolder : [String : JSON]?  , attrName : String) -> Double {
        if (attrHolder != nil ) {
            if let value = attrHolder?[attrName]?.doubleValue {
                return value
            }
        }
        return 0.0
    }
}
