//
//  ForumsModeler.swift
//  Eduopus
//
//  Created by Mohamed Selim Refaat on 5/28/19.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation
import SwiftyJSON

class ConnectorPagination {
    var PAGES_LOADED : Int?
    var isLastPageReached : Bool?
    var isLoading : Bool?
    var isModeling : Bool?
  
}
