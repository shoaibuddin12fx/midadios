//
//  Language.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation
import SBFramework

class Language{
    
    static var languageDictionary = [String : String]()

    static func getTranslationOf(findKey : String, defaultWord : String) -> String {
        if(languageDictionary.count > 0){
            let findKeyValue = findKey.lowercased().replacingOccurrences(of: " ", with: "")

            if let foundValue = languageDictionary[findKeyValue] {
                return foundValue
            }
        }
        return defaultWord
    }
    
    static func getTranslationOf(findAndDefaultKey : String) -> String {
        if(languageDictionary.count > 0){
            let findKeyValue = findAndDefaultKey.lowercased().replacingOccurrences(of: " ", with: "")
            
            if let foundValue = languageDictionary[findKeyValue] {
                return foundValue
            }
        }
        return findAndDefaultKey
    }
}
