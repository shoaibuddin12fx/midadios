//
//  Strings.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 11/1/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation
import SBFramework

class Strings {
     static var x_login_school_name = "Midad App"
}

extension String {
    func localized(lang:String) ->String {

        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        let bundle = Bundle(path: path!)
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
}

extension CreatorLabel{
    
    override public var font: UIFont! {
        get { return super.font }
        set {
            super.font = UIFont(name: "Cairo", size: super.font.pointSize)
        }
    }
}

extension SolLabelView{
    
    override public var font: UIFont! {
        get { return super.font }
        set {
            super.font = UIFont(name: "Cairo", size: super.font.pointSize)
        }
    }
}

extension SolViewController{
    
    open override func viewDidLayoutSubviews() {
        let attributes = [NSAttributedString.Key.font: UIFont(name: "Cairo-Regular", size: 17)!]
        self.navigationController?.navigationBar.titleTextAttributes = attributes
    }
    
}






