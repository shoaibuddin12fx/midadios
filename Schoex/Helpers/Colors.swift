//
//  Colors.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation
import UIKit

class Colors {
    public var colorsDictionary:[String:String] = [
        "mycolor" :                                     "#95000000",
        "mycolor2":                                     "#95C23333",
        "mycolor3":                                     "#95333CC2",
        "trans_white":                                  "#09FFFFFF",
        "transparent":                                  "#00ffffff",
        "x_boxes_header_back":                          "#fff1f1f1",//1
        "x_boxes_container_back":                       "#ffffffff",//2
        "x_boxes_header_text":                          "#ff434343",//3
        "x_boxes_main_text":                            "#ff434343",//4
        "x_boxes_sub_title_text":                       "#ff787878",//5
        "x_boxes_above_header_text":                    "#ffd64e4e",//6
        
        "x_boxes_menu_icon":                            "#ffffffff",//7
        "x_boxes_menu_icon_background_normal":          "#ff4782AD",//8
        "x_boxes_menu_icon_background_pressed":         "#ff366587",//9
        
        "x_divider_color":                              "#43d7d7d7",// 10
        "x_hint_text_color":                            "#ffd6d6d6",// 11
        
        "x_boxes_menu_text":                            "#ffffffff",// 12
        "x_boxes_menu_text_background_normal":          "#ffE9484E",// 13
        "x_boxes_menu_text_background_pressed":         "#ffbe353a",// 14
        
        "x_login_school_name":                          "#ffffffff",// 15
        "x_login_back":                                 "#ff047dac",// 16
        
        "x_header_icons":                               "#ffffffff",// 17
        "x_header_back":                                "#00000000",// 18
        "x_header_text":                                "#ffffffff",// 19
        
        "x_dash_header_text":                           "#ffffffff",// 20
        "x_dash_main_text":                             "#ffffffff",// 21
        "x_dash_sub_text":                              "#ffbcbcbc",// 22
        "x_dash_icons":                                 "#ffffffff",// 23
        
        "x_gen_back":                                   "#ffededed",// 24
        "x_gen_icons":                                  "#ffffffff",// 25
        "x_gen_main_text":                              "#ffffffff",// 26
        "x_gen_sub_title_text":                         "#ffc4c4c4",// 27
        
        "x_drawer_header_color":                        "#ff282E3A",// 28
        "x_drawer_title_text":                          "#ffffffff",// 29
        "x_drawer_sub_title_text":                      "#ffd2d2d2",// 30
        "x_drawer_list_item_text":                      "#ff333333",// 31
        "x_drawer_icon_normal":                         "#ffE9484E",// 32
        "x_drawer_icon_pressed":                        "#ffa43034",// 33
        
        "x_dash_side_button_1_back_normal":             "#ffE94E43",// 34
        "x_dash_side_button_1_back_pressed":            "#ffb93b32",// 35
        "x_dash_side_button_2_back_normal":             "#ff4782AD",// 36
        "x_dash_side_button_2_back_pressed":            "#ff39688b",// 37
        "x_dash_side_button_text":                      "#ffffffff",// 38
        "x_dash_top_tabs_icons":                        "#ff81cc02",// 39
        
        "x_calendar_header_back":                       "#ffffffff",// 40
        "x_calendar_header_text":                       "#71000000",// 41
        "x_calendar_no_events_back":                    "#71000000",// 42
        "x_calendar_have_events_back":                  "#fff96060",// 43
        "x_calendar_events_text":                       "#ffffffff",// 44
        "x_calendar_events_icon":                       "#ffffffff",// 45
        "x_calendar_more_back_normal":                  "#ffd64e4e",// 46
        "x_calendar_more_back_pressed":                 "#ffac3c3c",// 47
        
        "x_bottom_process_button_back_pressed":         "#ff2d2d2d",// 48
        "x_bottom_process_button_back_normal":          "#ff353535",// 49
        "x_bottom_process_button_text":                 "#ffffffff",// 50
        
        "white":                                        "#ffffffff"// 51
        
    ]
}
