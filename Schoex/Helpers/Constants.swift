//
//  Constants.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 11/3/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation

class Constants {
    
    
    /* ============================= BASE URL INSTRUCTIONS ====================================
     
     " BASE URL is web domain of your school which you use to open your school at web browser "
     
     ** Please follow these instructions when you write your Base URL:
     1. Base URL must be the same you registered inside our website Solutionsbricks.com when you purchased IOS application licence
     2. Base URL must start with your protocol http:// or https:// [ same protocol and URL registered inside your Solutionsbricks.com account ]
     3. Don't write "/login" or any schoex page path at end of your base URL, simply write only basic url
     
     =============== More information or support at Solutionsbricks.com =====================*/
    static var BASE_URL = "http://midad.ly"
    
    /* ============================= LICENCE CODE INSTRUCTIONS ===========================================
     
     " When you purchase IOS application licence from Solutionsbricks.com, you can find your licence code inside your account's products "
     
     ** Please follow these instructions when you write your licence code:
     1. Every solution sold by Solutionsbricks.com have its unique licence code, so don't use licence code of any another product like Android application or web products
     2. IOS licence code must start with schstdios
     3. After purchase IOS application, find licence code inside your account at Solutionsbricks.com, go to " My Bricks " section then to " My Products " section then find IOS application product then copy licence code to here inside the code.
     
     =============== More information or support at Solutionsbricks.com =====================*/
    static var LICENCE_CODE = "schstdios-4530500.2-5e713d3d15fe4"
    
    /* ================= Don't Change =============== */
    static var VERSION_CODE = 300
    
    /* ================= Don't Change =============== */
    static func getModRewriteBaseUrl() -> String{
        
        let defaults = UserDefaults.standard
        if let base_url = defaults.string(forKey: "base_url") {
            return "\(base_url)/index.php"
        }else{
            return "\(BASE_URL)/index.php"
        }
        
        
        
        
        
        
        
        
        
    }
    static func getWithoutModRewriteBaseUrl() -> String{
        return BASE_URL.replacingOccurrences(of: "/index.php", with: "")
    }
}
