//
//  Fonts.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 10/7/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation
import UIKit

class Fonts{
    static let FontRobotoCondensed_Bold : String = "RobotoCondensed-Bold"
    static let FontRobotoCondensed_BoldItalic : String  = "RobotoCondensed-BoldItalic"
    static let FontRobotoCondensed_Italic : String  = "RobotoCondensed-Italic"
    static let FontRobotoCondensed_Light : String  = "RobotoCondensed-Light"
    static let FontRobotoCondensed_LightItalic : String  = "RobotoCondensed-LightItalic"
    static let FontRobotoCondensed_Regular : String  = "RobotoCondensed-Regular"
    
    enum labelStyle{
        case HeaderTitle
        case HeaderSubTitle
        case DetailsTitle
        case DetailsSubTitle

    }
}

extension UILabel {
    func customFont(fontName : String,fontSize : Int) {
        self.font = UIFont(name: fontName, size: CGFloat(fontSize))
    }
    func setStyle(style : Fonts.labelStyle){
        self.numberOfLines = 0
        switch style {
            case Fonts.labelStyle.HeaderTitle:
                self.customFont(fontName: Fonts.FontRobotoCondensed_Bold, fontSize: 20)
            case Fonts.labelStyle.HeaderSubTitle:
                self.customFont(fontName: Fonts.FontRobotoCondensed_Regular, fontSize: 15)
            case Fonts.labelStyle.DetailsTitle:
                self.customFont(fontName: Fonts.FontRobotoCondensed_Bold, fontSize: 17)
            case Fonts.labelStyle.DetailsSubTitle:
                self.customFont(fontName: Fonts.FontRobotoCondensed_Regular, fontSize: 14)
        }
    }
}
