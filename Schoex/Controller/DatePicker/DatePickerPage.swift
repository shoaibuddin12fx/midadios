//
//  DatePickerPage.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/30/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework
import WebKit

class DatePickerPage: SolViewController,WKNavigationDelegate {
    
    @IBOutlet weak var navLeftButton: UIBarButtonItem!
    

    @IBOutlet weak var pickerView: WKWebView!
    fileprivate var bridge: WKWebViewJavascriptBridge!
    
    fileprivate var numOfLoadingRequest = 0
    var datePickedDelegate: DatePickerProtocol?
    var tag : String = ""
    var goBackAfterDatePicked = true
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //============================ Base Functions =================//
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initNavBar()
        initPage()
        self.navLeftButton.image = Utils.getBackIcon()
        self.view.backgroundColor = UIColor.clear
        
        pickerView.navigationDelegate = self
        self.pickerView.isOpaque = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    //============================ Init Functions =================//
    
    func initPage() {
        
        
        bridge = WKWebViewJavascriptBridge(webView: pickerView)
        bridge.register(handlerName: "datePickerCallback") { (parameters, callback) in
            if let params = parameters, let paramsString = params["date"] as? String {
                self.sendDateToProtocol(dateSelected: paramsString)
            }
        }
        self.loadLocalWebPage()
        
        // nav bar title
        self.navigationItem.title = Utils.siteTitle
    }
    
    func sendDateToProtocol(dateSelected:String) {
        // input date is in format yyyy-mm-dd
        let splittedDate = dateSelected.split{$0 == "-"}.map(String.init)
        var formattedDate = Utils.App_DateFormat

        if splittedDate.count == 3{
            formattedDate = formattedDate.replacingOccurrences(of: "dd", with: splittedDate[2], options: .literal, range: nil)
            formattedDate = formattedDate.replacingOccurrences(of: "mm", with: splittedDate[1], options: .literal, range: nil)
            formattedDate = formattedDate.replacingOccurrences(of: "yyyy", with: splittedDate[0], options: .literal, range: nil)
            datePickedDelegate?.onDatePicked(tag: tag, dateSelected: formattedDate)
        }else{
            datePickedDelegate?.onDatePicked(tag: tag, dateSelected: "")
        }
        if goBackAfterDatePicked {
            goBack()
        }
    }

    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("loaded")
        
        self.numOfLoadingRequest += 1
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.numOfLoadingRequest -= 1
        if (self.numOfLoadingRequest == 0) {
            //self.loadingActivity.stopAnimating()
            print("load ended")
        }
    }
    
    func webView(webView: WKWebView, didFailNavigation navigation: WKNavigation, withError error: NSError) {
        print("\(error)")
    }
    
    fileprivate func printReceivedParmas(_ data: AnyObject) {
        print("Swift recieved data passed from JS: \(data)")
    }
    
    fileprivate func loadLocalWebPage() {
        guard let urlPath = Bundle.main.url(forResource: Utils.App_CalendarType, withExtension: "html") else {
            print("Couldn't find the file in bundle!")
            return
        }
        
        var urlString: String
        do {
            urlString  = try String(contentsOf: urlPath)
            self.pickerView.loadHTMLString(urlString, baseURL: urlPath)
        }
        catch let error as NSError {
            NSLog("\(error)")
            return
        }
    }
    
    @IBAction func navLeftButtonAction(_ sender: Any) {
         goBack()
    }
    func goBack()  {
        self.navigationController?.popViewController(animated: true)
    }
}

protocol DatePickerProtocol {
    func onDatePicked(tag : String, dateSelected:String)
}

