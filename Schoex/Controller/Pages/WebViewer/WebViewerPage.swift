//
//  WebViewerPage.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 10/24/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import WebKit
import UIKit
import SBFramework
import SwiftyJSON

class WebViewerPage: SolViewController {

    @IBOutlet weak var pageTitle: UILabel!
    @IBOutlet weak var navLeftButton: UIBarButtonItem!
    @IBOutlet weak var pageWebView: WKWebView!
    
    var pageTitleData : String?
    var pageHtmlData : String?
    
    var newsId : Int?
    var eventsId : Int?

    var backToSplash = false
    
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navLeftButton.image = Utils.getBackIcon()
        self.pageTitle.textColor = SolColors.getColorByName(colorName: "x_boxes_main_text")
        initControllerStates()
        initNavBar()
        
        if newsId != nil{
            self.loadData(id : newsId!,about : "newsboard")
        }else if eventsId != nil{
            self.loadData(id : eventsId!,about : "events")
        }
        
        setPageData()
        // nav bar title
        self.navigationItem.title = Utils.siteTitle
    }
    

    func setPageData() {
        if(pageTitleData != nil){
            iHaveContentNow = true
            self.endLoading(error: nil) // Set Content
            pageTitle.text = pageTitleData!
        }
        if(pageHtmlData != nil){
            iHaveContentNow = true
            self.endLoading(error: nil) // Set Content

            var pageHtmlDataFull = "<meta name=\"viewport\" content=\"user-scalable=no, width=device-width, initial-scale=1.0, maximum-scale=1.0\">"+pageHtmlData!
            
            let font = UIFont(name: "Cairo-Regular", size: 16)
            
            pageHtmlDataFull = String(format: "<span style=\"font-family: %@; font-size: %i\">%@</span>", font?.fontName ?? "Cairo-Regular14", Int(font?.pointSize ?? 16), pageHtmlDataFull)
            
            pageWebView.loadHTMLString(pageHtmlDataFull, baseURL: nil)
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        setupInitialViewState()
        
    }
    func initControllerStates() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(loadData))
        super.initControllerStates(failureView: failureView)
    }
    
    //==================== Load data ===================================//
    @objc func loadData( id : Int , about : String) {
        startLoading ()
        
        var url = ""
        if about == "newsboard"{
            url = "\(Constants.getModRewriteBaseUrl())/newsboard/\(id)"
        }else if about == "events"{
            url = "\(Constants.getModRewriteBaseUrl())/events/\(id)"
        }


        SBFramework.request(url, method: .get).responseJSON { response in
            switch response.result {
            case .success(let value):
                
                //======== Parse result ==========//
                let json = JSON(value)
                if json.null == nil {
                    if about == "newsboard"{
                        self.pageTitleData = Utils.getStringValueOf(sourceInput: json,itemKey: "newsTitle")
                        self.pageHtmlData = Utils.getStringValueOf(sourceInput: json,itemKey: "newsText")
                    }else if about == "events"{
                        self.pageTitleData = Utils.getStringValueOf(sourceInput: json,itemKey: "eventTitle")
                        self.pageHtmlData = Utils.getStringValueOf(sourceInput: json,itemKey: "eventDescription")
                    }
                    
                    self.setPageData()
                }else{
                    self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
                }
            case .failure(_):

                self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
            }
            
        }
    }
    
    @IBAction func navLeftButtonAction(_ sender: Any) {
        if backToSplash{
            let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "SplashScreenStoryboard") as! SplashScreen
            nextVC.bypassLogin = true
            self.navigationController!.pushViewController(nextVC, animated: true)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
}
