//
//  ViewController.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/15/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework
import SwiftyJSON
import PagingMenuController

class DashboardPage:SolViewController {
    
    @IBOutlet weak var navLeftButton: UIBarButtonItem!
   
    var dashboardJsonData : JSON?

    //============================ Base Functions =================//
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initNavBar()
        initSideMenu()
        
        initPage()
        initTabs()

    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showNavigationBar()
        UIView.appearance().semanticContentAttribute = .forceLeftToRight

    }
    
    
    //============================ Init Functions =================//

    func initPage() {
        self.navLeftButton.image = UIImage(named: "icn_drawer")
        // nav bar title
        self.navigationItem.title = Utils.siteTitle
        
        
    }
   
    @IBAction func navLeftButtonAction(_ sender: Any) {
        showMenu()
    }
   
    func initTabs()  {
        let pagingMenuController = self.childViewControllers.first as! PagingMenuController
        
        
        
        let options = segmentOptions(dashboardJson: dashboardJsonData!)
        let attributes = [NSAttributedString.Key.font: UIFont(name: "Cairo-Regular", size: 17)!]
        pagingMenuController.navigationController?.navigationBar.titleTextAttributes = attributes
        
        pagingMenuController.setup(options)
        pagingMenuController.move(toPage: 1)
    }
    
    struct segmentOptions: PagingMenuControllerCustomizable {
        
        var leaderboardViewController: DashboardLeaderboardPage =
            DashboardLeaderboardPage.instantiateFromStoryboard()
        var statsViewController: DashboardStatsPage =
            DashboardStatsPage.instantiateFromStoryboard()
        var newsViewController: DashboardNewsPage =
            DashboardNewsPage.instantiateFromStoryboard()

        
        init(dashboardJson: JSON) {
            leaderboardViewController.dashboardJson = dashboardJson
            statsViewController.dashboardJson = dashboardJson
            newsViewController.dashboardJson = dashboardJson
        }
        
        var componentType: ComponentType {
            return .all(menuOptions: MenuOptions(), pagingControllers: [leaderboardViewController, statsViewController,newsViewController])
        }
        var menuControllerSet: MenuControllerSet {
            return .multiple
        }
        
        var backgroundColor: UIColor {
            return UIColor.clear
        }
        
        struct MenuItemLeaderboard: MenuItemViewCustomizable {
            var displayMode: MenuItemDisplayMode {
                let image = UIImage (named : "dash_tab_leaderboard_unselected")?.resizeWithHeight(height: 29)
                let selectedImage = UIImage (named : "dash_tab_leaderboard_selected")?.resizeWithHeight(height: 29)?.tint(SolColors.getColorByName(colorName: "x_dash_top_tabs_icons"))
                return .image(image: image!, selectedImage: selectedImage!)
            }
        }
        
        struct MenuItemStats: MenuItemViewCustomizable {
            var displayMode: MenuItemDisplayMode {
                let image = UIImage (named : "dash_tab_stat_unselected")?.resizeWithHeight(height: 29)
                let selectedImage = UIImage (named : "dash_tab_stat_selected")?.resizeWithHeight(height: 29)?.tint(SolColors.getColorByName(colorName: "x_dash_top_tabs_icons"))
                
                return .image(image: image!, selectedImage: selectedImage!)
            }
        }
        
        struct MenuItemNews: MenuItemViewCustomizable {
            var displayMode: MenuItemDisplayMode {
                let image = UIImage (named : "dash_tab_news_unselected")?.resizeWithHeight(height: 29)
                let selectedImage = UIImage (named : "dash_tab_news_selected")?.resizeWithHeight(height: 29)?.tint(SolColors.getColorByName(colorName: "x_dash_top_tabs_icons"))
                return .image(image: image!, selectedImage: selectedImage!)
            }
        }
        
        struct MenuOptions: MenuViewCustomizable {
            var displayMode: MenuDisplayMode {
                return .segmentedControl
            }
            var itemsOptions: [MenuItemViewCustomizable] {
                return [MenuItemLeaderboard(), MenuItemStats(), MenuItemNews()]
            }
            var height: CGFloat {
                return 50
            }
            var backgroundColor: UIColor {
                return UIColor.clear
            }
            var selectedBackgroundColor: UIColor {
                return SolColors.getColorByName(colorName: "transparent")!
            }
            var focusMode: MenuFocusMode {
                return .underline(height: 2, color: SolColors.getColorByName(colorName: "x_dash_top_tabs_icons")!, horizontalPadding: 2, verticalPadding: 2)
            }
        }
        
        
        
    }
}
