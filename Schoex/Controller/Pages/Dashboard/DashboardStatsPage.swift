import UIKit
import SBFramework

import SwiftyJSON
import GSImageViewerController


class DashboardStatsPage: UIViewController {
    
    var dashboardJson : JSON?
    var cornerRadius : CGFloat = 5
    
    class func instantiateFromStoryboard() -> DashboardStatsPage {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "DashboardStatsPageStoryboard") as! DashboardStatsPage
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func loadView() {
        let qLinksLeaderboardWidget = WidgetCreator()
        let dashboardView = DashboardView()
        
        var quickLinksArray = [quickLink]()
        if Utils.isUserHavePermission(checkThisModules: ["classSch.list"]){
            if Utils.isModuleActivated(moduleName: "classSchAct"){
                quickLinksArray.append(quickLink(tag: 1,title: "Class Schedule",image: "qlink_class_sch"))
            }
        }
        
        if Utils.isUserHavePermission(checkThisModules: ["Assignments.list"]){
            if Utils.isModuleActivated(moduleName: "assignmentsAct"){
                quickLinksArray.append(quickLink(tag: 2,title: "Assignments",image: "qlink_assignments"))
            }
        }
        
        if Utils.isUserHavePermission(checkThisModules: ["examsList.list","examsList.View"]){
            quickLinksArray.append(quickLink(tag: 3,title: "Exams List",image: "qlink_exams"))
        }
        
        if Utils.isUserHavePermission(checkThisModules: ["events.list","events.View"]){
            if Utils.isModuleActivated(moduleName: "eventsAct"){
                quickLinksArray.append(quickLink(tag: 4,title: "Events",image: "qlink_events"))
            }
        }
        
        if Utils.isModuleActivated(moduleName: "attendanceAct"){
            if(Utils.isUserHavePermission(checkThisModules: ["myAttendance.myAttendance","students.Attendance"])){
                if Utils.LoggedInUser.LoggedInUserRole == Utils.AppRoles.Student{
                    quickLinksArray.append(quickLink(tag: 5,title: "Attendance",image: "qlink_attendance"))
                }else if Utils.LoggedInUser.LoggedInUserRole == Utils.AppRoles.Parent {
                    quickLinksArray.append(quickLink(tag: 6,title: "Attendance",image: "qlink_attendance"))
                }
            }
            if Utils.isUserHavePermission(checkThisModules: ["Attendance.takeAttendance"]){
                quickLinksArray.append(quickLink(tag: 7,title: "Attendance",image: "qlink_attendance"))
            }
        }
        
   
        if Utils.isUserHavePermission(checkThisModules: ["mediaCenter.View"]){
            if Utils.isModuleActivated(moduleName: "mediaAct") {
                quickLinksArray.append(quickLink(tag: 8,title: "Media Center",image: "qlink_media_center"))
            }
        }
        
        
        Defaults.setupDefaultWidgetStatus(widget: qLinksLeaderboardWidget)
        qLinksLeaderboardWidget.setupStatusViews(loadingText: "Loading data", loadingImage: "no_data", emptyText: "No data yet", emptyImage: "no_data", errorText: "Error loading data", errorImage: "no_data")
        
        
        var coversViews = [UIImageView]()
        var dataViews = [CreatorContainerView]()
        
        if quickLinksArray.count > 0{
            
            let screenSize: CGRect = UIScreen.main.bounds
            let screenWidth = screenSize.width
            var boxWidth : Float = 190
            if screenWidth <= 440 {
                boxWidth = Float(( screenWidth - 40 ) / 2)
            }
            
            for currentItem in quickLinksArray {
                
                let coverImage = UIImageView()
                coverImage.clipsToBounds = true
                coverImage.contentMode = .scaleAspectFill
                coverImage.image = UIImage(named : currentItem.image)
                coverImage.layer.cornerRadius = cornerRadius
                
                let coverLabel = UILabel()
                coverLabel.translatedText = currentItem.title;
                coverLabel.applyDashMainStyle(BiggerSizeMode: false)
                
                coverImage.flex.justifyContent(.center).define { (flex) in
                    flex.addItem(coverLabel).alignSelf(.center).padding(5)
                }
                coverImage.tag = currentItem.tag
                
                let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.quickLinksSelector (_:)))
                coverImage.isUserInteractionEnabled = true
                coverImage.addGestureRecognizer(gesture)
                
                coversViews.append(coverImage)
            }
            
            for i in 1...coversViews.count {
                if (i % 2) == 0 {
                    continue
                }
                var currentAv : Bool = false
                var nextAv : Bool = false
                var currentItem : UIImageView?
                var nextItem : UIImageView?
                
                if quickLinksArray.indices.contains((i-1)){
                    currentItem = coversViews[i - 1]
                    currentAv = true
                    if quickLinksArray.indices.contains(i){
                        nextItem = coversViews[i]
                        nextAv = true
                    }
                }
                if currentAv {
                    let mView = CreatorContainerView()
                    
                    mView.flex.direction(.row).justifyContent(.spaceAround).marginBottom(15).define { (flex) in
                        flex.addItem(currentItem!).width(CGFloat(boxWidth)).height(75)
                        if nextAv{ flex.addItem(nextItem!).width(CGFloat(boxWidth)).height(75) }
                    }
                    dataViews.append(mView)
                }
                
            }
            
        }
        
        qLinksLeaderboardWidget.dataViewsCopies = 7
        qLinksLeaderboardWidget.injectDataViews(dataViews: dataViews) // Inject at anytime
        qLinksLeaderboardWidget.prepareDataView() // Don't call untill you set data copies
        qLinksLeaderboardWidget.changeStatus(currentActiveView: .dataSet)
        
        dashboardView.injectView(mView: qLinksLeaderboardWidget.createView())
        
        dashboardView.createView()
        self.view = dashboardView
    }
    
    @objc func quickLinksSelector (_ sender:UITapGestureRecognizer){
        let coverImage = sender.view as? UIImageView
        let tag = coverImage?.tag
        if tag == 1{
            let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "ClassScheduleChoosePageStoryboard")
            self.navigationController!.pushViewController(nextVC!, animated: true)
        }else if tag == 2{
            let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "AssignmentsPageStoryboard")
            self.navigationController!.pushViewController(nextVC!, animated: true)
        }else if tag == 3{
            let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "ExamsPageStoryboard")
            self.navigationController!.pushViewController(nextVC!, animated: true)
        }else if tag == 4{
            let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "EventsPageStoryboard")
            self.navigationController!.pushViewController(nextVC!, animated: true)
        }else if tag == 5{
            let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "AttendanceByStudentPageStoryboard")
            self.navigationController!.pushViewController(nextVC!, animated: true)
        }else if tag == 6{
            let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "AttendanceByParentChoosePageStoryboard")
            self.navigationController!.pushViewController(nextVC!, animated: true)
        }else if tag == 7{
            let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "AttendancePageStoryboard")
            self.navigationController!.pushViewController(nextVC!, animated: true)
        }else if tag == 8{
            let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "MediaCenterPageStoryboard")
            self.navigationController!.pushViewController(nextVC!, animated: true)
        }
    }
    
}

class quickLink {
    var tag : Int = 0
    var title : String = ""
    var image : String = ""
    
    init(tag:Int,title:String,image:String) {
        self.tag = tag
        self.title = title
        self.image = image
        
    }
}

