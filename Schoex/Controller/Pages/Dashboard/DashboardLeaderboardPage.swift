import UIKit
import SBFramework

import SwiftyJSON
import GSImageViewerController


class DashboardLeaderboardPage: UIViewController {
    
    var dashboardJson : JSON?
    
    let studentLeaderboardWidget = WidgetCreator()
    let teacherLeaderboardWidget = WidgetCreator()
    var studentsLeaderboardList = [DashLeadModel]()
    var teacherLeaderboardList = [DashLeadModel]()
    
    class func instantiateFromStoryboard() -> DashboardLeaderboardPage {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "DashboardLeaderboardPageStoryboard") as! DashboardLeaderboardPage
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setLeadboards()
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func loadView() {
        parseDashboardData()
        createStudentsLeaderboardWidget()
        createTeachersLeaderboardWidget()
        
        //=================== Create dashboard view ===================//
        let dashboardView = DashboardView()
        if Utils.isUserHavePermission(checkThisModules: ["dashboard.studentLeaderboard"]){
            dashboardView.injectView(mView: studentLeaderboardWidget.createView())
        }
        if Utils.isUserHavePermission(checkThisModules: ["dashboard.teacherLeaderboard"]){
            dashboardView.injectView(mView: teacherLeaderboardWidget.createView())
        }
        dashboardView.createView()
        
        // Set dashboard view to controller view
        self.view = dashboardView
    }
    
    func parseDashboardData() {
        
        //Parse students leaderboard
        if let json = dashboardJson{
            if let studentLeaderBoard = json["studentLeaderBoard"].array {
                for item in studentLeaderBoard {
                    studentsLeaderboardList.append(DashLeadModel(
                        id : Utils.getIntValueOf(sourceInput: item,itemKey: "id") ,
                        name : Utils.getStringValueOf(sourceInput: item,itemKey: "fullName") ,
                        msg : Utils.getStringValueOf(sourceInput: item,itemKey: "isLeaderBoard")
                        )
                    )
                }
            }else{
                studentLeaderboardWidget.changeStatus(currentActiveView: .error)
            }
        }else{
            studentLeaderboardWidget.changeStatus(currentActiveView: .error)
        }
        
        //Parse teacher leaderboard
        if let json = dashboardJson{
            if let teacherLeaderBoard = json["teacherLeaderBoard"].array {
                for item in teacherLeaderBoard {
                    
                    teacherLeaderboardList.append(DashLeadModel(
                        id : Utils.getIntValueOf(sourceInput: item,itemKey: "id") ,
                        name : Utils.getStringValueOf(sourceInput: item,itemKey: "fullName") ,
                        msg : Utils.getStringValueOf(sourceInput: item,itemKey: "isLeaderBoard")
                        )
                    )
                }
            }else{
                teacherLeaderboardWidget.changeStatus(currentActiveView: .error)
            }
        }else{
            teacherLeaderboardWidget.changeStatus(currentActiveView: .error)
        }
    }
    
    
    func setLeadboards() {
        
        if(studentsLeaderboardList.count > 0){
            // Set data
            var index = 0
            for mView in studentLeaderboardWidget.dataViews! {
                if studentsLeaderboardList.indices.contains(index){
                    let cView = mView as! ViewsCreator1
                    cView.text1!.text = studentsLeaderboardList[index].name
                    cView.text2!.text = studentsLeaderboardList[index].msg
                    cView.mainImage!.setSource(imageName: "\(Constants.getModRewriteBaseUrl())/dashboard/profileImage/\(studentsLeaderboardList[index].id!)", fromInternet: true)
                    index = index + 1
                }
            }
            studentLeaderboardWidget.changeStatus(currentActiveView: .dataSet)
        }else{
            studentLeaderboardWidget.changeStatus(currentActiveView: .empty)
        }
        
        if(teacherLeaderboardList.count > 0){
            // Set data
            var index = 0
            for mView in teacherLeaderboardWidget.dataViews! {
                if teacherLeaderboardList.indices.contains(index){
                    let cView = mView as! ViewsCreator1
                    cView.text1!.text = teacherLeaderboardList[index].name
                    cView.text2!.text = teacherLeaderboardList[index].msg
                    cView.mainImage!.setSource(imageName: "\(Constants.getModRewriteBaseUrl())/dashboard/profileImage/\(teacherLeaderboardList[index].id!)", fromInternet: true)
                    
                    index = index + 1
                }
            }
            teacherLeaderboardWidget.changeStatus(currentActiveView: .dataSet)
        }else{
            teacherLeaderboardWidget.changeStatus(currentActiveView: .empty)
        }
        
    }
    
    func createStudentsLeaderboardWidget() {
        //=================== Create students leaderboard item ===================//
        let dataViewsCopies = 4
        var dataViews = [ViewsCreator1]()
        
        var index = 0
        for _ in studentsLeaderboardList{
            if index == 4{break}
            dataViews.append(ViewsCreator1())
            index = index + 1
        }
        
        for mView in dataViews {
            mView.isOn_text1 = true
            mView.text1?.applyDashMainStyle(BiggerSizeMode : false)
            mView.text1?.setMargins(leftMargin: 0, topMargin: 0, rightMargin: 10, bottomMargin: 0)
            
            mView.isOn_text2 = true
            mView.text2?.applyDashSubStyle(BiggerSizeMode : false)
            mView.text2?.setMargins(leftMargin: 0, topMargin: 0, rightMargin: 10, bottomMargin: 0)
            
            
            mView.isOn_mainImage = true
            mView.mainImage!.setViewSize(overAllSize: 35)
            mView.mainImage!.setSource(imageName: "icon_pages_user", fromInternet: false)
            mView.mainImage?.setMargins(leftMargin: 0, topMargin: 6, rightMargin: 10, bottomMargin: 10)
            
            _ = mView.createView()
        }
        
        //=================== Create students leaderboard widget ===================//
        Defaults.setupDefaultWidgetStatus(widget: studentLeaderboardWidget)
        studentLeaderboardWidget.setupStatusViews(
            loadingText: Language.getTranslationOf(findKey: "loading",defaultWord: "Loading"),
            loadingImage: "no_student_leader",
            emptyText: Language.getTranslationOf(findKey: "noStudents",defaultWord: "No students in leaderboard yet"),
            emptyImage: "no_student_leader",
            errorText: Language.getTranslationOf(findKey: "errorOccurred",defaultWord: "Error Occurred"),
            errorImage: "no_student_leader")
        
        
        studentLeaderboardWidget.changeStatus(currentActiveView: .loading)
        
        //students leaderboard header
        let widgetHeader = ViewsCreator1()
        widgetHeader.isOn_text1 = true
        widgetHeader.text1?.applyDashHeaderStyle(BiggerSizeMode: true)
        widgetHeader.text1?.text = Language.getTranslationOf(findKey: "studentLeaderboard",defaultWord: "Students Leaderboard")
        widgetHeader.text1?.setMargins(leftMargin: 0, topMargin: 6, rightMargin: 10, bottomMargin: 10)
        
        //others
        studentLeaderboardWidget.dataViewsCopies = dataViewsCopies
        studentLeaderboardWidget.injectHeaderView(headerView: widgetHeader.createView(), redrawWithEveryDrawCycle: false)
        studentLeaderboardWidget.injectDataViews(dataViews: dataViews) // Inject at anytime
        studentLeaderboardWidget.prepareDataView() // Don't call untill you set data copies
    }
    
    func createTeachersLeaderboardWidget() {
        //=================== Create leaderboard item ===================//
        let dataViewsCopies = 4
        var dataViews = [ViewsCreator1]()
        
        var index = 0
        for _ in teacherLeaderboardList{
            if index == 4{break}
            dataViews.append(ViewsCreator1())
            index = index + 1
        }
        
        for mView in dataViews {
            mView.isOn_text1 = true
            mView.text1?.applyDashMainStyle(BiggerSizeMode : false)
            mView.text1?.setMargins(leftMargin: 0, topMargin: 0, rightMargin: 10, bottomMargin: 0)
            
            mView.isOn_text2 = true
            mView.text2?.applyDashSubStyle(BiggerSizeMode : false)
            mView.text2?.setMargins(leftMargin: 0, topMargin: 0, rightMargin: 10, bottomMargin: 0)
            
            mView.isOn_mainImage = true
            mView.mainImage!.setViewSize(overAllSize: 35)
            mView.mainImage!.setSource(imageName: "icon_pages_user", fromInternet: false)
            mView.mainImage?.setMargins(leftMargin: 0, topMargin: 6, rightMargin: 10, bottomMargin: 10)
            
            _ = mView.createView()
        }
        
        //=================== Create leaderboard widget ===================//
        Defaults.setupDefaultWidgetStatus(widget: teacherLeaderboardWidget)
        teacherLeaderboardWidget.setupStatusViews(
            loadingText: Language.getTranslationOf(findKey: "loading",defaultWord: "Loading"),
            loadingImage: "no_teacher_leader",
            emptyText: Language.getTranslationOf(findKey: "noTeachers",defaultWord: "No teachers in leaderboard yet"),
            emptyImage: "no_teacher_leader",
            errorText: Language.getTranslationOf(findKey: "errorOccurred",defaultWord: "Error Occurred"),
            errorImage: "no_teacher_leader")
        teacherLeaderboardWidget.changeStatus(currentActiveView: .loading)
        
        //students leaderboard header
        let widgetHeader = ViewsCreator1()
        widgetHeader.isOn_text1 = true
        widgetHeader.text1?.applyDashHeaderStyle(BiggerSizeMode: true)
        widgetHeader.text1?.text = Language.getTranslationOf(findKey: "teacherLeaderboard",defaultWord: "Teachers Leaderboard")
        widgetHeader.text1?.setMargins(leftMargin: 0, topMargin: 6, rightMargin: 10, bottomMargin: 10)
        widgetHeader.text1?.font = UIFont(name: "Cairo", size: (widgetHeader.text1?.font.pointSize)!)
        
        //others
        teacherLeaderboardWidget.dataViewsCopies = dataViewsCopies
        teacherLeaderboardWidget.injectHeaderView(headerView: widgetHeader.createView(), redrawWithEveryDrawCycle: false)
        teacherLeaderboardWidget.injectDataViews(dataViews: dataViews) // Inject at anytime
        teacherLeaderboardWidget.prepareDataView() // Don't call untill you set data copies
    }
}


