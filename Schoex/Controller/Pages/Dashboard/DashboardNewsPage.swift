import UIKit
import SBFramework
import SwiftyJSON
import GSImageViewerController


class DashboardNewsPage: UIViewController {
    
    var dashboardJson : JSON?
    let newsEventsWidget = WidgetCreator()
    var newsEventsList = [NewsDashModel]()
    
    
    class func instantiateFromStoryboard() -> DashboardNewsPage {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "DashboardNewsPageStoryboard") as! DashboardNewsPage
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNewsEvents()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func loadView() {
        parseDashboardData()
        createNewsEventsWidget()
        
        //=================== Create dashboard view ===================//
        let dashboardView = DashboardView()
        dashboardView.injectView(mView: newsEventsWidget.createView())
        dashboardView.createView()
        
        // Set dashboard view to controller view
        self.view = dashboardView
    }
    
    
    
    func setNewsEvents() {
        
        //Parse news & events
        if(newsEventsList.count > 0){
            // Set data
            var index = 0
            for mView in newsEventsWidget.dataViews! {
                if newsEventsList.indices.contains(index){
                    let cView = mView as! ViewsCreator1
                    cView.text1!.text = newsEventsList[index].title
                    cView.text2!.text = newsEventsList[index].start
                    
                    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.openNewsItem))
                    cView.tag = index
                    cView.addGestureRecognizer(tapGesture)
                    
                    index = index + 1
                }
            }
            newsEventsWidget.changeStatus(currentActiveView: .dataSet)
        }else{
            newsEventsWidget.changeStatus(currentActiveView: .empty)
        }
        
        
    }
    
    
    
    func parseDashboardData() {
        //Parse news & events
        if let json = dashboardJson{
            if let dataList = json["newsEvents"].array {
                for item in dataList {
                    newsEventsList.append(NewsDashModel(
                        id : Utils.getIntValueOf(sourceInput: item,itemKey: "id") ,
                        title : Utils.getStringValueOf(sourceInput: item,itemKey: "title") ,
                        type : Utils.getStringValueOf(sourceInput: item,itemKey: "type"),
                        start : Utils.getStringValueOf(sourceInput: item,itemKey: "start")
                        )
                    )
                }
            }else{
                newsEventsWidget.changeStatus(currentActiveView: .error)
            }
        }else{
            newsEventsWidget.changeStatus(currentActiveView: .error)
        }
    }
    
    
    @objc func openNewsItem (sender: UITapGestureRecognizer){
        let index = sender.view?.tag
        let item = newsEventsList[index!]
        
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "WebViewerPageStoryboard") as! WebViewerPage
        if item.isNews!{
            nextVC.newsId = item.id
        }else{
            nextVC.eventsId = item.id
        }
        self.navigationController!.pushViewController(nextVC, animated: true)
        
    }
    
    func createNewsEventsWidget() {
        //=================== Create students leaderboard item ===================//
        let dataViewsCopies = 4
        var dataViews = [ViewsCreator1]()
        
        var index = 0
        for _ in newsEventsList{
            if index == 4{break}
            dataViews.append(ViewsCreator1())
            index = index + 1
        }
        
        for mView in dataViews {
            mView.isOn_text1 = true
            mView.text1?.applyDashMainStyle(BiggerSizeMode : false)
            mView.text1?.setMargins(leftMargin: 0, topMargin: 0, rightMargin: 10, bottomMargin: 0)
            
            mView.isOn_text2 = true
            mView.text2?.applyDashSubStyle(BiggerSizeMode : false)
            mView.text2?.setMargins(leftMargin: 0, topMargin: 0, rightMargin: 10, bottomMargin: 10)
            
            mView.isOn_mainImage = true
            mView.mainImage!.setViewSize(overAllSize: 20)
            mView.mainImage!.setColor( color: SolColors.getColorByName(colorName: "x_dash_icons")!)
            mView.mainImage!.setSource(imageName: "news", fromInternet: false)
            mView.mainImage?.setMargins(leftMargin: 0, topMargin: 6, rightMargin: 10, bottomMargin: 10)
            
            _ = mView.createView()
        }
        
        //=================== Create students leaderboard widget ===================//
        Defaults.setupDefaultWidgetStatus(widget: newsEventsWidget)
        newsEventsWidget.setupStatusViews(loadingText: "Loading news", loadingImage: "no_data", emptyText: "No news or events yet", emptyImage: "no_data", errorText: "Error loading news", errorImage: "no_data")
        newsEventsWidget.changeStatus(currentActiveView: .loading)
        
        //students leaderboard header
        let widgetHeader = ViewsCreator1()
        widgetHeader.isOn_text1 = true
        widgetHeader.text1?.applyDashHeaderStyle(BiggerSizeMode: true)
        widgetHeader.text1?.text = Language.getTranslationOf(findKey: "NewsEvents",defaultWord: "News - Events")
        widgetHeader.text1?.setMargins(leftMargin: 0, topMargin: 6, rightMargin: 10, bottomMargin: 10)
        
        //others
        newsEventsWidget.dataViewsCopies = dataViewsCopies
        newsEventsWidget.injectHeaderView(headerView: widgetHeader.createView(), redrawWithEveryDrawCycle: false)
        newsEventsWidget.injectDataViews(dataViews: dataViews) // Inject at anytime
        newsEventsWidget.prepareDataView() // Don't call untill you set data copies
    }
    
}


