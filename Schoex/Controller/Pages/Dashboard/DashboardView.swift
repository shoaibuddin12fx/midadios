//
//  WidgetCreator.swift
//  FlexLayoutSample
//
//  Created by SolutionsBricks Mobile Development Team on 10/21/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit


class DashboardView: UIView {

    //==================== GENERAL VARAIBLES ===============//
    fileprivate let contentView = UIScrollView()
    fileprivate var rootFlexContainer = UIView()
    fileprivate var viewsArray = [UIView]()

    func injectView(mView : UIView) {
        viewsArray.append(mView)
    }
    
    func createView() {
        
        
        rootFlexContainer.flex.direction(.column).padding(12).define { (flex) in
            rootFlexContainer.flex.direction(.column).define { (flex) in
                for view in viewsArray {
                    flex.addItem(view)
                }
            }
        }
        
        contentView.addSubview(rootFlexContainer)
        addSubview(contentView)
    }
    
    //==================== CONSTRUCTORS ===============//
    init() {
        super.init(frame: .zero)

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.pin.top().bottom().left().right()
        rootFlexContainer.pin.top().left().right()
        
        if Utils.App_IsRtl{
            rootFlexContainer.flex.layoutDirection(.rtl).layout(mode: .adjustHeight)
        }else{
            rootFlexContainer.flex.layout(mode: .adjustHeight)
        }
        
        contentView.contentSize = rootFlexContainer.frame.size

    }
   
}
