//
//  ViewController.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/15/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework
import SwiftyJSON


class SubjectsPage:SolViewController,UITableViewDataSource,UITableViewDelegate,RenewTokenCallback {
    
    @IBOutlet weak var dataTableView: UITableView!
    @IBOutlet weak var navLeftButton: UIBarButtonItem!

    var searchMode:Bool = false
    
    var page : Int = 1
    var loadMoreLocked : Bool = false
    
    //============================ Base Functions =================//
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initTableView(tableView: dataTableView)
        initNavBar()
        initSideMenu()
        initControllerStates()
        
        initPage()

    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupInitialViewState()
        
    }
    
    //============================ Init Functions =================//

    func initPage() {
        if searchAbout != nil {
            searchMode = true
            self.navLeftButton.image = Utils.getBackIcon()
        }else{
            searchMode = false
            self.navLeftButton.image = UIImage(named: "icn_drawer")
        }
        self.loadData(isLoadMore: false)
        
        // nav bar title
        self.navigationItem.title = Utils.siteTitle
    }
    
  
    func initControllerStates() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(failLoadData))
        super.initControllerStates(failureView: failureView)
    }
    
    @objc func failLoadData() {
        loadData(isLoadMore : false)
    }
    
    //==================== Load data ===================================//
    @objc func loadData(isLoadMore : Bool) {
        if(!isLoadMore){
            self.page = 1
            self.loadMoreLocked = false
            self.dataArray.removeAll()
            self.dataTableView.reloadData()
            startLoading()
        }else{
            if(loadMoreLocked){return}
        }
        
        let url = "\(Constants.getModRewriteBaseUrl())/subjects/listAll/\(self.page)"
        
        let builder : DataRequest
        if(searchMode && searchAbout != nil){
            let searchParameters:Parameters = [
                "searchInput": searchAbout as! String
            ]
            builder = SBFramework.request(url, method: .post,parameters: searchParameters)
        }else{
            builder = SBFramework.request(url, method: .get)
        }
        
        builder.responseJSON { response in
            switch response.result {
            case .success(let value):
                
                //======== Parse result ==========//
                let json = JSON(value)
                if (json["error"].null == nil){
                    if(!self.tokenRenewTried){ Utils.renewToken(callback : self,navigationController : self.navigationController!) }
                }
                if let jsonArray = json["subjects"].array {
                    
                    //================= Fill data array ==================//
                    for item in jsonArray {
                        self.dataArray.append(SubjectsModel(
                            id : Utils.getIntValueOf(sourceInput: item,itemKey:"id") ,
                            subjectTitle : Utils.getStringValueOf(sourceInput: item,itemKey:"subjectTitle") ,
                            className : Utils.getStringValueOf(sourceInput: item,itemKey:"className") ,
                            teacherName : Utils.getStringValueOf(sourceInput: item,itemKey:"teacherName") ,
                            teacherId : Utils.getStringValueOf(sourceInput: item,itemKey:"teacherId") ,
                            passGrade : Utils.getStringValueOf(sourceInput: item,itemKey:"passGrade") ,
                            finalGrade : Utils.getStringValueOf(sourceInput: item,itemKey:"finalGrade")
                            )
                        )
                    }
                   
                    if(isLoadMore){
                        if(jsonArray.count == 0){
                            self.loadMoreLocked = true
                        }
                    }
                    self.endLoading(error: nil) // Set Content
                    self.dataTableView.reloadData()
                    self.page = self.page + 1
                    
                }else{
                    self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
                }
            case .failure(let error):
                Utils.checkFailureCause(error : error, callback: self, navigationController: self.navigationController!,tokenRenewTried:self.tokenRenewTried)
                if self.tokenRenewTried {self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) }
            }
        }
    }
    var tokenRenewTried = false
    func afterRenewToken(success: Bool) {
        tokenRenewTried = true
        if success {
            loadData(isLoadMore: false)
        }else{
            self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
        }
    }
    
    //==================== Nav Bar Buttons Actions ===================================//

    @IBAction func reloadPage(_ sender: Any) {
        self.dataArray.removeAll()
        loadData(isLoadMore: false)
    }
    
    @IBAction func searchButtonAction(_ sender: Any) {
        showSearchDialog(targetStoryboard: "SubjectsPageStoryboard")
    }
    
   
    @IBAction func navLeftButtonAction(_ sender: Any) {
        if(self.searchMode){
            self.navigationController?.popViewController(animated: true)
        }else{
            showMenu()
        }
    }

    //==================== Table view options ===================================//
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = dataArray.count - 1
        if indexPath.row == lastElement {
            self.loadData(isLoadMore: true)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:SubjectsCell = tableView.dequeueReusableCell(withIdentifier: "cellCon", for: indexPath) as! SubjectsCell
        cell.item = dataArray[indexPath.row] as! SubjectsModel
        return cell
    }
    
}






