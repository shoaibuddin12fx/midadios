//
//  ViewController.swift
//  EduopusKit
//
//  Created by Mohamed Selim Refaat on 3/3/19.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework
import Kingfisher
import NVActivityIndicatorView
import SnapKit

class MyPayrollPage:SolViewController,UITableViewDelegate,UITableViewDataSource,SBConnectorInterface,SBModelerInterface {
    
    let loaderContainer = UIView(frame: CGRect.zero)
    let netSalaryLabel = UILabel(frame: CGRect.zero)
    let overtimeLabel = UILabel(frame: CGRect.zero)
    let amountLabel = UILabel(frame: CGRect.zero)
    let methodLabel = UILabel(frame: CGRect.zero)
    let commentsLabel = UILabel(frame: CGRect.zero)

    var networkConnector : MyPayrollConnector!
    var modeler : MyPayrollModeler!
    let pageView = MyPayrollPageView(frame: CGRect.zero)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //================================= Setup Controller =============================//
        title = Language.getTranslationOf(findKey: "My Payroll",defaultWord: "My Payroll")
        
        // Set RTL
        if Utils.App_IsRtl {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }else{
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }
        
        initControllerStates()
        
        // Set Background image or color
        if Bools.x_gen_back_is_image {
            let backgroundImage = CustomImageView(frame: UIScreen.main.bounds)
            backgroundImage.image = UIImage(named: "x_gen_back")
            backgroundImage.contentMode = UIView.ContentMode.scaleAspectFill
            self.view.insertSubview(backgroundImage, at: 0)
        }else{
            view.backgroundColor = SolColors.getColorByName(colorName: "x_gen_back")
        }
        
        // Set Status bar background
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        statusBarView.backgroundColor = SolColors.getColorByName(colorName: "x_header_back")
        view.addSubview(statusBarView)
        
        // Set Navigation bar background
        navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController!.navigationBar.shadowImage = UIImage()
        UINavigationBar.appearance().backgroundColor = SolColors.getColorByName(colorName: "x_header_back")

        // Set Navigation title color
        navigationController!.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : SolColors.getColorByName(colorName: "x_header_text")!]
        
        // Set Navigation bar icons and colors
        let rightBarButtonItem = getNavBarButton(custom: false, image: nil, nonCustomIcon: .refresh, color: SolColors.getColorByName(colorName: "x_header_icons"),size: 35.0, tapTargetAction: #selector(navRightButtonOneTapped), tapTarget : self)
        
      
        if (rightBarButtonItem != nil){
            navigationItem.rightBarButtonItems = [rightBarButtonItem!]
        }

        let leftBarButtonItem = getNavBarButton(custom: true, image: UIImage(named: "icn_drawer"), nonCustomIcon: nil, color: SolColors.getColorByName(colorName: "x_header_icons"),size: 35.0, tapTargetAction: #selector(navLeftButtonTapped), tapTarget : self)
        if (leftBarButtonItem != nil){
            navigationItem.leftBarButtonItems = [leftBarButtonItem!]
        }
        

        
        //================================= Processing =============================//
        // Init variables
        networkConnector = MyPayrollConnector(connectorInterface : self)
        modeler = MyPayrollModeler(modelerInterface : self)

        // Start processing data
        networkConnector.getMyPayrollData(tag: "myPayroll")
    }
   
    
  
    func initControllerStates() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(loadData))
        super.initControllerStates(failureView: failureView)
    }
    
    @objc func loadData() {
        networkConnector.getMyPayrollData(tag: "myPayroll")
    }
    
    func getNavBarButton(custom : Bool, image : UIImage?, nonCustomIcon : UIBarButtonItem.SystemItem?, color : UIColor?, size : Double, tapTargetAction : Selector, tapTarget : Any) -> UIBarButtonItem? {
        
        if(custom && image != nil){
            let button = UIButton(type: .custom)
            
            if (color != nil){
                button.setImage(image!.tint(color), for: .normal)
            }else{
                button.setImage(image!, for: .normal)
            }
            button.frame = CGRect(x: 0.0, y: 0.0, width: size, height: size)
            button.addTarget(tapTarget, action: tapTargetAction, for: .touchUpInside)
            return UIBarButtonItem(customView: button)
            
        }else if (nonCustomIcon != nil){
            let button = UIBarButtonItem(barButtonSystemItem: nonCustomIcon!, target: self, action: tapTargetAction)
            if (color != nil){
                button.tintColor = color
            }
            return button
        }
        return nil
    }
    
    @objc
    func navLeftButtonTapped() {
        
        self.showMenu()

    }
    
    @objc
    func navRightButtonOneTapped() {
        self.dataArray.removeAll()
        pageView.tableViewLayout.tableView.reloadData()
        
        networkConnector.resetRequest(tag: "myPayroll")
        networkConnector.getMyPayrollData(tag: "myPayroll")
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //====================== Nav Bar Common Options ========================//
   
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupInitialViewState()

        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        pageView.createView(delegate: self, dataSource: self)
        pageView.tableViewLayout.tableView.register(TableViewCell<MyPayrollPageTableCell>.self, forCellReuseIdentifier: "cellView")
        self.view.addSubview(pageView)

        
        pageView.snp.makeConstraints { (make) -> Void in

            if self.navigationController != nil {
                let navStatusHeight = UIApplication.shared.statusBarFrame.height + self.navigationController!.navigationBar.frame.height
                make.top.equalTo(self.view).offset(navStatusHeight) // Below Navigation Bar                
            }
            make.left.right.bottom.equalTo(self.view)
        }
        
        
    }
    
    func onRequestStarted(tag: String) {
        print("started \(tag)")
        startLoading()

    }
    
    func onRequestSucceeded(tag: String, response: Any) {
       
        if (tag == "myPayroll") {
            networkConnector.setRequestModeling(tag: "myPayroll", isModeling: true)
            modeler.modelMyPayroll(jsonData: response, tag : "myPayroll")
        }else if(tag == "myPayrollDetails"){
            networkConnector.setRequestModeling(tag: "myPayrollDetails", isModeling: true)
            modeler.modelPayrollDetails(jsonData: response, tag : "myPayrollDetails")
        }
    }
    
    func onRequestFailed(tag: String, exception: Error) {
        print("failed \(tag)")
        
        if(tag == "myPayrollDetails"){
            loaderContainer.isHidden = true
            Utils.showMsg(msg: "Error Occurred")
        }else{
            self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
        }
    }
    
    func onPreparingUrl(tag: String, lastLoadedPage: Int) -> String {
        return "\(Constants.getModRewriteBaseUrl())/my_payroll"
    }
    
    func onModelingStarted(tag: String) {
        
    }
    
    func onModelingSucceeded(tag: String, modeledData: Any) {
        networkConnector.increaseRequestOneLoadedPage(tag: tag)
        networkConnector.setRequestModeling(tag: tag, isModeling: false)
        
        if (tag == "myPayroll") {
            // Process vacations
            if (modeledData is [MyPayrollModel]) {
                
                if let dataObj = modeledData as? [MyPayrollModel]{
                    if (networkConnector.getRequestLastLoadedPage(tag: "myPayroll") == 0){
                        self.dataArray.removeAll()
                    }
                    self.dataArray.append(contentsOf: dataObj)

                    if (dataObj.count == 0){
                        networkConnector.setRequestLastPageReached(tag: "myPayroll", isLastPageReached: true)
                    }
                }

            }
            self.endLoading(error: nil) // Set Content
            pageView.tableViewLayout.tableView.reloadData()
            
        }else if(tag == "myPayrollDetails"){
            if (modeledData is MyPayrollDetailsModel ) {
                
                if let dataObj = modeledData as? MyPayrollDetailsModel {
                    networkConnector.setRequestLastPageReached(tag: "myPayrollDetails", isLastPageReached: true)
                    setSalaryDetails(salaryDetails: dataObj)
                }
            }
        }

    }
    
    func onModelingFailed(tag: String, error: String) {
       
        if(tag == "myPayrollDetails"){
            loaderContainer.isHidden = true
            Utils.showMsg(msg: "Error Occurred")
        }else{
            print("modeling failed \(tag)")
            networkConnector.setRequestModeling(tag: tag, isModeling: false)
            networkConnector.setRequestLoading(tag: tag, isLoading: false)
            
            self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:TableViewCell<MyPayrollPageTableCell> = tableView.dequeueReusableCell(withIdentifier: "cellView", for: indexPath) as! TableViewCell<MyPayrollPageTableCell>
        
        let data = self.dataArray[indexPath.row] as! MyPayrollModel
        
        cell.layout.content.text = "\(data.pay_month ?? 0) / \(data.pay_year ?? 0)"
        cell.layout.subContent.text = Language.getTranslationOf(findAndDefaultKey : data.salary_type ?? "NA") ?? "NA"
        
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        
   
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = self.dataArray[indexPath.row] as! MyPayrollModel

        networkConnector.getMyPayrollDetails(url: "\(Constants.getModRewriteBaseUrl())/my_payroll/\(item.id ?? 0)", tag: "myPayrollDetails")
        
        let controller = createPayrollDetails(item: item)
        controller.presentOn(presentingViewController: self, animated: true, onDismiss: {
        })
        
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
    
    
    private func createPayrollDetails(item : MyPayrollModel) -> DialogViewController {

        let controller = DialogViewController()

        
        controller.maxWidth = 420
        controller.minHeight = 200

        controller.title = "\(item.pay_month ?? 0) / \(item.pay_year ?? 0) (\(Language.getTranslationOf(findAndDefaultKey :item.salary_type ?? "NA") ))"
        controller.titleLabel.font = UIFont.systemFont(ofSize: 22, weight: UIFont.Weight.bold)
        controller.closeButton.setTitle(Language.getTranslationOf(findAndDefaultKey :"Done"), for: .normal)

       
      
        netSalaryLabel.text = Language.getTranslationOf(findAndDefaultKey :"Net Salary") + " : NA"
        netSalaryLabel.textAlignment = .left
        netSalaryLabel.isHidden = true
        controller.addArrangedSubview(view: netSalaryLabel)

        overtimeLabel.text = Language.getTranslationOf(findAndDefaultKey :"Overtime") + " : NA"
        overtimeLabel.textAlignment = .left
        overtimeLabel.isHidden = true
        controller.addArrangedSubview(view: overtimeLabel)

        amountLabel.text = Language.getTranslationOf(findAndDefaultKey :"Amount") + " : NA"
        amountLabel.textAlignment = .left
        amountLabel.isHidden = true
        controller.addArrangedSubview(view: amountLabel)
        
        methodLabel.text = Language.getTranslationOf(findAndDefaultKey :"Method") + " : NA"
        methodLabel.textAlignment = .left
        methodLabel.isHidden = true
        controller.addArrangedSubview(view: methodLabel)
        
        commentsLabel.text = Language.getTranslationOf(findAndDefaultKey :"Comments") + " : NA"
        commentsLabel.textAlignment = .left
        commentsLabel.isHidden = true
        controller.addArrangedSubview(view: commentsLabel)
        
        loaderContainer.layer.masksToBounds = true
        loaderContainer.isHidden = false
        controller.addArrangedSubview(view: loaderContainer)

        let loader = NVActivityIndicatorView(frame: CGRect.init(x: 0, y: 0, width: 25, height: 25), type: .lineSpinFadeLoader, color: UIColor.black)
        loader.startAnimating()
        loaderContainer.addSubview(loader)

        loader.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
        loaderContainer.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(40)
        }

       
        
        return controller
    }

    func setSalaryDetails(salaryDetails : MyPayrollDetailsModel) {

        loaderContainer.isHidden = true
        
        netSalaryLabel.isHidden = false
        netSalaryLabel.text = Language.getTranslationOf(findAndDefaultKey :"Net Salary") + " : \(salaryDetails.currency_symbol ?? "NA")\(salaryDetails.salary_value ?? "")"

        overtimeLabel.isHidden = false
        overtimeLabel.text = Language.getTranslationOf(findAndDefaultKey :"Overtime") + " : ( \(salaryDetails.hour_count ?? "NA") ) \(salaryDetails.currency_symbol ?? "NA")\(salaryDetails.hour_overtime ?? "NA")"
        
        amountLabel.isHidden = false
        amountLabel.text = Language.getTranslationOf(findAndDefaultKey :"Amount") + " : \(salaryDetails.currency_symbol ?? "NA")\(salaryDetails.pay_amount ?? "")"
        
        methodLabel.isHidden = false
        methodLabel.text = Language.getTranslationOf(findAndDefaultKey :"Method") + " : \(salaryDetails.pay_method ?? "")"
        
        commentsLabel.isHidden = false
        if(salaryDetails.pay_comments != ""){
          commentsLabel.text = Language.getTranslationOf(findAndDefaultKey :"Comments") + " : \(salaryDetails.pay_comments ?? "NA")"
        }

    }
}

