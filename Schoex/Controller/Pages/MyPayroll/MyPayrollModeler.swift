//
//  ForumsModeler.swift
//  Eduopus
//
//  Created by Mohamed Selim Refaat on 5/28/19.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation
import SwiftyJSON

class MyPayrollModeler {
    
    var modelerInterface : SBModelerInterface!
    
    //================================  Constructor ================================//
    init(modelerInterface : SBModelerInterface) {
        self.modelerInterface = modelerInterface
    }
    

    // Get Data
    func modelMyPayroll(jsonData : Any, tag : String)  {
        
        //Notify interface about starting modeling
        if (modelerInterface != nil){
            self.modelerInterface.onModelingStarted(tag: tag)
        }
        
        //Convert json data to object
        let dataJson = SBParser.convertToJson(data: jsonData)
        
        var dataList = [MyPayrollModel]()

        if (dataJson.null == nil){

            let payrollData = SBParser.getJsonArray(attrHolder: dataJson)
            
            for payroll in payrollData {
                
                let payrollDic = SBParser.getJsonDictionary(attrHolder: payroll)
                
                if(payrollDic != nil){
                    
                    let payrollItem = MyPayrollModel(
                        id: SBParser.getInt(attrHolder: payrollDic, attrName: "id"),
                        pay_month : SBParser.getInt(attrHolder: payrollDic, attrName: "pay_month"),
                        pay_year : SBParser.getInt(attrHolder: payrollDic, attrName: "pay_year"),
                        salary_type : SBParser.getString(attrHolder: payrollDic, attrName: "salary_type")
                    )
                    
                    dataList.append(payrollItem)
                    
                }
            }
            
            if (modelerInterface != nil){
                modelerInterface.onModelingSucceeded(tag: tag, modeledData: dataList)
            }
            
        }else{
            if (modelerInterface != nil){
                modelerInterface.onModelingFailed(tag: tag, error: "")
            }
        }
    }
    
    func modelPayrollDetails(jsonData : Any, tag : String)  {
        
        //Notify interface about starting modeling
        if (modelerInterface != nil){
            self.modelerInterface.onModelingStarted(tag: tag)
        }
        
        //Convert json data to object
        let dataJson = SBParser.convertToJson(data: jsonData)


        if (dataJson.null == nil){
            let payrollData = SBParser.getJsonDictionary(attrHolder: dataJson)
            let payrollDetailsData = SBParser.getJsonDictionary(attrHolder: dataJson, attrName: "details")
            
            let dataObject = MyPayrollDetailsModel(
                currency_symbol: SBParser.getString(attrHolder: payrollData, attrName: "currency_symbol"),
                id: SBParser.getInt(attrHolder: payrollDetailsData, attrName: "id"),
                hour_count: SBParser.getString(attrHolder: payrollDetailsData, attrName: "hour_count"),
                hour_overtime: SBParser.getString(attrHolder: payrollDetailsData, attrName: "hour_overtime"),
                pay_amount: SBParser.getString(attrHolder: payrollDetailsData, attrName: "pay_amount"),
                pay_by_userid: SBParser.getInt(attrHolder: payrollDetailsData, attrName: "pay_by_userid"),
                pay_comments: SBParser.getString(attrHolder: payrollDetailsData, attrName: "pay_comments"),
                pay_method: SBParser.getString(attrHolder: payrollDetailsData, attrName: "pay_method"),
                pay_month: SBParser.getString(attrHolder: payrollDetailsData, attrName: "pay_month"),
                pay_year: SBParser.getString(attrHolder: payrollDetailsData, attrName: "pay_year"),
                salary_type: SBParser.getString(attrHolder: payrollDetailsData, attrName: "salary_type"),
                salary_value: SBParser.getString(attrHolder: payrollDetailsData, attrName: "salary_value")
            )
            
            if (modelerInterface != nil){
                modelerInterface.onModelingSucceeded(tag: tag, modeledData: dataObject)
            }
            
        }else{
            if (modelerInterface != nil){
                modelerInterface.onModelingFailed(tag: tag, error: "")
            }
        }
    }
}
