
import Foundation
import UIKit
import SnapKit
import NVActivityIndicatorView

class MyPayrollPageView: UIView {
    
    //============================================ Variables ======================================//

    // Handlers
    var shouldSetupConstraints = true
    
    // Views

    var tableViewLayout = TableViewLayout(frame: CGRect.zero)

   
    //============================================ Creator ======================================//
    
    func createView(delegate : UITableViewDelegate, dataSource : UITableViewDataSource) {
        
        makeTableView(createView : true, updateCons : false, _delegate : delegate, _dataSource : dataSource)
        
    }
    
    override func updateConstraints() {
        if(shouldSetupConstraints) {
            // AutoLayout constraints
            
            makeTableView(createView : false, updateCons : true, _delegate : nil, _dataSource : nil)
            
            shouldSetupConstraints = false
        }
        super.updateConstraints()
    }
    
    //============================================ Creator helpers ======================================//


    func makeTableView(createView : Bool, updateCons : Bool, _delegate : UITableViewDelegate?, _dataSource : UITableViewDataSource?) {
        if createView {
            tableViewLayout.createTable(delegate: _delegate!,dataSource: _dataSource!)
            tableViewLayout.tableView.register(TableViewCell<MyPayrollPageTableCell>.self,forCellReuseIdentifier: "cellView")
            self.addSubview(tableViewLayout)
        }
        
        if updateCons {
            tableViewLayout.snp.makeConstraints { (make) -> Void in
                make.leading.trailing.top.bottom.equalToSuperview()
                //make.bottom.equalTo(viewsStackConatiner.snp.bottom)
            }
        }
    }
    
  
    //============================================ Initialization ======================================//
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
