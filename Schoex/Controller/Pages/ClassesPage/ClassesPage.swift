//
//  ViewController.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/15/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework
import SwiftyJSON


class ClassesPage:SolViewController,UITableViewDataSource,UITableViewDelegate,RenewTokenCallback {
    
    @IBOutlet weak var dataTableView: UITableView!
    @IBOutlet weak var navLeftButton: UIBarButtonItem!

    var searchMode:Bool = false
    
    var page : Int = 1
    var loadMoreLocked : Bool = false
    
    //============================ Base Functions =================//
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initTableView(tableView: dataTableView)
        initNavBar()
        initSideMenu()
        initControllerStates()
        
        initPage()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupInitialViewState()
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //============================ Init Functions =================//

    func initPage() {
        if searchAbout != nil {
            searchMode = true
            self.navLeftButton.image = Utils.getBackIcon()
        }else{
            searchMode = false
            self.navLeftButton.image = UIImage(named: "icn_drawer")
        }
        self.loadData(isLoadMore: false)
        
        // nav bar title
        self.navigationItem.title = Utils.siteTitle
    }
    
    func initControllerStates() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(failLoadData))
        super.initControllerStates(failureView: failureView)
    }
    
    @objc func failLoadData() {
        loadData(isLoadMore : false)
    }
    
    //==================== Load data ===================================//
    @objc func loadData(isLoadMore : Bool) {
        if(!isLoadMore){
            self.page = 1
            self.loadMoreLocked = false
            self.dataArray.removeAll()
            self.dataTableView.reloadData()
            startLoading()
        }else{
            if(loadMoreLocked){return}
        }
        
        let url = "\(Constants.getModRewriteBaseUrl())/classes/listAll/\(self.page)"
        
        let builder : DataRequest
        if(searchMode && searchAbout != nil){
            let searchParameters:Parameters = [
                "searchInput": searchAbout!
            ]
            builder = SBFramework.request(url, method: .post,parameters: searchParameters)
        }else{
            builder = SBFramework.request(url, method: .get)
        }
        
        builder.responseJSON { response in
            switch response.result {
            case .success(let value):
                
                //======== Parse result ==========//
                let json = JSON(value)
                if (json["error"].null == nil){
                    if(!self.tokenRenewTried){ Utils.renewToken(callback : self,navigationController : self.navigationController!) }
                }
                if let jsonArray = json["classes"].array {
                    
                    //================= Fill data array ==================//
                    for item in jsonArray {
                        self.dataArray.append(ClassesModel(
                            id : Utils.getIntValueOf(sourceInput: item,itemKey:"id") ,
                            className : Utils.getStringValueOf(sourceInput: item,itemKey:"className") ,
                            classTeacher : Utils.getStringValueOf(sourceInput: item,itemKey:"classTeacher") ,
                            dormitoryName : Utils.getStringValueOf(sourceInput: item,itemKey:"dormitoryName")
                            )
                        )
                    }
                    
                    if(isLoadMore){
                        if(jsonArray.count == 0){
                            self.loadMoreLocked = true
                        }
                    }
                    self.endLoading(error: nil) // Set Content
                    self.dataTableView.reloadData()
                    self.page = self.page + 1
                    

                }else{
                    self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
                }
            case .failure(let error):
                Utils.checkFailureCause(error : error, callback: self, navigationController: self.navigationController!,tokenRenewTried:self.tokenRenewTried)
                if self.tokenRenewTried {self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) }
            }
        }
    }
    var tokenRenewTried = false
    func afterRenewToken(success: Bool) {
        tokenRenewTried = true
        if success {
            loadData(isLoadMore: false)
        }else{
            self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
        }
    }
    
    //==================== Nav Bar Buttons Actions ===================================//

    @IBAction func reloadPage(_ sender: Any) {
        self.dataArray.removeAll()
        loadData(isLoadMore: false)
    }
    
    @IBAction func searchButtonAction(_ sender: Any) {
        showSearchDialog(targetStoryboard: "ClassesPageStoryboard")
    }
    
   
    @IBAction func navLeftButtonAction(_ sender: Any) {
        if(self.searchMode){
            self.navigationController?.popViewController(animated: true)
        }else{
            showMenu()
        }
    }
    

    //==================== Table view options ===================================//
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = dataArray.count - 1
        if indexPath.row == lastElement {
            self.loadData(isLoadMore: true)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:ClassesCell = tableView.dequeueReusableCell(withIdentifier: "cellCon", for: indexPath) as! ClassesCell
        cell.classItem = dataArray[indexPath.row] as! ClassesModel
        return cell
    }
    
}




