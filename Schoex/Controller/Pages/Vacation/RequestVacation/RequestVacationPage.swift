//
//  ViewController.swift
//  EduopusKit
//
//  Created by Mohamed Selim Refaat on 3/3/19.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework
import Kingfisher
import Eureka
import TransitionButton


class RequestVacationPage:FormViewController,SBConnectorInterface,SBModelerInterface,DatePickerProtocol {
    
    
    
    var networkConnector : RequestVacationConnector!
    var modeler : RequestVacationModeler!
    var NilSelect = "No Select"
    var transitionButton:TransitionButton?

    var selectedFormFromDate:String?
    var selectedFormToDate:String?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //================================= Setup Controller =============================//
        title = Language.getTranslationOf(findKey: "reqVacation",defaultWord: "Request vacation")
        
        // Set RTL
        if Utils.App_IsRtl {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }else{
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }
        
        // Set Background image or color
        if Bools.x_gen_back_is_image {
            let backgroundImage = CustomImageView(frame: UIScreen.main.bounds)
            backgroundImage.image = UIImage(named: "x_gen_back")
            backgroundImage.contentMode = UIView.ContentMode.scaleAspectFill
            self.view.insertSubview(backgroundImage, at: 0)
        }else{
            view.backgroundColor = SolColors.getColorByName(colorName: "x_gen_back")
        }
        
        // Set Status bar background
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        statusBarView.backgroundColor = SolColors.getColorByName(colorName: "x_header_back")
        view.addSubview(statusBarView)
        
        // Set Navigation bar background
        navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController!.navigationBar.shadowImage = UIImage()
        UINavigationBar.appearance().backgroundColor = SolColors.getColorByName(colorName: "x_header_back")

        // Set Navigation title color
        navigationController!.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : SolColors.getColorByName(colorName: "x_header_text")!]
        
        // Set Navigation bar icons and colors
       
        let leftBarButtonItem = getNavBarButton(custom: true, image: Utils.getBackIcon(), nonCustomIcon: nil, color: SolColors.getColorByName(colorName: "x_header_icons"),size: 35.0, tapTargetAction: #selector(navLeftButtonTapped), tapTarget : self)
        if (leftBarButtonItem != nil){
            navigationItem.leftBarButtonItems = [leftBarButtonItem!]
        }
        
        //================================= Processing =============================//
        // Init variables
        networkConnector = RequestVacationConnector(connectorInterface : self)
        modeler = RequestVacationModeler(modelerInterface : self)

        createForm()
    }
   
    func initNavBar (){
        showNavigationBar()
        navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController!.navigationBar.shadowImage = UIImage()
        
        // navigation title color
        navigationController!.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : SolColors.getColorByName(colorName: "x_header_text")!]
        
        // navigation bar
        UINavigationBar.appearance().backgroundColor = SolColors.getColorByName(colorName: "x_header_back")
        
        // status bar
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        statusBarView.backgroundColor = SolColors.getColorByName(colorName: "x_header_back")
        view.addSubview(statusBarView)
        

    }
    
    func createForm() {
        form
            +++ Section()
            <<< LabelRow ("from_date") {
                    $0.title = Language.getTranslationOf(findKey: "from",defaultWord: "From")
                    $0.value = NilSelect
                }.onCellSelection { cell, row in
                    self.openDatePicker(tag: "from_date")
                }.cellSetup { cell, row in
                    cell.backgroundColor = .clear
                    cell.textLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
                    cell.detailTextLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
                }.cellUpdate{ cell, row in
                    cell.textLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
                }
            <<< LabelRow ("to_date") {
                $0.title = Language.getTranslationOf(findKey: "to",defaultWord: "To")
                $0.value = NilSelect
            }.onCellSelection { cell, row in
                self.openDatePicker(tag: "to_date")
            }.cellSetup { cell, row in
                cell.backgroundColor = .clear
                cell.textLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
                cell.detailTextLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
            }.cellUpdate{ cell, row in
                cell.textLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
            }
            +++ Section() {
                var header = HeaderFooterView<CustomButton>(.nibFile(name: "CustomButton", bundle: nil))
                header.onSetupView = { (view, section) -> () in
                    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.proccessBtnPressed))
                    view.transitionBtn.addGestureRecognizer(tapGesture)
                    self.transitionButton = view.transitionBtn
                }
                $0.header = header
            }
        self.tableView?.separatorStyle = .none
        self.tableView?.backgroundColor = UIColor.clear
    }
    
    
    @objc func proccessBtnPressed()  {
        if(selectedFormFromDate != nil && selectedFormToDate != nil){
            self.transitionButton!.startAnimation()
            let qualityOfServiceClass = DispatchQoS.QoSClass.background
            let backgroundQueue = DispatchQueue.global(qos: qualityOfServiceClass)
            backgroundQueue.async(execute: {
                self.networkConnector.getRequestVacationData(tag: "vacationRequest",selectedFormFromDate: self.selectedFormFromDate!,selectedFormToDate: self.selectedFormToDate!)
            })
        }
    }
    
    
    @objc func openDatePicker(tag : String) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let nextVC = storyboard.instantiateViewController(withIdentifier: ("DatePickerPageStoryboard")) as! DatePickerPage
        nextVC.datePickedDelegate = self
        nextVC.tag = tag
        self.navigationController!.pushViewController(nextVC, animated: true)
    }
    
    
    func getNavBarButton(custom : Bool, image : UIImage?, nonCustomIcon : UIBarButtonItem.SystemItem?, color : UIColor?, size : Double, tapTargetAction : Selector, tapTarget : Any) -> UIBarButtonItem? {
        
        if(custom && image != nil){
            let button = UIButton(type: .custom)
            
            if (color != nil){
                button.setImage(image!.tint(color), for: .normal)
            }else{
                button.setImage(image!, for: .normal)
            }
            button.frame = CGRect(x: 0.0, y: 0.0, width: size, height: size)
            button.addTarget(tapTarget, action: tapTargetAction, for: .touchUpInside)
            return UIBarButtonItem(customView: button)
            
        }else if (nonCustomIcon != nil){
            let button = UIBarButtonItem(barButtonSystemItem: nonCustomIcon!, target: self, action: tapTargetAction)
            if (color != nil){
                button.tintColor = color
            }
            return button
        }
        return nil
    }
    
    @objc
    func navLeftButtonTapped() {
        
        self.navigationController?.popViewController(animated: true)

    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //====================== Nav Bar Common Options ========================//
   
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func onRequestStarted(tag: String) {
        print("started \(tag)")

    }
    
    func onRequestSucceeded(tag: String, response: Any) {
       
        if (tag == "vacationRequest") {
            networkConnector.setRequestModeling(tag: "vacationRequest", isModeling: true)
            modeler.modelRequestVacation(jsonData: response, tag : "vacationRequest")
        }
    }
    
    func onRequestFailed(tag: String, exception: Error) {
        print("failed \(tag)")
        
        DispatchQueue.main.async(execute: { () -> Void in
            self.transitionButton!.stopAnimation(animationStyle: .shake, completion: {})
        })
        Utils.showMsg(msg: exception.localizedDescription)
    }
    
    func onPreparingUrl(tag: String, lastLoadedPage: Int) -> String {
        if(tag == "vacationRequest"){
            return "\(Constants.getModRewriteBaseUrl())/vacation"
        }
        return ""
    }
    
    func onModelingStarted(tag: String) {
        
    }
    
    func onModelingSucceeded(tag: String, modeledData: Any) {
        networkConnector.setRequestModeling(tag: tag, isModeling: false)
        networkConnector.setRequestLoading(tag: tag, isLoading: false)
        
        if (tag == "vacationRequest") {
            // Process vacations
            if (modeledData is [VacationRequestModel]) {
                if let dataObj = modeledData as? [VacationRequestModel]{

                    self.transitionButton!.stopAnimation(animationStyle: .expand, completion: {
                        let nextVC = RequestVacationApprovePage()
                        nextVC.dataArray = dataObj
                        self.navigationController!.pushViewController(nextVC, animated: true)
                    })
                    
                }else{
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.transitionButton!.stopAnimation(animationStyle: .shake, completion: {})
                    })
                }
            }else{
                DispatchQueue.main.async(execute: { () -> Void in
                    self.transitionButton!.stopAnimation(animationStyle: .shake, completion: {})
                })
            }
        }

    }
    
    func onModelingFailed(tag: String, error: String) {
        print("modeling failed \(tag)")
        networkConnector.setRequestModeling(tag: tag, isModeling: false)
        networkConnector.setRequestLoading(tag: tag, isLoading: false)
        
        DispatchQueue.main.async(execute: { () -> Void in
            self.transitionButton!.stopAnimation(animationStyle: .shake, completion: {})
        })
        Utils.showMsg(msg: error)
        
    }

    func getLabelItem(tag:String) -> LabelRow {
        return (form.rowBy(tag: tag) as! LabelRow)
    }
    
    //============================ Protocols callbacks =================//

    func onDatePicked(tag : String, dateSelected: String) {
        if(tag == "from_date"){
            self.getLabelItem(tag: "from_date").value = dateSelected
            self.selectedFormFromDate = dateSelected
        }else if(tag == "to_date"){
            self.getLabelItem(tag: "to_date").value = dateSelected
            self.selectedFormToDate = dateSelected
        }
    }
}

