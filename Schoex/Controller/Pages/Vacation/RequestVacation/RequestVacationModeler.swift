//
//  ForumsModeler.swift
//  Eduopus
//
//  Created by Mohamed Selim Refaat on 5/28/19.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation
import SwiftyJSON

class RequestVacationModeler {
    
    var modelerInterface : SBModelerInterface!
    
    //================================  Constructor ================================//
    init(modelerInterface : SBModelerInterface) {
        self.modelerInterface = modelerInterface
    }
    

    // Get CategoriesList
    func modelRequestVacation(jsonData : Any, tag : String)  {
        
        //Notify interface about starting modeling
        if (modelerInterface != nil){
            self.modelerInterface.onModelingStarted(tag: tag)
        }
        
        print(jsonData)
        //Convert json data to object
        let dataJson = SBParser.convertToJson(data: jsonData)
        
        var dataList = [VacationRequestModel]()

        if (dataJson.null == nil){
            let VacationsResponse = SBParser.getJsonDictionary(attrHolder: dataJson)
            
            let status = SBParser.getString(attrHolder: VacationsResponse, attrName: "status")
        
            if(status == "success"){
                let VacationsDays = SBParser.getJsonArray(attrHolder: VacationsResponse, attrName: "data")

                for vacation in VacationsDays {
                               
                               let VacationDic = SBParser.getJsonDictionary(attrHolder: vacation)
                               

                               if(VacationDic != nil){
                                   
                                   let vacationItem = VacationRequestModel(
                                       date: SBParser.getString(attrHolder: VacationDic, attrName: "date"),
                                       dow : SBParser.getString(attrHolder: VacationDic, attrName: "dow"),
                                       status : SBParser.getString(attrHolder: VacationDic, attrName: "status"),
                                       timestamp : SBParser.getString(attrHolder: VacationDic, attrName: "timestamp")
                                   )
                                   
                                   dataList.append(vacationItem)
                                  
                               }
                }
                           
                if (modelerInterface != nil){
                    modelerInterface.onModelingSucceeded(tag: tag, modeledData: dataList)
                }
            }else{
                if (modelerInterface != nil){
                    modelerInterface.onModelingFailed(tag: tag,error : SBParser.getString(attrHolder: VacationsResponse, attrName: "message"))
                }
            }
        }else{
            if (modelerInterface != nil){
                modelerInterface.onModelingFailed(tag: tag,error: "")
            }
        }
    }
    
}
