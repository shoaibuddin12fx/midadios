//
//  ViewController.swift
//  EduopusKit
//
//  Created by Mohamed Selim Refaat on 3/3/19.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework
import Kingfisher
import SwiftyJSON

class RequestVacationApprovePage:SolViewController,UITableViewDelegate,UITableViewDataSource,SBConnectorInterface,SBModelerInterface {
    
    
    
    let pageView = RequestVacationApprovePageView(frame: CGRect.zero)
    var networkConnector : RequestVacationConnector!
    var modeler : RequestVacationModeler!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //================================= Setup Controller =============================//
        title = Language.getTranslationOf(findKey: "vacation",defaultWord: "Vacation")
        
        // Set RTL
        if Utils.App_IsRtl {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }else{
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }
        
        initControllerStates()
        
        // Set Background image or color
        if Bools.x_gen_back_is_image {
            let backgroundImage = CustomImageView(frame: UIScreen.main.bounds)
            backgroundImage.image = UIImage(named: "x_gen_back")
            backgroundImage.contentMode = UIView.ContentMode.scaleAspectFill
            self.view.insertSubview(backgroundImage, at: 0)
        }else{
            view.backgroundColor = SolColors.getColorByName(colorName: "x_gen_back")
        }
        
        // Set Status bar background
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        statusBarView.backgroundColor = SolColors.getColorByName(colorName: "x_header_back")
        view.addSubview(statusBarView)
        
        // Set Navigation bar background
        navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController!.navigationBar.shadowImage = UIImage()
        UINavigationBar.appearance().backgroundColor = SolColors.getColorByName(colorName: "x_header_back")

        // Set Navigation title color
        navigationController!.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : SolColors.getColorByName(colorName: "x_header_text")!]
        
      
        

        let leftBarButtonItem = getNavBarButton(custom: true, image: Utils.getBackIcon(), nonCustomIcon: nil, color: SolColors.getColorByName(colorName: "x_header_icons"),size: 35.0, tapTargetAction: #selector(navLeftButtonTapped), tapTarget : self)
        if (leftBarButtonItem != nil){
            navigationItem.leftBarButtonItems = [leftBarButtonItem!]
        }
    
        // Init variables
        networkConnector = RequestVacationConnector(connectorInterface : self)
        modeler = RequestVacationModeler(modelerInterface : self)

    }
   
    
  
    func initControllerStates() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(goBack))
        super.initControllerStates(failureView: failureView)
    }
    
    @objc func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getNavBarButton(custom : Bool, image : UIImage?, nonCustomIcon : UIBarButtonItem.SystemItem?, color : UIColor?, size : Double, tapTargetAction : Selector, tapTarget : Any) -> UIBarButtonItem? {
        
        if(custom && image != nil){
            let button = UIButton(type: .custom)
            
            if (color != nil){
                button.setImage(image!.tint(color), for: .normal)
            }else{
                button.setImage(image!, for: .normal)
            }
            button.frame = CGRect(x: 0.0, y: 0.0, width: size, height: size)
            button.addTarget(tapTarget, action: tapTargetAction, for: .touchUpInside)
            return UIBarButtonItem(customView: button)
            
        }else if (nonCustomIcon != nil){
            let button = UIBarButtonItem(barButtonSystemItem: nonCustomIcon!, target: self, action: tapTargetAction)
            if (color != nil){
                button.tintColor = color
            }
            return button
        }
        return nil
    }
    
    @objc
    func navLeftButtonTapped() {
        
        self.navigationController?.popViewController(animated: true)

    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //====================== Nav Bar Common Options ========================//
   
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupInitialViewState()

        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        pageView.createView(delegate: self, dataSource: self)
        pageView.tableViewLayout.tableView.register(TableViewCell<RequestVacationApprovePageTableCell>.self, forCellReuseIdentifier: "cellView")
        self.view.addSubview(pageView)

        
        pageView.snp.makeConstraints { (make) -> Void in

            if self.navigationController != nil {
                let navStatusHeight = UIApplication.shared.statusBarFrame.height + self.navigationController!.navigationBar.frame.height
                make.top.equalTo(self.view).offset(navStatusHeight) // Below Navigation Bar                
            }
            make.left.right.bottom.equalTo(self.view)
        }
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.approveVacation (_:)))
        pageView.approveVacationButton.addGestureRecognizer(gesture)
    }
    
    @objc
    func approveVacation(_ sender: UITapGestureRecognizer) {
        self.networkConnector.confirmVacation(tag: "vacationConfirm",daysArray: self.dataArray as! [VacationRequestModel])
    }
    
    func onRequestStarted(tag: String) {
        print("started \(tag)")
        Utils.showMsg(msg: Language.getTranslationOf(findAndDefaultKey:"Please wait"))

    }
    
    func onRequestSucceeded(tag: String, response: Any) {
       
        if (tag == "vacationConfirm") {
            networkConnector.setRequestModeling(tag: "vacationConfirm", isModeling: true)
            Utils.parseUpdateResponseByMessage(jsonResponse: JSON(response))
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func onRequestFailed(tag: String, exception: Error) {
        print("failed \(tag)")
        Utils.showMsg(msg: "Error Occurred")

    }
    
    func onPreparingUrl(tag: String, lastLoadedPage: Int) -> String {
        return "\(Constants.getModRewriteBaseUrl())/vacation/confirm"
    }
    
    func onModelingStarted(tag: String) {
        
    }
    
    func onModelingSucceeded(tag: String, modeledData: Any) {
        networkConnector.increaseRequestOneLoadedPage(tag: tag)
        networkConnector.setRequestModeling(tag: tag, isModeling: false)
        

    }
    
    func onModelingFailed(tag: String, error: String) {
        networkConnector.setRequestModeling(tag: tag, isModeling: false)
        networkConnector.setRequestLoading(tag: tag, isLoading: false)
        
        Utils.showMsg(msg: "Error Occurred")

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:TableViewCell<RequestVacationApprovePageTableCell> = tableView.dequeueReusableCell(withIdentifier: "cellView", for: indexPath) as! TableViewCell<RequestVacationApprovePageTableCell>
        
        let data = self.dataArray[indexPath.row] as! VacationRequestModel
        cell.layout.content.text = data.date
        
        
        if(data.status != nil && data.status != ""){
            cell.layout.subContent.isHidden = false
            
            if(data.status == "offv"){
                cell.layout.subContent.text = "Official Vacation"
            }else if(data.status == "dow"){
                cell.layout.subContent.text = "Week Day off"
            }
        }else {
            cell.layout.subContent.isHidden = true
        }
        
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        
   
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
}

