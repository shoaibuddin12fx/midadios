
import Foundation
import UIKit
import SnapKit
import NVActivityIndicatorView

class RequestVacationApprovePageView: UIView {
    
    //============================================ Variables ======================================//
    var parentContainerView: UIStackView!
    var approveVacationButton: UIView!
    var approveVacationButtonText: UILabel!

    // Handlers
    var shouldSetupConstraints = true
    
    // Views

    var tableViewLayout = TableViewLayout(frame: CGRect.zero)

   
    //============================================ Creator ======================================//
    
    func createView(delegate : UITableViewDelegate, dataSource : UITableViewDataSource) {
        
        makeTableView(createView : true, updateCons : false, _delegate : delegate, _dataSource : dataSource)
        
    }
    
    override func updateConstraints() {
        if(shouldSetupConstraints) {
            // AutoLayout constraints
            
            makeTableView(createView : false, updateCons : true, _delegate : nil, _dataSource : nil)
            
            shouldSetupConstraints = false
        }
        super.updateConstraints()
    }
    
    //============================================ Creator helpers ======================================//


    func makeTableView(createView : Bool, updateCons : Bool, _delegate : UITableViewDelegate?, _dataSource : UITableViewDataSource?) {
        if createView {
            // init parentContainerView
            parentContainerView = UIStackView(frame: CGRect.zero)
            parentContainerView.translatesAutoresizingMaskIntoConstraints = false
            parentContainerView.axis = .vertical
                            
           
            tableViewLayout.createTable(delegate: _delegate!,dataSource: _dataSource!)
            tableViewLayout.tableView.register(TableViewCell<RequestVacationApprovePageTableCell>.self,forCellReuseIdentifier: "cellView")
            parentContainerView.addArrangedSubview(tableViewLayout)
            
            approveVacationButton = UIView(frame: CGRect.zero)
            approveVacationButton.backgroundColor = UIColor.black
            approveVacationButton.layer.masksToBounds = true
            parentContainerView.addArrangedSubview(approveVacationButton)
            
            // init content
            approveVacationButtonText = UILabel(frame: CGRect.zero)
            approveVacationButtonText.textColor = UIColor.white
            approveVacationButtonText.translatedText = Language.getTranslationOf(findKey: "reqVacation",defaultWord: "Request vacation")
            approveVacationButtonText.font = UIFont(name: Fonts.FontRobotoCondensed_Bold, size: 18)
            approveVacationButtonText.numberOfLines = 0
            approveVacationButton.addSubview(approveVacationButtonText)
            
                  
            self.addSubview(parentContainerView)
                 
        }
        
        if updateCons {
            parentContainerView.snp.makeConstraints { (make) in
                make.leading.top.equalToSuperview()
                make.trailing.equalToSuperview()
                make.bottom.equalToSuperview()
            }
            approveVacationButton.snp.makeConstraints { (make) in
                make.height.equalTo(80)
                make.leading.equalToSuperview()
                make.trailing.equalToSuperview()
                make.bottom.equalToSuperview()
            }
            approveVacationButtonText.snp.makeConstraints { (make) in
                make.centerX.centerY.equalToSuperview()
            }
        }
    }
    
  
    //============================================ Initialization ======================================//
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
}
