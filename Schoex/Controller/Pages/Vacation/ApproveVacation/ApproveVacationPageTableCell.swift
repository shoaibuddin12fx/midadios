
import Foundation
import UIKit
import SnapKit
import SBFramework

/**
 USE UIVIEW AS CELL AND REVERSE
 
 SOURCE : https://medium.com/anysuggestion/how-to-share-layout-code-between-uiview-and-uitableviewcell-subclasses-929441a9752a
 
 --> AS CELL
 itemViewType4.tableView.register(TableViewCell<ItemViewType5>.self, forCellReuseIdentifier: "view5")
 let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "view5", for: indexPath)

 --> AS UIVIEW
 let view5 = ViewConverter<ItemViewType5>(frame: .zero)

 **/
class ApproveVacationPageTableCell: ViewLayout {
    
    var cardView: UIView!
    
    var parentContainerView: UIStackView!
    var headerContainerView: UIStackView!
    var headerMainView: UIStackView!
    var footerContainerView: UIStackView!
    var actionButtonsContainerView: UIStackView!

    var user_img: UIImageView!
    var title: UILabel!
    var subtitle: UILabel!
    var content: UILabel!
    var subContent: UILabel!
    
    var approveButton:ActionButton!
    var rejectButton:ActionButton!
    
    var cornerRadius: CGFloat = 5
    
    var shadowOffsetWidth: Int = 0
    var shadowOffsetHeight: Int = 3
    var shadowColor: UIColor? = UIColor.black
    var shadowOpacity: Float = 0.5
    
    required init() {}
    
    func layout(on containerView: UIView) {
        
        // init cardView
        cardView = UIView(frame: CGRect.zero)
        cardView.backgroundColor = SolColors.getColorByName(colorName: "white")
        cardView.layer.masksToBounds = true
        cardView.layer.cornerRadius = cornerRadius
        cardView.layer.shadowColor = shadowColor?.cgColor
        cardView.layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        cardView.layer.shadowOpacity = shadowOpacity
        
        containerView.addSubview(cardView)
        
        // init parentContainerView
        parentContainerView = UIStackView(frame: CGRect.zero)
        parentContainerView.translatesAutoresizingMaskIntoConstraints = false
        parentContainerView.axis = .vertical
        
        cardView.addSubview(parentContainerView)
        
        // init headerContainerView
        headerContainerView = UIStackView(frame: CGRect.zero)
        headerContainerView.backgroundColor = SolColors.getColorByName(colorName: "white")
        headerContainerView.translatesAutoresizingMaskIntoConstraints = false
        headerContainerView.axis = .horizontal
        headerContainerView.distribution = .fill
        headerContainerView.alignment = .leading // Very important
        parentContainerView.addArrangedSubview(headerContainerView)
        
        // init user image
        user_img = UIImageView(frame: CGRect.zero)
        user_img.layer.masksToBounds = true
        user_img.layer.cornerRadius = 30
        headerContainerView.addArrangedSubview(user_img)
        
        // init headerMainView
        headerMainView = UIStackView(frame: CGRect.zero)
        headerMainView.backgroundColor = SolColors.getColorByName(colorName: "white")
        headerMainView.translatesAutoresizingMaskIntoConstraints = false
        headerMainView.axis = .vertical
        headerMainView.alignment = .leading
        headerMainView.isLayoutMarginsRelativeArrangement = true
        headerMainView.distribution = .fill
        
        headerContainerView.addArrangedSubview(headerMainView)
        
        // init title
        title = UILabel(frame: CGRect.zero)
        title.numberOfLines = 3
        title.textColor = SolColors.getColorByName(colorName: "x_boxes_main_text")
        title.font = UIFont(name: Fonts.FontRobotoCondensed_Bold, size: 19)
        
        headerMainView.addArrangedSubview(title)
        
        
        // init subtitle
        subtitle = UILabel(frame: CGRect.zero)
        subtitle.numberOfLines = 1
        subtitle.textColor = SolColors.getColorByName(colorName: "x_boxes_sub_title_text")
        subtitle.font = UIFont(name: Fonts.FontRobotoCondensed_Regular, size: 15)
        headerMainView.addArrangedSubview(subtitle)
        
        // init footerContainerView
        footerContainerView = UIStackView(frame: CGRect.zero)
        footerContainerView.translatesAutoresizingMaskIntoConstraints = false
        footerContainerView.axis = .vertical
        parentContainerView.addArrangedSubview(footerContainerView)
        
        // init content
        content = UILabel(frame: CGRect.zero)
        content.textColor = SolColors.getColorByName(colorName: "x_boxes_sub_title_text")
        content.font = UIFont(name: Fonts.FontRobotoCondensed_Bold, size: 14)
        content.numberOfLines = 1
        footerContainerView.addArrangedSubview(content)
        
        // init subContent
        subContent = UILabel(frame: CGRect.zero)
        subContent.textColor = SolColors.getColorByName(colorName: "x_boxes_main_text")
        subContent.font = UIFont(name: Fonts.FontRobotoCondensed_Regular, size: 16)
        subContent.numberOfLines = 1
        footerContainerView.addArrangedSubview(subContent)
                
        // add spacer
        footerContainerView.addArrangedSubview(getSpacer(width: 0, height: 15))
        
        
        // init actionButtonsContainerView
        actionButtonsContainerView = UIStackView(frame: CGRect.zero)
        actionButtonsContainerView.translatesAutoresizingMaskIntoConstraints = false
        actionButtonsContainerView.axis = .horizontal
        actionButtonsContainerView.isLayoutMarginsRelativeArrangement = true
        actionButtonsContainerView.layoutMargins = UIEdgeInsets(top: 0, left: 15, bottom: 15, right: 15)
        actionButtonsContainerView.spacing = 15

        parentContainerView.addArrangedSubview(actionButtonsContainerView)
        
        // init approveButton
        approveButton = ActionButton()
        approveButton.title = Language.getTranslationOf(findKey: "acceptedVacation",defaultWord: "Accepted Vacation")
        approveButton.setBackgroundColor(Utils.getColor(hexColor: "00943e"), for: .normal)
        approveButton.loadingIndicatorAlignment = .center
        approveButton.underlineTitleDisabled = true
        approveButton.loadingIndicatorStyle = .ballClipRotatePulse
        approveButton.extendSize = CGSize(width: 50, height: 20)
        approveButton.contentEdgeInsets = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
        approveButton.imageAlignment = .leftEdge
        approveButton.isRoundedButton = true
        
        actionButtonsContainerView.addArrangedSubview(approveButton)

        // init rejectButton
        rejectButton = ActionButton()
        rejectButton.title = Language.getTranslationOf(findKey: "rejectedVacation",defaultWord: "Rejected Vacation")
        rejectButton.setBackgroundColor(Utils.getColor(hexColor: "940023"), for: .normal)
        rejectButton.loadingIndicatorAlignment = .center
        rejectButton.underlineTitleDisabled = true
        rejectButton.loadingIndicatorStyle = .ballClipRotatePulse
        rejectButton.extendSize = CGSize(width: 50, height: 20)
        rejectButton.contentEdgeInsets = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
        rejectButton.imageAlignment = .leftEdge
        rejectButton.isRoundedButton = true

        actionButtonsContainerView.addArrangedSubview(rejectButton)
        
        // add spacer
        //footerContainerView.addArrangedSubview(getSpacer(width: 0, height: 15))
        
        //============================== Make Constraints ==============================//
        
        cardView.snp.makeConstraints { (make) in
            make.leading.top.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.bottom.equalTo(parentContainerView.snp.bottom).priority(750)
        }
        
        parentContainerView.snp.makeConstraints { (make) in
            make.leading.top.equalToSuperview()
            make.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        
        // Header
        headerContainerView.snp.makeConstraints { (make) in
            make.leading.top.trailing.equalToSuperview()
        }
        
        user_img.snp.makeConstraints { (make) in
            make.width.height.equalTo(60)
            make.leading.top.equalToSuperview().offset(15)
        }
        
        headerMainView.snp.makeConstraints { (make) in
            make.leading.equalTo(user_img.snp.trailing)
            make.top.equalToSuperview()
            make.trailing.equalToSuperview()
        }
        
        title.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(15)
            make.trailing.equalToSuperview().offset(-15)
            make.leading.equalToSuperview().offset(10)
        }
        
        subtitle.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(10)
            make.trailing.equalToSuperview().offset(-15)
            make.top.equalTo(title.snp.bottom)
        }
        
        // Footer
        footerContainerView.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().inset(15)
            make.trailing.equalToSuperview().inset(15)
            make.top.equalTo(headerContainerView.snp.bottom)
        }
        content.snp.makeConstraints { (make) in
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.top.equalToSuperview().offset(5)
        }
        subContent.snp.makeConstraints { (make) in
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.top.equalTo(content.snp.bottom).offset(5)
        }
        
        actionButtonsContainerView.snp.makeConstraints { (make) in
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.top.equalTo(footerContainerView.snp.bottom)
            make.bottom.equalTo(containerView)
        }
        
    }
    
    
    
    func getSpacer(width : Int?, height : Int?) -> UIView {
        let spacer = UIView(frame: CGRect.zero)
        spacer.snp.makeConstraints { (make) in
            if (width != nil && height == nil){
                make.top.bottom.equalToSuperview()
                make.width.equalTo(width ?? 0)
            }else if (height != nil && width == nil){
                make.trailing.leading.equalToSuperview()
                make.height.equalTo(height ?? 0)
            }else{
                make.height.equalTo(height ?? 0)
                make.width.equalTo(width ?? 0)
            }
        }
        return spacer
    }
}
