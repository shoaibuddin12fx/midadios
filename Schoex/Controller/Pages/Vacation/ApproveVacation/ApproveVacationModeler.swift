//
//  ForumsModeler.swift
//  Eduopus
//
//  Created by Mohamed Selim Refaat on 5/28/19.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation
import SwiftyJSON

class ApproveVacationModeler {
    
    var modelerInterface : SBModelerInterface!
    
    //================================  Constructor ================================//
    init(modelerInterface : SBModelerInterface) {
        self.modelerInterface = modelerInterface
    }
    

    // Get CategoriesList
    func modelApproveVacation(jsonData : Any, tag : String)  {
        
        //Notify interface about starting modeling
        if (modelerInterface != nil){
            self.modelerInterface.onModelingStarted(tag: tag)
        }
        
        
        //Convert json data to object
        let dataJson = SBParser.convertToJson(data: jsonData)
        
        var dataList = [VacationApproveModel]()

        if (dataJson.null == nil){

            let Vacations = SBParser.getJsonArray(attrHolder: dataJson)
            
            for vacation in Vacations {
                
                let VacationDic = SBParser.getJsonDictionary(attrHolder: vacation)
                
                if(VacationDic != nil){
                    let id = SBParser.getInt(attrHolder: VacationDic, attrName: "id")
                    
                    let vacationItem = VacationApproveModel(
                        id: id,
                        userid: SBParser.getInt(attrHolder: VacationDic, attrName: "userid"),
                        fullName : SBParser.getString(attrHolder: VacationDic, attrName: "fullName"),
                        email : SBParser.getString(attrHolder: VacationDic, attrName: "email"),
                        role : SBParser.getString(attrHolder: VacationDic, attrName: "role"),
                        vacDate : SBParser.getString(attrHolder: VacationDic, attrName: "vacDate")
                    )
                    
                    dataList.append(vacationItem)
                    
                }
            }
            
            if (modelerInterface != nil){
                modelerInterface.onModelingSucceeded(tag: tag, modeledData: dataList)
            }
            
        }else{
            if (modelerInterface != nil){
                modelerInterface.onModelingFailed(tag: tag, error: "")
            }
        }
    }
    
}
