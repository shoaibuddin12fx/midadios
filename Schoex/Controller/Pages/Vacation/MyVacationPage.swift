//
//  ViewController.swift
//  EduopusKit
//
//  Created by Mohamed Selim Refaat on 3/3/19.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework
import Kingfisher


class MyVacationPage:SolViewController,UITableViewDelegate,UITableViewDataSource,SBConnectorInterface,SBModelerInterface {
    
    
    
    var networkConnector : MyVacationConnector!
    var modeler : MyVacationModeler!
    let pageView = MyVacationPageView(frame: CGRect.zero)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //================================= Setup Controller =============================//
        title = Language.getTranslationOf(findKey: "vacation",defaultWord: "Vacation")
        
        // Set RTL
        if Utils.App_IsRtl {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }else{
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }
        
        initControllerStates()
        
        // Set Background image or color
        if Bools.x_gen_back_is_image {
            let backgroundImage = CustomImageView(frame: UIScreen.main.bounds)
            backgroundImage.image = UIImage(named: "x_gen_back")
            backgroundImage.contentMode = UIView.ContentMode.scaleAspectFill
            self.view.insertSubview(backgroundImage, at: 0)
        }else{
            view.backgroundColor = SolColors.getColorByName(colorName: "x_gen_back")
        }
        
        // Set Status bar background
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        statusBarView.backgroundColor = SolColors.getColorByName(colorName: "x_header_back")
        view.addSubview(statusBarView)
        
        // Set Navigation bar background
        navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController!.navigationBar.shadowImage = UIImage()
        UINavigationBar.appearance().backgroundColor = SolColors.getColorByName(colorName: "x_header_back")

        // Set Navigation title color
        navigationController!.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : SolColors.getColorByName(colorName: "x_header_text")!]
        
        // Set Navigation bar icons and colors
        let rightBarButtonItem = getNavBarButton(custom: false, image: nil, nonCustomIcon: .refresh, color: SolColors.getColorByName(colorName: "x_header_icons"),size: 35.0, tapTargetAction: #selector(navRightButtonOneTapped), tapTarget : self)
        
        let requestVacationBarButtonItem = getNavBarButton(custom: true, image: UIImage(named: "ic_request")!, nonCustomIcon: nil, color: SolColors.getColorByName(colorName: "x_header_icons"),size: 35.0, tapTargetAction: #selector(navRequestVacationTapped), tapTarget : self)
        
         let approveVacationBarButtonItem = getNavBarButton(custom: true, image: UIImage(named: "ic_approve")!, nonCustomIcon: nil, color: SolColors.getColorByName(colorName: "x_header_icons"),size: 35.0, tapTargetAction: #selector(navApproveVacationTapped), tapTarget : self)
        
        if (rightBarButtonItem != nil){
            var buttons : [UIBarButtonItem] = [UIBarButtonItem]()
            
            buttons.append(rightBarButtonItem!)
            
            if Utils.isUserHavePermission(checkThisModules: ["Vacation.reqVacation"]){
                buttons.append(requestVacationBarButtonItem!)
            }
            if Utils.isUserHavePermission(checkThisModules: ["Vacation.appVacation"]){
                buttons.append(approveVacationBarButtonItem!)
            }
            navigationItem.rightBarButtonItems = buttons
        }

        let leftBarButtonItem = getNavBarButton(custom: true, image: UIImage(named: "icn_drawer"), nonCustomIcon: nil, color: SolColors.getColorByName(colorName: "x_header_icons"),size: 35.0, tapTargetAction: #selector(navLeftButtonTapped), tapTarget : self)
        if (leftBarButtonItem != nil){
            navigationItem.leftBarButtonItems = [leftBarButtonItem!]
        }
        

        
        //================================= Processing =============================//
        // Init variables
        networkConnector = MyVacationConnector(connectorInterface : self)
        modeler = MyVacationModeler(modelerInterface : self)

        if Utils.isUserHavePermission(checkThisModules: ["Vacation.myVacation"]){
            // Start processing data
            networkConnector.getVacationData(tag: "vacations")
        }
    }
   
    
  
    func initControllerStates() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(loadData))
        super.initControllerStates(failureView: failureView)
    }
    
    @objc func loadData() {
        networkConnector.getVacationData(tag: "vacations")
    }
    
    func getNavBarButton(custom : Bool, image : UIImage?, nonCustomIcon : UIBarButtonItem.SystemItem?, color : UIColor?, size : Double, tapTargetAction : Selector, tapTarget : Any) -> UIBarButtonItem? {
        
        if(custom && image != nil){
            let button = UIButton(type: .custom)
            
            if (color != nil){
                button.setImage(image!.tint(color), for: .normal)
            }else{
                button.setImage(image!, for: .normal)
            }
            button.frame = CGRect(x: 0.0, y: 0.0, width: size, height: size)
            button.addTarget(tapTarget, action: tapTargetAction, for: .touchUpInside)
            return UIBarButtonItem(customView: button)
            
        }else if (nonCustomIcon != nil){
            let button = UIBarButtonItem(barButtonSystemItem: nonCustomIcon!, target: self, action: tapTargetAction)
            if (color != nil){
                button.tintColor = color
            }
            return button
        }
        return nil
    }
    
    @objc
    func navLeftButtonTapped() {
        
        self.showMenu()

    }
    
    @objc
    func navRightButtonOneTapped() {
        self.dataArray.removeAll()
        pageView.tableViewLayout.tableView.reloadData()
        
        networkConnector.resetRequest(tag: "vacations")
        networkConnector.getVacationData(tag: "vacations")
    }
    
    @objc
    func navRequestVacationTapped() {
        let vc = RequestVacationPage()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc
    func navApproveVacationTapped() {
        let vc = ApproveVacationPage()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //====================== Nav Bar Common Options ========================//
   
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupInitialViewState()

        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        pageView.createView(delegate: self, dataSource: self)
        pageView.tableViewLayout.tableView.register(TableViewCell<MyVacationPageTableCell>.self, forCellReuseIdentifier: "cellView")
        self.view.addSubview(pageView)

        
        pageView.snp.makeConstraints { (make) -> Void in

            if self.navigationController != nil {
                let navStatusHeight = UIApplication.shared.statusBarFrame.height + self.navigationController!.navigationBar.frame.height
                make.top.equalTo(self.view).offset(navStatusHeight) // Below Navigation Bar                
            }
            make.left.right.bottom.equalTo(self.view)
        }
        
        
    }
    
    func onRequestStarted(tag: String) {
        print("started \(tag)")
        startLoading()

    }
    
    func onRequestSucceeded(tag: String, response: Any) {
       
        if (tag == "vacations") {
            networkConnector.setRequestModeling(tag: "vacations", isModeling: true)
            modeler.modelVacation(jsonData: response, tag : "vacations")
        }
    }
    
    func onRequestFailed(tag: String, exception: Error) {
        print("failed \(tag)")
        self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error

    }
    
    func onPreparingUrl(tag: String, lastLoadedPage: Int) -> String {
        return "\(Constants.getModRewriteBaseUrl())/vacation/mine"
    }
    
    func onModelingStarted(tag: String) {
        
    }
    
    func onModelingSucceeded(tag: String, modeledData: Any) {
        networkConnector.increaseRequestOneLoadedPage(tag: tag)
        networkConnector.setRequestModeling(tag: tag, isModeling: false)
        
        if (tag == "vacations") {
            // Process vacations
            if (modeledData is [VacationModel]) {
                
                if let dataObj = modeledData as? [VacationModel]{
                    if (networkConnector.getRequestLastLoadedPage(tag: "vacations") == 0){
                        self.dataArray.removeAll()
                    }
                    self.dataArray.append(contentsOf: dataObj)

                    if (dataObj.count == 0){
                        networkConnector.setRequestLastPageReached(tag: "vacations", isLastPageReached: true)
                    }
                }

            }
            self.endLoading(error: nil) // Set Content
            pageView.tableViewLayout.tableView.reloadData()
            
        }

    }
    
    func onModelingFailed(tag: String, error: String) {
        print("moldling failed \(tag)")
        networkConnector.setRequestModeling(tag: tag, isModeling: false)
        networkConnector.setRequestLoading(tag: tag, isLoading: false)
        
        self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:TableViewCell<MyVacationPageTableCell> = tableView.dequeueReusableCell(withIdentifier: "cellView", for: indexPath) as! TableViewCell<MyVacationPageTableCell>
        
        let data = self.dataArray[indexPath.row] as! VacationModel
        cell.layout.content.text = data.vacDate
        
        if(data.acceptedVacation == 1){
            cell.layout.subContent.text = Language.getTranslationOf(findKey: "acceptedVacation",defaultWord: "Accepted Vacation")
        }else if(data.acceptedVacation == -1){
            cell.layout.subContent.text = Language.getTranslationOf(findKey: "waitAction",defaultWord: "Waiting Action")
        }else if(data.acceptedVacation == 0){
            cell.layout.subContent.text = Language.getTranslationOf(findKey: "rejectedVacation",defaultWord: "Rejected Vacation")
        }else{
            cell.layout.subContent.text = Language.getTranslationOf(findKey: "unknown",defaultWord: "Unknown")
        }
        
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        
   
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
}

