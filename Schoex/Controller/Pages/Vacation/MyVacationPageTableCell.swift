
import Foundation
import UIKit
import SnapKit
import SBFramework

/**
 USE UIVIEW AS CELL AND REVERSE
 
 SOURCE : https://medium.com/anysuggestion/how-to-share-layout-code-between-uiview-and-uitableviewcell-subclasses-929441a9752a
 
 --> AS CELL
 itemViewType4.tableView.register(TableViewCell<ItemViewType5>.self, forCellReuseIdentifier: "view5")
 let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "view5", for: indexPath)

 --> AS UIVIEW
 let view5 = ViewConverter<ItemViewType5>(frame: .zero)

 **/
class MyVacationPageTableCell: ViewLayout {
    
    var cardView: UIView!
    
    var parentContainerView: UIStackView!
    var footerContainerView: UIStackView!
    
    var content: UILabel!
    var subContent: UILabel!
    
    var cornerRadius: CGFloat = 5
    
    var shadowOffsetWidth: Int = 0
    var shadowOffsetHeight: Int = 3
    var shadowColor: UIColor? = UIColor.black
    var shadowOpacity: Float = 0.5
    
    required init() {}
    
    func layout(on containerView: UIView) {
        
        // init cardView
        cardView = UIView(frame: CGRect.zero)
        cardView.backgroundColor = SolColors.getColorByName(colorName: "white")
        cardView.layer.masksToBounds = true
        cardView.layer.cornerRadius = cornerRadius
        cardView.layer.shadowColor = shadowColor?.cgColor
        cardView.layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        cardView.layer.shadowOpacity = shadowOpacity
        
        containerView.addSubview(cardView)
        
        // init parentContainerView
        parentContainerView = UIStackView(frame: CGRect.zero)
        parentContainerView.translatesAutoresizingMaskIntoConstraints = false
        parentContainerView.axis = .vertical
        
        cardView.addSubview(parentContainerView)
        
     
        // init footerContainerView
        footerContainerView = UIStackView(frame: CGRect.zero)
        footerContainerView.translatesAutoresizingMaskIntoConstraints = false
        footerContainerView.axis = .vertical
        parentContainerView.addArrangedSubview(footerContainerView)
        
   
        // init content
        content = UILabel(frame: CGRect.zero)
        content.textColor = SolColors.getColorByName(colorName: "x_boxes_main_text")
        content.font = UIFont(name: Fonts.FontRobotoCondensed_Bold, size: 18)
        content.numberOfLines = 0
        footerContainerView.addArrangedSubview(content)
        
        // init subContent
        subContent = UILabel(frame: CGRect.zero)
        subContent.textColor = SolColors.getColorByName(colorName: "x_boxes_sub_title_text")
        subContent.font = UIFont(name: Fonts.FontRobotoCondensed_Regular, size: 15)
        subContent.numberOfLines = 1
        footerContainerView.addArrangedSubview(subContent)
        
        footerContainerView.addArrangedSubview(subContent)
        
        // add spacer
        footerContainerView.addArrangedSubview(getSpacer(width: 0, height: 15))
        
        //============================== Make Constraints ==============================//
        
        cardView.snp.makeConstraints { (make) in
            make.leading.top.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.bottom.equalTo(parentContainerView.snp.bottom).priority(750)
        }
        
        parentContainerView.snp.makeConstraints { (make) in
            make.leading.top.equalToSuperview()
            make.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        
       
        // Footer
       
        footerContainerView.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().inset(15)
            make.trailing.equalToSuperview().inset(15)
            make.top.equalToSuperview().inset(15)
            make.bottom.equalTo(containerView)
        }
       
        content.snp.makeConstraints { (make) in
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview().offset(-15)
        }
        subContent.snp.makeConstraints { (make) in
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview().offset(-15)
            make.top.equalTo(content.snp.bottom).offset(5)
        }
        
    }
    
    func getSpacer(width : Int?, height : Int?) -> UIView {
        let spacer = UIView(frame: CGRect.zero)
        spacer.snp.makeConstraints { (make) in
            if (width != nil && height == nil){
                make.top.bottom.equalToSuperview()
                make.width.equalTo(width ?? 0)
            }else if (height != nil && width == nil){
                make.trailing.leading.equalToSuperview()
                make.height.equalTo(height ?? 0)
            }else{
                make.height.equalTo(height ?? 0)
                make.width.equalTo(width ?? 0)
            }
        }
        return spacer
    }
}
