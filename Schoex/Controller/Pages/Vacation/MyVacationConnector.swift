//
//  ForumsModeler.swift
//  Eduopus
//
//  Created by Mohamed Selim Refaat on 5/28/19.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class MyVacationConnector {
    var connectorInterface : SBConnectorInterface!
    var paginationHandler : [String : ConnectorPagination]?
    
    //================================  Constructor ================================//
    init(connectorInterface : SBConnectorInterface) {
        self.connectorInterface = connectorInterface
        self.paginationHandler = [String : ConnectorPagination]()
    }
    
    //================================ getForum ===============================//
    func getVacationData(tag : String) {
        
        // Make sure that request not loading
        if(!self.isRequestLoading(tag: tag) && !self.isRequestModeling(tag : tag)) {
            var _url = "http://example.com"
            
            //Notify interface about starting request & get URL
            if (self.connectorInterface != nil){
                self.connectorInterface.onRequestStarted(tag: tag)
                let t_url = connectorInterface.onPreparingUrl(tag: tag, lastLoadedPage: getRequestLastLoadedPage(tag : tag))
                if t_url != "" {
                    _url = t_url
                }
            }
            
            //Set page as loading
            self.setRequestLoading(tag : tag, isLoading : true);

            //Create headers
            let headers: HTTPHeaders = [
                "Authorization": "Bearer \(Utils.AppToken!)"
            ]
            
            Alamofire.request(_url, method: .get, headers: headers).responseJSON { response in
                switch response.result {
                case .success(let value):
                    
                    // Pass result as string to callback
                    if (self.connectorInterface != nil){
                        self.connectorInterface.onRequestSucceeded(tag : tag, response : value)
                    }
                    
                    // Set Request not loading
                    self.setRequestLoading(tag : tag, isLoading : false)
                    
                case .failure(let error):
                    
                    // Callback fail interface
                    if (self.connectorInterface != nil){
                        self.connectorInterface.onRequestFailed(tag: tag,exception: error)
                    }
                    
                    // Set Request not loading
                    self.setRequestLoading(tag : tag, isLoading : false)
                    
                }
            }
            
        }
        
    }
    
    //================================ Pagination management ===============================//

    func isRequestLoading(tag : String) -> Bool {
        if let Val = self.paginationHandler?[tag] {
            return Val.isLoading ?? false
        }
        return false;
    }
    
    func isRequestModeling(tag : String) -> Bool {
        if let Val = self.paginationHandler?[tag] {
            return Val.isModeling ?? false
        }
        return false
    }
    
    func isRequestLastPageReached(tag : String) -> Bool {
        if let Val = self.paginationHandler?[tag] {
            return Val.isLastPageReached ?? false
        }
        return false
    }
    
    func getRequestLastLoadedPage(tag : String) -> Int {
        if let Val = self.paginationHandler?[tag] {
            return Val.PAGES_LOADED ?? 0
        }
        return 0
    }
    
    
    func setRequestLoading(tag : String, isLoading : Bool) {
        if let Val = self.paginationHandler?[tag] {
            Val.isLoading = isLoading
        }else{
            let cPage = ConnectorPagination()
            cPage.isLoading = isLoading
            self.paginationHandler?[tag] = cPage
        }
    }
    
    func setRequestModeling(tag : String, isModeling : Bool) {
        if let Val = self.paginationHandler?[tag] {
            Val.isModeling = isModeling;
        }else{
            let cPage = ConnectorPagination()
            cPage.isModeling = isModeling
            self.paginationHandler?[tag] = cPage
        }
    }
    
    func setRequestLastPageReached(tag : String, isLastPageReached : Bool) {
        if let Val = self.paginationHandler?[tag] {
            Val.isLastPageReached = isLastPageReached;
        }else{
            let cPage = ConnectorPagination()
            cPage.isLastPageReached = isLastPageReached
            self.paginationHandler?[tag] = cPage
        }
    }
    
    func setRequestPagesPage(tag : String, pagesLoaded : Int) {
        if let Val = self.paginationHandler?[tag] {
            Val.PAGES_LOADED = pagesLoaded
        }else{
            let cPage = ConnectorPagination()
            cPage.PAGES_LOADED = pagesLoaded
            self.paginationHandler?[tag] = cPage
        }
    }
    
    func increaseRequestOneLoadedPage(tag : String) {
        if let Val = self.paginationHandler?[tag] {
            Val.PAGES_LOADED = (Val.PAGES_LOADED ?? 0) + 1;
        }else{
            let cPage = ConnectorPagination()
            cPage.PAGES_LOADED = 1
            self.paginationHandler?[tag] = cPage
        }
    }
    
    func resetRequest(tag : String){
        self.paginationHandler?.removeValue(forKey: tag)
    }
}
