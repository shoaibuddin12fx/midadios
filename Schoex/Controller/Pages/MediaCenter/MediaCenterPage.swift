//
//  ViewController.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/15/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework
import SwiftyJSON
import PagingMenuController

class MediaCenterPage:SolViewController,RenewTokenCallback {
    
    @IBOutlet weak var navLeftButton: UIBarButtonItem!
    
    var albumID:Int = 0
    var albumsDataArray = [AlbumsModel]()
    var mediaDataArray = [PhotoModel]()

    
    //============================ Base Functions =================//
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initNavBar()
        initSideMenu()
        initControllerStates()

        initPage()
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupInitialViewState()
        UIView.appearance().semanticContentAttribute = .forceLeftToRight

    }
    
    func initControllerStates() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(loadData))
        super.initControllerStates(failureView: failureView)
    }
    
    //============================ Init Functions =================//

    func initPage() {
        if albumID == 0{
            self.navLeftButton.image = UIImage(named: "icn_drawer")
        }else{
            self.navLeftButton.image = Utils.getBackIcon()
        }
        self.loadData()
        // nav bar title
        self.navigationItem.title = Utils.siteTitle
    }

    //==================== Load data ===================================//
    @objc func loadData() {
        startLoading()
        
        let url:String
        if albumID != 0 {
            url = "\(Constants.getModRewriteBaseUrl())/media/listAll/\(albumID)"
        }else{
            url = "\(Constants.getModRewriteBaseUrl())/media/listAll"
        }
        
        
        SBFramework.request(url, method: .get).responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                if (json["error"].null == nil){
                    if(!self.tokenRenewTried){ Utils.renewToken(callback : self,navigationController : self.navigationController!) }
                }
                // parse albums
                if let jsonArray = json["albums"].array {
                    
                    //================= Fill data array ==================//
                    for item in jsonArray {
                        self.albumsDataArray.append(AlbumsModel(
                            id: Utils.getIntValueOf(sourceInput: item,itemKey:"id") ,
                            albumTitle: Utils.getStringValueOf(sourceInput: item,itemKey:"albumTitle") ,
                            albumDescription: Utils.getStringValueOf(sourceInput: item,itemKey:"albumDescription") ,
                            albumImage: Utils.getStringValueOf(sourceInput: item,itemKey:"albumImage") ,
                            albumParent: Utils.getStringValueOf(sourceInput: item,itemKey:"albumParent")
                            )
                        )
                    }
                    
                }
                
                // parse media
                if let jsonArray = json["media"].array {
                    
                    //================= Fill data array ==================//
                    for item in jsonArray {
                        self.mediaDataArray.append(PhotoModel(
                            id: Utils.getIntValueOf(sourceInput: item,itemKey:"id") ,
                            albumId: Utils.getStringValueOf(sourceInput: item,itemKey:"albumId") ,
                            mediaType: Utils.getIntValueOf(sourceInput: item,itemKey:"mediaType") ,
                            mediaURL: Utils.getStringValueOf(sourceInput: item,itemKey:"mediaURL") ,
                            mediaURLThumb: Utils.getStringValueOf(sourceInput: item,itemKey:"mediaURLThumb") ,
                            mediaTitle: Utils.getStringValueOf(sourceInput: item,itemKey:"mediaTitle") ,
                            mediaDescription: Utils.getStringValueOf(sourceInput: item,itemKey:"mediaDescription") ,
                            mediaDate: Utils.getStringValueOf(sourceInput: item,itemKey:"mediaDate")
                            )
                        )
                    }
                    
                }
                self.iHaveContentNow = true
                self.endLoading(error: nil) // Set Content
                self.initTabs()
            case .failure(let error):
                Utils.checkFailureCause(error : error, callback: self, navigationController: self.navigationController!,tokenRenewTried:self.tokenRenewTried)
                if self.tokenRenewTried {self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) }
            }
        }
    }
    var tokenRenewTried = false
    func afterRenewToken(success: Bool) {
        tokenRenewTried = true
        if success {
            loadData()
        }else{
            self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
        }
    }
    
    //==================== Nav Bar Buttons Actions ===================================//
   
    @IBAction func navLeftButtonAction(_ sender: Any) {
        if albumID == 0{
            showMenu()
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }

    //==================== Tabs functions ===================================//

    func initTabs()  {
        let pagingMenuController = self.childViewControllers.first as! PagingMenuController
        let options = segmentOptions(albumsDataArrayArg: albumsDataArray,mediaDataArrayArg: mediaDataArray)
        let attributes = [NSAttributedString.Key.font: UIFont(name: "Cairo-Regular", size: 17)!]
        pagingMenuController.navigationController?.navigationBar.titleTextAttributes = attributes
        pagingMenuController.setup(options)
    }
    
    struct MenuItemAlbums: MenuItemViewCustomizable {}
    struct MenuItemMedia: MenuItemViewCustomizable {}
    
    struct segmentOptions: PagingMenuControllerCustomizable {

        var albumsViewController: MediaDetailsPage = MediaDetailsPage.instantiateFromStoryboard()
        var mediaViewController: MediaDetailsPage = MediaDetailsPage.instantiateFromStoryboard()
        
        
        init(albumsDataArrayArg: [AlbumsModel], mediaDataArrayArg: [PhotoModel]) {
            albumsViewController.dataArray = albumsDataArrayArg
            albumsViewController.currentViewIsAlbum = true
            mediaViewController.dataArray = mediaDataArrayArg
            mediaViewController.currentViewIsAlbum = false
        }
        
        var componentType: ComponentType {
            return .all(menuOptions: MenuOptions(), pagingControllers: [albumsViewController, mediaViewController])
        }
        var menuControllerSet: MenuControllerSet {
            return .single
        }
        
        var backgroundColor: UIColor {
            return UIColor.clear
        }
        
        struct MenuItemAlbums: MenuItemViewCustomizable {
            var displayMode: MenuItemDisplayMode {
                let title = MenuItemText(
                    text: Language.getTranslationOf(findAndDefaultKey:"Albums"),
                    color: SolColors.getColorByName(colorName: "x_gen_main_text")!,
                    selectedColor : SolColors.getColorByName(colorName: "x_gen_sub_title_text")!)
                return .text(title: title)
            }
        }
        
        struct MenuItemMedia: MenuItemViewCustomizable {
            var displayMode: MenuItemDisplayMode {
                let title = MenuItemText(
                    text: Language.getTranslationOf(findAndDefaultKey:"Media"),
                    color: SolColors.getColorByName(colorName: "x_gen_main_text")!,
                    selectedColor : SolColors.getColorByName(colorName: "x_gen_sub_title_text")!)
                return .text(title: title)
            }
        }
        
        
        struct MenuOptions: MenuViewCustomizable {
            var displayMode: MenuDisplayMode {
                return .segmentedControl
            }
            var itemsOptions: [MenuItemViewCustomizable] {
                return [MenuItemAlbums(), MenuItemMedia()]
            }
            var height: CGFloat {
                return 50
            }
            var backgroundColor: UIColor {
                return UIColor.clear
            }
            var selectedBackgroundColor: UIColor {
                return SolColors.getColorByName(colorName: "transparent")!
            }
        }
        
    }
}




