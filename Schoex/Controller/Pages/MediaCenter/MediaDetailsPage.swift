import UIKit
import AlamofireImage
import Alamofire
import SwiftyJSON
import GSImageViewerController
import SBFramework

class MediaDetailsPage: SolViewController,FSPagerViewDataSource,FSPagerViewDelegate {
    
    @IBOutlet weak var navLeftButton: UIBarButtonItem!
    @IBOutlet weak var slideTitle: UILabel!
    @IBOutlet weak var slideDescription: UILabel!
    
    var currentViewIsAlbum:Bool = true
    
    class func instantiateFromStoryboard() -> MediaDetailsPage {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "MediaDetailsPageStoryboard") as! MediaDetailsPage
    }
    
    // MARK: - Lifecycle
    
    @IBOutlet weak var pagerView: FSPagerView! {
        didSet {
            self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
            self.pagerView.transformer = FSPagerViewTransformer(type:.linear)
            let transform = CGAffineTransform(scaleX: 0.6, y: 0.75)
            self.pagerView.itemSize = self.pagerView.frame.size.applying(transform)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBOutlet weak var pageControl: FSPageControl! {
        didSet {
            self.pageControl.numberOfPages = dataArray.count
            self.pageControl.contentHorizontalAlignment = .right
            self.pageControl.contentInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.slideTitle.isHidden = true
        self.slideTitle.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
        self.slideDescription.isHidden = true
        self.slideDescription.textColor = SolColors.getColorByName(colorName: "x_gen_sub_title_text")
        
        initControllerStates()
        
    }
    
    func initControllerStates() {
        let failureView = ErrorView(frame: view.frame)
        super.initControllerStates(failureView: failureView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupInitialViewState()
        
        view.backgroundColor = UIColor.clear
        view.isOpaque = false
    }

    
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return dataArray.count
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        
        cell.imageView?.contentMode = .scaleAspectFill
        cell.imageView?.clipsToBounds = true;

        if currentViewIsAlbum {
            let albumItem : AlbumsModel = dataArray[index] as! AlbumsModel
            if index == 0{
                self.slideTitle.isHidden = false
                self.slideDescription.isHidden = false
                self.slideTitle.text = albumItem.albumTitle
                self.slideDescription.text = albumItem.albumDescription
            }
            cell.imageUrl = "\(Constants.getWithoutModRewriteBaseUrl())/uploads/media/\(albumItem.albumImage!)"
            cell.loadImage()
            
        }else{
            let mediaItem : PhotoModel = dataArray[index] as! PhotoModel

            if (mediaItem.mediaType == 0) {
                cell.imageUrl = "\(Constants.getWithoutModRewriteBaseUrl())/uploads/media/\(mediaItem.mediaURL!)"
                mediaItem.mediaIsVideo = false
            }else if (mediaItem.mediaType == 1 || mediaItem.mediaType == 2) {
                cell.imageUrl = "\(Constants.getWithoutModRewriteBaseUrl())/uploads/media/\(mediaItem.mediaURLThumb!)"
                mediaItem.mediaIsVideo = true
                print(cell.imageUrl)
            }

            cell.loadImage()
            if index == 0{
                self.slideTitle.isHidden = false
                self.slideDescription.isHidden = false
                self.slideTitle.text = mediaItem.mediaTitle
                self.slideDescription.text = mediaItem.mediaDescription
            }
        }

        return cell
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
        self.pageControl.currentPage = index
    }
    
    func pagerView(_ pagerView: FSPagerView, didHighlightItemAt index: Int){
        if currentViewIsAlbum {
            let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "MediaCenterPageStoryboard") as! MediaCenterPage
            nextVC.albumID = (dataArray[index] as! AlbumsModel).id!
            self.navigationController!.pushViewController(nextVC, animated: true)
            
        }else{
            let item:PhotoModel = dataArray[index] as! PhotoModel
            if item.mediaIsVideo {
                openURL(scheme: item.mediaURL!)
            }else{
                
                let imageUrl = "\(Constants.getWithoutModRewriteBaseUrl())/uploads/media/\(item.mediaURL!)"
                let photoManager: PhotoManager = PhotoManager()
                _ = photoManager.retrieveImage(for: imageUrl) { image in
                    let imageInfo   = GSImageInfo(image: image, imageMode: .aspectFit)
                    let imageViewer = GSImageViewerController(imageInfo: imageInfo)
                    self.navigationController?.pushViewController(imageViewer, animated: true)
                    
                }
                
            }
        }
    }
    
    func openURL(scheme: String) {
        if let url = URL(string: scheme) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:],
                                          completionHandler: {
                                            (success) in
                                            print("Open \(scheme): \(success)")
                })
            } else {
                let success = UIApplication.shared.openURL(url)
                print("Open \(scheme): \(success)")
            }
        }
    }

    
    func pagerViewDidScroll(_ pagerView: FSPagerView) {
        guard self.pageControl.currentPage != pagerView.currentIndex else {
            return
        }
        self.pageControl.currentPage = pagerView.currentIndex
        
        self.slideTitle.isHidden = false
        self.slideDescription.isHidden = false
        
        if currentViewIsAlbum {
            self.slideTitle.text = (dataArray[pagerView.currentIndex] as! AlbumsModel).albumTitle
            self.slideDescription.text = (dataArray[pagerView.currentIndex] as! AlbumsModel).albumDescription
        }else{
            self.slideTitle.text = (dataArray[pagerView.currentIndex] as! PhotoModel).mediaTitle
            self.slideDescription.text = (dataArray[pagerView.currentIndex] as! PhotoModel).mediaDescription
        }
        
    }

}


