//
//  ViewController.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/15/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework
import SwiftyJSON


class EventsPage:SolViewController,UITableViewDataSource,UITableViewDelegate,RenewTokenCallback {
    
    @IBOutlet weak var dataTableView: UITableView!
    @IBOutlet weak var navLeftButton: UIBarButtonItem!

    var searchMode:Bool = false
    
    //============================ Base Functions =================//
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initTableView(tableView: dataTableView)
        initNavBar()
        initSideMenu()
        initControllerStates()
        
        initPage()

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupInitialViewState()
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //============================ Init Functions =================//

    func initPage() {
     
        if searchAbout != nil {
            searchMode = true
            searchInDataArray();
            self.navLeftButton.image = Utils.getBackIcon()
        }else{
            searchMode = false
            startLoading()
            self.navLeftButton.image = UIImage(named: "icn_drawer")
            self.loadData()
        }
        // nav bar title
        self.navigationItem.title = Utils.siteTitle
    }
    
    func initControllerStates() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(loadData))
        super.initControllerStates(failureView: failureView)
    }
    
    //==================== Load data ===================================//
    @objc func loadData() {
        self.dataTableView.reloadData()
        startLoading ()

        let url = "\(Constants.getModRewriteBaseUrl())/events/listAll"
        
       
        
        SBFramework.request(url, method: .get).responseJSON { response in
            switch response.result {
            case .success(let value):
               
                
                //======== Parse result ==========//
                let json = JSON(value)
                
                if (json["error"].null == nil){
                    if(!self.tokenRenewTried){ Utils.renewToken(callback : self,navigationController : self.navigationController!) }
                }
                
                if let jsonArray = json["events"].array {
                    
                    //================= Fill data array ==================//
                    for item in jsonArray {
                        print(Utils.getStringValueOf(sourceInput: item,itemKey: "eventDescription"))
                        print(item["eventDescription"])
                        print("-------")
                        self.dataArray.append(EventsModel(
                            id : Utils.getIntValueOf(sourceInput: item,itemKey: "id") ,
                            eventTitle : Utils.getStringValueOf(sourceInput: item,itemKey: "eventTitle") ,
                            eventDescription : Utils.getStringValueOf(sourceInput: item,itemKey: "eventDescription") ,
                            eventDate : Utils.getStringValueOf(sourceInput: item,itemKey: "eventDate") ,
                            eventFor : Utils.getStringValueOf(sourceInput: item,itemKey: "eventFor") ,
                            enentPlace : Utils.getStringValueOf(sourceInput: item,itemKey: "enentPlace")
                            )
                        )
                    }
                    self.dataArray.reverse()
                    self.endLoading(error: nil) // Set Content
                    self.dataTableView.reloadData()

                }else{
                    self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
                }
            case .failure(let error):
                print("error")
                print(error)

                Utils.checkFailureCause(error : error, callback: self, navigationController: self.navigationController!,tokenRenewTried:self.tokenRenewTried)
                if self.tokenRenewTried {self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) }
            }
        }
    }
    var tokenRenewTried = false
    func afterRenewToken(success: Bool) {
        tokenRenewTried = true
        if success {
            loadData()
        }else{
            self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
        }
    }
    
    //==================== Nav Bar Buttons Actions ===================================//

    @IBAction func reloadPage(_ sender: Any) {
        self.dataArray.removeAll()
        loadData();
    }
    
    @IBAction func searchButtonAction(_ sender: Any) {
        showSearchDialog(targetStoryboard: "EventsPageStoryboard")
    }
    
   
    @IBAction func navLeftButtonAction(_ sender: Any) {
        if(self.searchMode){
            self.navigationController?.popViewController(animated: true)
        }else{
            showMenu()
        }
    }
    
    //============================ Search Functions =================//
    
    func searchInDataArray(){
        if(self.dataArray.count > 0){
            var searchDataArray = [Any]()
            for item in dataArray {
                let cItem = item as! EventsModel
                if(cItem.eventTitle!.lowercased().contains(self.searchAbout!)
                    || cItem.eventDescription!.lowercased().contains(self.searchAbout!)
                    || cItem.eventFor!.lowercased().contains(self.searchAbout!)){
                    searchDataArray.append(cItem)
                }
            }
            dataArray.removeAll()
            dataArray.append(contentsOf : searchDataArray)
            self.endLoading(error: nil) // Set Content
        }else{
            self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil))
        }
    }

    //==================== Table view options ===================================//
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:EventsCell = tableView.dequeueReusableCell(withIdentifier: "cellCon", for: indexPath) as! EventsCell
        cell.item = dataArray[indexPath.row] as! EventsModel
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if Utils.isUserHavePermission(checkThisModules: ["events.View"]){
            let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "WebViewerPageStoryboard") as! WebViewerPage
            
            let item = self.dataArray[indexPath.row] as! EventsModel
            nextVC.eventsId = item.id
            
            self.navigationController!.pushViewController(nextVC, animated: true)
        }
        
    }
    
}






