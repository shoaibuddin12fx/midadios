//
//  NewsPage.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/15/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework
import SwiftyJSON




class OnlineExamsMarksPage:SolViewController,UITableViewDataSource,UITableViewDelegate,RenewTokenCallback {
    
    @IBOutlet weak var dataTableView: UITableView!
    @IBOutlet weak var navLeftButton: UIBarButtonItem!
    var subjectsDictionary = [String: String]()

    var examID : Int = 0
    var searchMode:Bool = false
    var examDegreeSuccess : Int = Utils.NilInt
    //============================ Base Functions =================//

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        initTableView(tableView: dataTableView)
        initNavBar()
        initSideMenu()
        initControllerStates()
        
        initPage()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupInitialViewState()
        
    }
    
    //============================ Init Functions =================//

    func initPage() {
        
        if searchAbout != nil {
            searchMode = true
            searchInDataArray();
        }else{
            searchMode = false
            self.loadData()
        }

        self.navLeftButton.image = Utils.getBackIcon()
        
        // nav bar title
        self.navigationItem.title = Utils.siteTitle
    }
    
    func initControllerStates() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(loadData))
        super.initControllerStates(failureView: failureView)
    }
    
    //==================== Load data ===================================//
   
    @objc func loadData() {
        self.dataArray.removeAll()
        self.dataTableView.reloadData()
        
        startLoading()
        
        let url = "\(Constants.getModRewriteBaseUrl())/onlineExams/marks/\(self.examID)"

        
        SBFramework.request(url, method: .get).responseJSON { response in
            switch response.result {
            case .success(let value):
                
                //======== Parse result ==========//
                let json = JSON(value)
                if (json["error"].null == nil){
                    if(!self.tokenRenewTried){ Utils.renewToken(callback : self,navigationController : self.navigationController!) }
                }
                self.examDegreeSuccess = Utils.getIntValueOrNilOf(sourceInput: json, itemKey: "examDegreeSuccess")

                // Parse grades
                if let jsonArray = json["grade"].array {
                    if jsonArray.count > 0 {
                        //================= Fill data array ==================//
                        for item in jsonArray {
                            self.dataArray.append(OnlineExamsMarksModel(
                                id : Utils.getIntValueOf(sourceInput: item,itemKey:"id") ,
                                studentId : Utils.getIntValueOf(sourceInput: item,itemKey:"studentId"),
                                examGrade : Utils.getIntValueOrNilOf(sourceInput: item,itemKey:"examGrade") ,
                                examDate : Utils.getStringValueOf(sourceInput: item,itemKey:"examDate"),
                                FullName : Utils.getStringValueOf(sourceInput: item,itemKey:"fullName")
                                )
                            )
                        }
                        self.endLoading(error: nil) // Set Content
                        self.dataTableView.reloadData()
                    }else{
                        self.endLoading(error: nil) // No Content
                    }
                }else{
                    self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
                }
            case .failure(let error):
                Utils.checkFailureCause(error : error, callback: self, navigationController: self.navigationController!,tokenRenewTried:self.tokenRenewTried)
                if self.tokenRenewTried {self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) }
            }
        }
    }
    var tokenRenewTried = false
    func afterRenewToken(success: Bool) {
        tokenRenewTried = true
        if success {
            loadData()
        }else{
            self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
        }
    }
    
    //==================== Nav Bar Buttons Actions ===================================//
    @IBAction func reloadPage(_ sender: Any) {
        loadData();
    }
    
    @IBAction func searchButtonAction(_ sender: Any) {
        let currentVC = self.storyboard?.instantiateViewController(withIdentifier:"OnlineExamsMarksPageStoryboard") as! OnlineExamsMarksPage
        currentVC.examDegreeSuccess = self.examDegreeSuccess
        showSearchDialog(targetStoryboard: "OnlineExamsMarksPageStoryboard",openController: currentVC)
    }
    
    func searchInDataArray(){
        if(self.dataArray.count > 0){
            var searchDataArray = [Any]()
            for item in dataArray {
                let cItem = item as! OnlineExamsMarksModel
                if(cItem.FullName!.lowercased().contains(self.searchAbout!)
                    || cItem.examDate!.lowercased().contains(self.searchAbout!)){
                    searchDataArray.append(cItem)
                }
            }
            dataArray.removeAll()
            dataArray.append(contentsOf : searchDataArray)
            self.endLoading(error: nil) // Set Content
        }else{
            self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil))
        }
    }
    
    @IBAction func navLeftButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //==================== Table view options ===================================//
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:OnlineExamsMarksCell = tableView.dequeueReusableCell(withIdentifier: "cellCon", for: indexPath) as! OnlineExamsMarksCell
        cell.examDegreeSuccess = self.examDegreeSuccess
        cell.item = dataArray[indexPath.row] as! OnlineExamsMarksModel // must be last line to executed
        return cell
    }

}
