//
//  ViewController.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/15/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework
import SwiftyJSON


class InvoicesPage:SolViewController,UITableViewDataSource,UITableViewDelegate,RenewTokenCallback {
    
    @IBOutlet weak var dataTableView: UITableView!
    @IBOutlet weak var navLeftButton: UIBarButtonItem!

    var page : Int = 1
    var loadMoreLocked : Bool = false
    
    var searchMode:Bool = false
    
    //============================ Base Functions =================//
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initTableView(tableView: dataTableView)
        initNavBar()
        initSideMenu()
        initControllerStates()
        
        initPage()

    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupInitialViewState()
        
    }
    
    //============================ Init Functions =================//

    func initPage() {
        if searchAbout != nil {
            searchMode = true
            self.navLeftButton.image = Utils.getBackIcon()
        }else{
            searchMode = false
            self.navLeftButton.image = UIImage(named: "icn_drawer")
        }
        self.loadData(isLoadMore: false)
        // nav bar title
        self.navigationItem.title = Utils.siteTitle
    }
    
    func initControllerStates() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(failLoadData))
        super.initControllerStates(failureView: failureView)
    }
    
    //==================== Load data ===================================//
    @objc func failLoadData() {
        loadData(isLoadMore : false)
    }
    
    @objc func loadData(isLoadMore : Bool) {
        
        if(!isLoadMore){
            self.page = 1
            self.loadMoreLocked = false
            self.dataArray.removeAll()
            self.dataTableView.reloadData()
            startLoading()
        }else{
            if(loadMoreLocked){return}
        }
        
        let url = "\(Constants.getModRewriteBaseUrl())/invoices/listAll/\(self.page)"
        
        if(searchMode){
            //=================== Load Data - Within search =======================//
            let searchParameters:Parameters = [
                "searchInput": [
                    "text" : searchAbout
                ]
            ]
            
            SBFramework.request(url, method: .post,  parameters : searchParameters, encoding: JSONEncoding.default).responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    if (json["error"].null == nil){
                        if(!self.tokenRenewTried){ Utils.renewToken(callback : self,navigationController : self.navigationController!) }
                    }
                    self.parseData(response: value,isLoadMore : isLoadMore)
                case .failure(let error):
                    Utils.checkFailureCause(error : error, callback: self, navigationController: self.navigationController!,tokenRenewTried:self.tokenRenewTried)
                    if self.tokenRenewTried {self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) }
                }
            }
            
        }else{
            //=================== Load Data - No search =======================//
            SBFramework.request(url, method: .get).responseJSON { response in
                switch response.result {
                case .success(let value):
                    self.parseData(response: value,isLoadMore : isLoadMore)
                case .failure(let error):
                    Utils.checkFailureCause(error : error, callback: self, navigationController: self.navigationController!,tokenRenewTried:self.tokenRenewTried)
                    if self.tokenRenewTried {self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) }
                }
            }
        }
    }
    var tokenRenewTried = false
    func afterRenewToken(success: Bool) {
        tokenRenewTried = true
        if success {
            loadData(isLoadMore : false)
        }else{
            self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
        }
    }
    func parseData(response:Any,isLoadMore:Bool)  {
        //======== Parse result ==========//
        let json = JSON(response)
        let CSymbol = Utils.getStringValueOf(sourceInput: json,itemKey:"currency_symbol")

        
        if let jsonArray = json["invoices"].array {
            
            //================= Fill data array ==================//
            for item in jsonArray {
               
                let invoice:InvoicesModel = InvoicesModel(
                    id : Utils.getIntValueOf(sourceInput: item,itemKey:"id") ,
                    fullName : Utils.getStringValueOf(sourceInput: item,itemKey:"fullName") ,
                    paymentTitle : Utils.getStringValueOf(sourceInput: item,itemKey:"paymentTitle") ,
                    paymentDescription : Utils.getStringValueOf(sourceInput: item,itemKey:"paymentDescription") ,
                    paymentAmount : Utils.getStringValueOf(sourceInput: item,itemKey:"paymentAmount") ,
                    paymentDate : Utils.getStringValueOf(sourceInput: item,itemKey:"paymentDate") ,
                    dueDate : Utils.getStringValueOf(sourceInput: item,itemKey:"dueDate"),
                    dicountedAmount: Utils.getStringValueOf(sourceInput: item,itemKey:"paymentDiscounted") ,
                    paidAmount : Utils.getStringValueOf(sourceInput: item,itemKey:"paidAmount") ,
                    paymentStatus : Utils.getIntValueOrNilOf(sourceInput: item,itemKey:"paymentStatus"),
                    paymentCurrency : CSymbol)
                
                if invoice.paymentStatus != Utils.NilInt{
                    if (invoice.paymentStatus == 0){
                        invoice.paymentStatusText = "UNPAID"
                    }else if (invoice.paymentStatus == 1){
                        invoice.paymentStatusText = "PAID"
                    }else if (invoice.paymentStatus == 2){
                        invoice.paymentStatusText = "Partially Paid"
                    }else{
                        invoice.paymentStatusText = Utils.Nil
                    }
                }else{
                    invoice.paymentStatusText = Utils.Nil
                }
                
                self.dataArray.append(invoice)
                
            }
            if(isLoadMore){
                if(jsonArray.count == 0){
                    self.loadMoreLocked = true
                }
            }
            
            self.endLoading(error: nil) // Set Content
            self.dataTableView.reloadData()
            self.page = self.page + 1
            
        }else{
            self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
        }
    }
    //==================== Nav Bar Buttons Actions ===================================//

    @IBAction func reloadPage(_ sender: Any) {
        loadData(isLoadMore: false);
    }
    
    @IBAction func searchButtonAction(_ sender: Any) {
        showSearchDialog(targetStoryboard: "InvoicesPageStoryboard")
    }
    
   
    @IBAction func navLeftButtonAction(_ sender: Any) {
        if(self.searchMode){
            self.navigationController?.popViewController(animated: true)
        }else{
            showMenu()
        }
    }

    //==================== Table view options ===================================//
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:InvoicesCell = tableView.dequeueReusableCell(withIdentifier: "cellCon", for: indexPath) as! InvoicesCell
        cell.item = dataArray[indexPath.row] as! InvoicesModel
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = dataArray.count - 1
        if indexPath.row == lastElement {
            self.loadData(isLoadMore: true)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if Utils.isUserHavePermission(checkThisModules: ["Invoices.View"]){
            let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "InvoiceDetailsPageStoryboard") as! InvoiceDetailsPage
            
            let item = self.dataArray[indexPath.row] as! InvoicesModel
            nextVC.invoiceID = String(item.id!)
            
            self.navigationController!.pushViewController(nextVC, animated: true)
        }
        
    }
    
}






