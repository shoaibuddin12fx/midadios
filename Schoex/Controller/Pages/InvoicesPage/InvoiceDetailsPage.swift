import UIKit
import SBFramework
import SwiftyJSON
import GSImageViewerController


class InvoiceDetailsPage: SolViewController,RenewTokenCallback {
    
    var dashboardJson : JSON?
    @IBOutlet weak var navLeftButton: UIBarButtonItem!
    let detailsWidget = WidgetCreator()
    let productsWidget = WidgetCreator()
    var invoiceID : String = ""
    var subjectsDictionary = [String: String]()
    let containerView = DashboardView()
    var currencySymbol = ""
    var amountTax = ""
    
    class func instantiateFromStoryboard() -> InvoiceDetailsPage {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "InvoiceDetailsPageStoryboard") as! InvoiceDetailsPage
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initNavBar()
        self.navLeftButton.image = Utils.getBackIcon()
        // nav bar title
        self.navigationItem.title = Utils.siteTitle
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }
   
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func loadView() {
        createInvoiceDetailsView()
        createProductsHeaderView()
        
        //=================== Create dashboard view ===================//
        containerView.injectView(mView: detailsWidget.createView())
        containerView.injectView(mView: productsWidget.createView())
        containerView.backgroundColor =  UIColor(patternImage: UIImage(named: "x_gen_back")!)
        containerView.createView()
        
        if(invoiceID != ""){
            detailsWidget.changeStatus(currentActiveView: .loading)
            productsWidget.changeStatus(currentActiveView: .loading)
            loadData()
        }else{
            detailsWidget.changeStatus(currentActiveView: .error)
        }

        // Set dashboard view to controller view
        self.view = containerView
    }
    
    func loadData() {
        
        let url = "\(Constants.getModRewriteBaseUrl())/invoices/invoice/\(invoiceID)"

        SBFramework.request(url, method: .get).responseJSON { response in
            switch response.result {
            case .success(let value):
                
                //======== Parse result ==========//
                let json = JSON(value)
                if (json["error"].null == nil){
                    if(!self.tokenRenewTried){ Utils.renewToken(callback : self,navigationController : self.navigationController!) }
                }
                print(json)
                
                if !(json.null != nil) {
                    
                    //Parse Currency Symbol
                    self.currencySymbol = Utils.getStringValueOf(sourceInput: json,itemKey: "currency_symbol")
                    self.amountTax = Utils.getStringValueOf(sourceInput: json,itemKey: "paymentTax")

                    //Parse Payment data
                    if let paymentData = json["payment"].dictionary {
                        
                        let header = (self.detailsWidget.headerViewContainer as! ViewsCreator1)
                        // Title
                        header.text1!.text = self.getStringFromDic(dic: paymentData, key: "paymentTitle")
                        // Description
                        header.text2!.text = self.getStringFromDic(dic: paymentData, key: "paymentDescription")
                        
                        _ = (self.detailsWidget.headerViewContainer as! ViewsCreator1).createView()

                        
                        // Status
                        let status = self.getStringFromDic(dic: paymentData, key: "paymentStatus")
                        print(status)
                        var statusString = "NA"
                        switch status {
                        case "0":
                            statusString = Language.getTranslationOf(findKey: "unpaid",defaultWord: "UNPAID")
                        case "1":
                            statusString = Language.getTranslationOf(findKey: "paid",defaultWord: "PAID")
                        case "2":
                            statusString = Language.getTranslationOf(findKey: "ppaid",defaultWord: "Partially Paid")
                        default:
                            statusString = "NA"
                        }
                        
                        (self.detailsWidget.dataViews![0] as! ViewsCreator1).text2!.text = "\(statusString)"
                        
                        // Date
                        if Utils.App_IsRtl{
                            (self.detailsWidget.dataViews![5] as! ViewsCreator1).text2!.text = "\(self.getStringFromDic(dic: paymentData, key: "dueDate")) - \(self.getStringFromDic(dic: paymentData, key: "paymentDate"))"
                            
                        }else{
                            (self.detailsWidget.dataViews![5] as! ViewsCreator1).text2!.text = "\(self.getStringFromDic(dic: paymentData, key: "paymentDate")) - \(self.getStringFromDic(dic: paymentData, key: "dueDate"))"
                            
                        }
                        
                        
                        //====================================== From User =====================//
                        
                        //===== From User FullName ======//
                        var fromData = ""
                        
                        let fromNameValue = Utils.getStringValueOf(sourceInput: json,itemKey: "siteTitle")
                        if(fromNameValue != "" ){
                            fromData += fromNameValue
                            fromData += "\n"
                        }
                        
                        //===== From User Phone ======//
                        let fromPhoneValue = Utils.getStringValueOf(sourceInput: json,itemKey: "phoneNo")
                        if(fromPhoneValue != "" ){
                            fromData += fromPhoneValue
                            fromData += "\n"
                        }
                        
                        //===== From User Email ======//
                        let fromEmailValue = Utils.getStringValueOf(sourceInput: json,itemKey: "systemEmail")
                        if(fromEmailValue != "" ){
                            fromData += fromEmailValue
                            fromData += "\n"
                        }
                        
                        //===== From User Address 1 ======//
                        let fromAddress1Value = Utils.getStringValueOf(sourceInput: json,itemKey: "address")
                        if(fromAddress1Value != "" ){
                            fromData += fromAddress1Value
                            fromData += "\n"
                        }
                        
                        //===== From User Address 2 ======//
                        let fromAddress2Value = Utils.getStringValueOf(sourceInput: json,itemKey: "address2")
                        if(fromAddress2Value != "" ){
                            fromData += fromAddress2Value
                            fromData += "\n"
                        }
                        
                        (self.detailsWidget.dataViews![6] as! ViewsCreator1).text2!.text = "\(fromData)"
                        
                        //Parse Products
                        if let payRows = paymentData["paymentRows"] {
                            if let paymentRowsArray = payRows.array {
                                //================= Fill data array ==================//
                                var dataMappedDictionary = [String: String]()
                                if paymentRowsArray.count > 0 {
                                    for item in paymentRowsArray {
                                        if let itemDic = item.dictionary {
                                            let title = self.getStringFromDic(dic: itemDic, key: "title")
                                            let amount = self.attachCurrencySymbol(value : self.getStringFromDic(dic: itemDic, key: "amount"))
                                            
                                            if title != Utils.Nil && amount != Utils.Nil {
                                                dataMappedDictionary[title] = amount
                                            }
                                        }
                                        
                                    }
                                    self.createProductsView(dataMappedDic: dataMappedDictionary)
                                    self.productsWidget.changeStatus(currentActiveView: .dataSet)
                                }else{
                                    self.productsWidget.changeStatus(currentActiveView: .empty)
                                }
                            }else{
                                self.productsWidget.changeStatus(currentActiveView: .empty)
                            }
                        }
                        
                        
                    }
                    
                    if let toUserData = json["user"].dictionary {
                        
                        //====================================== To User =====================//
                        
                        //===== To User FullName ======//
                        var toData = ""
                        
                        
                        let toNameValue = self.getStringFromDic(dic: toUserData, key: "fullName")
                        if(toNameValue != "" ){
                            toData += toNameValue
                            toData += "\n"
                        }
                        
                        //===== From User Phone ======//
                        let toPhoneValue = self.getStringFromDic(dic: toUserData, key: "phoneNo")
                        if(toPhoneValue != "" ){
                            toData += toPhoneValue
                            toData += "\n"
                        }
                        
                        //===== From User Email ======//
                        let toEmailValue = self.getStringFromDic(dic: toUserData, key: "email")
                        if(toEmailValue != "" ){
                            toData += toEmailValue
                            toData += "\n"
                        }
                        
                        //===== From User Address 1 ======//
                        let toAddress1Value = self.getStringFromDic(dic: toUserData, key: "address")
                        if(toAddress1Value != "" ){
                            toData += toAddress1Value
                            toData += "\n"
                        }
                        
                        //===== From User Class Name ======//
                        let toClassValue = self.getStringFromDic(dic: toUserData, key: "className")
                        if(toClassValue != "" ){
                            toData += toClassValue
                            toData += "\n"
                        }
                        
                        (self.detailsWidget.dataViews![7] as! ViewsCreator1).text2!.text = "\(toData)"
                        
                    }
                    
                    //Parse Prices data
                    if let pricesData = json["prices"].dictionary {
                        
                        
                        
                        (self.detailsWidget.dataViews![1] as! ViewsCreator1).text1!.text = "\(Language.getTranslationOf(findAndDefaultKey:"Sub total")) - \(Language.getTranslationOf(findKey: "payTax", defaultWord: "Payment Tax")) (\(self.amountTax)%)"
                        
                        // Sub total - Payment tax
                        
                        if Utils.App_IsRtl{
                           
                            (self.detailsWidget.dataViews![1] as! ViewsCreator1).text2!.text = "\(self.attachCurrencySymbol(value: self.getStringFromDic(dic: pricesData, key: "tax_value"))) - \(self.attachCurrencySymbol(value: self.getStringFromDic(dic: pricesData, key: "original")))"
                            
                            // Total - Discount
                            (self.detailsWidget.dataViews![2] as! ViewsCreator1).text2!.text = "\(self.attachCurrencySymbol(value: self.getStringFromDic(dic: pricesData, key: "discount"))) - \(self.attachCurrencySymbol(value: self.getStringFromDic(dic: pricesData, key: "total_with_tax")))"
                            
                            // Paid - Pending
                            (self.detailsWidget.dataViews![3] as! ViewsCreator1).text2!.text = "\(self.attachCurrencySymbol(value: self.getStringFromDic(dic: pricesData, key: "total_pending"))) - \(self.attachCurrencySymbol(value: self.getStringFromDic(dic: pricesData, key: "paid_value")))"
                        
                        }else{
                            
                            (self.detailsWidget.dataViews![1] as! ViewsCreator1).text2!.text = "\(self.attachCurrencySymbol(value: self.getStringFromDic(dic: pricesData, key: "original"))) - \(self.attachCurrencySymbol(value: self.getStringFromDic(dic: pricesData, key: "tax_value")))"
                            
                            // Total - Discount
                            (self.detailsWidget.dataViews![2] as! ViewsCreator1).text2!.text = "\(self.attachCurrencySymbol(value: self.getStringFromDic(dic: pricesData, key: "total_with_tax"))) - \(self.attachCurrencySymbol(value: self.getStringFromDic(dic: pricesData, key: "discount")))"
                            
                            // Paid - Pending
                            (self.detailsWidget.dataViews![3] as! ViewsCreator1).text2!.text = "\(self.attachCurrencySymbol(value: self.getStringFromDic(dic: pricesData, key: "paid_value"))) - \(self.attachCurrencySymbol(value: self.getStringFromDic(dic: pricesData, key: "total_pending")))"
                        }
                        
                    }
                    
                    
                    self.detailsWidget.changeStatus(currentActiveView: .dataSet)
                    
                    self.containerView.createView() // must recreate widgets container view as if you set label text with larger height will find widget overlap other one - must call it if you reset views after while like load data from internet - used if you use more than one widget in same page
                }else{
                    self.detailsWidget.changeStatus(currentActiveView: .error)
                }
            case .failure(let error):
                Utils.checkFailureCause(error : error, callback: self, navigationController: self.navigationController!,tokenRenewTried:self.tokenRenewTried)
                if self.tokenRenewTried {self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) }
                self.detailsWidget.changeStatus(currentActiveView: .error)
            }
            
        }
        
    }

    var tokenRenewTried = false
    func afterRenewToken(success: Bool) {
        tokenRenewTried = true
        if success {
            loadData()
        }else{
            self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
        }
    }
    
    @IBAction func navLeftButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func attachCurrencySymbol(value : String) -> String {
        return "\(self.currencySymbol) \(value)"
    }
    
    func getStringFromDic(dic : [String : JSON],key : String) -> String {
        if let value = dic[key] {
            if let itemValue = value.string {
                return itemValue
            }else{
                if value.number != nil {
                    if let itemValue = value.number {
                        return "\(itemValue)"
                    }else{
                        return "NA"
                    }
                }else{
                    return "NA"
                }
            }
        }else{
            return "NA"
        }
    }
    
    func createInvoiceDetailsView() {
        //=================== Create Views ===================//
        Defaults.setupDefaultWidgetStatus(widget: detailsWidget)
        detailsWidget.setupStatusViews(loadingText: Language.getTranslationOf(findKey: "loading",defaultWord: "Loading"), loadingImage: "no_data", emptyText: Language.getTranslationOf(findKey: "No Entry",defaultWord: "No details"), emptyImage: "no_data", errorText: Language.getTranslationOf(findKey: "errorOccurred",defaultWord: "Error Occurred"), errorImage: "no_data")
        
        //---------------------------- Header ---------------------------//
        let headerView = ViewsCreator1()
        
        headerView.isOn_text1 = true
        headerView.text1?.applyDashMainStyle(BiggerSizeMode : false)
            
        headerView.isOn_text2 = true
        headerView.text2?.applyDashSubStyle(BiggerSizeMode : false)
        headerView.text2?.bottomMargin = 12
        
        //---------------------------- Deatils ---------------------------//
        var detailsViews = [UIView]()


        //Invoice Status
        let statusView = ViewsCreator1()
        setupDetailsView(detailsView: statusView)
        statusView.text1!.text = "\(Language.getTranslationOf(findAndDefaultKey:"Status"))"
        statusView.text1!.setMargins(leftMargin: 0, topMargin: 0, rightMargin: 10, bottomMargin: 0)
        
        statusView.mainImage!.setSource(imageName: "icon_pages_notes", fromInternet: false)
        statusView.mainImage!.setMargins(leftMargin: 0, topMargin: 6, rightMargin: 10, bottomMargin: 10)
        statusView.mainImage!.setViewSize(overAllSize: 33)
        statusView.text2!.bottomMargin = 15
        statusView.text1!.topMargin = 6
        
        _ = statusView.createView()
        detailsViews.append(statusView)
        
        //Invoice Total Sub total & Pay tax
        let subTotalTaxView = ViewsCreator1()
        setupDetailsView(detailsView: subTotalTaxView)
        subTotalTaxView.text1!.text = "\(Language.getTranslationOf(findAndDefaultKey:"Sub total")) - \(Language.getTranslationOf(findAndDefaultKey:"Payment Tax"))"
        subTotalTaxView.text1!.setMargins(leftMargin: 0, topMargin: 0, rightMargin: 10, bottomMargin: 0)
        
        subTotalTaxView.isOn_mainImage = false
        subTotalTaxView.text2!.bottomMargin = 15
        
        _ = subTotalTaxView.createView()
        detailsViews.append(subTotalTaxView)
        
        //Invoice Total Amount - Discount
        let totalView = ViewsCreator1()
        setupDetailsView(detailsView: totalView)
        totalView.text1!.text = "\(Language.getTranslationOf(findAndDefaultKey:"Total")) - \(Language.getTranslationOf(findAndDefaultKey:"Discount"))"
        totalView.text1!.setMargins(leftMargin: 0, topMargin: 0, rightMargin: 10, bottomMargin: 0)
        
        totalView.isOn_mainImage = false
        totalView.text2!.bottomMargin = 15

        _ = totalView.createView()
        detailsViews.append(totalView)
        
        
        //Invoice Paid & Pending Amount
        let paidPendingView = ViewsCreator1()
        setupDetailsView(detailsView: paidPendingView)
        paidPendingView.text1!.text = "\(Language.getTranslationOf(findAndDefaultKey:"Paid Amount")) - \(Language.getTranslationOf(findAndDefaultKey:"Pending Amount"))"
        paidPendingView.text1!.setMargins(leftMargin: 0, topMargin: 0, rightMargin: 10, bottomMargin: 0)
        
        paidPendingView.isOn_mainImage = false
        paidPendingView.text2!.bottomMargin = 15

        _ = paidPendingView.createView()
        detailsViews.append(paidPendingView)
        
        
        //Invoice details header
        let invDetailsHeaderView = ViewsCreator1()
        
        invDetailsHeaderView.isOn_topDivider = true
        invDetailsHeaderView.isOn_bottomDivider = true
        invDetailsHeaderView.dividerColor = SolColors.getColorByName(colorName: "x_divider_color")!
        
        invDetailsHeaderView.isOn_text1 = true
        invDetailsHeaderView.text1!.text = Language.getTranslationOf(findKey: "invDetails", defaultWord: "Invoice Details")
        invDetailsHeaderView.text1?.applyDashMainStyle(BiggerSizeMode : false)
        invDetailsHeaderView.text1!.topMargin = 4
        invDetailsHeaderView.text1!.bottomMargin = 4

        _ = invDetailsHeaderView.createView()
        detailsViews.append(invDetailsHeaderView)
        
        //Invoice Date - Due date
        let dateDueDateView = ViewsCreator1()
        setupDetailsView(detailsView: dateDueDateView)
        dateDueDateView.text1!.text = "\(Language.getTranslationOf(findAndDefaultKey:"Date")) - \(Language.getTranslationOf(findAndDefaultKey:"Due Date"))"
        dateDueDateView.text1!.setMargins(leftMargin: 0, topMargin: 10, rightMargin: 20, bottomMargin: 0)
        dateDueDateView.text2!.setMargins(leftMargin: 0, topMargin: 0, rightMargin: 20, bottomMargin: 0)

        dateDueDateView.mainImage!.setSource(imageName: "icon_pages_date", fromInternet: false)
        dateDueDateView.mainImage!.setMargins(leftMargin: 0, topMargin: 6, rightMargin: 10, bottomMargin: 10)
        dateDueDateView.mainImage!.setViewSize(overAllSize: 33)
        dateDueDateView.mainImage?.topMargin = 15

        _ = dateDueDateView.createView()
        detailsViews.append(dateDueDateView)
        
        //Invoice From data
        let fromView = ViewsCreator1()
        setupDetailsView(detailsView: fromView)
        fromView.text1!.text = "\(Language.getTranslationOf(findAndDefaultKey:"From"))"
        fromView.text1!.setMargins(leftMargin: 0, topMargin: 0, rightMargin: 10, bottomMargin: 0)
        
        fromView.mainImage!.setSource(imageName: "x_login_logo", fromInternet: false)
        fromView.mainImage!.setMargins(leftMargin: 0, topMargin: 6, rightMargin: 10, bottomMargin: 10)
        
        _ = fromView.createView()
        detailsViews.append(fromView)
        
        //Invoice To data
        let toView = ViewsCreator1()
        setupDetailsView(detailsView: toView)
        toView.text1!.text = "\(Language.getTranslationOf(findAndDefaultKey:"To"))"
        toView.text1!.setMargins(leftMargin: 0, topMargin: 0, rightMargin: 10, bottomMargin: 0)
        
        toView.mainImage!.setSource(imageName: "icon_pages_user", fromInternet: false)
        toView.mainImage!.setMargins(leftMargin: 0, topMargin: 6, rightMargin: 10, bottomMargin: 10)
        toView.mainImage!.setViewSize(overAllSize: 33)

        _ = toView.createView()
        detailsViews.append(toView)
        
        
        detailsWidget.injectHeaderView(headerView: headerView, redrawWithEveryDrawCycle: false)
        detailsWidget.injectDataViews(dataViews: detailsViews) // Inject at anytime
        detailsWidget.prepareDataView() // Don't call untill you set data copies
    }
    
    func createProductsHeaderView() {
        //=================== Create Views ===================//
        Defaults.setupDefaultWidgetStatus(widget: productsWidget)
        
        productsWidget.setupStatusViews(loadingText: Language.getTranslationOf(findKey: "loading",defaultWord: "Loading"), loadingImage: "no_data", emptyText: Language.getTranslationOf(findKey: "No Entry",defaultWord: "No details"), emptyImage: "no_data", errorText: Language.getTranslationOf(findKey: "errorOccurred",defaultWord: "Error Occurred"), errorImage: "no_data")
        
        //Sch Header
        let productsHeaderView = ViewsCreator1()
        
        productsHeaderView.isOn_topDivider = true
        productsHeaderView.isOn_bottomDivider = true
        productsHeaderView.dividerColor = SolColors.getColorByName(colorName: "x_divider_color")!
        
        productsHeaderView.isOn_text1 = true
        productsHeaderView.text1!.text = Language.getTranslationOf(findAndDefaultKey:"Products")
        productsHeaderView.text1?.applyDashMainStyle(BiggerSizeMode : false)
        productsHeaderView.text1!.topMargin = 7
        
        _ = productsHeaderView.createView()
        
        productsWidget.injectHeaderView(headerView: productsHeaderView, redrawWithEveryDrawCycle: false)
    }
    
    func createProductsView(dataMappedDic : [String: String]) {
        //=================== Create Views ===================//
        var detailsViews = [UIView]()
        
        for (key, value) in dataMappedDic {
            let schView = ViewsCreator1()
            
            schView.isOn_text1 = true
            schView.text1!.applyDashMainStyle(BiggerSizeMode : false)
            schView.text1!.setMargins(leftMargin: 0, topMargin: 6, rightMargin: 10, bottomMargin: 4)
            schView.text1!.text = key
            
            schView.isOn_text2 = true
            schView.text2!.applyDashSubStyle(BiggerSizeMode : false)
            schView.text2!.setMargins(leftMargin: 0, topMargin: 0, rightMargin: 10, bottomMargin: 10)
            schView.text2!.text = value
            
            _ = schView.createView()
            detailsViews.append(schView)
        }
        
        productsWidget.injectDataViews(dataViews: detailsViews) // Inject at anytime
        productsWidget.prepareDataView() // Don't call untill you set data copies
    }
    
    func setupDetailsView(detailsView : ViewsCreator1) {
        detailsView.isOn_text1 = true
        detailsView.text1?.applyDashMainStyle(BiggerSizeMode : false)
        
        detailsView.isOn_text2 = true
        detailsView.text2?.applyDashSubStyle(BiggerSizeMode : false)
        detailsView.text2?.setMargins(leftMargin: 0, topMargin: 0, rightMargin: 10, bottomMargin: 0)

        detailsView.isOn_mainImage = true
        detailsView.mainImage!.setViewSize(overAllSize: 40)
    }
    
}


