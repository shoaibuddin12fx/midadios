import UIKit
import SBFramework

import SwiftyJSON
import GSImageViewerController
import XLPagerTabStrip


class ClassScheduleDetailsPage:SolViewController,UITableViewDataSource,UITableViewDelegate {
    

    @IBOutlet weak var navLeftButton: UIBarButtonItem!
    @IBOutlet weak var dataTableView: UITableView!
    
    class func instantiateFromStoryboard() -> ClassScheduleDetailsPage {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ClassScheduleDetailsPageStoryboard") as! ClassScheduleDetailsPage
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initTableView(tableView: dataTableView)
        initControllerStates()
        
        self.navLeftButton.image = Utils.getBackIcon()
        // nav bar title
        self.navigationItem.title = Utils.siteTitle;
        setupInitialViewState()
        
//        self.dataTableView.transform = CGAffineTransform(rotationAngle: .pi * 2);
//        dataTableView.transform =  CGAffineTransform(rotationAngle: .pi * 2);
        
    }

    
    func initControllerStates() {
        let failureView = ErrorView(frame: view.frame)
        super.initControllerStates(failureView: failureView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
//            print("reloaded");
            
//        if(Utils.App_IsRtl){
//            dataTableView.semanticContentAttribute = .forceRightToLeft
//        }else{
//            dataTableView.semanticContentAttribute = .forceLeftToRight
//        }
            
            
//            self.dataTableView.reloadData()
            
//        })
        
        
        view.backgroundColor = UIColor.clear
        view.isOpaque = false
        
        
        
    }

    //==================== Table view options ===================================//
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:ClassScheduleDetailsCell = tableView.dequeueReusableCell(withIdentifier: "cellCon", for: indexPath) as! ClassScheduleDetailsCell
        cell.item = dataArray[indexPath.row] as! ClassesSchModel;
        
        // cell allignment
        
//        if(Utils.App_IsRtl){
//            cell.semanticContentAttribute = .forceRightToLeft
//        }else{
//            cell.semanticContentAttribute = .forceLeftToRight
//        }
        
        
        return cell
    }

  
}


