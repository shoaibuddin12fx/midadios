//
//  AttendancePageViewController.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/29/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SwiftyJSON
import SBFramework

import Eureka
import TransitionButton

class ClassScheduleChoosePage: FormViewController {
    
    @IBOutlet weak var navLeftButton: UIBarButtonItem!
    
    // in case sections enabled
    var classesNamesArray = [String]()
    var classesSectionsDict = [String:[String : Int]]()
    var selectedFormSection:Int?
    var activeSectionsDic = [String : Int]()

    // in case sections disabled
    var classesDict = [String:Int]()
    var selectedFormClass:Int? // class id

    var NilSelect = "No Select"

    var transitionButton:TransitionButton?
    //============================ Base Functions =================//
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initNavBar()
        initSideMenu()
        
        initPage()
        
        
    }
   
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //====================== Nav Bar Common Options ========================//
    func initNavBar (){
        showNavigationBar()
        
        navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController!.navigationBar.shadowImage = UIImage()
        
        // navigation title color
        navigationController!.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : SolColors.getColorByName(colorName: "x_header_text")!]
        
        // navigation bar
        UINavigationBar.appearance().backgroundColor = SolColors.getColorByName(colorName: "x_header_back")
        
        // status bar
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        statusBarView.backgroundColor = SolColors.getColorByName(colorName: "x_header_back")
        view.addSubview(statusBarView)
        

    }
    
    @IBAction func navLeftButtonAction(_ sender: Any) {
        showMenu()
    }
    
    //====================== Side Menu Common Options ========================//
    func initSideMenu (){
        
        if Utils.App_IsRtl{
            SideMenuManager.menuRightNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        }else{
            SideMenuManager.menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        }
        
        
        SideMenuManager.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        
        //SideMenuManager.menuAnimationBackgroundColor = UIColor(patternImage: UIImage(named: "background")!)
    }
    
    func showMenu() {
        if Utils.App_IsRtl{
            present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
        }else{
            present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
        }
    }
    func hideMenu() {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (Utils.App_IsRtl){
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }
        
    }
    
    //============================ Init Functions =================//
    
    func initPage() {
        
        if Utils.App_IsSectionsEnabled{
            loadClassesSections()
        }else{
            loadClasses()
        }
        createForm()
        
        // nav bar title
        self.navigationItem.title = Utils.siteTitle
    }
    
    //============================ Load data =================//

    func loadClasses() {
        let url = "\(Constants.getModRewriteBaseUrl())/classschedule/listAll"
        
        
        SBFramework.request(url, method: .get).responseJSON { response in
            switch response.result {
            case .success(let value):
                
                //======== Parse result ==========//
                let json = JSON(value)
                if let jsonArray = json["classes"].array {
                    
                    if(jsonArray.count > 0){
                        
                        //parse classes
                        let classesPickerItem = self.getPickerItem(tag: "classes")
                        classesPickerItem.options.removeAll()
                        for item in jsonArray {
                            let classTitle = item["className"].string!
                            self.classesDict[classTitle] = item["id"].int!
                            classesPickerItem.options.append(classTitle)
                        }
                        
                        // Select first item and reload
                        classesPickerItem.value = classesPickerItem.options.first!
                        self.selectedFormClass = self.classesDict[classesPickerItem.options.first!]
                        
                        // Attach listener in case change selected item
                        classesPickerItem.onChange{ row in
                            self.selectedFormClass = self.classesDict[row.value!]
                        }
                        classesPickerItem.reload()
                    }
                }
            case .failure(let error):
                print(error)
                break
            }
            
        }
    }
    
    func loadClassesSections() {
            let url = "\(Constants.getModRewriteBaseUrl())/classschedule/listAll"
        
            SBFramework.request(url, method: .get).responseJSON { response in
                switch response.result {
                case .success(let value):
                    
                    //======== Parse result ==========//
                    let json = JSON(value)
                    
                    //parse classes - sections
                    let sectionsDic = json["sections"].dictionary
                    let classesPickerItem = self.getPickerItem(tag: "classes")
                    
                    if(sectionsDic != nil && sectionsDic!.count > 0){
                        for (className,subJson):(String, JSON) in sectionsDic! {
                            self.classesNamesArray.append(className)
                            if let sectionsArray = subJson.array {
                                var inSectionsDict = [String:Int]()
                                for sectionObject in sectionsArray {
                                    inSectionsDict["\(Utils.getStringValueOf(sourceInput: sectionObject, itemKey: "sectionName")) - \(Utils.getStringValueOf(sourceInput: sectionObject, itemKey: "sectionTitle"))"] = Utils.getIntValueOf(sourceInput: sectionObject, itemKey: "id")
                                }
                                self.classesSectionsDict[className] = inSectionsDict
                            }
                            classesPickerItem.options.append(className)
                        }
                        
                        classesPickerItem.value = classesPickerItem.options.first!
                        self.setSections(className: classesPickerItem.options.first!)
                        classesPickerItem.reload()
                        
                        self.getPickerItem(tag: "sections").onChange{ row in
                            self.selectedFormSection = self.activeSectionsDic[row.value!]
                        }
                        
                        classesPickerItem.onChange{ row in
                            self.setSections(className: row.value!)
                        }
                    }
                case .failure(let error):
                    print(error)
                    break
                }
                
            }
        
    }
    
    func setSections(className : String) {
        let sectionsPickerItem = self.getPickerItem(tag: "sections")
        
        if(sectionsPickerItem.options.count > 0){
            sectionsPickerItem.options.removeAll()
        }
        
        if let sectionDic = self.classesSectionsDict[className] {
            self.activeSectionsDic = sectionDic
            if sectionDic.count > 0{
                for (sectionNameValue,_):(String,Int) in sectionDic {
                    sectionsPickerItem.options.append(sectionNameValue)
                }
                sectionsPickerItem.value = sectionsPickerItem.options.first
                self.selectedFormSection = self.activeSectionsDic[sectionsPickerItem.options.first!]
            }else{
                sectionsPickerItem.value = self.NilSelect
            }
        }else{
            sectionsPickerItem.value = self.NilSelect
        }
        
        sectionsPickerItem.reload()
    }
    //============================ Form manage functions =================//
    
    func createForm() {
        
        if Utils.App_IsSectionsEnabled {
            form
                +++ Section()
                <<< PickerInputRow<String>("classes"){
                        $0.title = Language.getTranslationOf(findAndDefaultKey:"Classes")
                    }.cellSetup { cell, row in
                        cell.backgroundColor = .clear
                        cell.textLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
                        cell.detailTextLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
                        cell.detailTextLabel?.font = UIFont(name: "Cairo", size: cell.detailTextLabel?.font.pointSize ?? 16)
                    }.cellUpdate{ cell, row in
                        cell.textLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
                        cell.textLabel?.font = UIFont(name: "Cairo", size: cell.textLabel?.font.pointSize ?? 16)
                    }
                <<< PickerInputRow<String>("sections"){
                    $0.title = Language.getTranslationOf(findAndDefaultKey:"Sections")
                    }.cellSetup { cell, row in
                        cell.backgroundColor = .clear
                        cell.textLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
                        cell.detailTextLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
                        cell.detailTextLabel?.font = UIFont(name: "Cairo", size: cell.detailTextLabel?.font.pointSize ?? 16)
                    }.cellUpdate{ cell, row in
                        cell.textLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
                        cell.textLabel?.font = UIFont(name: "Cairo", size: cell.textLabel?.font.pointSize ?? 16)
                    }
        }else{
            form
                +++ Section()
                <<< PickerInputRow<String>("classes"){
                    $0.title = "Classes"
                    }.cellSetup { cell, row in
                        cell.backgroundColor = .clear
                        cell.textLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
                        cell.detailTextLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
                        cell.detailTextLabel?.font = UIFont(name: "Cairo", size: cell.detailTextLabel?.font.pointSize ?? 16)
                    }.cellUpdate{ cell, row in
                        cell.textLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
                        cell.textLabel?.font = UIFont(name: "Cairo", size: cell.textLabel?.font.pointSize ?? 16)
                    }
        }

        form
            +++ Section() {
                var header = HeaderFooterView<CustomButton>(.nibFile(name: "CustomButton", bundle: nil))
                header.onSetupView = { (view, section) -> () in
                    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.proccessBtnPressed))
                    view.transitionBtn.addGestureRecognizer(tapGesture)
                    self.transitionButton = view.transitionBtn
                }
                $0.header = header
            }
        
        self.tableView?.separatorStyle = .none
        self.tableView?.backgroundColor = UIColor.clear
    }
    
    @objc func proccessBtnPressed()  {
        self.transitionButton!.startAnimation() // 2: Then start the animation when the user tap the button
        let qualityOfServiceClass = DispatchQoS.QoSClass.background
        let backgroundQueue = DispatchQueue.global(qos: qualityOfServiceClass)
        backgroundQueue.async(execute: {
            self.getSchData()
        })
    }
    
    func getSchData() {
        let url : String?
        if Utils.App_IsSectionsEnabled{
            url = "\(Constants.getModRewriteBaseUrl())/classschedule/\(self.selectedFormSection ?? 0)"
        }else{
            url = "\(Constants.getModRewriteBaseUrl())/classschedule/\(self.selectedFormClass ?? 0)"
        }
        
 
        SBFramework.request(url!, method: .get).responseJSON { response in
            switch response.result {
            case .success(let value):
                
                //======== Parse result ==========//
                let json = JSON(value)
                
                if json.null == nil{

                    var schDataArray = [Int : (dayName : String,schData : [ClassesSchModel])]()
                    
                    if let scheduleDic = json["schedule"].dictionary {
                        for(dayNumber,schItem) in scheduleDic{
                            let dayName = Utils.getStringValueOf(sourceInput: schItem, itemKey: "dayName")
                            
                            var oneDaySchArray = [ClassesSchModel]()
                            if let subArray = schItem["sub"].array {
                                
                                for subItem in subArray {
                                    oneDaySchArray.append(
                                        ClassesSchModel(
                                            id : Utils.getIntValueOf(sourceInput: subItem,itemKey:"id") ,
                                            dayName : dayName,
                                            sectionId : Utils.getIntValueOf(sourceInput: subItem,itemKey:"sectionId")  ,
                                            subjectId : Utils.getStringValueOf(sourceInput: subItem,itemKey:"subjectId"),
                                            startPeriod : Utils.getStringValueOf(sourceInput: subItem,itemKey:"start"),
                                            endPeriod : Utils.getStringValueOf(sourceInput: subItem,itemKey:"end")
                                        )
                                    )
                                }
                            }
                            schDataArray[Int(dayNumber)!] = (dayName : dayName,schData : oneDaySchArray)
                        }
                    }
                    
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.transitionButton!.stopAnimation(animationStyle: .expand, completion: {
                            
                            //self.initTabs(schDataArray : schDataArray)
                            let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "ClassSchedulePageStoryboard") as! ClassSchedulePage
                            
                            nextVC.schDataArray = schDataArray

                            self.navigationController!.pushViewController(nextVC, animated: true)
                            
                            
                        })
                    })
                    
                }else{
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.transitionButton!.stopAnimation(animationStyle: .shake, completion: {})
                    })
                }
            case .failure(let error):
                DispatchQueue.main.async(execute: { () -> Void in
                    self.transitionButton!.stopAnimation(animationStyle: .shake, completion: {})
                })
                print(error)
            }
            
        }
    }
    
    
    func getPickerItem(tag:String) -> PickerInputRow<String> {
        return (form.rowBy(tag: tag) as! PickerInputRow)
    }
    func getLabelItem(tag:String) -> LabelRow {
        return (form.rowBy(tag: tag) as! LabelRow)
    }
 
}




