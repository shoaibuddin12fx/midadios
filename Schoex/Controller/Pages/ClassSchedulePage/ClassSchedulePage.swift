//
//  ViewController.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/15/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework
import SwiftyJSON
import XLPagerTabStrip
import PagingMenuController

class ClassSchedulePage : SolViewController {
    
    @IBOutlet weak var navLeftButton: UIBarButtonItem!
    
    var schDataArray = [Int : (dayName : String,schData : [ClassesSchModel])]()
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //============================ Base Functions =================//
    override func viewDidLoad() {
        super.viewDidLoad()

        initNavBar()

        initPage()
        
        if(schDataArray.count > 0){
            initTabs()
        }

        // nav bar title
        self.navigationItem.title = Utils.siteTitle
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showNavigationBar()
        UIView.appearance().semanticContentAttribute = .forceLeftToRight

    }
    
    func initTabs()  {
        let pagingMenuController = self.childViewControllers.first as! PagingMenuController
        let options = segmentOptions(schDataArray: schDataArray)
        let attributes = [NSAttributedString.Key.font: UIFont(name: "Cairo-Regular", size: 17)!]
        pagingMenuController.navigationController?.navigationBar.titleTextAttributes = attributes
        pagingMenuController.setup(options)
    }
    
    
    struct segmentOptions: PagingMenuControllerCustomizable {
        var schDataArray = [Int : (dayName : String,schData : [ClassesSchModel])]()

        var leaderboardViewController: DashboardLeaderboardPage = DashboardLeaderboardPage.instantiateFromStoryboard()
        var statsViewController: DashboardStatsPage = DashboardStatsPage.instantiateFromStoryboard()
        var newsViewController: DashboardNewsPage = DashboardNewsPage.instantiateFromStoryboard()
        
        var pagesControllers =  [UIViewController]()
        
        init(schDataArray: [Int : (dayName : String,schData : [ClassesSchModel])] ) {
            
            self.schDataArray = schDataArray
            
            var i = 1
            for _ in self.schDataArray {
                if let schItem = schDataArray[i]{
                    let classScheduleDetailsPage:ClassScheduleDetailsPage =  ClassScheduleDetailsPage.instantiateFromStoryboard()
                    classScheduleDetailsPage.dataArray = schItem.schData
                    pagesControllers.append(classScheduleDetailsPage)
                }
                i = i + 1
            }
            
        }
        
        var componentType: ComponentType {
            return .all(menuOptions: MenuOptions(schDataArray : self.schDataArray), pagingControllers: pagesControllers)
        }
        var menuControllerSet: MenuControllerSet {
            return .multiple
        }
        
        var backgroundColor: UIColor {
            return UIColor.clear
        }
        
        struct MenuOptions: MenuViewCustomizable {
            var schDataArray = [Int : (dayName : String,schData : [ClassesSchModel])]()

            init(schDataArray: [Int : (dayName : String,schData : [ClassesSchModel])]) {
                self.schDataArray = schDataArray
            }
            
            
            var displayMode: MenuDisplayMode {
                return .standard(widthMode: .flexible, centerItem: false, scrollingMode: .pagingEnabled)
            }

            var itemsOptions: [MenuItemViewCustomizable] {
                
                var menuOpts = [MenuItemViewCustomizable]()
                var i = 1
                
                for _ in self.schDataArray {
                    if let schItem = schDataArray[i]{
                        
                        let dayName = schItem.dayName
                        var findName = dayName
                        if dayName == "Thursday"{
                            findName = "Thurusday"
                        }
                        
                        struct MenuItemOpt: MenuItemViewCustomizable {
                            var dayName = ""
                            var findName = ""
                            
                            init(findName: String, dayName : String) {
                                self.dayName = dayName
                                self.findName = findName
                            }
                            var displayMode: MenuItemDisplayMode {
                                let title = MenuItemText(
                                    text : Language.getTranslationOf(findKey: findName,defaultWord: dayName),
                                    color : SolColors.getColorByName(colorName: "x_gen_main_text")!,
                                    selectedColor : SolColors.getColorByName(colorName: "x_gen_sub_title_text")!,
                                    font: UIFont(name:"Cairo",size:19)!,
                                    selectedFont: UIFont(name:"Cairo",size:19)!
                                )
                                return .text(title: title)
                            }
                            
                        }
                        menuOpts.append(MenuItemOpt(findName: findName,dayName : dayName))
                    }
                    
                    i = i + 1
                }
                return menuOpts
            }
            
            var height: CGFloat {
                return 50
            }
            
            var backgroundColor: UIColor {
                return UIColor.clear
            }
            var selectedBackgroundColor: UIColor {
                return SolColors.getColorByName(colorName: "transparent")!
            }
            var focusMode: MenuFocusMode {
                return .underline(height: 2, color: SolColors.getColorByName(colorName: "x_dash_top_tabs_icons")!, horizontalPadding: 2, verticalPadding: 2)
            }
        }
        
        
        
    }
   
    //============================ Init Functions =================//

    func initPage() {
        self.navLeftButton.image = Utils.getBackIcon()
        // nav bar title
        self.navigationItem.title = Utils.siteTitle
    }

    //==================== Nav Bar Buttons Actions ===================================//
   
    @IBAction func navLeftButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}






