//
//  ViewController.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/15/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework
import SwiftyJSON
import FileBrowser

class AssignmentsPage:SolViewController,UITableViewDataSource,UITableViewDelegate,RenewTokenCallback {
    
    @IBOutlet weak var dataTableView: UITableView!
    @IBOutlet weak var navLeftButton: UIBarButtonItem!

    var searchMode:Bool = false

    var page : Int = 1
    var loadMoreLocked : Bool = false
    
    //============================ Base Functions =================//
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initTableView(tableView: dataTableView)
        initNavBar()
        initSideMenu()
        initControllerStates()
        
        initPage()

    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupInitialViewState()
        
    }
    
    //============================ Init Functions =================//

    func initPage() {
        if searchAbout != nil {
            searchMode = true
            self.navLeftButton.image = Utils.getBackIcon()
        }else{
            searchMode = false
            self.navLeftButton.image = UIImage(named: "icn_drawer")
        }
        self.loadData(isLoadMore: false)
        
        // nav bar title
        self.title = Utils.siteTitle
        
        // Add button action
        if !Utils.isUserHavePermission(checkThisModules: ["Assignments.AddAssignments"]){
            // remove button action
            self.navigationItem.rightBarButtonItem = nil
        }
    }
    
    func initControllerStates() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(failLoadData))
        super.initControllerStates(failureView: failureView)
    }
    
    @objc func failLoadData() {
        loadData(isLoadMore : false)
    }
    
    //==================== Load data ===================================//
    @objc func loadData(isLoadMore : Bool) {
        if(!isLoadMore){
            self.page = 1
            self.loadMoreLocked = false
            self.dataArray.removeAll()
            self.dataTableView.reloadData()
            startLoading()
        }else{
            if(loadMoreLocked){return}
        }
        
        let url = "\(Constants.getModRewriteBaseUrl())/assignments/listAll/\(self.page)"
        
        let builder : DataRequest
        if(searchMode && searchAbout != nil){
            let searchParameters:Parameters = [
                "searchInput": searchAbout!
            ]
            builder = SBFramework.request(url, method: .post,parameters: searchParameters)
        }else{
            builder = SBFramework.request(url, method: .get)
        }
        
        builder.responseJSON { response in
            switch response.result {
            case .success(let value):
                
                //======== Parse result ==========//
                let json = JSON(value)
                if (json["error"].null == nil){
                    if(!self.tokenRenewTried){ Utils.renewToken(callback : self,navigationController : self.navigationController!) }
                }
                if let jsonArray = json["assignments"].array {
                    
                    //================= Fill data array ==================//
                    for item in jsonArray {
                        self.dataArray.append(AssignmentsModel(
                            id : Utils.getIntValueOf(sourceInput: item,itemKey:"id") ,
                            AssignTitle : Utils.getStringValueOf(sourceInput: item,itemKey:"AssignTitle") ,
                            AssignDescription : Utils.getStringValueOf(sourceInput: item,itemKey:"AssignDescription") ,
                            AssignDeadLine : Utils.getStringValueOf(sourceInput: item,itemKey:"AssignDeadLine") ,
                            AssignFile : Utils.getStringValueOrNilOf(sourceInput: item,itemKey:"AssignFile")
                            )
                        )
                    }
                    
                    if(isLoadMore){
                        if(jsonArray.count == 0){
                            self.loadMoreLocked = true
                        }
                    }
                    self.endLoading(error: nil) // Set Content
                    self.dataTableView.reloadData()
                    self.page = self.page + 1
                    

                }else{
                    self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
                }
            case .failure(let error):
                Utils.checkFailureCause(error : error, callback: self, navigationController: self.navigationController!,tokenRenewTried:self.tokenRenewTried)
                if self.tokenRenewTried {self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) }
            }
        }
    }
    var tokenRenewTried = false
    func afterRenewToken(success: Bool) {
        tokenRenewTried = true
        if success {
            loadData(isLoadMore: false)
        }else{
            self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
        }
    }
    
    //==================== Nav Bar Buttons Actions ===================================//

    @IBAction func reloadPage(_ sender: Any) {
        self.dataArray.removeAll()
        loadData(isLoadMore: false)
    }
    
    @IBAction func searchButtonAction(_ sender: Any) {
        showSearchDialog(targetStoryboard: "AssignmentsPageStoryboard")
    }
    
    @IBAction func navOpenExplorer(_ sender: Any) {
        let fileBrowser = FileBrowser()
        self.present(fileBrowser, animated: true, completion: nil)
    }
    
    @IBAction func navLeftButtonAction(_ sender: Any) {
        if(self.searchMode){
            self.navigationController?.popViewController(animated: true)
        }else{
            showMenu()
        }
    }
    
    @IBAction func navAddButtonAction(_ sender: Any) {
        if Utils.isUserHavePermission(checkThisModules: ["Assignments.AddAssignments"]){
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let nextVC = storyboard.instantiateViewController(withIdentifier: "AssignmentsAddPageStoryboard") as! AssignmentsAddPage
            self.navigationController!.pushViewController(nextVC, animated: true)
        }else{
            Utils.showMsg(msg: Language.getTranslationOf(findKey: "no_access",defaultWord: "You have no access to do this, please contact adminstrator"))
        }
        
    }
    
   
    //==================== Table view options ===================================//
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = dataArray.count - 1
        if indexPath.row == lastElement {
            self.loadData(isLoadMore: true)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:AssignmentsCell = tableView.dequeueReusableCell(withIdentifier: "cellCon", for: indexPath) as! AssignmentsCell
        cell.navController = self.navigationController
        cell.item = dataArray[indexPath.row] as! AssignmentsModel
        return cell
    }
    
    
}






