//
//  AttendancePageViewController.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/29/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SwiftyJSON
import SBFramework
import Alamofire
import Eureka
import TransitionButton

class AssignmentsAddPage : FormViewController,DatePickerProtocol,UIDocumentMenuDelegate,UIDocumentPickerDelegate,UINavigationControllerDelegate {
    
    
    @IBOutlet weak var navLeftButton: UIBarButtonItem!
    var classesDict = [String:Int]()
    var subjectsDict = [String:Int]()
    var sectionsDict = [String:Int]()

    var selectedFileURL:URL!
    var selectedFormSubject:String = ""
    var selectedFormDate:String = ""
    var NilSelect = "No Select"
    var selectedAssignTitle:String = ""
    var selectedAssignDescription:String = ""

    var transitionButton:TransitionButton?
    //============================ Base Functions =================//
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initNavBar()
        initSideMenu()
        
        initPage()
        
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //====================== Nav Bar Common Options ========================//
    func initNavBar (){
        showNavigationBar()
        
        navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController!.navigationBar.shadowImage = UIImage()
        
        // navigation title color
        navigationController!.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : SolColors.getColorByName(colorName: "x_header_text")!]
        
        // navigation bar
        UINavigationBar.appearance().backgroundColor = SolColors.getColorByName(colorName: "x_header_back")
        
        // status bar
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        statusBarView.backgroundColor = SolColors.getColorByName(colorName: "x_header_back")
        view.addSubview(statusBarView)
        
        self.navLeftButton.image = Utils.getBackIcon()

    }
    
    @IBAction func navLeftButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //====================== Side Menu Common Options ========================//
    func initSideMenu (){
        
        if Utils.App_IsRtl{
            SideMenuManager.menuRightNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        }else{
            SideMenuManager.menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        }
        
        SideMenuManager.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    //============================ Init Functions =================//
    
    func initPage() {
        loadClasses()
        createForm()
        
        
        // nav bar title
        self.navigationItem.title = Utils.siteTitle
    }
    
    //============================ Load data =================//

    func loadClasses() {
        let url = "\(Constants.getModRewriteBaseUrl())/assignments/listAll"
        
        SBFramework.request(url, method: .get).responseJSON { response in
            switch response.result {
            case .success(let value):
                
                //======== Parse result ==========//
                let json = JSON(value)
                if let jsonArray = json["classes"].array {
                    
                    //parse classes
                    let classesPickerItem = self.getMultiplePickerItem(tag: "classes")
                    classesPickerItem.options?.removeAll()

                    if(jsonArray.count > 0){
                        
                        for item in jsonArray {
                            let classTitle = item["className"].string!
                            print(classTitle)
                            self.classesDict[classTitle] = item["id"].int!
                            classesPickerItem.options?.append(classTitle)
                        }
                        
                        // Select first item and reload
                        classesPickerItem.value = [(classesPickerItem.options?.first)!]
                        classesPickerItem.reload()
                        self.loadSubjectsSections()
                        
                        // Attach listener in case change selected item
                        classesPickerItem.onChange{ row in
                            
                            self.loadSubjectsSections()
                            
                            // clear sections and subjects
                            if Utils.App_IsSectionsEnabled{
                                self.getMultiplePickerItem(tag: "sections").options?.removeAll()
                                self.getMultiplePickerItem(tag: "sections").value = []
                                self.getMultiplePickerItem(tag: "sections").reload()
                                self.sectionsDict.removeAll()
                            }
                            self.getPickerItem(tag: "subjects").options.removeAll()
                            self.getPickerItem(tag: "subjects").value = self.NilSelect
                        }
                    }
                }
            case .failure(let error):
                print(error)
                break
            }
            
        }
    }
    
    func loadSubjectsSections() {
        //====== Vars =======//
        var parameters: Parameters = Parameters()
        var count = 0
        var sectionsTitles = [String]()

        let url = "\(Constants.getModRewriteBaseUrl())/dashboard/sectionsSubjectsList"
        
        //====== Get picker items =======//
        let sectionsPickerItem = self.getMultiplePickerItem(tag: "sections")
        let classesPickerItem = self.getMultiplePickerItem(tag: "classes")
        
        //====== If no classes => Return =======//
        if(classesPickerItem.value == nil){
            return
        }
        
        //====== Prepare classes =======//
        for className in classesPickerItem.value!{
            parameters["classes[\(count)]"]  = self.classesDict[className]!
            count = count + 1
        }
        
        //====== Fetch sections and subjects =======//
        if (classesPickerItem.value?.count)! > 0 {
            
            SBFramework.request(url, method: .post, parameters: parameters).responseJSON { response in
                
                switch response.result {
                
                    case .success(let value):
                        
                        //======== Parse result ==========//
                        let json = JSON(value)
                        
                        // Parse sections
                        if Utils.App_IsSectionsEnabled {
                            if let jsonArray = json["sections"].array {
                                
                                if jsonArray.count > 0{
                                    
                                    // Loop on sections
                                    for item in jsonArray {
                                        let sectionTitle = "\(Utils.getStringValueOf(sourceInput: item, itemKey: "sectionName")) - \(Utils.getStringValueOf(sourceInput: item, itemKey: "sectionTitle"))"
                                        self.sectionsDict[sectionTitle] = Utils.getIntValueOf(sourceInput: item, itemKey: "id")
                                        sectionsTitles.append(sectionTitle)
                                    }
                                    
                                    // Set sections
                                    sectionsPickerItem.optionsProvider = .lazy({ (form, completion) in
                                        completion(sectionsTitles)
                                    })
                                    sectionsPickerItem.reload()
                                }
                            }
                        }
                        
                        
                        // Parse subjects
                        if let jsonArray = json["subjects"].array {
                            
                            let subjectsPickerItem = self.getPickerItem(tag: "subjects")
                            
                            if jsonArray.count > 0{
                                
                                for item in jsonArray {
                                    let subjectTitle = Utils.getStringValueOf(sourceInput: item, itemKey: "subjectTitle")
                                    self.subjectsDict[subjectTitle] = Utils.getIntValueOf(sourceInput: item, itemKey: "id")
                                    subjectsPickerItem.options.append(subjectTitle)
                                }
                                
                                // Select first item and reload
                                subjectsPickerItem.value = subjectsPickerItem.options.first
                                self.selectedFormSubject = subjectsPickerItem.options.first!
                                
                                subjectsPickerItem.onChange{ row in
                                    self.selectedFormSubject = row.value!
                                }
                            }else{
                                subjectsPickerItem.value = self.NilSelect
                            }
                            subjectsPickerItem.reload()
                        }
                    
                    case .failure(let error):
                            print(error)
                        break
                    }
                }
        }
    }
    
    //============================ Form manage functions =================//
    
    func createForm() {
        let classesRow = MultipleSelectorRow<String>("classes"){
                $0.title = Language.getTranslationOf(findKey: "classes",defaultWord: "Classes")
                $0.options = []
            }.cellSetup { cell, row in
                cell.backgroundColor = .clear
                cell.textLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
                cell.detailTextLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
            }.cellUpdate{ cell, row in
                cell.textLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
            }

        
        let sectionsItem = MultipleSelectorRow<String>("sections"){
                $0.title = Language.getTranslationOf(findKey: "sections",defaultWord: "Sections")
                $0.options = []
                $0.hidden = Condition.function(["sections"], { form in
                    return !Utils.App_IsSectionsEnabled
                })
            }.cellSetup { cell, row in
                cell.backgroundColor = .clear
                cell.textLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
                cell.detailTextLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
            }.cellUpdate{ cell, row in
                cell.textLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
            }
        
        let subjectsItem = PickerInputRow<String>("subjects") { row in
                row.title = Language.getTranslationOf(findKey: "subjects",defaultWord: "Subjects")
            }.cellSetup { cell, row in
                cell.backgroundColor = .clear
                cell.textLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
                cell.detailTextLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
            }.cellUpdate{ cell, row in
                cell.textLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
            }
        
        let dateItem = LabelRow ("AssignDeadLine") {
            $0.title = Language.getTranslationOf(findKey: "date",defaultWord: "Date")
            $0.value = NilSelect
            }.onCellSelection { cell, row in
                self.openDatePicker()
            }.cellSetup { cell, row in
                cell.backgroundColor = .clear
                cell.textLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
                cell.detailTextLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
            }.cellUpdate{ cell, row in
                cell.textLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
            }
        
        
        let titleItem = TextRow("AssignTitle").cellSetup { cell, row in
            cell.textField.text = Language.getTranslationOf(findKey: "AssignmentTitle",defaultWord: "Assignment Title")

            }.cellSetup { cell, row in
                cell.backgroundColor = SolColors.getColorByName(colorName: "trans_white")
                cell.textField.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
            }.cellUpdate{ cell, row in
                cell.textField.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
            }.onChange{ row in
                self.selectedAssignTitle = row.cell.textField.text!
                print(self.selectedAssignTitle)
            }
        
        let descriptionItem = TextAreaRow("AssignDescription") {
            $0.title = Language.getTranslationOf(findKey: "AssignmentDescription",defaultWord: "Assignment Description")
            }.cellSetup { cell, row in
                cell.backgroundColor = SolColors.getColorByName(colorName: "trans_white")
                cell.textView.backgroundColor = .clear
                cell.textView.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
                
            }.cellUpdate{ cell, row in
                cell.textView.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
            }.onChange{ row in
                self.selectedAssignDescription = row.cell.textView.text
                print(self.selectedAssignDescription)
            }
        
        // ===== Title of Assignment Title ======//
        let assignmentsTitle = LabelRow(){
            $0.title = Language.getTranslationOf(findKey: "AssignmentTitle",defaultWord: "Assignment Title")
            }.cellSetup { cell, row in
                cell.backgroundColor = .clear
                cell.textLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_sub_title_text")
                cell.detailTextLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_sub_title_text")
                cell.textLabel?.font = UIFont.systemFont(ofSize: 15)
            }.cellUpdate{ cell, row in
                cell.textLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_sub_title_text")
                cell.textLabel?.font = UIFont.systemFont(ofSize: 15)
        }
        
        // ===== Title of Assignment Description ======//
        let assignmentsDescription = LabelRow(){
                $0.title = Language.getTranslationOf(findKey: "AssignmentDescription",defaultWord: "Assignment Description")
            }.cellSetup { cell, row in
                cell.backgroundColor = .clear
                cell.textLabel?.backgroundColor = .clear
                cell.detailTextLabel?.backgroundColor = .clear

                cell.textLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_sub_title_text")
                cell.detailTextLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_sub_title_text")
                cell.textLabel?.font = UIFont.systemFont(ofSize: 15)
            }.cellUpdate{ cell, row in
                cell.textLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_sub_title_text")
                cell.textLabel?.font = UIFont.systemFont(ofSize: 15)
            }
        
        // ===== Title of File Picker ======//
        let filePicker = LabelRow("AssignFile"){
                $0.title = "Pick File"
                $0.cell.backgroundColor = .clear
                $0.cell.textLabel?.backgroundColor = .clear
                $0.cell.detailTextLabel?.backgroundColor = .clear

                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.self.buttonClicked))
                $0.cell.addGestureRecognizer(tapGesture)
            
            }.cellSetup { cell, row in
                cell.textLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
                cell.detailTextLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
                
            }.cellUpdate{ cell, row in
                cell.textLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
            }

        
        let processBtn = Section() {
            var header = HeaderFooterView<CustomButton>(.nibFile(name: "CustomButton", bundle: nil))
            header.onSetupView = { (view, section) -> () in
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.proccessBtnPressed))
                view.transitionBtn.addGestureRecognizer(tapGesture)
                self.transitionButton = view.transitionBtn
            }
            $0.header = header
        }
    
        
        form +++ assignmentsTitle
            <<< titleItem
            <<< assignmentsDescription
            <<< descriptionItem
            <<< filePicker
            <<< classesRow
            <<< sectionsItem
            <<< subjectsItem
            <<< dateItem
            +++ processBtn
        
        
        self.tableView?.separatorStyle = .none
        self.tableView?.backgroundColor = UIColor.clear
    }
    
    @objc func buttonClicked(_ sender: AnyObject?) {
        print("errr")
        let importMenu = UIDocumentPickerViewController(documentTypes: ["public.image", "public.audio", "public.movie", "public.text", "public.item", "public.content", "public.source-code"], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
    }
    
    @objc func proccessBtnPressed()  {
        self.transitionButton!.startAnimation() // 2: Then start the animation when the user tap the button
        let qualityOfServiceClass = DispatchQoS.QoSClass.background
        let backgroundQueue = DispatchQueue.global(qos: qualityOfServiceClass)
        backgroundQueue.async(execute: {
            self.getAttendanceData()
        })
    }
    
    func getAttendanceData() {
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
            ]
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            // ======= Set ( title & description & date ) =======//
            multipartFormData.append(self.selectedAssignTitle.data(using: String.Encoding.utf8)!, withName: "AssignTitle")
            multipartFormData.append(self.selectedAssignDescription.data(using: String.Encoding.utf8)!, withName: "AssignDescription")
            multipartFormData.append(self.selectedFormDate.data(using: String.Encoding.utf8)!, withName: "AssignDeadLine")
            
            // ======= Set classes =======//
            let classesPickerItem = self.getMultiplePickerItem(tag: "classes")
            if let classesValues = classesPickerItem.value{
                var count = 0
                for className in classesValues{
                    multipartFormData.append("\(self.classesDict[className]!)".data(using: String.Encoding.utf8)!, withName: "classId[\(count)]")
                    count = count + 1
                }
            }
            
            // ======= Set sections =======//
            let sectionsPickerItem = self.getMultiplePickerItem(tag: "sections")
            if let sectionsValues = sectionsPickerItem.value{
                var count = 0
                for sectionName in sectionsValues{
                    multipartFormData.append("\(self.sectionsDict[sectionName]!)".data(using: String.Encoding.utf8)!, withName: "sectionId[\(count)]")
                    count = count + 1
                }
            }
            
            // ======= Set subject =======//
            if let subjectID = self.subjectsDict[self.selectedFormSubject] {
                multipartFormData.append("\(String(describing: subjectID))".data(using: String.Encoding.utf8)!, withName: "subjectId")
            }
            
            // ======= Set file =======//
            if (self.selectedFileURL != nil){
                multipartFormData.append(self.selectedFileURL!, withName: "AssignFile")
            }
            
            }, to: SBFramework.strip(st: "\(Constants.getModRewriteBaseUrl())/assignments"), method: .post, headers: headers,
                encodingCompletion: { encodingResult in
                    switch encodingResult {
                    case .success(let upload, _, _):
                            upload.responseJSON { response in
                                
                                self.transitionButton!.stopAnimation(animationStyle: .expand, completion: {
                                    let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "AssignmentsPageStoryboard") as! AssignmentsPage
                                    self.navigationController!.pushViewController(nextVC, animated: true)
                                })
                                print(response)
                            }
                    case .failure(let error):
                            DispatchQueue.main.async(execute: { () -> Void in
                                self.transitionButton!.stopAnimation(animationStyle: .shake, completion: {})
                            })
                            print(error)
                    }
        })
        
    }
    
    func getValue(array : [String:Int] , value : String) -> Int {
        if let found = array[value]{
            return found
        }else{
            return 0
        }
    }
    func getMultiplePickerItem(tag:String) -> MultipleSelectorRow<String> {
        return (form.rowBy(tag: tag) as! MultipleSelectorRow)
    }
    func getPickerItem(tag:String) -> PickerInputRow<String> {
        return (form.rowBy(tag: tag) as! PickerInputRow)
    }
    func getLabelItem(tag:String) -> LabelRow {
        return (form.rowBy(tag: tag) as! LabelRow)
    }
    func getTextRowItem(tag:String) -> TextRow {
        return (form.rowBy(tag: tag) as! TextRow)
    }
    func getTextAreaRowItem(tag:String) -> TextAreaRow {
        return (form.rowBy(tag: tag) as! TextAreaRow)
    }
    
    @objc func openDatePicker() {
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: ("DatePickerPageStoryboard")) as! DatePickerPage
        nextVC.datePickedDelegate = self
        self.navigationController!.pushViewController(nextVC, animated: true)
        
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let myURL = url as URL
        self.selectedFileURL = myURL
        self.getLabelItem(tag: "AssignFile").cell.detailTextLabel?.text = myURL.deletingPathExtension().lastPathComponent
        print("import result : \(myURL)")
    }
    
    
    func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
    
    //============================ Protocols callbacks =================//

    func onDatePicked(tag : String, dateSelected: String) {
        self.getLabelItem(tag: "AssignDeadLine").value = dateSelected
        self.selectedFormDate = dateSelected
    }
    
}




