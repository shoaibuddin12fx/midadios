import UIKit
import SBFramework
import SwiftyJSON
import GSImageViewerController


class ExamsDetailsPage: SolViewController,RenewTokenCallback {
    
    var dashboardJson : JSON?
    @IBOutlet weak var navLeftButton: UIBarButtonItem!
    let detailsWidget = WidgetCreator()
    let examSchWidget = WidgetCreator()
    var examID : Int = 0
    var subjectsDictionary = [String: String]()
    let containerView = DashboardView()

    class func instantiateFromStoryboard() -> ExamsDetailsPage {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ExamsDetailsPageStoryboard") as! ExamsDetailsPage
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initNavBar()
        self.navLeftButton.image = Utils.getBackIcon()
        // nav bar title
        self.navigationItem.title = Utils.siteTitle
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }
   
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func loadView() {
        createExamsDetailsView()
        createSchView()
        
        //=================== Create dashboard view ===================//
        containerView.injectView(mView: detailsWidget.createView())
        containerView.injectView(mView: examSchWidget.createView())
        containerView.backgroundColor =  UIColor(patternImage: UIImage(named: "x_gen_back")!)
        containerView.createView()
        
        if(examID != 0){
            detailsWidget.changeStatus(currentActiveView: .loading)
            examSchWidget.changeStatus(currentActiveView: .loading)
            loadData()
        }else{
            detailsWidget.changeStatus(currentActiveView: .error)
        }

        // Set dashboard view to controller view
        self.view = containerView
    }
    
    func loadData() {
        
        let url = "\(Constants.getModRewriteBaseUrl())/examsList/\(examID)"
        
        SBFramework.request(url, method: .get).responseJSON { response in
            switch response.result {
            case .success(let value):
                
                //======== Parse result ==========//
                let json = JSON(value)
                if (json["error"].null == nil){
                    if(!self.tokenRenewTried){ Utils.renewToken(callback : self,navigationController : self.navigationController!) }
                }
                print(json)
                
                if !(json.null != nil) {
                    //Parse Classes
                    var classesNames = "NA"
                        if let classesArray = json["examClassesNames"].array {
                            //================= Fill data array ==================//
                            var counter = 0
                            for item in classesArray {
                                if let className = item["className"].string {
                                    if counter == 0 { classesNames = "" }
                                    classesNames =  classesNames + className
                                    if counter != classesArray.count - 1 { classesNames = classesNames + " , " }
                                    counter = counter + 1
                                }
                            }
                        }
                    //Parse Schedule
                    if let schArray = json["examSchedule"].array {
                        //================= Fill data array ==================//
                        var subjectsMappedDictionary = [String: String]()
                        if schArray.count > 0 {
                            for item in schArray {
                                let subjectId = Utils.getStringValueOrNilOf(sourceInput: item,itemKey: "subject")
                                if subjectId != Utils.Nil && self.subjectsDictionary[subjectId] != nil {
                                    subjectsMappedDictionary[self.subjectsDictionary[subjectId]!] = Utils.getStringValueOf(sourceInput: item,itemKey: "stDate")
                                }
                            }
                            self.createScheduleView(subjectsMappedDic: subjectsMappedDictionary)
                            self.examSchWidget.changeStatus(currentActiveView: .dataSet)
                        }else{
                            self.examSchWidget.changeStatus(currentActiveView: .empty)
                        }
                    }else{
                        self.examSchWidget.changeStatus(currentActiveView: .error)
                    }
                    
                    //Parse Exam Data
                    let headerTitle = (self.detailsWidget.headerViewContainer as! ViewsCreator1)
                    headerTitle.text1!.text = Utils.getStringValueOf(sourceInput: json,itemKey: "examTitle")
                    
                    let headerDesc = (self.detailsWidget.headerViewContainer as! ViewsCreator1)
                    headerDesc.text2!.text = Utils.getStringValueOf(sourceInput: json,itemKey: "examDescription")
                    
                    _ = (self.detailsWidget.headerViewContainer as! ViewsCreator1).createView()
                    
                    (self.detailsWidget.dataViews![0] as! ViewsCreator1).text2!.text = classesNames
                    
                    (self.detailsWidget.dataViews![1] as! ViewsCreator1).text2!.text = Utils.getStringValueOf(sourceInput: json,itemKey: "examDate")
                    
                    (self.detailsWidget.dataViews![2] as! ViewsCreator1).text2!.text = Utils.getStringValueOf(sourceInput: json,itemKey: "examEndDate")
                    
                    self.detailsWidget.changeStatus(currentActiveView: .dataSet)
                    
                    self.containerView.createView() // must recreate widgets container view as if you set label text with larger height will find widget overlap other one - must call it if you reset views after while like load data from internet - used if you use more than one widget in same page
                    
                }else{
                    self.detailsWidget.changeStatus(currentActiveView: .error)
                    self.examSchWidget.changeStatus(currentActiveView: .error)
                }
            case .failure(let error):
                Utils.checkFailureCause(error : error, callback: self, navigationController: self.navigationController!,tokenRenewTried:self.tokenRenewTried)
                if self.tokenRenewTried {self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) }
                self.detailsWidget.changeStatus(currentActiveView: .error)
                self.examSchWidget.changeStatus(currentActiveView: .error)
            }
            
        }
    }

    var tokenRenewTried = false
    func afterRenewToken(success: Bool) {
        tokenRenewTried = true
        if success {
            loadData()
        }else{
            self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
        }
    }
    
    @IBAction func navLeftButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func createExamsDetailsView() {
        //=================== Create Views ===================//
        Defaults.setupDefaultWidgetStatus(widget: detailsWidget)
        detailsWidget.setupStatusViews(loadingText: Language.getTranslationOf(findKey: "loading",defaultWord: "Loading"), loadingImage: "no_data", emptyText: Language.getTranslationOf(findKey: "No Entry",defaultWord: "No details"), emptyImage: "no_data", errorText: Language.getTranslationOf(findKey: "errorOccurred",defaultWord: "Error Occurred"), errorImage: "no_data")
        
        //Header
        let headerView = ViewsCreator1()
        
        headerView.isOn_text1 = true
        headerView.text1?.applyDashMainStyle(BiggerSizeMode : false)
            
        headerView.isOn_text2 = true
        headerView.text2?.applyDashSubStyle(BiggerSizeMode : false)
        headerView.text2?.bottomMargin = 12
        
        //Deatils
        var detailsViews = [UIView]()

        let classsDetailsView = ViewsCreator1()
        setupDetailsView(detailsView: classsDetailsView)
        classsDetailsView.text1!.translatedText = "Classes"
        classsDetailsView.text1!.setMargins(leftMargin: 0, topMargin: 0, rightMargin: 10, bottomMargin: 0)
        
        classsDetailsView.mainImage!.setSource(imageName: "icon_pages_class", fromInternet: false)
        classsDetailsView.mainImage!.setMargins(leftMargin: 0, topMargin: 6, rightMargin: 10, bottomMargin: 10)

        _ = classsDetailsView.createView()
        detailsViews.append(classsDetailsView)
        
        let dateDetailsView = ViewsCreator1()
        setupDetailsView(detailsView: dateDetailsView)
        dateDetailsView.text1!.translatedText = "Date"
        dateDetailsView.text1!.setMargins(leftMargin: 0, topMargin: 0, rightMargin: 10, bottomMargin: 0)

        
        dateDetailsView.mainImage!.setSource(imageName: "icon_pages_start_time", fromInternet: false)
        dateDetailsView.mainImage!.setMargins(leftMargin: 0, topMargin: 6, rightMargin: 10, bottomMargin: 10)

        _ = dateDetailsView.createView()
        detailsViews.append(dateDetailsView)
        
        let endDateDetailsView = ViewsCreator1()
        setupDetailsView(detailsView: endDateDetailsView)
        endDateDetailsView.text1!.translatedText = "End Date"
        endDateDetailsView.text1!.setMargins(leftMargin: 0, topMargin: 0, rightMargin: 10, bottomMargin: 0)

        endDateDetailsView.mainImage!.setSource(imageName: "icon_pages_end_time", fromInternet: false)
        endDateDetailsView.mainImage!.setMargins(leftMargin: 0, topMargin: 6, rightMargin: 10, bottomMargin: 10)

        _ = endDateDetailsView.createView()
        detailsViews.append(endDateDetailsView)

        detailsWidget.injectHeaderView(headerView: headerView, redrawWithEveryDrawCycle: false)
        detailsWidget.injectDataViews(dataViews: detailsViews) // Inject at anytime
        detailsWidget.prepareDataView() // Don't call untill you set data copies
    }
    
    func createSchView() {
        //=================== Create Views ===================//
        Defaults.setupDefaultWidgetStatus(widget: examSchWidget)
        examSchWidget.setupStatusViews(loadingText: Language.getTranslationOf(findKey: "loading",defaultWord: "Loading"), loadingImage: "no_data", emptyText: Language.getTranslationOf(findKey: "schNotExist",defaultWord: "No schedule yet"), emptyImage: "no_data", errorText: Language.getTranslationOf(findKey: "errorOccurred",defaultWord: "Error Occurred"), errorImage: "no_data")

        //Sch Header
        let schHeaderView = ViewsCreator1()
        
        schHeaderView.isOn_topDivider = true
        schHeaderView.isOn_bottomDivider = true
        schHeaderView.dividerColor = SolColors.getColorByName(colorName: "x_divider_color")!
        
        schHeaderView.isOn_text1 = true
        schHeaderView.text1!.text = Language.getTranslationOf(findAndDefaultKey:"Exam Schedule")
        schHeaderView.text1?.applyDashMainStyle(BiggerSizeMode : false)
        schHeaderView.text1!.topMargin = 4
        
        _ = schHeaderView.createView()
        
        examSchWidget.injectHeaderView(headerView: schHeaderView, redrawWithEveryDrawCycle: false)
    }
    
    func createScheduleView(subjectsMappedDic : [String: String]) {
        //=================== Create Views ===================//
        var detailsViews = [UIView]()
        
        for (key, value) in subjectsMappedDic {
            let schView = ViewsCreator1()
            
            schView.isOn_text1 = true
            schView.text1!.applyDashMainStyle(BiggerSizeMode : false)
            schView.text1!.setMargins(leftMargin: 0, topMargin: 6, rightMargin: 10, bottomMargin: 4)
            schView.text1!.text = key
            
            schView.isOn_text2 = true
            schView.text2!.applyDashSubStyle(BiggerSizeMode : false)
            schView.text2!.setMargins(leftMargin: 0, topMargin: 0, rightMargin: 10, bottomMargin: 10)
            schView.text2!.text = value
            
            _ = schView.createView()
            detailsViews.append(schView)
        }
        
        examSchWidget.injectDataViews(dataViews: detailsViews) // Inject at anytime
        examSchWidget.prepareDataView() // Don't call untill you set data copies
    }
    
    func setupDetailsView(detailsView : ViewsCreator1) {
        detailsView.isOn_text1 = true
        detailsView.text1?.applyDashMainStyle(BiggerSizeMode : false)
        
        detailsView.isOn_text2 = true
        detailsView.text2?.applyDashSubStyle(BiggerSizeMode : false)
        detailsView.text2?.setMargins(leftMargin: 0, topMargin: 0, rightMargin: 10, bottomMargin: 0)

        detailsView.isOn_mainImage = true
        detailsView.mainImage!.setViewSize(overAllSize: 40)
    }
    
}


