//
//  NewsPage.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/15/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework
import SwiftyJSON




class StudentsAttendancePage:SolViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var dataTableView: UITableView!
    @IBOutlet weak var navLeftButton: UIBarButtonItem!
    var subjectsDictionary = [String: String]()

    var studentID : Int = 0
    var searchMode:Bool = false
    
    //============================ Base Functions =================//

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        initTableView(tableView: dataTableView)
        initNavBar()
        initSideMenu()
        initControllerStates()
        
        initPage()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupInitialViewState()
        
    }
    
    //============================ Init Functions =================//

    func initPage() {
        
        if searchAbout != nil {
            searchMode = true
            searchInDataArray();
        }else{
            searchMode = false
            self.loadData()
        }

        self.navLeftButton.image = Utils.getBackIcon()
        // nav bar title
        self.navigationItem.title = Utils.siteTitle
    }
    
    func initControllerStates() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(loadData))
        super.initControllerStates(failureView: failureView)
    }
    
    //==================== Load data ===================================//
   
    @objc func loadData() {
        self.dataArray.removeAll()
        self.dataTableView.reloadData()
        startLoading()
        
        let url = "\(Constants.getModRewriteBaseUrl())/students/attendance/\(self.studentID)"
        
        SBFramework.request(url, method: .get).responseJSON { response in
            switch response.result {
            case .success(let value):
                
                //======== Parse result ==========//
                let json = JSON(value)
                
                // Parse subjects
                if !(json["subjects"].null != nil){
                    
                    let subjectDic = json["subjects"].dictionary
                    
                    for (key,subJson):(String, JSON) in subjectDic! {
                        self.subjectsDictionary[key] = subJson.string
                    }
                }
                
                // Parse attendance
                if let jsonArray = json["attendance"].array {
                    if jsonArray.count > 0 {
                        //================= Fill data array ==================//
                        for item in jsonArray {
                            let subjectID = Utils.getStringValueOrNilOf(sourceInput: item,itemKey:"subjectId")
                            let statusID = Utils.getIntValueOrNilOf(sourceInput: item,itemKey:"status")
                            var statusName : String = "NA"
                            if statusID != Utils.NilInt{ statusName = AttendanceControlPage.statusIdentifiers[statusID]! }
                            
                            var subjectName : String = Utils.Nil
                            if subjectID != Utils.Nil && self.subjectsDictionary[subjectID] != nil
                            {  subjectName = self.subjectsDictionary[subjectID]! }
                            
                            self.dataArray.append(StudentsAttendanceModel(
                                id : Utils.getIntValueOf(sourceInput: item,itemKey:"id") ,
                                studentId : Utils.getIntValueOf(sourceInput: item,itemKey:"studentId") ,
                                subjectId : subjectID,
                                date : Utils.getStringValueOf(sourceInput: item,itemKey:"date") ,
                                status : statusID,
                                subjectName: subjectName,
                                statusName: statusName
                                )
                            )
                        }
                        self.endLoading(error: nil) // Set Content
                        self.dataTableView.reloadData()
                    }else{
                        self.endLoading(error: nil) // No Content
                    }
                }else{
                    self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
                }
            case .failure(let error):
                print(error)
                self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
                
            }
            
        }
    }
    
    //==================== Nav Bar Buttons Actions ===================================//
    @IBAction func reloadPage(_ sender: Any) {
        loadData();
    }
    
    @IBAction func searchButtonAction(_ sender: Any) {
        
        let currentVC = self.storyboard?.instantiateViewController(withIdentifier:"StudentsAttendancePageStoryboard") as! StudentsAttendancePage
        currentVC.studentID = self.studentID
        
        showSearchDialog(targetStoryboard: "StudentsAttendancePageStoryboard",openController: currentVC)
        
    }
    
    func searchInDataArray(){
        if(self.dataArray.count > 0){
            var searchDataArray = [Any]()
            for item in dataArray {
                let cItem = item as! StudentsAttendanceModel
                if(cItem.date!.lowercased().contains(self.searchAbout!)
                    || cItem.subjectName!.lowercased().contains(self.searchAbout!)
                    || cItem.statusName!.lowercased().contains(self.searchAbout!)){
                    searchDataArray.append(cItem)
                }
            }
            dataArray.removeAll()
            dataArray.append(contentsOf : searchDataArray)
            self.endLoading(error: nil) // Set Content
        }else{
            self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil))
        }
    }
    
    @IBAction func navLeftButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //==================== Table view options ===================================//
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:StudentsAttendanceCell = tableView.dequeueReusableCell(withIdentifier: "cellCon", for: indexPath) as! StudentsAttendanceCell
        cell.item = dataArray[indexPath.row] as! StudentsAttendanceModel
        return cell
    }

}
