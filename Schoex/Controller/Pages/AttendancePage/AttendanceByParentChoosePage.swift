//
//  ViewController.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/15/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework
import SwiftyJSON


class AttendanceByParentChoosePage:SolViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var dataTableView: UITableView!
    @IBOutlet weak var navLeftButton: UIBarButtonItem!
    var subjectsDictionary = [String: String]()

    var studentId : Int = 0
    //============================ Base Functions =================//
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initTableView(tableView: dataTableView)
        initNavBar()
        initSideMenu()
        initControllerStates()
        
        initPage()

    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupInitialViewState()
        
    }
    
    //============================ Init Functions =================//

    func initPage() {
        
        self.navLeftButton.image = Utils.getBackIcon()
        self.loadData()
        
        // nav bar title
        self.navigationItem.title = Utils.siteTitle
    }
    
    func initControllerStates() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(loadData))
        super.initControllerStates(failureView: failureView)
    }
    
    //==================== Load data ===================================//
    @objc func loadData() {
        self.dataArray.removeAll()
        self.dataTableView.reloadData()
        startLoading ()

        let url = "\(Constants.getModRewriteBaseUrl())/attendance/stats"
        
        SBFramework.request(url, method: .get).responseJSON { response in
            switch response.result {
            case .success(let value):
                print(value)
                //======== Parse result ==========//
                let json = JSON(value)
                if !(json.null != nil){
                    // Parse subjects
                    if !(json["subjects"].null != nil){
                        
                        let subjectDic = json["subjects"].dictionary
                        
                        for (key,subJson):(String, JSON) in subjectDic! {
                            self.subjectsDictionary[key] = subJson.string
                        }
                    }
                    // Parse data
                    if !(json["studentAttendance"].null != nil){
                        
                        for (_,attendanceParentJson):(String, JSON) in json["studentAttendance"] {
                            
                            var studentAttendance : AttendanceByParentChooseModel?
                            var attendanceArray = [AttendanceByParentModel]()
                            // Parse student attendance
                            if let attendanceArrayJson = attendanceParentJson["d"].array {
                                for item in attendanceArrayJson {
                                    let status = Utils.getIntValueOf(sourceInput: item,itemKey:"status")
                                    let subjectId = Utils.getStringValueOrNilOf(sourceInput: item,itemKey:"subject")
                                    var subjectName = subjectId
                                    if subjectId != Utils.Nil && self.subjectsDictionary[subjectId] != nil{
                                        subjectName = self.subjectsDictionary[subjectId]!
                                    }
                                    let studentAttendance = AttendanceByParentModel(
                                        date: Utils.getStringValueOf(sourceInput: item,itemKey:"date"),
                                        status: status,
                                        subject: subjectName,
                                        statusName: AttendanceControlPage.statusIdentifiers[status]!
                                    )

                                    attendanceArray.append(studentAttendance)
                                }
                            }
                            
                            // Parse student data
                            if !(attendanceParentJson["n"].null != nil){
                                studentAttendance = AttendanceByParentChooseModel(
                                    name: Utils.getStringValueOf(sourceInput: attendanceParentJson["n"],itemKey:"name"),
                                    studentRollId: Utils.getStringValueOf(sourceInput: attendanceParentJson["n"],itemKey:"studentRollId"),
                                    attendanceArray: attendanceArray
                                )
                            }
                            if studentAttendance != nil{
                                self.dataArray.append(studentAttendance!)
                            }
                        }
                        self.endLoading(error: nil) // Set Content
                        self.dataTableView.reloadData()
                        
                    }else{
                        self.endLoading(error: nil) // Set Content                        
                    }
                }else{
                    self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
                }
            case .failure(let error):
                print(error)
                self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
            }
            
        }
    }
    
    //==================== Nav Bar Buttons Actions ===================================//

    @IBAction func reloadPage(_ sender: Any) {
        self.dataArray.removeAll()
        loadData();
    }
    

    @IBAction func navLeftButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
  
    //==================== Table view options ===================================//
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:AttendanceByParentChooseCell = tableView.dequeueReusableCell(withIdentifier: "cellCon", for: indexPath) as! AttendanceByParentChooseCell
        cell.item = dataArray[indexPath.row] as! AttendanceByParentChooseModel
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "AttendanceByParentPageStoryboard") as! AttendanceByParentPage
        
        let item = self.dataArray[indexPath.row] as! AttendanceByParentChooseModel
        nextVC.dataArray = item.attendanceArray!
        
        self.navigationController!.pushViewController(nextVC, animated: true)
    }
}






