//
//  ViewController.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/15/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework
import SwiftyJSON

class AttendanceControlPage:SolViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var dataTableView: UITableView!
    @IBOutlet weak var navLeftButton: UIBarButtonItem!
    @IBOutlet weak var submitAttendanceButton: UIButton!
    
    var searchMode:Bool = false
    var attendanceDay: String = String()
    var classId: Int = Int()
    var sectionId: Int = Int()
    var subjectId: Int = Int()
    var studentsArray : [JSON] = [JSON]()
    
    static let statusIdentifiers:[Int:String] = [
        1:Language.getTranslationOf(findKey: "Present", defaultWord: "Present"),
        0:Language.getTranslationOf(findKey: "Absent", defaultWord: "Absent"),
        2:Language.getTranslationOf(findKey: "Late", defaultWord: "Late"),
        3:Language.getTranslationOf(findKey: "LateExecuse", defaultWord: "Late with excuse"),
        4:Language.getTranslationOf(findKey: "earlyDismissal", defaultWord: "Early Dismissal"),
        9:Language.getTranslationOf(findKey: "acceptedVacation", defaultWord: "Accepted Vacation")
    ]

    static let statusIdentifiersReversed:[String:Int] = [
        Language.getTranslationOf(findKey: "Present", defaultWord: "Present") : 1,
        Language.getTranslationOf(findKey: "Absent", defaultWord: "Absent") : 0,
        Language.getTranslationOf(findKey: "Late", defaultWord: "Late") : 2,
        Language.getTranslationOf(findKey: "LateExecuse", defaultWord: "Late with excuse") : 3,
        Language.getTranslationOf(findKey: "earlyDismissal", defaultWord: "Early Dismissal") : 4,
        Language.getTranslationOf(findKey: "acceptedVacation", defaultWord: "Accepted Vacation") : 9
    ]
    
    //============================ Base Functions =================//
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        initNavBar()
        initSideMenu()
        initControllerStates()
        
        initPage()
       
        dataTableView.register(AttendanceControlCell.self, forCellReuseIdentifier: "cellCon")
        dataTableView.register(AttendanceVacationControlCell.self, forCellReuseIdentifier: "cellVacationCon")

        
        self.dataTableView.estimatedRowHeight = 10
        self.dataTableView.pin.topLeft().bottomRight()

    }
   
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupInitialViewState()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
        dataTableView.reloadData()
        
    }
    //============================ Init Functions =================//

    func initPage() {
        self.navLeftButton.image = Utils.getBackIcon()
        
        if searchAbout != nil {
            searchMode = true
            searchInDataArray();
        }else{
            searchMode = false
            startLoading()
            self.loadData()
        }
        
        Utils.setButtonBackgroundColors(
            targetButton: submitAttendanceButton,
            normalState: "x_bottom_process_button_back_normal",
            pressedState: "x_bottom_process_button_back_pressed")
        
        submitAttendanceButton.setTitleColor(SolColors.getColorByName(colorName: "x_bottom_process_button_text"), for: .normal)
        
        // nav bar title
        self.navigationItem.title = Utils.siteTitle
    }
    
    func initControllerStates() {
        let failureView = ErrorView(frame: view.frame)
        super.initControllerStates(failureView: failureView)
    }
    
    //==================== Load data ===================================//
    
    @IBAction func submitAttendance(_ sender: Any) {
        Utils.showMsg(msg: Language.getTranslationOf(findAndDefaultKey:"Please wait"))

        let url = "\(Constants.getModRewriteBaseUrl())/attendance"
        
        var fullAttendanceData = [Any]()
        
        for dataObject in dataArray {
            let studentItem:AttendanceControlModel = dataObject as! AttendanceControlModel
            var studentAttendance : [String : Any] = [
                "id" : studentItem.StudentId,
                "studentRollId" : studentItem.StudentRollId,
                "name" : studentItem.StudentName,
                "attendance" : studentItem.StatusId
            ]
            if (studentItem.vacation){
                studentAttendance["vacation"] = studentItem.vacation
                studentAttendance["vacationStat"] = studentItem.vacationStat
            }
            fullAttendanceData.append(studentAttendance)
        }

        let parameters: Parameters = [
            "attendanceDay":    attendanceDay,
            "classId" :         classId,
            "subjectId" :       subjectId,
            "subject" :         subjectId,
            "stAttendance" :    fullAttendanceData
        ]
        
        SBFramework.request(url, method: .post,parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success(let value):
                Utils.parseUpdateResponse(jsonResponse: JSON(value))
                
            case .failure(_):
                Utils.showMsg(msg: "Error Occurred")
            }
            
        }
    }
    
    @IBAction func searchButtonAction(_ sender: Any) {
        
        let currentVC = self.storyboard?.instantiateViewController(withIdentifier:"AttendanceControlPageStoryboard") as! AttendanceControlPage
        currentVC.attendanceDay = self.attendanceDay
        currentVC.classId = self.classId
        currentVC.sectionId = self.sectionId
        currentVC.subjectId = self.subjectId
        
        showSearchDialog(targetStoryboard: "AttendanceControlPageStoryboard",openController: currentVC)
    }
    
    @objc func loadData() {
            //================= Fill data array ==================//
            for item in studentsArray {
                
                self.dataArray.append(AttendanceControlModel(
                    StudentName: Utils.getStringValueOf(sourceInput: item,itemKey: "name") ,
                    Status: Utils.getIntValueOrNilOf(sourceInput: item,itemKey: "attendance") ,
                    StudentRollId: Utils.getStringValueOf(sourceInput: item,itemKey: "studentRollId") ,
                    StudentId: Utils.getIntValueOf(sourceInput: item,itemKey: "id") ,
                    vacation: Utils.getBooleanValueOf(sourceInput: item,itemKey: "vacation") ,
                    vacationStat: Utils.getStringValueOrNilOf(sourceInput: item,itemKey: "vacationStat")
                    )
                )

            }
            self.endLoading(error: nil) // Set Content
            self.dataTableView.reloadData()
    }
    
    //==================== Nav Bar Buttons Actions ===================================//


    func searchInDataArray(){
        if(self.dataArray.count > 0){
            var searchDataArray = [Any]()
            for item in dataArray {
                let cItem = item as! AttendanceControlModel
                if(cItem.StudentName.lowercased().contains(self.searchAbout!)
                    || cItem.StudentRollId.lowercased().contains(self.searchAbout!)){
                    searchDataArray.append(cItem)
                }
            }
            dataArray.removeAll()
            dataArray.append(contentsOf : searchDataArray)
            self.endLoading(error: nil) // Set Content
        }else{
            self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil))
        }
    }
    
    @IBAction func navLeftButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

    
    //==================== Table view options ===================================//


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = dataArray[indexPath.row] as! AttendanceControlModel
        if(item.vacation){
            // There vacattion
            let cell = dataTableView.dequeueReusableCell(withIdentifier: "cellVacationCon", for: indexPath) as! AttendanceVacationControlCell
            cell.item = item
            return cell
        }else{
            // No vacation
            let cell = dataTableView.dequeueReusableCell(withIdentifier: "cellCon", for: indexPath) as! AttendanceControlCell
            cell.item = item
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    
}


