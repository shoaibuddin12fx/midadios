//
//  AttendancePageViewController.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/29/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SwiftyJSON
import SBFramework
import Eureka
import TransitionButton

class StaffAttendancePage: FormViewController,DatePickerProtocol {
    
    @IBOutlet weak var navLeftButton: UIBarButtonItem!

    var selectedFormDate:String?
    var NilSelect = "No Select"

    var transitionButton:TransitionButton?
    //============================ Base Functions =================//
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initNavBar()
        initSideMenu()
        
        initPage()
        
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //====================== Nav Bar Common Options ========================//
    func initNavBar (){
        showNavigationBar()
        navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController!.navigationBar.shadowImage = UIImage()
        
        // navigation title color
        navigationController!.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : SolColors.getColorByName(colorName: "x_header_text")!]
        
        // navigation bar
        UINavigationBar.appearance().backgroundColor = SolColors.getColorByName(colorName: "x_header_back")
        
        // status bar
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        statusBarView.backgroundColor = SolColors.getColorByName(colorName: "x_header_back")
        view.addSubview(statusBarView)
        

    }
    
    @IBAction func navLeftButtonAction(_ sender: Any) {
        showMenu()
    }
    
    //====================== Side Menu Common Options ========================//
    func initSideMenu (){
        
        if Utils.App_IsRtl{
            SideMenuManager.menuRightNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        }else{
            SideMenuManager.menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        }
        
        SideMenuManager.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        
        //SideMenuManager.menuAnimationBackgroundColor = UIColor(patternImage: UIImage(named: "background")!)
    }
    
    func showMenu() {
        if Utils.App_IsRtl{
            present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
        }else{
            present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
        }
    }
    func hideMenu() {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    //============================ Init Functions =================//
    
    func initPage() {
        createForm()
        
        // nav bar title
        self.navigationItem.title = Utils.siteTitle
    }
    
    //============================ Form manage functions =================//
    
    func createForm() {
        form
            
            +++ Section()
            <<< LabelRow ("date") {
                    $0.title = Language.getTranslationOf(findKey: "date",defaultWord: "Date")
                    $0.value = NilSelect
                }.onCellSelection { cell, row in
                    self.openDatePicker()
                }.cellSetup { cell, row in
                    cell.backgroundColor = .clear
                    cell.textLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
                    cell.detailTextLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
                }.cellUpdate{ cell, row in
                    cell.textLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
                }
            +++ Section() {
                var header = HeaderFooterView<CustomButton>(.nibFile(name: "CustomButton", bundle: nil))
                header.onSetupView = { (view, section) -> () in
                    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.proccessBtnPressed))
                    view.transitionBtn.addGestureRecognizer(tapGesture)
                    self.transitionButton = view.transitionBtn
                }
                $0.header = header
            }
        self.tableView?.separatorStyle = .none
        self.tableView?.backgroundColor = UIColor.clear
    }
    
    @objc func proccessBtnPressed()  {
        self.transitionButton!.startAnimation()
        let qualityOfServiceClass = DispatchQoS.QoSClass.background
        let backgroundQueue = DispatchQueue.global(qos: qualityOfServiceClass)
        backgroundQueue.async(execute: {
            self.getAttendanceData()
        })
    }
    
    func getAttendanceData() {
        let url = "\(Constants.getModRewriteBaseUrl())/sattendance/list"
        
        let parameters: Parameters = [
            "attendanceDay":    selectedFormDate ?? ""
            ]


        SBFramework.request(url, method: .post,parameters: parameters).responseJSON { response in
            switch response.result {
            case .success(let value):
                
                //======== Parse result ==========//
                let json = JSON(value)
                print(json)
                if let jsonArray = json["teachers"].array {
                    if jsonArray.count > 0 {
                        DispatchQueue.main.async(execute: { () -> Void in
                            // 4: Stop the animation, here you have three options for the `animationStyle` property:
                            // .expand: useful when the task has been compeletd successfully and you want to expand the button and transit to another view controller in the completion callback
                            // .shake: when you want to reflect to the user that the task did not complete successfly
                            // .normal
                            self.transitionButton!.stopAnimation(animationStyle: .expand, completion: {
                                let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "StaffAttendanceControlPageStoryboard") as! StaffAttendanceControlPage
                                nextVC.attendanceDay = self.selectedFormDate!
                                nextVC.staffArray = jsonArray
                                self.navigationController!.pushViewController(nextVC, animated: true)
                            })
                        })
                    }else{
                        DispatchQueue.main.async(execute: { () -> Void in
                            self.transitionButton!.stopAnimation(animationStyle: .shake, completion: {})
                        })
                    }
                }else{
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.transitionButton!.stopAnimation(animationStyle: .shake, completion: {})
                    })
                }
            case .failure(let error):
                DispatchQueue.main.async(execute: { () -> Void in
                    self.transitionButton!.stopAnimation(animationStyle: .shake, completion: {})
                })
                print(error)
            }
            
        }
    }
    
    func getPickerItem(tag:String) -> PickerInputRow<String> {
        return (form.rowBy(tag: tag) as! PickerInputRow)
    }
    func getLabelItem(tag:String) -> LabelRow {
        return (form.rowBy(tag: tag) as! LabelRow)
    }
    
    @objc func openDatePicker() {
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: ("DatePickerPageStoryboard")) as! DatePickerPage
        nextVC.datePickedDelegate = self
        self.navigationController!.pushViewController(nextVC, animated: true)
        
    }
    
    //============================ Protocols callbacks =================//

    func onDatePicked(tag : String, dateSelected: String) {
        self.getLabelItem(tag: "date").value = dateSelected
        self.selectedFormDate = dateSelected
    }
    
}




