//
//  AttendancePageViewController.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/29/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SwiftyJSON
import SBFramework

import Eureka
import TransitionButton

class AttendancePage: FormViewController,DatePickerProtocol {
    
    @IBOutlet weak var navLeftButton: UIBarButtonItem!
    var classesDict = [String:Int]()
    var subjectsDict = [String:Int]()
    var sectionsDict = [String:Int]()

    var selectedFormClass:String = ""
    var selectedFormSection:String = ""
    var selectedFormSubject:String = ""
    var selectedFormDate:String = ""
    var NilSelect = "No Select"

    var transitionButton:TransitionButton?
    //============================ Base Functions =================//
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initNavBar()
        initSideMenu()
        
        initPage()
        
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //====================== Nav Bar Common Options ========================//
    func initNavBar (){
        showNavigationBar()
        
        navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController!.navigationBar.shadowImage = UIImage()
        
        // navigation title color
        navigationController!.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : SolColors.getColorByName(colorName: "x_header_text")!]
        
        // navigation bar
        UINavigationBar.appearance().backgroundColor = SolColors.getColorByName(colorName: "x_header_back")
        
        // status bar
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        statusBarView.backgroundColor = SolColors.getColorByName(colorName: "x_header_back")
        view.addSubview(statusBarView)
        
  
    }
    
    @IBAction func navLeftButtonAction(_ sender: Any) {
        showMenu()
    }
    
    //====================== Side Menu Common Options ========================//
    func initSideMenu (){
        
        if Utils.App_IsRtl{
            SideMenuManager.menuRightNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        }else{
            SideMenuManager.menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        }
        
        SideMenuManager.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        
    }
    
    func showMenu() {
        if Utils.App_IsRtl{
            present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
        }else{
            present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
        }
    }
    func hideMenu() {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    //============================ Init Functions =================//
    
    func initPage() {
        loadClasses()
        createForm()
        
        // nav bar title
        self.navigationItem.title = Utils.siteTitle
    }
    
    //============================ Load data =================//

    func loadClasses() {
        let url = "\(Constants.getModRewriteBaseUrl())/attendance/data"
        
        SBFramework.request(url, method: .get).responseJSON { response in
            switch response.result {
            case .success(let value):
                
                //======== Parse result ==========//
                let json = JSON(value)
                if let jsonArray = json["classes"].array {
                    
                    //parse classes
                    let classesPickerItem = self.getPickerItem(tag: "classes")
                    
                    if(jsonArray.count > 0){
                        for item in jsonArray {
                            let classTitle = item["className"].string!
                            self.classesDict[classTitle] = item["id"].int!
                            classesPickerItem.options.append(classTitle)
                        }
                        
                        // Select first item and reload
                        classesPickerItem.value = classesPickerItem.options.first!
                        self.selectedFormClass = classesPickerItem.options.first!
                        classesPickerItem.reload()
                        self.loadSubjectsSections()
                        
                        // Attach listener in case change selected item
                        classesPickerItem.onChange{ row in
                            self.selectedFormClass = row.value!
                            self.loadSubjectsSections()
                            
                            // clear sections and subjects
                            if Utils.App_IsSectionsEnabled{
                                self.getPickerItem(tag: "sections").options.removeAll()
                                self.getPickerItem(tag: "sections").value = self.NilSelect
                            }
                            self.getPickerItem(tag: "subjects").options.removeAll()
                            self.getPickerItem(tag: "subjects").value = self.NilSelect
                        }
                    }
                }
            case .failure(let error):
                print(error)
                break
            }
            
        }
    }
    
    func loadSubjectsSections() {
        if self.selectedFormClass != "" && self.classesDict[self.selectedFormClass] != nil {
            
            let url = "\(Constants.getModRewriteBaseUrl())/dashboard/sectionsSubjectsList"
            
            let parameters: Parameters = ["classes": self.classesDict[self.selectedFormClass]!]
            
            SBFramework.request(url, method: .post, parameters: parameters).responseJSON { response in
                switch response.result {
                case .success(let value):
                    
                    //======== Parse result ==========//
                    let json = JSON(value)
                    
                    //parse sections
                    if Utils.App_IsSectionsEnabled {
                        if let jsonArray = json["sections"].array {
                            
                            let sectionsPickerItem = self.getPickerItem(tag: "sections")
                            
                            if jsonArray.count > 0{
                                for item in jsonArray {
                                    
                                    let sectionTitle = "\(Utils.getStringValueOf(sourceInput: item, itemKey: "sectionName")) - \(Utils.getStringValueOf(sourceInput: item, itemKey: "sectionTitle"))"
                                    self.sectionsDict[sectionTitle] = Utils.getIntValueOf(sourceInput: item, itemKey: "id")
                                    sectionsPickerItem.options.append(sectionTitle)
                                }
                                
                                // Select first item and reload
                                sectionsPickerItem.value = sectionsPickerItem.options.first
                                self.selectedFormSection = sectionsPickerItem.options.first!
                                
                                sectionsPickerItem.onChange{ row in
                                    self.selectedFormSection = row.value!
                                }
                            }else{
                                sectionsPickerItem.value = self.NilSelect
                            }
                            sectionsPickerItem.reload()
                        }
                    }
                    
                    
                    //parse subjects
                    if let jsonArray = json["subjects"].array {
                        
                        let subjectsPickerItem = self.getPickerItem(tag: "subjects")
                        
                        if jsonArray.count > 0{
                            
                            for item in jsonArray {
                                let subjectTitle = Utils.getStringValueOf(sourceInput: item, itemKey: "subjectTitle")
                                self.subjectsDict[subjectTitle] = Utils.getIntValueOf(sourceInput: item, itemKey: "id")
                                subjectsPickerItem.options.append(subjectTitle)
                            }
                            
                            // Select first item and reload
                            subjectsPickerItem.value = subjectsPickerItem.options.first
                            self.selectedFormSubject = subjectsPickerItem.options.first!
                            
                            subjectsPickerItem.onChange{ row in
                                self.selectedFormSubject = row.value!
                            }
                        }else{
                            subjectsPickerItem.value = self.NilSelect
                        }
                        subjectsPickerItem.reload()
                    }
                    
                case .failure(let error):
                    print(error)
                    break
                }
                
            }
        }
    }
    
    //============================ Form manage functions =================//
    
    func createForm() {
        let classesRow = PickerInputRow<String>("classes"){
            $0.title = Language.getTranslationOf(findKey: "classes",defaultWord: "Classes")
            }.cellSetup { cell, row in
                cell.backgroundColor = .clear
                cell.textLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
                cell.detailTextLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
            }.cellUpdate{ cell, row in
                cell.textLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
            }
        let sectionsItem = PickerInputRow<String>("sections"){
                $0.title = Language.getTranslationOf(findKey: "sections",defaultWord: "Sections")
                $0.hidden = Condition.function(["sections"], { form in
                    return !Utils.App_IsSectionsEnabled
                })
            }.cellSetup { cell, row in
                cell.backgroundColor = .clear
                cell.textLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
                cell.detailTextLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
            }.cellUpdate{ cell, row in
                cell.textLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
            }
        let subjectsItem = PickerInputRow<String>("subjects") { row in
                row.title = Language.getTranslationOf(findKey: "subjects",defaultWord: "Subjects")
                row.hidden = Condition.function(["subjects"], { form in
                    return Utils.App_AttendanceModelIsClass
                })
            }.cellSetup { cell, row in
                cell.backgroundColor = .clear
                cell.textLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
                cell.detailTextLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
            }.cellUpdate{ cell, row in
                cell.textLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
            }
        let dateItem = LabelRow ("date") {
            $0.title = Language.getTranslationOf(findKey: "date",defaultWord: "Date")
            $0.value = NilSelect
            }.onCellSelection { cell, row in
                self.openDatePicker()
            }.cellSetup { cell, row in
                cell.backgroundColor = .clear
                cell.textLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
                cell.detailTextLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
            }.cellUpdate{ cell, row in
                cell.textLabel?.textColor = SolColors.getColorByName(colorName: "x_gen_main_text")
            }
        let processBtn = Section() {
            var header = HeaderFooterView<CustomButton>(.nibFile(name: "CustomButton", bundle: nil))
            header.onSetupView = { (view, section) -> () in
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.proccessBtnPressed))
                view.transitionBtn.addGestureRecognizer(tapGesture)
                self.transitionButton = view.transitionBtn
            }
            $0.header = header
            }
        form +++ Section()
            <<< classesRow
            <<< sectionsItem
            <<< subjectsItem
            <<< dateItem
            +++ processBtn
        
        
        self.tableView?.separatorStyle = .none
        self.tableView?.backgroundColor = UIColor.clear
    }
    
    @objc func proccessBtnPressed()  {
        self.transitionButton!.startAnimation() // 2: Then start the animation when the user tap the button
        let qualityOfServiceClass = DispatchQoS.QoSClass.background
        let backgroundQueue = DispatchQueue.global(qos: qualityOfServiceClass)
        backgroundQueue.async(execute: {
            self.getAttendanceData()
        })
    }
    
    func getAttendanceData() {
        let url = "\(Constants.getModRewriteBaseUrl())/attendance/list"
        var parameters: Parameters = [
            "attendanceDay":    selectedFormDate                    ,
            "classId" :         classesDict[selectedFormClass] ?? ""
            ]
        if Utils.App_IsSectionsEnabled{
            parameters["sectionId"] = sectionsDict[selectedFormSection]   ?? ""
        }
        if !Utils.App_AttendanceModelIsClass{
            parameters["subjectId"] = subjectsDict[selectedFormSubject]   ?? ""
        }

        SBFramework.request(url, method: .post,parameters: parameters).responseJSON { response in
            switch response.result {
            case .success(let value):
                
                //======== Parse result ==========//
                let json = JSON(value)
                print(json)
                if let jsonArray = json["students"].array {
                    if jsonArray.count > 0 {
                        DispatchQueue.main.async(execute: { () -> Void in
                            // 4: Stop the animation, here you have three options for the `animationStyle` property:
                            // .expand: useful when the task has been compeletd successfully and you want to expand the button and transit to another view controller in the completion callback
                            // .shake: when you want to reflect to the user that the task did not complete successfly
                            // .normal
                            self.transitionButton!.stopAnimation(animationStyle: .expand, completion: {
                                let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "AttendanceControlPageStoryboard") as! AttendanceControlPage
                                nextVC.attendanceDay = self.selectedFormDate
                                nextVC.classId = self.getValue(array:self.classesDict,value : self.selectedFormClass)
                                nextVC.sectionId = self.getValue(array:self.sectionsDict,value : self.selectedFormSection)
                                nextVC.subjectId =
                                    self.getValue(array:self.subjectsDict,value : self.selectedFormSubject)
                                nextVC.studentsArray = jsonArray
                                
                                self.navigationController!.pushViewController(nextVC, animated: true)
                            })
                        })
                    }else{
                        DispatchQueue.main.async(execute: { () -> Void in
                            self.transitionButton!.stopAnimation(animationStyle: .shake, completion: {})
                        })
                    }
                }else{
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.transitionButton!.stopAnimation(animationStyle: .shake, completion: {})
                    })
                }
            case .failure(let error):
                DispatchQueue.main.async(execute: { () -> Void in
                    self.transitionButton!.stopAnimation(animationStyle: .shake, completion: {})
                })
                print(error)
            }
            
        }
    }
    
    func getValue(array : [String:Int] , value : String) -> Int {
        if let found = array[value]{
            return found
        }else{
            return 0
        }
    }
    
    func getPickerItem(tag:String) -> PickerInputRow<String> {
        return (form.rowBy(tag: tag) as! PickerInputRow)
    }
    func getLabelItem(tag:String) -> LabelRow {
        return (form.rowBy(tag: tag) as! LabelRow)
    }
    
    @objc func openDatePicker() {
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: ("DatePickerPageStoryboard")) as! DatePickerPage
        nextVC.datePickedDelegate = self
        self.navigationController!.pushViewController(nextVC, animated: true)
        
    }
    
    //============================ Protocols callbacks =================//

    func onDatePicked(tag : String, dateSelected: String) {
        self.getLabelItem(tag: "date").value = dateSelected
        self.selectedFormDate = dateSelected
    }
    
}




