//
//  ViewController.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/15/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework
import SwiftyJSON


class AttendanceByStudentPage:SolViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var dataTableView: UITableView!
    @IBOutlet weak var navLeftButton: UIBarButtonItem!
    var subjectsDictionary = [String: String]()

    var searchMode:Bool = false
    var studentId : Int = 0
    //============================ Base Functions =================//
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initTableView(tableView: dataTableView)
        initNavBar()
        initSideMenu()
        initControllerStates()
        
        initPage()

    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupInitialViewState()
        
    }
    
    //============================ Init Functions =================//

    func initPage() {
        if searchAbout != nil {
            searchMode = true
            searchInDataArray();
            self.navLeftButton.image = Utils.getBackIcon()
        }else{
            searchMode = false
            startLoading()
            self.navLeftButton.image = UIImage(named: "icn_drawer")
            self.loadData()
        }
        // nav bar title
        self.navigationItem.title = Utils.siteTitle
    }
    
    func initControllerStates() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(loadData))
        super.initControllerStates(failureView: failureView)
    }
    
    //==================== Load data ===================================//
    @objc func loadData() {
        self.dataArray.removeAll()
        self.dataTableView.reloadData()
        startLoading ()

        let url = "\(Constants.getModRewriteBaseUrl())/attendance/stats"
        
        SBFramework.request(url, method: .get).responseJSON { response in
            switch response.result {
            case .success(let value):
                print(value)
                //======== Parse result ==========//
                let json = JSON(value)
                if !(json.null != nil){
                    // Parse data
                    if !(json["studentAttendance"].null != nil){
                        // Parse student attendance
                        if let attendanceArrayJson = json["studentAttendance"].array {
                            for item in attendanceArrayJson {
                                let status = Utils.getIntValueOf(sourceInput: item,itemKey:"status")
                                
                                let studentAttendance = AttendanceByParentModel(
                                    date: Utils.getStringValueOf(sourceInput: item,itemKey:"date"),
                                    status: status,
                                    subject: Utils.getStringValueOrNilOf(sourceInput: item,itemKey:"subject"),
                                    statusName: AttendanceControlPage.statusIdentifiers[status]!
                                )
                                
                                self.dataArray.append(studentAttendance)
                            }
                        }
                        self.endLoading(error: nil) // Set Content
                        self.dataTableView.reloadData()
                        
                    }else{
                        self.endLoading(error: nil) // Set Content
                    }
                }else{
                    self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
                }
            case .failure(let error):
                print(error)
                self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
            }
            
        }
    }
    
    //==================== Nav Bar Buttons Actions ===================================//

    @IBAction func reloadPage(_ sender: Any) {
        self.dataArray.removeAll()
        loadData();
    }
    
    func searchInDataArray(){
        if(self.dataArray.count > 0){
            var searchDataArray = [Any]()
            for item in dataArray {
                let cItem = item as! AttendanceByParentModel
                if(cItem.date!.lowercased().contains(self.searchAbout!)
                    || cItem.statusName!.lowercased().contains(self.searchAbout!)
                    || cItem.subject!.lowercased().contains(self.searchAbout!)){
                    searchDataArray.append(cItem)
                }
            }
            dataArray.removeAll()
            dataArray.append(contentsOf : searchDataArray)
            self.endLoading(error: nil) // Set Content
        }else{
            self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil))
        }
    }
    
    @IBAction func searchButtonAction(_ sender: Any) {
        showSearchDialog(targetStoryboard: "AttendanceByStudentPageStoryboard")
    }
    
    @IBAction func navLeftButtonAction(_ sender: Any) {
        if(self.searchMode){
            self.navigationController?.popViewController(animated: true)
        }else{
            showMenu()
        }
    }
    
    //==================== Table view options ===================================//
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:AttendanceByStudentCell = tableView.dequeueReusableCell(withIdentifier: "cellCon", for: indexPath) as! AttendanceByStudentCell
        cell.item = dataArray[indexPath.row] as! AttendanceByParentModel
        return cell
    }

}






