//
//  ViewController.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/15/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework
import SwiftyJSON
import FileBrowser
import SafariServices
import Lightbox

class StudyMaterialsPage:SolViewController,UITableViewDataSource,UITableViewDelegate,RenewTokenCallback, StudyMaterialsCellDelegate, StudyMaterialHeaderVIewDelegate {
    
    
    
    @IBOutlet weak var dataTableView: UITableView!
    @IBOutlet weak var navLeftButton: UIBarButtonItem!

    var searchMode:Bool = false;
    var selectedSubject = "";
    
    var subjects: [SubjectsModel] = [];
    
    //============================ Base Functions =================//
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        dataTableView.register(UINib(nibName: "StudyMaterialHeaderVIew", bundle: nil), forHeaderFooterViewReuseIdentifier: "StudyMaterialHeaderVIew")
        
        
        initTableView(tableView: dataTableView)
        initNavBar()
        initSideMenu()
        initControllerStates();
        
        initPage()

        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupInitialViewState()
        
    }
    
    //============================ Init Functions =================//

    func initPage() {
        if searchAbout != nil {
            searchMode = true
            searchInDataArray();
            self.navLeftButton.image = Utils.getBackIcon()
        }else{
            searchMode = false
            startLoading()
            self.navLeftButton.image = UIImage(named: "icn_drawer")
            self.loadData()
        }
        // nav bar title
        self.navigationItem.title = Utils.siteTitle
    }
    
    func initControllerStates() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(loadData))
        super.initControllerStates(failureView: failureView)
    }
    
    //==================== Load data ===================================//
    @objc func loadData() {
        self.dataArray.removeAll()
        self.dataTableView.reloadData()
        startLoading ()
        
        let url = "\(Constants.getModRewriteBaseUrl())/materials/listAll?selectedSubject=\(selectedSubject)"
        print(url)
        SBFramework.request(url, method: .get).responseJSON { response in
            switch response.result {
            case .success(let value):
                
                //======== Parse result ==========//
                let json = JSON(value)
                if (json["error"].null == nil){
                    if(!self.tokenRenewTried){ Utils.renewToken(callback : self,navigationController : self.navigationController!) }
                }
                
                self.subjects = [];
                if let subArray = json["subjects"].array {
                    
                    print("AAA");
                    for item in subArray {
                        self.subjects.append(SubjectsModel(
                            id : Utils.getIntValueOf(sourceInput: item,itemKey:"id") ,
                            subjectTitle : Utils.getStringValueOf(sourceInput: item,itemKey:"subjectTitle") ,
                            className : Utils.getStringValueOf(sourceInput: item,itemKey:"className") ,
                            teacherName : Utils.getStringValueOf(sourceInput: item,itemKey:"teacherName") ,
                            teacherId : Utils.getStringValueOf(sourceInput: item,itemKey:"teacherId") ,
                            passGrade : Utils.getStringValueOf(sourceInput: item,itemKey:"passGrade") ,
                            finalGrade : Utils.getStringValueOf(sourceInput: item,itemKey:"finalGrade")
                        ));
                    }
                }
                
                
                if let jsonArray = json["materials"].array {
                    
                    //================= Fill data array ==================//
                    for item in jsonArray {
                        self.dataArray.append(StudyMaterialsModel(
                            id : Utils.getIntValueOf(sourceInput: item,itemKey:"id") ,
                            subjectId : Utils.getIntValueOf(sourceInput: item,itemKey:"subjectId") ,
                            subject : Utils.getStringValueOf(sourceInput: item,itemKey:"subject") ,
                            material_title : Utils.getStringValueOf(sourceInput: item,itemKey:"material_title") ,
                            material_description : Utils.getStringValueOf(sourceInput: item,itemKey:"material_description") ,
                            material_file : Utils.getDictValueOf(sourceInput: item,itemKey:"material_file") ,
                            classes : Utils.getStringValueOf(sourceInput: item,itemKey:"classes")
                            )
                        )
                    }
                    self.endLoading(error: nil) // Set Content
                    self.dataTableView.reloadData()

                    
                    //self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
                    //self.endLoading(error: nil) // No Content

                }else{
                    self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
                }
            case .failure(let error):
                Utils.checkFailureCause(error : error, callback: self, navigationController: self.navigationController!,tokenRenewTried:self.tokenRenewTried)
                if self.tokenRenewTried {self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) }
            }
        }
    }
    var tokenRenewTried = false
    func afterRenewToken(success: Bool) {
        tokenRenewTried = true
        if success {
            loadData()
        }else{
            self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
        }
    }
    
    //==================== Nav Bar Buttons Actions ===================================//

    @IBAction func reloadPage(_ sender: Any) {
        self.dataArray.removeAll()
        loadData();
    }
    
    @IBAction func searchButtonAction(_ sender: Any) {
        showSearchDialog(targetStoryboard: "StudyMaterialsPageStoryboard")
    }
    
   
    @IBAction func navLeftButtonAction(_ sender: Any) {
        if(self.searchMode){
            self.navigationController?.popViewController(animated: true)
        }else{
            showMenu()
        }
    }
    
    @IBAction func navOpenExplorer(_ sender: Any) {
        let fileBrowser = FileBrowser()
        self.present(fileBrowser, animated: true, completion: nil)
    }
    //============================ Search Functions =================//
    
    func searchInDataArray(){
        if(self.dataArray.count > 0){
            var searchDataArray = [Any]()
            for item in dataArray {
                let cItem = item as! StudyMaterialsModel
                if(cItem.material_title!.lowercased().contains(self.searchAbout!)
                    || cItem.material_description!.lowercased().contains(self.searchAbout!)
                    || cItem.subject!.lowercased().contains(self.searchAbout!)
                    || cItem.classes!.lowercased().contains(self.searchAbout!)){
                    searchDataArray.append(cItem)
                }
            }
            dataArray.removeAll()
            dataArray.append(contentsOf : searchDataArray)
            self.endLoading(error: nil) // Set Content
        }else{
            self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil))
        }
    }

    //==================== Table view options ===================================//
    
    func setSelectedSubject(cell: StudyMaterialHeaderVIew, index: Int) {
        //
        // call the data filter according to the index filter
        let subject = self.subjects[index];
        if let choice = subject.id{
            selectedSubject = "\(choice)"
            loadData();
        };
        
        
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        //
        let cell = dataTableView.dequeueReusableHeaderFooterView(withIdentifier: "StudyMaterialHeaderVIew") as! StudyMaterialHeaderVIew
        cell.setData(subjects: self.subjects, selectedSubject: selectedSubject)
        cell.delegate = self;
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        //
        return CGFloat(43.5);
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:StudyMaterialsCell = tableView.dequeueReusableCell(withIdentifier: "cellCon", for: indexPath) as! StudyMaterialsCell
        cell.navController = self.navigationController
        cell.item = dataArray[indexPath.row] as! StudyMaterialsModel
        cell.delegate = self;
        return cell
    }
    
    func callDetailView(cell: StudyMaterialsCell, item: StudyMaterialsModel) {
        
        print("move to detail view ");
        let url = "\(Constants.getModRewriteBaseUrl())/materials/download/\(item.id!)"
        
        SBFramework.request(url, method: .get).responseJSON { response in
            switch response.result {
            case .success(let value):
                
                //======== Parse result ==========//
                let json = JSON(value)
                if (json["flag"] == true){
                
                    let link = json["link"].rawValue as! String;
                    if let url = URL(string: link) {
                        print(url);
                        UIApplication.shared.open(url)
                    }
                
                }else{
                    self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
                }
            case .failure(let error):
                Utils.checkFailureCause(error : error, callback: self, navigationController: self.navigationController!,tokenRenewTried:self.tokenRenewTried)
                if self.tokenRenewTried {self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) }
            }
        }
        
        
    }
    
    func openLink(cell: StudyMaterialsCell, links: [String:Any]) {
        //
        
        if let link = links["link"], let url = URL(string: link as! String), let isvideo = links["isvideo"] {
                        print(url);
        //                UIApplication.shared.open(url)
            var images: [LightboxImage] = [];
            let flag = isvideo as! Bool;
            
            if(flag){
                images.append(LightboxImage(
                    image: UIImage(named: "AppIcon")!,
                    text: "",
                    videoURL: URL(string: link as! String)
                ))
            }else{
                images.append(LightboxImage(
                  imageURL: URL(string: link as! String)!
                ))
            }

            // Create an instance of LightboxController.
            let controller = LightboxController(images: images)

            // Set delegates.
//            controller.pageDelegate = self
//            controller.dismissalDelegate = self

            // Use dynamic background.
            controller.dynamicBackground = true

            // Present your controller.
            self.navigationController?.pushViewController(controller, animated: true);
            
            // present(controller, animated: true, completion: nil)
            
            
        }
        
        
//        let vc = SFSafariViewController(url: link)
//        vc.hidesBottomBarWhenPushed = true;
//        vc.hideNavigationBar()
//        present(vc, animated: true)
    }
    
}






