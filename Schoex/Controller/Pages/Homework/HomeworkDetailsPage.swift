import UIKit
import SBFramework
import SwiftyJSON
import GSImageViewerController


class HomeworkDetailsPage: SolViewController,RenewTokenCallback {
    
    var dashboardJson : JSON?
    @IBOutlet weak var navLeftButton: BarItem!
    let detailsWidget = WidgetCreator()
    var homeworkID : String = ""
    let containerView = DashboardView()

    class func instantiateFromStoryboard() -> HomeworkDetailsPage {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "HomeworkDetailsPageStoryboard") as! HomeworkDetailsPage
    }
    
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initNavBar()
        self.navLeftButton.image = Utils.getBackIcon()
        // nav bar title
        self.navigationItem.title = Utils.siteTitle
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }
   
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func loadView() {
        createHomeworkDetailsView()
        
        //=================== Create dashboard view ===================//
        containerView.injectView(mView: detailsWidget.createView())
        containerView.backgroundColor =  UIColor(patternImage: UIImage(named: "x_gen_back")!)
        containerView.createView()
        
        if(homeworkID != ""){
            detailsWidget.changeStatus(currentActiveView: .loading)
            loadData()
        }else{
            detailsWidget.changeStatus(currentActiveView: .error)
        }

        // Set dashboard view to controller view
        self.view = containerView
    }

    
    func loadData() {
        
        let url = "\(Constants.getModRewriteBaseUrl())/homeworks_view/\(homeworkID)"

        
        SBFramework.request(url, method: .get).responseJSON { response in
            switch response.result {
            case .success(let value):
                
                //======== Parse result ==========//
                let json = JSON(value)
                if (json["error"].null == nil){
                    if(!self.tokenRenewTried){ Utils.renewToken(callback : self,navigationController : self.navigationController!) }
                }
                print(json)
                
                if !(json.null != nil) {
                    
                    // Set title
                    let headerTitle = (self.detailsWidget.headerViewContainer as! ViewsCreator1)
                    headerTitle.text1!.text = Utils.getStringValueOf(sourceInput: json,itemKey: "homeworkTitle")
                    
                    // Set description
                    let headerDesc = (self.detailsWidget.headerViewContainer as! ViewsCreator1)
                    headerDesc.text2!.text = Utils.getStringValueOf(sourceInput: json,itemKey: "homeworkDescription").html2String
                    
                    // ------------ Create header ------------ //
                    _ = (self.detailsWidget.headerViewContainer as! ViewsCreator1).createView()
                    
                    // Set teacher Name
                    if let teacherDic = json["teacher"].dictionary {
                        if let fullName = teacherDic["fullName"]?.string {
                            (self.detailsWidget.dataViews![0] as! ViewsCreator1).text2!.text = fullName
                        }
                    }
                    
                    // Set Date
                    (self.detailsWidget.dataViews![1] as! ViewsCreator1).text2!.text = Utils.getStringValueOf(sourceInput: json,itemKey: "homeworkDate")
                    
                    // Set Sub Date & Eval Date
                    let homeworkSubmissionDate = Utils.getStringValueOf(sourceInput: json,itemKey: "homeworkSubmissionDate")
                    let homeworkEvaluationDate = Utils.getStringValueOf(sourceInput: json,itemKey: "homeworkEvaluationDate")
                    (self.detailsWidget.dataViews![2] as! ViewsCreator1).text2!.text = "\(homeworkSubmissionDate) - \(homeworkEvaluationDate)"

                    // Set Classes
                    var classesNames = "NA"
                    if let classesArray = json["classes"].array {
                        //================= Fill data array ==================//
                        var counter = 0
                        for item in classesArray {
                            if let className = item["className"].string {
                                if counter == 0 { classesNames = "" }
                                classesNames =  classesNames + className
                                if counter != classesArray.count - 1 { classesNames = classesNames + " , " }
                                counter = counter + 1
                            }
                        }
                    }
                    (self.detailsWidget.dataViews![3] as! ViewsCreator1).text2!.text = "\(classesNames)"

                    // Set Sections & Subject
                    var SubjectTitle = ""
                    if let teacherDic = json["subject"].dictionary {
                        if let subjectTitle = teacherDic["subjectTitle"]?.string {
                            SubjectTitle = subjectTitle
                        }
                    }
                    var sectionsNames = "NA"
                    if let sectionsArray = json["sections"].array {
                        var counter = 0
                        for item in sectionsArray {
                            if let sectionName = item["sectionName"].string {
                                if counter == 0 { sectionsNames = "" }
                                sectionsNames =  sectionsNames + sectionName
                                if counter != sectionsArray.count - 1 { sectionsNames = sectionsNames + " , " }
                                counter = counter + 1
                            }
                        }
                    }
                    (self.detailsWidget.dataViews![4] as! ViewsCreator1).text1!.text = "\(Language.getTranslationOf(findKey: "sections",defaultWord: "Sections")) : \(sectionsNames)"
                    (self.detailsWidget.dataViews![4] as! ViewsCreator1).text2!.text = "\(Language.getTranslationOf(findKey: "subject",defaultWord: "Subject")) : \(SubjectTitle)"

                    //-------- Parse Students  ------------//
                    // Parse applied students
                    /*if let appliedStudents = json["student_applied"].dictionary {
                        // Key is student id, value is student name
                        self.appliedStudentsDic = appliedStudents
                    }
                    // Parse not applied students
                    if let notAppliedStudents = json["student_not_applied"].dictionary {
                        // Key is student id, value is student name
                        self.notAppliedStudentsDic = notAppliedStudents
                    }*/
                    
                    // Init views

                    self.detailsWidget.changeStatus(currentActiveView: .dataSet)
                    
                    self.containerView.createView() // must recreate widgets container view as if you set label text with larger height will find widget overlap other one - must call it if you reset views after while like load data from internet - used if you use more than one widget in same page
                    
                }else{
                    self.detailsWidget.changeStatus(currentActiveView: .error)
                }
            case .failure(let error):
                Utils.checkFailureCause(error : error, callback: self, navigationController: self.navigationController!,tokenRenewTried:self.tokenRenewTried)
                if self.tokenRenewTried {self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) }
                self.detailsWidget.changeStatus(currentActiveView: .error)
            }
            
        }
    }

    
    
    var tokenRenewTried = false
    func afterRenewToken(success: Bool) {
        tokenRenewTried = true
        if success {
            loadData()
        }else{
            self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
        }
    }
    
    @IBAction func navLeftButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func createHomeworkDetailsView() {
        //=================== Create Views ===================//
        Defaults.setupDefaultWidgetStatus(widget: detailsWidget)
        detailsWidget.setupStatusViews(loadingText: Language.getTranslationOf(findKey: "loading",defaultWord: "Loading"), loadingImage: "no_data", emptyText: Language.getTranslationOf(findKey: "No Entry",defaultWord: "No details"), emptyImage: "no_data", errorText: Language.getTranslationOf(findKey: "errorOccurred",defaultWord: "Error Occurred"), errorImage: "no_data")
        
        //Header
        let headerView = ViewsCreator1()
        
        headerView.isOn_text1 = true
        headerView.text1?.applyDashMainStyle(BiggerSizeMode : false)
            
        headerView.isOn_text2 = true
        headerView.text2?.applyDashSubStyle(BiggerSizeMode : false)
        headerView.text2?.bottomMargin = 12
        
        //Deatils
        var detailsViews = [UIView]()

        //-------- Teacher ------------//
        let teacherDetailsView = ViewsCreator1()
        setupDetailsView(detailsView: teacherDetailsView)
        teacherDetailsView.text1!.translatedText = "Teacher"
        teacherDetailsView.text1!.setMargins(leftMargin: 0, topMargin: 0, rightMargin: 10, bottomMargin: 0)
        
        teacherDetailsView.mainImage!.setSource(imageName: "icon_pages_user", fromInternet: false)
        teacherDetailsView.mainImage!.setMargins(leftMargin: 0, topMargin: 6, rightMargin: 10, bottomMargin: 10)

        _ = teacherDetailsView.createView()
        detailsViews.append(teacherDetailsView)
        
        //-------- Date ------------//

        let dateDetailsView = ViewsCreator1()
        setupDetailsView(detailsView: dateDetailsView)
        dateDetailsView.text1!.translatedText = "Date"
        dateDetailsView.text1!.setMargins(leftMargin: 0, topMargin: 0, rightMargin: 10, bottomMargin: 0)

        
        dateDetailsView.mainImage!.setSource(imageName: "icon_pages_start_time", fromInternet: false)
        dateDetailsView.mainImage!.setMargins(leftMargin: 0, topMargin: 6, rightMargin: 10, bottomMargin: 10)

        _ = dateDetailsView.createView()
        detailsViews.append(dateDetailsView)
        
        //-------- Sub date & Eval Date ------------//
        
        let endDateDetailsView = ViewsCreator1()
        setupDetailsView(detailsView: endDateDetailsView)
        endDateDetailsView.text1!.text = "\(Language.getTranslationOf(findKey: "SubmissionDate",defaultWord: "Submission Date")) - \(Language.getTranslationOf(findKey: "EvaluationDate",defaultWord: "Evaluation Date"))"
        endDateDetailsView.text1!.setMargins(leftMargin: 0, topMargin: 0, rightMargin: 10, bottomMargin: 0)

        endDateDetailsView.mainImage!.setSource(imageName: "icon_pages_end_time", fromInternet: false)
        endDateDetailsView.mainImage!.setMargins(leftMargin: 0, topMargin: 6, rightMargin: 10, bottomMargin: 10)

        _ = endDateDetailsView.createView()
        detailsViews.append(endDateDetailsView)

        //-------- Classes ------------//
        
        let classesView = ViewsCreator1()
        setupDetailsView(detailsView: classesView)
        classesView.text1!.translatedText = "Classes"
        classesView.text1!.setMargins(leftMargin: 0, topMargin: 0, rightMargin: 10, bottomMargin: 0)
        
        classesView.mainImage!.setSource(imageName: "icon_pages_class", fromInternet: false)
        classesView.mainImage!.setMargins(leftMargin: 0, topMargin: 6, rightMargin: 10, bottomMargin: 10)
        
        _ = classesView.createView()
        detailsViews.append(classesView)

        //-------- Sections ------------//
        
        let sectionsSubjectView = ViewsCreator1()
        setupDetailsView(detailsView: sectionsSubjectView)
        sectionsSubjectView.text1!.setMargins(leftMargin: 0, topMargin: 0, rightMargin: 10, bottomMargin: 0)
        
        sectionsSubjectView.mainImage!.setSource(imageName: "icon_pages_subjects", fromInternet: false)
        sectionsSubjectView.mainImage!.setMargins(leftMargin: 0, topMargin: 6, rightMargin: 10, bottomMargin: 10)
        
        _ = sectionsSubjectView.createView()
        detailsViews.append(sectionsSubjectView)
        
        //-------- Answers buttons ------------//

        //Sch Header
        let schHeaderView = ViewsCreator1()
        
        schHeaderView.isOn_topDivider = true
        schHeaderView.isOn_bottomDivider = true
        schHeaderView.dividerColor = SolColors.getColorByName(colorName: "x_divider_color")!
        
        schHeaderView.isOn_text1 = true
        schHeaderView.text1!.text = Language.getTranslationOf(findAndDefaultKey:"Answers")
        schHeaderView.text1?.applyDashMainStyle(BiggerSizeMode : false)
        schHeaderView.text1!.topMargin = 6
        
        _ = schHeaderView.createView()

        detailsViews.append(schHeaderView)

        if Utils.isUserHavePermission(checkThisModules: ["Homework.Answers"]){
            
            let appliedAnswersView = ViewsCreator1()
            setupDetailsView(detailsView: appliedAnswersView)
            appliedAnswersView.text1!.translatedText = "Answers"
            appliedAnswersView.text2!.translatedText = "Applied"
            appliedAnswersView.text1!.setMargins(leftMargin: 0, topMargin: 6, rightMargin: 10, bottomMargin: 0)
            
            appliedAnswersView.mainImage!.setSource(imageName: "icon_pages_pass", fromInternet: false)
            appliedAnswersView.mainImage!.setMargins(leftMargin: 0, topMargin: 6, rightMargin: 10, bottomMargin: 10)
            
            _ = appliedAnswersView.createView()
            
            
            let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.openAppliedAnswers (_:)))
            appliedAnswersView.addGestureRecognizer(gesture)
            
            detailsViews.append(appliedAnswersView)
            
            
            let notAppliedAnswersView = ViewsCreator1()
            setupDetailsView(detailsView: notAppliedAnswersView)
            notAppliedAnswersView.text1!.translatedText = "Answers"
            notAppliedAnswersView.text2!.translatedText = Language.getTranslationOf(findKey: "notApplied",defaultWord: "Not yet Applied")
            notAppliedAnswersView.text1!.setMargins(leftMargin: 0, topMargin: 0, rightMargin: 10, bottomMargin: 0)
            
            notAppliedAnswersView.mainImage!.setSource(imageName: "icon_pages_inactive", fromInternet: false)
            notAppliedAnswersView.mainImage!.setMargins(leftMargin: 0, topMargin: 6, rightMargin: 10, bottomMargin: 10)
            
            _ = notAppliedAnswersView.createView()
            
            let gesture2 = UITapGestureRecognizer(target: self, action:  #selector (self.openNotAppliedAnswers (_:)))
            notAppliedAnswersView.addGestureRecognizer(gesture2)
            
            detailsViews.append(notAppliedAnswersView)

        }
        
        //-------- Set views ------------//

        detailsWidget.injectHeaderView(headerView: headerView, redrawWithEveryDrawCycle: false)
        detailsWidget.injectDataViews(dataViews: detailsViews) // Inject at anytime
        detailsWidget.prepareDataView() // Don't call untill you set data copies
    }
    
    
    @objc func openAppliedAnswers(_ sender:UITapGestureRecognizer){
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let nextVC = storyboard.instantiateViewController(withIdentifier: "HomeworkStudentsPageStoryboard") as! HomeworkStudentsPage
        nextVC.isApplied = true
        nextVC.homeworkID = homeworkID
        self.navigationController!.pushViewController(nextVC, animated: true)
        
    }
    
    @objc func openNotAppliedAnswers(_ sender:UITapGestureRecognizer){
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let nextVC = storyboard.instantiateViewController(withIdentifier: "HomeworkStudentsPageStoryboard") as! HomeworkStudentsPage
        nextVC.isApplied = false
        nextVC.homeworkID = homeworkID
        self.navigationController!.pushViewController(nextVC, animated: true)
        
    }
    
    func setupDetailsView(detailsView : ViewsCreator1) {
        detailsView.isOn_text1 = true
        detailsView.text1?.applyDashMainStyle(BiggerSizeMode : false)
        
        detailsView.isOn_text2 = true
        detailsView.text2?.applyDashSubStyle(BiggerSizeMode : false)
        detailsView.text2?.setMargins(leftMargin: 0, topMargin: 0, rightMargin: 10, bottomMargin: 0)

        detailsView.isOn_mainImage = true
        detailsView.mainImage!.setViewSize(overAllSize: 40)
    }
    
}


