//
//  ViewController.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/15/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework
import SwiftyJSON


class HomeworkStudentsPage:SolViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var dataTableView: UITableView!
    @IBOutlet weak var navLeftButton: UIBarButtonItem!

    var isApplied : Bool = false
    var homeworkID : String = ""

    var searchMode:Bool = false
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //============================ Base Functions =================//
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initTableView(tableView: dataTableView)
        initNavBar()
        initSideMenu()
        initControllerStates()
        
        initPage()

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupInitialViewState()
        
    }
    
    //============================ Init Functions =================//

    func initPage() {
        
        self.navLeftButton.image = Utils.getBackIcon()

        // nav bar title
        self.navigationItem.title = Utils.siteTitle

        if searchAbout != nil {
            searchMode = true
            searchInDataArray();
        }else{
            searchMode = false
            startLoading()
            reloadStudents()
    
        }
    }
    
    func initControllerStates() {
        let failureView = ErrorView(frame: view.frame)
        super.initControllerStates(failureView: failureView)
    }
   
    @IBAction func searchButtonAction(_ sender: Any) {
        showSearchDialog(targetStoryboard: "HomeworkStudentsPageStoryboard")
    }
    
    //============================ Search Functions =================//
    
    func searchInDataArray(){
        if(self.dataArray.count > 0){
            var searchDataArray = [Any]()
            for item in dataArray {
                let cItem = item as! HomeworkStudentModel
                if(cItem.name!.lowercased().contains(self.searchAbout!)){
                    searchDataArray.append(cItem)
                }
            }
            dataArray.removeAll()
            dataArray.append(contentsOf : searchDataArray)
            self.endLoading(error: nil) // Set Content
        }else{
            self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil))
        }
    }
    
   
    @IBAction func navLeftButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    //==================== Table view options ===================================//
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:HomeworkStudentsCell = tableView.dequeueReusableCell(withIdentifier: "cellCon", for: indexPath) as! HomeworkStudentsCell
        cell.item = dataArray[indexPath.row] as! HomeworkStudentModel
        cell.navController = self.navigationController
        
        
        cell.actionBtn.tag = indexPath.row
        cell.actionBtn.addTarget(self, action: #selector(studentApplied(_:)), for: .touchUpInside)

        
        return cell
    }
    
    func reloadStudents() {
        
        let url = "\(Constants.getModRewriteBaseUrl())/homeworks_view/\(homeworkID)"
      
        
        self.dataArray.removeAll()
        self.dataTableView.reloadData()
        startLoading()
        
        SBFramework.request(url, method: .get).responseJSON { response in
            switch response.result {
            case .success(let value):
                
                //======== Parse result ==========//
                let json = JSON(value)
                print(json)
                
                if !(json.null != nil) {
                    
                    //-------- Parse Students  ------------//
                    if(self.isApplied){
                        
                        // Parse applied students
                        if let appliedStudents = json["student_applied"].dictionary {
                            for(userID,userName) in appliedStudents{
                                if let userName = userName.string {
                                    self.dataArray.append(HomeworkStudentModel(id: userID, name: userName,homeworkID : self.homeworkID, isApplied : true))
                                }
                            }
                        }
                        
                    }else{
                        
                        // Parse not applied students
                        if let notAppliedStudents = json["student_not_applied"].dictionary {
                            for(userID,userName) in notAppliedStudents{
                                if let userName = userName.string {
                                    self.dataArray.append(HomeworkStudentModel(id: userID, name: userName,homeworkID : self.homeworkID, isApplied : false))
                                }
                            }
                        }
                        
                    }
                    self.endLoading(error: nil) // Set Content
                    self.dataTableView.reloadData()
                }else{
                    self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
                }
            case .failure( _):
                self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
            }
            
        }
    }
    
    @objc func studentApplied(_ button:UIButton){
        
        let item = dataArray[button.tag] as! HomeworkStudentModel
        let url = "\(Constants.getModRewriteBaseUrl())/homeworks/apply/\(item.homeworkID!)"

        
        var isAppliedInt = "-1"
        if(item.isApplied){
            isAppliedInt = "0"
        }else{
            isAppliedInt = "1"
        }
        let parameters: Parameters = ["student" : item.id!,"status" : isAppliedInt]
        
       
        self.dataArray.removeAll()
        self.dataTableView.reloadData()
        
        startLoading()
        
        SBFramework.request(url, method: .post,parameters: parameters).responseJSON { response in
            switch response.result {
            case .success(let value):
                print(value)
                
                //======== Parse result ==========//
                let json = JSON(value)
                print(json)
                
                if !(json.null != nil && json["status"].string == "success" ) {
                    if let data = json["data"].dictionary {
                        //-------- Parse Students  ------------//
                        if(item.isApplied){
                            
                            // Parse applied students
                            if let appliedStudents = data["student_applied"]?.dictionary {
                                
                                for(userID,userName) in appliedStudents{
                                    if let userName = userName.string {
                                        self.dataArray.append(HomeworkStudentModel(id: userID, name: userName,homeworkID : self.homeworkID, isApplied : true))
                                    }
                                }
                            }
                        }else{
                            // Parse not applied students
                            if let notAppliedStudents = data["student_not_applied"]?.dictionary {
                                for(userID,userName) in notAppliedStudents{
                                    if let userName = userName.string {
                                        self.dataArray.append(HomeworkStudentModel(id: userID, name: userName,homeworkID : self.homeworkID, isApplied : false))
                                    }
                                }
                            }
                        }
                       
                        self.endLoading(error: nil) // Set Content
                        self.dataTableView.reloadData()
                    }else{
                        self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
                    }
                }else{
                    self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
                }
                
                
            case .failure(let error):
                print(error)
                self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
            }
        }
    }
    
}






