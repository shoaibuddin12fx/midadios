//
//  ViewController.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/15/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework
import SwiftyJSON


class MessagesPage:SolViewController,UITableViewDataSource,UITableViewDelegate,RenewTokenCallback {
    
    @IBOutlet weak var dataTableView: UITableView!
    @IBOutlet weak var navLeftButton: UIBarButtonItem!

    var page : Int = 1
    var loadMoreLocked : Bool = false
    var searchMode:Bool = false
    
    var openThisMessageID: String!

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //============================ Base Functions =================//
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initTableView(tableView: dataTableView)
        initNavBar()
        initSideMenu()
        initControllerStates()
        
        
        if(openThisMessageID != nil && openThisMessageID != ""){
            let MessageDialogVC = MessagesDialogPage()
            MessageDialogVC.serverMessageDialogID = openThisMessageID
            self.navigationController?.pushViewController(MessageDialogVC, animated: true)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (Utils.App_IsRtl){
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }
        
        setupInitialViewState()
        
        initPage()
    }
    
    //============================ Init Functions =================//

    func initPage() {
        
        if searchAbout != nil {
            searchMode = true
            self.navLeftButton.image = Utils.getBackIcon()
            self.searchUser()
            
            // remove button action
            self.navigationItem.rightBarButtonItem = nil
            
        }else{
            searchMode = false
            self.navLeftButton.image = UIImage(named: "icn_drawer")
            self.loadData(isLoadMore: false)
        }
        
        
        // nav bar title
        self.navigationItem.title = Utils.siteTitle
    }
    
    func initControllerStates() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(failLoadData))
        super.initControllerStates(failureView: failureView)
    }
    
    //==================== Load data ===================================//
    @objc func failLoadData() {
        if(!searchMode){
            loadData(isLoadMore : false)
        }else{
            self.searchUser()
        }
    }
    
    @objc func loadData(isLoadMore : Bool) {
        //if (lastState == .loading) { return }
        if(!isLoadMore){
            self.page = 1
            self.loadMoreLocked = false
            self.dataArray.removeAll()
            self.dataTableView.reloadData()
            startLoading()
        }else{
            if(loadMoreLocked){return}
        }
        let url = "\(Constants.getModRewriteBaseUrl())/messages/listAll/\(self.page)"
        
            SBFramework.request(url, method: .get).responseJSON { response in
                switch response.result {
                case .success(let value):
                    
                    let json = JSON(value)
                    if (json["error"].null == nil){
                        if(!self.tokenRenewTried){ Utils.renewToken(callback : self,navigationController : self.navigationController!) }
                    }
                    self.parseData(response: value,isLoadMore : isLoadMore)
                case .failure(let error):
                    Utils.checkFailureCause(error : error, callback: self, navigationController: self.navigationController!,tokenRenewTried:self.tokenRenewTried)
                    if self.tokenRenewTried {self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) }
                }
            }
        
    }
    var tokenRenewTried = false
    func afterRenewToken(success: Bool) {
        tokenRenewTried = true
        if success {
            if(!searchMode){
                loadData(isLoadMore: false)
            }else{
                searchUser()
            }
        }else{
            self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
        }
    }
    func parseData(response:Any,isLoadMore:Bool)  {
        //======== Parse result ==========//
        let json = JSON(response)
        if let jsonArray = json["messages"].array {
            
            //================= Fill data array ==================//
            for item in jsonArray {
                self.dataArray.append(MessagesDialogModel(
                    id : Utils.getStringValueOf(sourceInput: item,itemKey:"id") ,
                    lastMessageDate : Utils.getStringValueOf(sourceInput: item,itemKey:"lastMessageDate") ,
                    lastMessage : Utils.getStringValueOf(sourceInput: item,itemKey:"lastMessage") ,
                    userId : Utils.getStringValueOf(sourceInput: item,itemKey:"userId"),
                    fullName : Utils.getStringValueOf(sourceInput: item,itemKey:"fullName"),
                    messageStatus : Utils.getStringValueOf(sourceInput: item,itemKey:"messageStatus")
                    )
                )
                
            }
            
            if(isLoadMore){
                if(jsonArray.count == 0){
                    self.loadMoreLocked = true
                }
            }
            
            self.endLoading(error: nil) // Set Content
            self.dataTableView.reloadData()
            self.page = self.page + 1

            //self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
            //self.endLoading(error: nil) // No Content
            
        }else{
            self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
        }
    }
    //==================== Nav Bar Buttons Actions ===================================//

    @IBAction func reloadPage(_ sender: Any) {
        if(!searchMode){
            loadData(isLoadMore: false);
        }else{
            searchUser()
        }
    }
    @IBAction func newMessageBtn(_ sender: Any) {
        showSearchDialog(targetStoryboard: "MessagesPageStoryboard")
    }
    
   
    @IBAction func navLeftButtonAction(_ sender: Any) {
         if(!searchMode){
            showMenu()
         }else{
            self.navigationController?.popViewController(animated: true)
        }
        
    }

    //==================== Table view options ===================================//
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:MessagesDialogsCell = tableView.dequeueReusableCell(withIdentifier: "cellCon", for: indexPath) as! MessagesDialogsCell
        cell.item = dataArray[indexPath.row] as? MessagesDialogModel
        cell.navController = self.navigationController
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if(!searchMode){
            let lastElement = dataArray.count - 1
            if indexPath.row == lastElement {
                self.loadData(isLoadMore: true)
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let MessageDialogVC = MessagesDialogPage()
        let item = self.dataArray[indexPath.row] as! MessagesDialogModel
        
        if(!searchMode){
            MessageDialogVC.serverMessageDialogID = item.id ?? ""
            self.navigationController?.pushViewController(MessageDialogVC, animated: true)
        }else{
            sendUserNewMessage(toUsername: item.userId ?? "") // here userId contain username
        }
        
    }
    
    //========================== Send New Message =================================//
    
    @objc func searchUser() {
        self.dataArray.removeAll()
        self.dataTableView.reloadData()
        startLoading()
        
        let url = "\(Constants.getModRewriteBaseUrl())/messages/searchUser/\(self.searchAbout ?? "")"
        
        SBFramework.request(url, method: .get).responseJSON { response in
            switch response.result {
            case .success(let value):
                
                let json = JSON(value)

                // Parse Users
                if !(json.null != nil){
                    
                    if let usersDic = json.dictionary{
                        for (_,subJson):(String, JSON) in usersDic {
                            
                            self.dataArray.append(MessagesDialogModel(
                                id : Utils.getStringValueOf(sourceInput: subJson,itemKey:"id") ,
                                lastMessageDate : Utils.getStringValueOf(sourceInput: subJson,itemKey:"role") ,
                                lastMessage : Utils.getStringValueOf(sourceInput: subJson,itemKey:"email") ,
                                userId : Utils.getStringValueOf(sourceInput: subJson,itemKey:"username") ,
                                fullName : Utils.getStringValueOf(sourceInput: subJson,itemKey:"name"),
                                messageStatus : ""
                                )
                            )
                            
                        }
                        
                    }
                    self.endLoading(error: nil) // Set Content
                    self.dataTableView.reloadData()
                }else{
                    self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
                }
            case .failure(let error):
                Utils.checkFailureCause(error : error, callback: self, navigationController: self.navigationController!,tokenRenewTried:self.tokenRenewTried)
                if self.tokenRenewTried {self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) }
            }
        }
        
    }
    
    public func sendUserNewMessage(toUsername : String) {
        
        
        let alertController = UIAlertController(title: Language.getTranslationOf(findAndDefaultKey:"New message"), message: Language.getTranslationOf(findAndDefaultKey:"Write message to send"), preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: Language.getTranslationOf(findAndDefaultKey:"OK"), style: .default) { (_) in
            
            
            if let searchAboutWord = alertController.textFields?[0].text {
                if(searchAboutWord.count > 0){
                    
                    Utils.showMsg(msg: "Sending...")
                    
                    let url = "\(Constants.getModRewriteBaseUrl())/messages"
                    
                    let parameters: Parameters = [
                        "messageText": searchAboutWord ,
                        "toId" : toUsername
                    ]
                    
                    SBFramework.request(url, method: .post, parameters: parameters).responseJSON { response in
                        switch response.result {
                        case .success(let value):
                            
                            //======== Parse result ==========//
                            let json = JSON(value)
                            

                            if let returnedValue = json.dictionary{
                                if let messageID = returnedValue["messageId"]?.intValue {
                                    
                                    let MessageDialogVC = MessagesDialogPage()
                                    MessageDialogVC.serverMessageDialogID = String(messageID)
                                    self.navigationController?.pushViewController(MessageDialogVC, animated: true)
                                    
                                }else{
                                    Utils.showMsg(msg: "Error Occurred")
                                }
                            }else{
                                Utils.showMsg(msg: "Error Occurred")
                            }
                        case .failure(_):
                            Utils.showMsg(msg: "Error Occurred")
                        }
                        
                    }
                }else{
                    Utils.showMsg(msg: Language.getTranslationOf(findAndDefaultKey:"Write message to send"))
                }
            }else{
                Utils.showMsg(msg: Language.getTranslationOf(findAndDefaultKey:"Write message to send"))
            }
            
        }
        
        let cancelAction = UIAlertAction(title: Language.getTranslationOf(findAndDefaultKey:"Cancel"), style: .cancel) { (_) in }
        
        alertController.addTextField { (textField) in
            textField.placeholder = Language.getTranslationOf(findAndDefaultKey:"Enter text value")
        }
        
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}






