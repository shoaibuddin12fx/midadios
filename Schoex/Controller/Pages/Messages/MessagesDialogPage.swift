//
//  ViewController.swift
//  Messenger
//
//  Created by Stephen Radford on 08/06/2018.
//  Copyright © 2018 Cocoon Development Ltd. All rights reserved.
//

import UIKit
import SBFramework
import SwiftyJSON


class MessagesDialogPage: MSGMessengerViewController {
    
    let activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
    var myID = Utils.getLoggedInUser().LoggedInUserId
    var myName = Utils.getLoggedInUser().LoggedInUserFullName
    var myUser : User = User(displayName: Utils.getLoggedInUser().LoggedInUserFullName ?? "My", avatar: nil, avatarUrl: nil, isSender: true)
    
    var talkToUser : User = User(displayName: "NA", avatar: nil, avatarUrl: nil, isSender: false)
    var talkToID = 0
    var talkToName = ""
    
    var serverMessageDialogID = ""
    var id = 0
    var oldOffset : CGFloat = CGFloat()
    var checkNewMsgsTimer = Timer()
    
    var initMsgLoadLocked = false
    var oldMsgLoadLocked = false
    var newMsgLoadLocked = false
    
    
    override var style: MSGMessengerStyle {
        var style = MessengerKit.Styles.iMessage
        style.headerHeight = 0
        //        style.inputPlaceholder = "Message"
        //        style.alwaysDisplayTails = true
        //        style.outgoingBubbleColor = .magenta
        //        style.outgoingTextColor = .black
        //        style.incomingBubbleColor = .green
        //        style.incomingTextColor = .yellow
        //        style.backgroundColor = .orange
        //        style.inputViewBackgroundColor = .purple
        return style
    }
    
    
    lazy var messages: [[MSGMessage]] = {
        return []
    }()
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (Utils.App_IsRtl){
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }

        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.lightGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.8
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.navigationController?.navigationBar.layer.shadowRadius = 2
        
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.barTintColor = UIColor.white

        self.navigationController?.navigationBar.topItem?.title = Language.getTranslationOf(findAndDefaultKey:"Messages")
        let attributes = [NSAttributedString.Key.font: UIFont(name: "Cairo-Regular", size: 17)!]
        self.navigationController?.navigationItem.rightBarButtonItem?.setTitleTextAttributes(attributes, for: .normal)
        self.navigationController?.navigationItem.leftBarButtonItem?.setTitleTextAttributes(attributes, for: .normal)

        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.topItem?.title = Utils.siteTitle
        stopNewMsgsCheckTimer()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        // must set childViewControllerForStatusBarStyle extension ( look on Extenstions )
        return UIStatusBarStyle.default
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        dataSource = self
        delegate = self
        collectionView.backgroundView = activityIndicatorView
        
        loadMessages()
        
        
        checkNewMsgsTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.checkNewMessages), userInfo: nil, repeats: true)

    }
    
    func stopNewMsgsCheckTimer() {
        checkNewMsgsTimer.invalidate()
        checkNewMsgsTimer = Timer()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        collectionView.scrollToBottom(animated: false)
    }
    
    
    func postMessageToServer(inputView : MSGInputView) {
        
        if self.serverMessageDialogID != "" && self.talkToID != 0 && inputView.message.count > 0 {
            inputView.textView.isUserInteractionEnabled = false
            inputView.textView.label.text = "Sending..."
            
            let url = "\(Constants.getModRewriteBaseUrl())/messages/\(self.serverMessageDialogID)"
            
            let parameters: Parameters = [
                "reply": inputView.message ,
                "toId" : self.talkToID ,
                "disable" : true
            ]
            
            SBFramework.request(url, method: .post, parameters: parameters).responseJSON { response in
                switch response.result {
                case .success(let value):
                    
                    //======== Parse result ==========//
                    let json = JSON(value)
                    if let returnedValue = json.int{
                        if returnedValue != 1 {
                            Utils.showMsg(msg: "Error Occurred")
                        }else{
                            self.checkNewMessages()
                        }
                    }else{
                        Utils.showMsg(msg: "Error Occurred")
                    }
                    inputView.textView.isUserInteractionEnabled = true
                    inputView.textView.label.text = inputView.textView.placeholder

                case .failure(let error):
                    print(error)
                    Utils.showMsg(msg: "Error Occurred")
                    inputView.textView.isUserInteractionEnabled = true
                    inputView.textView.label.text = inputView.textView.placeholder

                }
                
            }
        }
    }
    
    func loadMessages() {
        if !initMsgLoadLocked{
            initMsgLoadLocked = true
            
            activityIndicatorView.startAnimating()
            
            let url = "\(Constants.getModRewriteBaseUrl())/messages/\(self.serverMessageDialogID)"
            
            SBFramework.request(url, method: .get).responseJSON { response in
                switch response.result {
                case .success(let value):
                    
                    //======== Parse result ==========//
                    let json = JSON(value)
                    
                    // Person you talk to "fullName", "toId" BUT "fromId" is my id
                    if let messageDet = json["messageDet"].dictionary {
                        
                        self.talkToName = messageDet["fullName"]?.string ?? ""
                        self.talkToID = messageDet["toId"]?.intValue ?? 0
                        

                        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: self.talkToName, style: UIBarButtonItemStyle.plain, target: self, action: nil)
                        
                        self.talkToUser = User(displayName: self.talkToName, avatar: nil, avatarUrl: nil, isSender: false)
                        
                    }else{
                        //error
                    }
                    if let messagesJSON = json["messages"].array {
                        if messagesJSON.count > 0{
                            for message in messagesJSON {
                                let fromID = Utils.getIntValueOf(sourceInput: message,itemKey:"fromId")
                                
                                self.insertTop(MSGMessage(id: Utils.getIntValueOf(sourceInput: message,itemKey:"id"),
                                                          body: .text(Utils.getStringValueOf(sourceInput: message,itemKey:"messageText")),
                                                          user: fromID == self.myID ? self.myUser : self.talkToUser,
                                                          sentAt: Utils.getStringValueOf(sourceInput: message,itemKey:"dateSentH"),
                                                          sentAtTimestamp : Utils.getStringValueOf(sourceInput: message,itemKey:"dateSent")),
                                               scrollToBottom: true)
                                
                            }
                        }else{
                            // no messages
                        }
                    }else{
                        //error
                        
                    }
                    
                    self.initMsgLoadLocked = false
                    self.activityIndicatorView.stopAnimating()
                    
                case .failure( _):
                    //error
                    self.initMsgLoadLocked = false
                    self.activityIndicatorView.stopAnimating()
                }
            }
        }
        
    }
    
    
    @objc
    func checkNewMessages() {
        
        if !newMsgLoadLocked && !initMsgLoadLocked {
            newMsgLoadLocked = true
            
            
            var url = ""
            if self.messages.count > 0{
                url = "\(Constants.getModRewriteBaseUrl())/messages/ajax/\(self.myID ?? 0)/\(self.talkToID)/\(self.messages[self.messages.count - 1][0].sentAtTimestamp)"
            }else{
                url = "\(Constants.getModRewriteBaseUrl())/messages/ajax/\(self.myID ?? 0)/\(self.talkToID) "
            }
            
            
            SBFramework.request(url, method: .get).responseJSON { response in
                switch response.result {
                case .success(let value):
                    
                    //======== Parse result ==========//
                    let json = JSON(value)

                    if let messagesJSON = json.array?.reversed() {
                        if messagesJSON.count > 0{
                            self.oldOffset = self.collectionView.contentSize.height - self.collectionView.contentOffset.y
                            for message in messagesJSON {
                                let fromID = Utils.getIntValueOf(sourceInput: message,itemKey:"fromId")
                                
                                self.insertBottom(MSGMessage(id: Utils.getIntValueOf(sourceInput: message,itemKey:"id"),
                                                          body: .text(Utils.getStringValueOf(sourceInput: message,itemKey:"messageText")),
                                                          user: fromID == self.myID ? self.myUser : self.talkToUser,
                                                          sentAt: Utils.getStringValueOf(sourceInput: message,itemKey:"dateSentH"),
                                                          sentAtTimestamp: Utils.getStringValueOf(sourceInput: message,itemKey:"dateSent")))
                                
                            }
                        }
                    }
                    
                    self.newMsgLoadLocked = false
                case .failure( _):
                    //error
                    self.newMsgLoadLocked = false
                }
            }
        }
        
    }
    
    
    func checkOldMsgs()  {
        if !oldMsgLoadLocked && !initMsgLoadLocked{
            oldMsgLoadLocked = true
            
            let url = "\(Constants.getModRewriteBaseUrl())/messages/before/\(self.myID ?? 0)/\(self.talkToID)/\(self.messages[0][0].sentAtTimestamp)"
            
            
            SBFramework.request(url, method: .get).responseJSON { response in
                switch response.result {
                case .success(let value):
                    
                    //======== Parse result ==========//
                    let json = JSON(value)
                    
                    if let messagesJSON = json.array {
                        if messagesJSON.count > 0{
                            self.oldOffset = self.collectionView.contentSize.height - self.collectionView.contentOffset.y
                            for message in messagesJSON {
                                let fromID = Utils.getIntValueOf(sourceInput: message,itemKey:"fromId")
                                
                                self.insertTop(MSGMessage(id: Utils.getIntValueOf(sourceInput: message,itemKey:"id"),
                                                          body: .text(Utils.getStringValueOf(sourceInput: message,itemKey:"messageText")),
                                                          user: fromID == self.myID ? self.myUser : self.talkToUser,
                                                          sentAt: Utils.getStringValueOf(sourceInput: message,itemKey:"dateSentH"),
                                                          sentAtTimestamp: Utils.getStringValueOf(sourceInput: message,itemKey:"dateSent")),
                                               scrollToBottom: false)
                                
                            }
                            self.oldMsgLoadLocked = false
                        }else{
                            // no old messages
                            self.oldMsgLoadLocked = true
                        }
                    }else{
                        self.oldMsgLoadLocked = false
                    }
                case .failure( _):
                    //error
                    self.oldMsgLoadLocked = false
                }
            }
        }
    }
    
    override func inputViewPrimaryActionTriggered(inputView: MSGInputView) {
        if(inputView.message.count > 0){
            
            self.postMessageToServer(inputView: inputView)
            
            /*id += 1
            let body: MSGMessageBody = .text(inputView.message)
            let message = MSGMessage(id: id, body: body, user: self.myUser, sentAt: "2550", sentAtTimestamp: "")
            insertBottom(message)*/
        }
        
    }
    
    func insertBottom(_ message: MSGMessage) {
        collectionView.performBatchUpdates({
            if self.messages.count > 0 {
                self.messages.append([message])
                
                let sectionIndex = self.messages.count - 1
                
                let itemIndex = self.messages[sectionIndex].count - 1
                
                self.collectionView.insertSections([sectionIndex])
                self.collectionView.insertItems(at: [IndexPath(item: itemIndex, section: sectionIndex)])
                
            } else {
                self.messages.append([message])
                let sectionIndex = self.messages.count - 1
                self.collectionView.insertSections([sectionIndex])
            }
        }, completion: { (_) in
            self.collectionView.scrollToBottom(animated: true)
            self.collectionView.layoutTypingLabelIfNeeded()
        })
        
    }
    
    func insertTop(_ message: MSGMessage,scrollToBottom : Bool) {
        collectionView.performBatchUpdates({
            if self.messages.count > 0 {
                
                self.messages.insert([message], at: 0)
                
                let sectionIndex = 0
                let itemIndex = 0//self.messages[sectionIndex].count - 1
                
                self.collectionView.insertSections([sectionIndex])
                self.collectionView.insertItems(at: [IndexPath(item: itemIndex, section: sectionIndex)])
                
            } else {
                self.messages.append([message])
                let sectionIndex = self.messages.count - 1
                self.collectionView.insertSections([sectionIndex])
            }
            
        }, completion: { (_) in
            if scrollToBottom {
                self.collectionView.scrollToBottom(animated: true)
                self.collectionView.layoutTypingLabelIfNeeded()
            }else{
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    self.collectionView.contentOffset = CGPoint(x: 0, y: self.collectionView.contentSize.height - self.oldOffset)
                }
                
            }
        })
        
    }
    
    
    
    override func insert(_ message: MSGMessage) {
        collectionView.performBatchUpdates({
            
            
            if self.messages.count > 0 {
                self.messages.append([message])
                
                let sectionIndex = self.messages.count - 1
                
                let itemIndex = self.messages[sectionIndex].count - 1
                
                self.collectionView.insertSections([sectionIndex])
                self.collectionView.insertItems(at: [IndexPath(item: itemIndex, section: sectionIndex)])
                
            } else {
                self.messages.append([message])
                let sectionIndex = self.messages.count - 1
                self.collectionView.insertSections([sectionIndex])
            }
            
        }, completion: { (_) in
            self.collectionView.scrollToBottom(animated: true)
            self.collectionView.layoutTypingLabelIfNeeded()
        })
        
    }
    
    override func insert(_ messages: [MSGMessage], callback: (() -> Void)? = nil) {
        
        collectionView.performBatchUpdates({
            for message in messages {
                if self.messages.count > 0 {
                    self.messages[self.messages.count - 1].append(message)
                    
                    let sectionIndex = self.messages.count - 1
                    let itemIndex = self.messages[sectionIndex].count - 1
                    self.collectionView.insertItems(at: [IndexPath(item: itemIndex, section: sectionIndex)])
                    
                } else {
                    self.messages.append([message])
                    let sectionIndex = self.messages.count - 1
                    self.collectionView.insertSections([sectionIndex])
                }
            }
        }, completion: { (_) in
            self.collectionView.scrollToBottom(animated: false)
            self.collectionView.layoutTypingLabelIfNeeded()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                callback?()
            }
        })
        
    }
    
}


// MARK: - MSGDataSource

extension MessagesDialogPage: MSGDataSource {
    
    func numberOfSections() -> Int {
        return messages.count
    }
    
    func numberOfMessages(in section: Int) -> Int {
        return messages[section].count
    }
    
    func message(for indexPath: IndexPath) -> MSGMessage {
        return messages[indexPath.section][indexPath.item]
    }
    
    func footerTitle(for section: Int) -> String? {
        return messages[section][0].sentAt
    }
    
    func headerTitle(for section: Int) -> String? {
        return messages[section].first?.user.displayName
    }
    
    
    
    func loadOldMessages(){
        checkOldMsgs()
    }
    
    
}

// MARK: - MSGDelegate

extension MessagesDialogPage: MSGDelegate {
    
    func linkTapped(url: URL) {
        print("Link tapped:", url)
    }
    
    func avatarTapped(for user: MSGUser) {
        print("Avatar tapped:", user)
    }
    
    func tapReceived(for message: MSGMessage) {
        print("Tapped: ", message)
    }
    
    func longPressReceieved(for message: MSGMessage) {
        print("Long press:", message)
    }
    
    func shouldDisplaySafari(for url: URL) -> Bool {
        return true
    }
    
    func shouldOpen(url: URL) -> Bool {
        return true
    }
    
}

