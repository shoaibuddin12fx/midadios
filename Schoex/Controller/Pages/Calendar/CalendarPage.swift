
import UIKit
import SBFramework
import SwiftyJSON
import WebKit

class CalendarPage: SolViewController,UITableViewDataSource,UITableViewDelegate,DatePickerProtocol,WKNavigationDelegate {
    
    @IBOutlet weak var navLeftButton: UIBarButtonItem!
    @IBOutlet weak var dataTableView: UITableView!

    var htmlView = WKWebView(frame: CGRect(), configuration: WKWebViewConfiguration())
    var bridge: WKWebViewJavascriptBridge!

    
    var blocksStates : [String:CalendarBlockStateModel] = [String:CalendarBlockStateModel]()
    var lastLoadedBlockFromCalendar = (startDay : "" , lastDay : "") // Last loaded block ( start,end days ) from calendar
    var daysNamePosition : [String:Int] = [String:Int]()

    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //============================ Base Functions =================//
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup Webview
        htmlView.navigationDelegate = self
        htmlView.isOpaque = false
        
        // Base Functions
        initTableView(tableView: dataTableView)
        initNavBar()
        initSideMenu()
        initControllerStates()
        
        // Init Functions
        initPage()
        
   
    }
   
    func initControllerStates() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(loadLocalWebPage))
        super.initControllerStates(failureView: failureView)
    }
    
    //============================ Init Functions =================//
    
    func initPage() {
        // Setup Page
        startLoading()
        self.navLeftButton.image = UIImage(named: "icn_drawer")
        self.title = Utils.siteTitle
        
        // Register Webview Bridge And Recieve Webview Response Here
        bridge = WKWebViewJavascriptBridge(webView: htmlView)
        bridge.isLogEnable = true
          
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupInitialViewState()
        self.loadLocalWebPage()
    }
    //==================== Load Calendar Dates From JS ===================================//
    
    @objc
    func loadLocalWebPage() {
        enum LoadDemoPageError: Error {
            case nilPath
        }
        
        do {
            guard let pagePath = Bundle.main.path(forResource: Utils.App_CalendarType, ofType: "html") else {
                throw LoadDemoPageError.nilPath
            }
            let pageHtml = try String(contentsOfFile: pagePath, encoding: .utf8)
            let baseURL = URL(fileURLWithPath: pagePath)
            htmlView.loadHTMLString(pageHtml, baseURL: baseURL)
        } catch LoadDemoPageError.nilPath {
            print(print("webView loadDemoPage error: pagePath is nil"))
            self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
        } catch let error {
            print("webView loadDemoPage error: \(error)")
            self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
        }
    }
    
    @objc func loadDatesFromCalendar() {
        
        // Load Dates
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { // Change `2.0` to the desired number of seconds.
            let data = ["gcalendar":Utils.App_CalendarType,"lDay":self.lastLoadedBlockFromCalendar.lastDay]
            self.bridge.call(handlerName: "getDaysHandler", data: data) { (response) in
                
                print("testJavascriptHandler responded: \(String(describing: response))")

                if response != nil {
                    
                    let daysArray = response as! [String]
                    
                    let startDay = daysArray[0]
                    let lastDay = daysArray[daysArray.count - 1]
                    
                    // Set last loaded block dates
                    self.lastLoadedBlockFromCalendar.startDay = startDay
                    self.lastLoadedBlockFromCalendar.lastDay  = lastDay
                    
                    // Add Blocks States With Loading State
                    self.blocksStates[startDay] = CalendarBlockStateModel(lastDay: lastDay, state: .loading)
                    
                    // Set Block Days
                    for day in daysArray {
                        let calDay = CalendarBlockDay(dayName: day, dayEvents: [CalendarEventModel](), startDayOfBlock: startDay)
                        self.dataArray.append(calDay)
                        let addingPosition = (self.dataArray as? [CalendarBlockDay])!.firstIndex(of: calDay)
                        self.daysNamePosition[day] = addingPosition
                        
                    }
                    
                    // Change page state to DATASET View
                    self.endLoading(error: nil) // Set Content
                    self.dataTableView.reloadData()
                    
                    self.loadDatesEvents(days: daysArray)
                    
                }
                
           }
        }
        
    }
    
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
       
     }
     
     func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.loadDatesFromCalendar()
     }
     
     func webView(webView: WKWebView, didFailNavigation navigation: WKNavigation, withError error: NSError) {
        self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
     }

    //==================== Load Dates Events From Server ===================================//

    
    @objc func loadDatesEvents(days : [String]) {
        
        let url = "\(Constants.getModRewriteBaseUrl())/calender?start=\(days[0])&end=\(days[days.count - 1])"
        
        SBFramework.request(url, method: .get).responseJSON { response in
            switch response.result {
            case .success(let value):
                //======== Parse result ==========//
                let json = JSON(value)
                if let jsonArray = json.array {

                    //================= Fill data array ==================//
                    for item in jsonArray {
                        
                        
                        let startDate = Utils.getStringValueOf(sourceInput: item,itemKey:"start")
                        let DestDate = String(startDate.prefix(10))
                        
                        let calEventItem = CalendarEventModel(
                            id: Utils.getStringValueOf(sourceInput: item,itemKey:"id"),
                            title: Utils.getStringValueOf(sourceInput: item,itemKey:"title"),
                            start: Utils.getStringValueOf(sourceInput: item,itemKey:"start"),
                            url: Utils.getStringValueOf(sourceInput: item,itemKey:"url"),
                            backgroundColor: Utils.getStringValueOf(sourceInput: item,itemKey:"backgroundColor"),
                            textColor: Utils.getStringValueOf(sourceInput: item,itemKey:"textColor"),
                            allDay: Utils.getStringValueOf(sourceInput: item,itemKey:"allDay")
                            )
                        print("wwq \(DestDate)")
                        if let dayPosition = self.daysNamePosition[DestDate]{
                            if let blockDay = (self.dataArray[dayPosition] as? CalendarBlockDay){
                                blockDay.dayEvents.append(calEventItem)
                            }
                        }
                    }
                    self.blocksStates[days[0]]?.state = .success
                    self.endLoading(error: nil) // Set Content
                    self.dataTableView.reloadData()
                    
                }else{
                    self.blocksStates[days[0]]?.state = .failed
                    self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
                }
                
            case .failure(_):
                self.blocksStates[days[0]]?.state = .failed
                self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
            }
        }
    }
    
    
    
    //==================== Nav Bar Buttons Actions ===================================//
    
    @IBAction func reloadPage(_ sender: Any) {
        self.dataArray.removeAll()
        self.blocksStates.removeAll()
        self.lastLoadedBlockFromCalendar.lastDay = ""
        self.lastLoadedBlockFromCalendar.startDay = ""
        self.daysNamePosition.removeAll()

        
        self.dataTableView.reloadData()
        
        //startLoading()
        loadLocalWebPage()
    }
    
    @IBAction func searchButtonAction(_ sender: Any) {
        self.openDatePicker()
    }

    @IBAction func navLeftButtonAction(_ sender: Any) {
        showMenu()
    }
    
    //==================== Table view options ===================================//
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:CalendarItemCell = tableView.dequeueReusableCell(withIdentifier: "cellCon", for: indexPath) as! CalendarItemCell
        let blockItem = dataArray[indexPath.row] as? CalendarBlockDay
        if let blockStateItem = blocksStates[(blockItem?.startDayOfBlock)!]{
            cell.blockState = blockStateItem.state
        }else{
            cell.blockState = CalendarBlockStateModel.blockState.failed
        }
        cell.navController = self.navigationController
        cell.index = indexPath.row
        cell.item = blockItem
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = dataArray.count - 1
        if indexPath.row == lastElement {
            self.loadDatesFromCalendar()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        /*let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeworkDetailsPageStoryboard") as! HomeworkDetailsPage
        
        let item = self.dataArray[indexPath.row] as! HomeworkModel
        nextVC.homeworkID = item.id!
        
        self.navigationController!.pushViewController(nextVC, animated: true)*/
    }

    //============================ Protocols callbacks =================//
    
    @objc func openDatePicker() {
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: ("DatePickerPageStoryboard")) as! DatePickerPage
        nextVC.datePickedDelegate = self
        nextVC.goBackAfterDatePicked = false
        self.navigationController!.pushViewController(nextVC, animated: true)
        
    }
    
    
    func onDatePicked(tag : String, dateSelected: String) {
        
        let replacedDate = dateSelected.replacingOccurrences(of: "/", with: "-")
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: ("CalendarDetailsPageStoryboard")) as! CalendarDetailsPage
        nextVC.selectedDate = replacedDate
        self.navigationController!.pushViewController(nextVC, animated: true)
        
    }
    
}
