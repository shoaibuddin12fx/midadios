//
//  ViewController.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/15/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework
import SwiftyJSON


class CalendarDetailsPage:SolViewController,UITableViewDataSource,UITableViewDelegate,RenewTokenCallback {
    
    @IBOutlet weak var dataTableView: UITableView!
    @IBOutlet weak var navLeftButton: UIBarButtonItem!
    
    var selectedDate : String = ""
    
    //============================ Base Functions =================//
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initTableView(tableView: dataTableView)
        initNavBar()
        initSideMenu()
        initControllerStates()
        
        initPage()

    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupInitialViewState()
        
    }
    
    //============================ Init Functions =================//

    func initPage() {
        
        startLoading()
        self.navLeftButton.image = Utils.getBackIcon()
        self.loadData()

        // nav bar title
        self.navigationItem.title = Utils.siteTitle
    }
    
    func initControllerStates() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(loadData))
        super.initControllerStates(failureView: failureView)
    }
    
    //==================== Load data ===================================//
    @objc func loadData() {
        self.dataArray.removeAll()
        self.dataTableView.reloadData()
        startLoading ()
        
        let url = "\(Constants.getModRewriteBaseUrl())/calender?start=\(selectedDate)&end=\(selectedDate)"
        
        SBFramework.request(url, method: .get).responseJSON { response in
            switch response.result {
            case .success(let value):
                
                //======== Parse result ==========//
                let json = JSON(value)
                if let jsonArray = json.array {
                    
                    //================= Fill data array ==================//
                    for item in jsonArray {
                        
                        let calEventItem = CalendarEventModel(
                            id: Utils.getStringValueOf(sourceInput: item,itemKey:"id"),
                            title: Utils.getStringValueOf(sourceInput: item,itemKey:"title"),
                            start: Utils.getStringValueOf(sourceInput: item,itemKey:"start"),
                            url: Utils.getStringValueOf(sourceInput: item,itemKey:"url"),
                            backgroundColor: Utils.getStringValueOf(sourceInput: item,itemKey:"backgroundColor"),
                            textColor: Utils.getStringValueOf(sourceInput: item,itemKey:"textColor"),
                            allDay: Utils.getStringValueOf(sourceInput: item,itemKey:"allDay")
                        )
                        self.dataArray.append(calEventItem)
                        
                    }
                    self.endLoading(error: nil) // Set Content
                    self.dataTableView.reloadData()
                    
                }else{
                    self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
                }
                
            case .failure(let error):
                Utils.checkFailureCause(error : error, callback: self, navigationController: self.navigationController!,tokenRenewTried:self.tokenRenewTried)
                if self.tokenRenewTried {self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) }
            }
        }
    }
    var tokenRenewTried = false
    func afterRenewToken(success: Bool) {
        tokenRenewTried = true
        if success {
            loadData()
        }else{
            self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
        }
    }
    
    //==================== Nav Bar Buttons Actions ===================================//

    @IBAction func reloadPage(_ sender: Any) {
        self.dataArray.removeAll()
        loadData();
    }
   
    @IBAction func navLeftButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
   

    //==================== Table view options ===================================//
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:CalendarDetailsCell = tableView.dequeueReusableCell(withIdentifier: "cellCon", for: indexPath) as! CalendarDetailsCell
        cell.item = dataArray[indexPath.row] as? CalendarEventModel
        return cell
    }
    
}

