//
//  ViewController.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/15/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework
import SwiftyJSON


class HostelsPage:SolViewController,UITableViewDataSource,UITableViewDelegate,RenewTokenCallback {
    
    @IBOutlet weak var dataTableView: UITableView!
    @IBOutlet weak var navLeftButton: UIBarButtonItem!

    var searchMode:Bool = false
    
    //============================ Base Functions =================//
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initTableView(tableView: dataTableView)
        initNavBar()
        initSideMenu()
        initControllerStates()
        
        initPage()

    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupInitialViewState()
        
    }
    
    //============================ Init Functions =================//

    func initPage() {
        if searchAbout != nil {
            searchMode = true
            searchInDataArray();
            self.navLeftButton.image = Utils.getBackIcon()
        }else{
            searchMode = false
            startLoading()
            self.navLeftButton.image = UIImage(named: "icn_drawer")
            self.loadData()
        }
        // nav bar title
        self.navigationItem.title = Utils.siteTitle
    }
    
    func initControllerStates() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(loadData))
        super.initControllerStates(failureView: failureView)
    }
    
    //==================== Load data ===================================//
    @objc func loadData() {
        self.dataTableView.reloadData()
        startLoading ()

        let url = "\(Constants.getModRewriteBaseUrl())/hostel/listAll"
        
        SBFramework.request(url, method: .get).responseJSON { response in
            switch response.result {
            case .success(let value):
                
                //======== Parse result ==========//
                let json = JSON(value)
                if (json["error"].null == nil){
                    if(!self.tokenRenewTried){ Utils.renewToken(callback : self,navigationController : self.navigationController!) }
                }
                if let jsonArray = json.array {
                    
                    //================= Fill data array ==================//
                    for item in jsonArray {
                        self.dataArray.append(HostelsModel(
                            id : Utils.getIntValueOf(sourceInput: item,itemKey:"id") ,
                            hostelTitle : Utils.getStringValueOf(sourceInput: item,itemKey:"hostelTitle") ,
                            hostelType : Utils.getStringValueOf(sourceInput: item,itemKey:"hostelType") ,
                            hostelAddress : Utils.getStringValueOf(sourceInput: item,itemKey:"hostelAddress") ,
                            hostelManager : Utils.getStringValueOf(sourceInput: item,itemKey:"hostelManager") ,
                            hostelNotes : Utils.getStringValueOf(sourceInput: item,itemKey:"hostelNotes")
                            )
                        )
                    }
                    self.endLoading(error: nil) // Set Content
                    self.dataTableView.reloadData()

                }else{
                    self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
                }
            case .failure(let error):
                Utils.checkFailureCause(error : error, callback: self, navigationController: self.navigationController!,tokenRenewTried:self.tokenRenewTried)
                if self.tokenRenewTried {self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) }
            }
        }
    }
    var tokenRenewTried = false
    func afterRenewToken(success: Bool) {
        tokenRenewTried = true
        if success {
            loadData()
        }else{
            self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
        }
    }
    
    //==================== Nav Bar Buttons Actions ===================================//

    @IBAction func reloadPage(_ sender: Any) {
        self.dataArray.removeAll()
        loadData();
    }
    
    @IBAction func searchButtonAction(_ sender: Any) {
        showSearchDialog(targetStoryboard: "HostelsPageStoryboard")
    }
    
   
    @IBAction func navLeftButtonAction(_ sender: Any) {
        if(self.searchMode){
            self.navigationController?.popViewController(animated: true)
        }else{
            showMenu()
        }
    }
    
    //============================ Search Functions =================//
    
    func searchInDataArray(){
        if(self.dataArray.count > 0){
            var searchDataArray = [Any]()
            for item in dataArray {
                let cItem = item as! HostelsModel
                if(cItem.hostelTitle!.lowercased().contains(self.searchAbout!)
                    || cItem.hostelType!.lowercased().contains(self.searchAbout!)
                    || cItem.hostelAddress!.lowercased().contains(self.searchAbout!)
                    || cItem.hostelNotes!.lowercased().contains(self.searchAbout!)){
                    searchDataArray.append(cItem)
                }
            }
            dataArray.removeAll()
            dataArray.append(contentsOf : searchDataArray)
            self.endLoading(error: nil) // Set Content
        }else{
            self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil))
        }
    }

    //==================== Table view options ===================================//
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:HostelsCell = tableView.dequeueReusableCell(withIdentifier: "cellCon", for: indexPath) as! HostelsCell
        cell.item = dataArray[indexPath.row] as! HostelsModel
        return cell
    }
    
}






