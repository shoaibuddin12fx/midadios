//
//  ViewController.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/15/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework
import SwiftyJSON


class StudentsMarksheetPage:SolViewController,UITableViewDataSource,UITableViewDelegate,RenewTokenCallback {
    
    @IBOutlet weak var dataTableView: UITableView!
    @IBOutlet weak var navLeftButton: UIBarButtonItem!

    var searchMode:Bool = false
    var studentId : Int = 0
    //============================ Base Functions =================//
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initTableView(tableView: dataTableView)
        initNavBar()
        initSideMenu()
        initControllerStates()
        
        initPage()

    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupInitialViewState()
        
    }
    
    //============================ Init Functions =================//

    func initPage() {
        
        self.navLeftButton.image = Utils.getBackIcon()
        if searchAbout != nil {
            searchMode = true
            searchInDataArray();
        }else{
            searchMode = false
            self.loadData()
        }
        
        // nav bar title
        self.navigationItem.title = Utils.siteTitle
    }
    
    func initControllerStates() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(loadData))
        super.initControllerStates(failureView: failureView)
    }
    
    //==================== Load data ===================================//
    @objc func loadData() {
        self.dataArray.removeAll()
        self.dataTableView.reloadData()
        startLoading ()

        let url = "\(Constants.getModRewriteBaseUrl())/students/marksheet/\(self.studentId)"
        
        
        SBFramework.request(url, method: .get).responseJSON { response in
            switch response.result {
            case .success(let value):
                
                //======== Parse result ==========//
                let json = JSON(value)
                if (json["error"].null == nil){
                    if(!self.tokenRenewTried){ Utils.renewToken(callback : self,navigationController : self.navigationController!) }
                }
                if (Utils.getStringValueOrNilOf(sourceInput: json, itemKey: "status") == "failed"){
                    self.endLoading(error: nil) // Set No Content
                }else{
                    if !(json.null != nil){
                        
                        for (_,examDetails):(String, JSON) in json {
                            var detailsArray = [StudentsMarksheetDetailsModel]()
                            var examMarksheetColsArray = [String : String]()

                            // Parse columns
                            if let examMarksheetCols = examDetails["examMarksheetColumns"].array {
                                for item in examMarksheetCols {
                                    examMarksheetColsArray[Utils.getStringValueOf(sourceInput: item,itemKey:"id")] = Utils.getStringValueOf(sourceInput: item,itemKey:"title")
                                }
                            }
                            
                            // Parse exam marks details
                            if !(examDetails["data"].null != nil){
                               
                                for (_,examMarksDetailsJson):(String, JSON) in examDetails["data"] {
                                    
                                    // Variables
                                    var examMarksDic = [String: String]()
                                    let examDetailsObject : StudentsMarksheetDetailsModel = StudentsMarksheetDetailsModel()
                                    
                                    // Parse exam marks
                                    if !(examMarksDetailsJson["examMark"].null != nil){
                                        for (key,subJson):(String, JSON) in examMarksDetailsJson["examMark"] {
                                            if examMarksheetColsArray[key] != nil{
                                                examMarksDic[examMarksheetColsArray[key]!] = subJson.string
                                            }
                                        }

                                        examDetailsObject.MarksColsMap = examMarksDic
                                    }
                                    // Parse exam details
                                    let totMark = Utils.getStringValueOrNilOf(sourceInput: examMarksDetailsJson,itemKey:"totalMarks")
                                    if totMark != ""{
                                        examDetailsObject.TotalMark = totMark
                                    }else{
                                        examDetailsObject.TotalMark = "NA"
                                    }
                                    
                                    examDetailsObject.examState = Utils.getStringValueOf(sourceInput: examMarksDetailsJson,itemKey:"examState")
                                    
                                    examDetailsObject.finalGrade = Utils.getStringValueOf(sourceInput: examMarksDetailsJson,itemKey:"finalGrade")
                                    
                                    examDetailsObject.passGrade = Utils.getStringValueOf(sourceInput: examMarksDetailsJson,itemKey:"passGrade")
                                    
                                    examDetailsObject.Grade = Utils.getStringValueOf(sourceInput: examMarksDetailsJson,itemKey:"grade")
                                    
                                    examDetailsObject.markComments = Utils.getStringValueOf(sourceInput: examMarksDetailsJson,itemKey:"markComments")
                                    
                                    examDetailsObject.subjectName = Utils.getStringValueOf(sourceInput: examMarksDetailsJson,itemKey:"subjectName")
                                    
                                    detailsArray.append(examDetailsObject)
                                }
                            }
                           
                            // Parse exam main info
                            let examId = Utils.getIntValueOf(sourceInput: examDetails,itemKey:"examId")
                            self.dataArray.append(StudentsMarksheetModel(
                                id : examId ,
                                title : Utils.getStringValueOf(sourceInput: examDetails,itemKey:"title") ,
                                totalMarks : Utils.getStringValueOrNilOf(sourceInput: examDetails,itemKey:"totalMarks") ,
                                pointsAvg : Utils.getStringValueOrNilOf(sourceInput: examDetails,itemKey:"pointsAvg") ,
                                detailsArray: detailsArray
                                )
                            )
                            
                            
                        }
                        self.endLoading(error: nil) // Set Content
                        self.dataTableView.reloadData()
                    }else{
                        self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
                    }
                }

            case .failure(let error):
                Utils.checkFailureCause(error : error, callback: self, navigationController: self.navigationController!,tokenRenewTried:self.tokenRenewTried)
                if self.tokenRenewTried {self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) }
            }
        }
    }
    var tokenRenewTried = false
    func afterRenewToken(success: Bool) {
        tokenRenewTried = true
        if success {
            loadData()
        }else{
            self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
        }
    }
    
    //==================== Nav Bar Buttons Actions ===================================//

    @IBAction func reloadPage(_ sender: Any) {
        self.dataArray.removeAll()
        loadData();
    }
    
    @IBAction func searchButtonAction(_ sender: Any) {
        let currentVC = self.storyboard?.instantiateViewController(withIdentifier:"StudentsMarksheetPageStoryboard") as! StudentsMarksheetPage
        currentVC.studentId = self.studentId
        showSearchDialog(targetStoryboard: "StudentsMarksheetPageStoryboard",openController: currentVC)
    }
    
   
    @IBAction func navLeftButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //============================ Search Functions =================//
    
    func searchInDataArray(){
        if(self.dataArray.count > 0){
            var searchDataArray = [Any]()
            for item in dataArray {
                let cItem = item as! StudentsMarksheetModel
                if(cItem.title!.lowercased().contains(self.searchAbout!)){
                    searchDataArray.append(cItem)
                }
            }
            dataArray.removeAll()
            dataArray.append(contentsOf : searchDataArray)
            self.endLoading(error: nil) // Set Content
        }else{
            self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil))
        }
    }

    //==================== Table view options ===================================//
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:StudentsMarksheetCell = tableView.dequeueReusableCell(withIdentifier: "cellCon", for: indexPath) as! StudentsMarksheetCell
        cell.item = dataArray[indexPath.row] as! StudentsMarksheetModel
        cell.navController = self.navigationController
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "StudentsMarksheetDetailsPageStoryboard") as! StudentsMarksheetDetailsPage
        
        let item = self.dataArray[indexPath.row] as! StudentsMarksheetModel
        nextVC.dataArray = item.detailsArray
        
        self.navigationController!.pushViewController(nextVC, animated: true)
    }
}






