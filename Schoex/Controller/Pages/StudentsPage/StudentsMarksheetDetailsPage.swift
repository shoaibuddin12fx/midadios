//
//  ViewController.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/15/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework
import SwiftyJSON


class StudentsMarksheetDetailsPage:SolViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var dataTableView: UITableView!
    @IBOutlet weak var navLeftButton: UIBarButtonItem!

    var searchMode:Bool = false
    //============================ Base Functions =================//
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initNavBar()
        initSideMenu()
        initControllerStates()
        
        initPage()

        self.dataTableView.register(StudentsMarksheetDetailsCell.self, forCellReuseIdentifier: "cellCon")
        self.dataTableView.estimatedRowHeight = 10
        self.dataTableView.pin.topLeft().bottomRight()
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupInitialViewState()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
        dataTableView.reloadData()
        
    }
    //============================ Init Functions =================//

    func initPage() {
        self.navLeftButton.image = Utils.getBackIcon()
        
        if searchAbout != nil {
            searchMode = true
            //searchInDataArray();
        }else{
            searchMode = false
        }
        // nav bar title
        self.navigationItem.title = Utils.siteTitle
    }
    
    func initControllerStates() {
        /*let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(loadData))
        super.initControllerStates(failureView: failureView)*/
    }
    
   
    
    //==================== Nav Bar Buttons Actions ===================================//

    @IBAction func reloadPage(_ sender: Any) {
    }
    
    @IBAction func searchButtonAction(_ sender: Any) {
        
    }
    
   
    @IBAction func navLeftButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    //==================== Table view options ===================================//
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = dataTableView.dequeueReusableCell(withIdentifier: "cellCon", for: indexPath) as! StudentsMarksheetDetailsCell
        cell.item = dataArray[indexPath.row] as! StudentsMarksheetDetailsModel
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}






