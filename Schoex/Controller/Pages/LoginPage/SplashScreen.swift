//
//  SplashScreen.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/20/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit

import SwiftyJSON
import NVActivityIndicatorView
import Oops
import SBFramework
import Firebase

class SplashScreen: UIViewController {
    
    @IBOutlet weak var SplashSchoolTitle: UILabel!
    @IBOutlet weak var loadingProgress: NVActivityIndicatorView!
    var bypassLogin : Bool = false
    var loginErrorCounter = 1
    var DashboardErrorCounter = 1
    @IBOutlet weak var backImage: UIImageView!
    var dashboardJsonData = JSON()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if !Bools.x_login_back_is_image {
            backImage.removeFromSuperview()
        }
        
        SplashSchoolTitle.text = Strings.x_login_school_name

        navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController!.navigationBar.shadowImage = UIImage()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        hideNavigationBar()

        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let _ = appDelegate.notificationWhere {
            self.loginFirst(loadDashboardAfter : false)
        }else{
            if bypassLogin {
                self.loadDashboardData(loadDashboardAfter : true)
            }else{
                self.loginFirst(loadDashboardAfter : true)
            }
        }
    }
  
    func redirect() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        
        if let whereStr = appDelegate.notificationWhere {
            
            NSLog("notification where \(whereStr)")
            
            if whereStr == "notification"{
                Utils.openDashboard(storyboard : storyboard!,navigationController : navigationController!,dashboardJsonData : dashboardJsonData)
            }else{
                Utils.openNotification(storyboard : storyboard!,navigationController : navigationController!,dashboardJsonData : dashboardJsonData)
            }
        }
        
        if let idStr = appDelegate.notificationId {
            NSLog("notification id \(idStr)")
            
        }
        
    }
  
    func loginFirst(loadDashboardAfter : Bool) {
        self.loadingProgress.startAnimating()
        
        if Utils.loginUsername != Utils.Nil && Utils.loginPassword != Utils.Nil {
            let url = "\(Constants.getModRewriteBaseUrl())/auth/authenticate"
            

            var paramFcmToken = ""
            if let fcmToken = InstanceID.instanceID().token() {
                paramFcmToken = fcmToken
            }

            let parameters: Parameters = [
                "username":   Utils.loginUsername,
                "password" :  Utils.loginPassword,
                "android_token":  paramFcmToken
            ]
            
            SBFramework.request(url, method: .post,parameters: parameters).responseJSON { response in
                switch response.result {
                case .success(let value):
                    
                    //======== Parse result ==========//
                    let json = JSON(value)
                    if let tokenValue = json["token"].string {
                        
                        let defaults = UserDefaults.standard
                        Utils.AppToken = tokenValue
                        defaults.set(tokenValue, forKey: "adr")
                        SolUtils.AppToken = tokenValue
                        defaults.set(Constants.BASE_URL, forKey: "aa")
                        defaults.set(Constants.LICENCE_CODE, forKey: "ab")
                        defaults.set(Constants.VERSION_CODE, forKey: "ac")
                        
                        self.loadDashboardData(loadDashboardAfter: loadDashboardAfter)
                    } else {
                        self.loginError()
                    }
                case .failure( _):
                    self.loginError()
                }
                
            }
        }else{
            let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginPageStoryboard") as! LoginPage
            self.navigationController!.pushViewController(nextVC, animated: false)
        }
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func loadDashboardData(loadDashboardAfter : Bool) {
        self.loadingProgress.startAnimating()
        
        let url = "\(Constants.getModRewriteBaseUrl())/dashaboard"
        let parameters: Parameters = ["nLowIosVersion": Constants.VERSION_CODE]
        SBFramework.request(url, method: .post, parameters: parameters).responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                if (json.null == nil){
                    // check error
                    let error = Utils.getStringValueOrNilOf(sourceInput: json, itemKey: "error")
                    if error != Utils.Nil {
                        if error == "iosNotCompatible" {
                            self.fatelError(errorMsg: "Please Contact Administrator, Compatibility Error - VNumber: \(Constants.VERSION_CODE)")
                        }else{
                             self.fatelError(errorMsg: "Please Contact Administrator, Unknown Error")
                        }
                        return
                    }
                    
                    // parse base user data
                    var loggedInUserRole = Utils.AppRoles.Student
                    let userRole = Utils.getStringValueOrNilOf(sourceInput: json, itemKey: "role")
                    switch (userRole) {
                    case "admin":
                        loggedInUserRole = Utils.AppRoles.Admin
                        break
                    case "teacher":
                        loggedInUserRole = Utils.AppRoles.Teacher
                        break
                    case "student":
                        loggedInUserRole = Utils.AppRoles.Student
                        break
                    case "parent":
                        loggedInUserRole = Utils.AppRoles.Parent
                        break
                    case "account":
                        loggedInUserRole = Utils.AppRoles.Accountant
                        break
                    default:
                        loggedInUserRole = Utils.AppRoles.Student
                        break
                    }
                    
                    let baseUser = json["baseUser"]
                    
                    Utils.saveLoggedInUser(loggedInUser :
                        LoggedUserModel(
                            LoggedInUserId : Utils.getIntValueOrNilOf(sourceInput: baseUser, itemKey: "id"),
                            LoggedInUserFullName:Utils.getStringValueOrNilOf(sourceInput: baseUser, itemKey: "fullName"),
                            LoggedInUserUsername: Utils.getStringValueOrNilOf(sourceInput: baseUser, itemKey: "username"),
                            LoggedInUserRole : loggedInUserRole
                        )
                    )
                    
                    // parse notifications times constants
                    let notificationsTimes = json["notification"]
                    let notTimeInsideMessagesInSeconds = Utils.getStringValueOrNilOf(sourceInput: notificationsTimes, itemKey: "m")
                    let notTimeOutsideMessagesInSeconds = Utils.getStringValueOrNilOf(sourceInput: notificationsTimes, itemKey: "c")
                    
                    if notTimeInsideMessagesInSeconds != Utils.Nil && notTimeInsideMessagesInSeconds != "" && notTimeOutsideMessagesInSeconds != Utils.Nil && notTimeOutsideMessagesInSeconds != "" {
                        Utils.saveNotificationsTimes(
                            notificationsRefreshInsideMessageInSeconds: Float(notTimeInsideMessagesInSeconds)!,
                            notificationsRefreshOutsideMessageInSeconds: Float(notTimeOutsideMessagesInSeconds)!
                        )
                    }
                    
                    // parse calendar
                    let calendar = Utils.getStringValueOrNilOf(sourceInput: json, itemKey: "gcalendar")
                    if calendar != Utils.Nil && calendar != "" { Utils.App_CalendarType = calendar }
                    
                    // parse site title
                    let siteTitle = Utils.getStringValueOrNilOf(sourceInput: json, itemKey: "siteTitle")
                    if siteTitle != Utils.Nil { Utils.siteTitle = siteTitle }
                    
                    // parse date format
                    let dateFormat = Utils.getStringValueOrNilOf(sourceInput: json, itemKey: "dateformat")
                    if dateFormat != Utils.Nil && dateFormat != "" {
                        Utils.App_DateFormat = dateFormat.lowercased().replacingOccurrences(of: "d", with: "dd").replacingOccurrences(of: "m", with: "mm").replacingOccurrences(of: "y", with: "yyyy")
                    }
                    
                    // parse attendance model
                    let attendanceModel = Utils.getStringValueOrNilOf(sourceInput: json, itemKey: "attendanceModel")
                    if attendanceModel != Utils.Nil && attendanceModel != "" {
                        Utils.App_AttendanceModelIsClass = ( attendanceModel == "class" )
                    }
                    
                    // parse sections state
                    let sectionsEnabled = Utils.getStringValueOrNilOf(sourceInput: json, itemKey: "enableSections")
                    if sectionsEnabled != Utils.Nil && sectionsEnabled != "" {
                        Utils.App_IsSectionsEnabled = ( sectionsEnabled != "0" )
                    }
                    
                    // parse language direction
                    let langUniversal = Utils.getStringValueOrNilOf(sourceInput: json, itemKey: "languageUniversal")
                    if langUniversal != Utils.Nil && langUniversal != "" {
                        Utils.App_IsRtl = langUniversal == "ar" 
                        SolUtils.App_IsRtl = Utils.App_IsRtl
                    }
                    
                    // parse language
                    if let languageDic = json["language"].dictionaryObject {
                        Language.languageDictionary = languageDic as! [String : String]
                        for (key, value) in languageDic {
                            Language.languageDictionary[key.lowercased()] = value as? String
                        }
                    }
                    
                    
                    // parse activated modules
                    if let activatedModules = json["activatedModules"].array{
                        var modules = [String]()
                        for module in activatedModules {
                            if let moduleString = module.string{
                                modules.append(moduleString)
                                print(moduleString)
                            }
                        }
                        Utils.activatedModules = modules
                        UserDefaults.standard.set(modules, forKey: "activatedModules")
                    }
                    
                    // parse admin permissions
                    if let adminPermJson = json["perms"].array{
                        var adminModules = [String]()
                        for module in adminPermJson {
                            if let moduleString = module.string{
                                adminModules.append(moduleString)
                                print(moduleString)
                            }
                        }
                        Utils.userPermissionsModules = adminModules
                        UserDefaults.standard.removeObject(forKey: "userCustomPerm")
                        UserDefaults.standard.set(adminModules, forKey: "userCustomPerm")
                    }
                    
                    self.dashboardJsonData = json
                    
                    
                    if loadDashboardAfter{
                        Utils.openDashboard(storyboard : self.storyboard!,navigationController : self.navigationController!,dashboardJsonData : self.dashboardJsonData)
                    }else{
                        self.redirect()
                    }
                }else{
                    self.loadingProgress.stopAnimating()
                    self.dashboardError(errorCode : 0)
                }
                
            case .failure(let error):
                self.loadingProgress.stopAnimating()
                if Int(error.localizedDescription) != nil{
                    self.dashboardError(errorCode : Int(error.localizedDescription)!)
                }else if Float(error.localizedDescription) != nil{
                    self.dashboardError(errorCode : Float(error.localizedDescription)!)
                }else{
                    self.dashboardError(errorCode : 0)
                }
            }
            
        }
    }
  
    func loginError() {
        self.loadingProgress.stopAnimating()
        
        if self.loginErrorCounter >= 3{
            
            let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginPageStoryboard") as! LoginPage
            self.navigationController!.pushViewController(nextVC, animated: true)
            
        }else{
            let configuration = Oops.Configuration(
                titleFont: UIFont(name: "RobotoCondensed-Bold", size: 20)!,
                textFont: UIFont(name: "RobotoCondensed-Bold", size: 14)!,
                buttonFont: UIFont(name: "RobotoCondensed-Bold", size: 14)!,
                showCloseButton: false,
                dynamicAnimatorActive: false
            )
            let alert = Oops(configuration: configuration)
            alert.addButton("TRY AGAIN") {
                self.loginFirst(loadDashboardAfter: true)
            }
            alert.show(.error,
                       title: "Error Occurred",
                       detail: "Tap to try again",
                       completeText: "OK")
            
            self.loginErrorCounter = self.loginErrorCounter + 1
        }
        
    }
    
    func dashboardError(errorCode : Int) {
        if (errorCode == 101020){
            Utils.showMsg(msg: Language.getTranslationOf(findAndDefaultKey:"Please wait"))
            Request.dsNP(callback: { str in
                self.loadDashboardData(loadDashboardAfter: true)
            
            })
        }else{
            if self.DashboardErrorCounter >= 3{
                
                let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginPageStoryboard") as! LoginPage
                self.navigationController!.pushViewController(nextVC, animated: true)
                
            }else{
                self.loadingProgress.stopAnimating()
                
                let configuration = Oops.Configuration(
                    titleFont: UIFont(name: "RobotoCondensed-Bold", size: 20)!,
                    textFont: UIFont(name: "RobotoCondensed-Bold", size: 14)!,
                    buttonFont: UIFont(name: "RobotoCondensed-Bold", size: 14)!,
                    showCloseButton: false,
                    dynamicAnimatorActive: false
                )
                let alert = Oops(configuration: configuration)
                alert.addButton("TRY AGAIN") {
                    self.loadDashboardData(loadDashboardAfter: true)
                }
                if errorCode != 0{
                    alert.show(.error,
                               title: "Error Occurred",
                               detail: "Tap to try again - Error Code : \(errorCode)",
                        completeText: "OK")
                }else{
                    alert.show(.error,
                               title: "Error Occurred",
                               detail: "Tap to try again",
                               completeText: "OK")
                }
                self.DashboardErrorCounter = self.DashboardErrorCounter + 1
            }
        }
    }
    
    func dashboardError(errorCode : Float) {
        if self.DashboardErrorCounter >= 3{
            
            let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginPageStoryboard") as! LoginPage
            self.navigationController!.pushViewController(nextVC, animated: true)
            
        }else{
            self.loadingProgress.stopAnimating()
            
            let configuration = Oops.Configuration(
                titleFont: UIFont(name: "RobotoCondensed-Bold", size: 20)!,
                textFont: UIFont(name: "RobotoCondensed-Bold", size: 14)!,
                buttonFont: UIFont(name: "RobotoCondensed-Bold", size: 14)!,
                showCloseButton: false,
                dynamicAnimatorActive: false
            )
            let alert = Oops(configuration: configuration)
            alert.addButton("TRY AGAIN") {
                self.loadDashboardData(loadDashboardAfter: true)
            }
            if errorCode != 0{
                alert.show(.error,
                           title: "Error Occurred",
                           detail: "Tap to try again - Error Code : \(errorCode)",
                    completeText: "OK")
            }else{
                alert.show(.error,
                           title: "Error Occurred",
                           detail: "Tap to try again",
                           completeText: "OK")
            }
            self.DashboardErrorCounter = self.DashboardErrorCounter + 1
        }
    }
    
    func fatelError(errorMsg : String) {
        self.loadingProgress.stopAnimating()
        
        let configuration = Oops.Configuration(
            titleFont: UIFont(name: "RobotoCondensed-Bold", size: 20)!,
            textFont: UIFont(name: "RobotoCondensed-Bold", size: 14)!,
            buttonFont: UIFont(name: "RobotoCondensed-Bold", size: 14)!,
            showCloseButton: false,
            dynamicAnimatorActive: false
        )
        let alert = Oops(configuration: configuration)
        alert.addButton("Exit") {
            let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginPageStoryboard") as! LoginPage
            self.navigationController!.pushViewController(nextVC, animated: true)
        }
        alert.show(.error,title: "Fatel Error Occurred",detail: errorMsg ,completeText: "OK")
    }
}



