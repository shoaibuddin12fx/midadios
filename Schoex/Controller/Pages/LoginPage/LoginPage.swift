//
//  AttendancePageViewController.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/29/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SwiftyJSON
import SBFramework

import Eureka
import TransitionButton
import Firebase
import ActionSheetPicker_3_0

class LoginPage: UIViewController,UITextFieldDelegate {

    let isLTRLanguage = true
    var cities: [CityDataModel] = [];
    var schoollist: [SchoolDataModel]  = [];
    var schoollistfiltered: [SchoolDataModel]  = [];
    
    @IBOutlet weak var cityBtn: UIButton!
    @IBOutlet weak var schoolBtn: UIButton!
    
    var city = "اختر مدينة";
    var school = "حدد المدرسة";
    
    var selectedCity = 0;
    var selectedSchool = 0;

    var selectedCityId = 0;
    var selectedSchoolId = 0;

    
    @IBOutlet weak var loginSchoolName: UILabel!
    var textFields: [SkyFloatingLabelTextField] = []
    @IBOutlet weak var InputLabelUsername: SkyFloatingLabelTextField!
    @IBOutlet weak var InputLabelPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var loginButton: TransitionButton!
    
    @IBOutlet weak var backImage: UIImageView!
    @IBOutlet weak var backColor: UIView!
    
    @IBOutlet weak var demoLoginButton: SolLabelView!
    @IBOutlet weak var privacyPolicyButton: UILabel!
    
    var demoLoginEnabled = false
    
    var pickedAccount: String? = "Admin"
    enum ItemsType : Int {
        case label, customView
    }
    
    //============================ Base Functions =================//
    override func viewDidLoad() {
        super.viewDidLoad()
        loginSchoolName.text = Strings.x_login_school_name
        SolUtils.dsNPEnabled = false
        
        if !Bools.x_login_back_is_image {
            backImage.removeFromSuperview()
        }
      
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.demoLogin (_:)))
        demoLoginButton.addGestureRecognizer(gesture)
        
        let gesture2 = UITapGestureRecognizer(target: self, action:  #selector (self.privacyPolicyOpen (_:)))
        privacyPolicyButton.addGestureRecognizer(gesture2)
        
        
        createForm()

        if(!demoLoginEnabled){
            demoLoginButton.isHidden = true
        }
        
        
        
        
    }
    
 
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    @objc func privacyPolicyOpen(_ sender:UITapGestureRecognizer){
        if let url = URL(string: "\(Constants.getModRewriteBaseUrl())/terms"),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:])
        }
        
    }
    
    @objc func demoLogin(_ sender:UITapGestureRecognizer){
        performSegue(withIdentifier: "DemoAccountSelector", sender: nil)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hideNavigationBar()
        
        // get cities and schools
        self.getSchoolsJsonSetInView();
        self.cityBtn.setTitle(city, for: .normal);
        self.schoolBtn.setTitle(school, for: .normal)

    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DemoAccountSelector" {
            let accountNav = segue.destination as! UINavigationController
            let accountPicker = accountNav.topViewController as! DemoLoginSelector
            accountPicker.currentSelectedValue = pickedAccount
            accountPicker.presentationType = DemoLoginSelector.PresentationType.names(0,0)
            accountPicker.updateSelectedValue = { (newSelectedValue) in
                self.pickedAccount = newSelectedValue
                if(self.pickedAccount == "Admin"){
                    self.InputLabelUsername.text = "admin"
                    self.InputLabelPassword.text! = "admin123"
                }else if(self.pickedAccount  == "Teacher"){
                    self.InputLabelUsername.text = "teacher"
                    self.InputLabelPassword.text! = "teacher123"
                }else if(self.pickedAccount == "Student"){
                    self.InputLabelUsername.text = "student"
                    self.InputLabelPassword.text! = "student123"
                }else if(self.pickedAccount == "Parent"){
                    self.InputLabelUsername.text = "parent"
                    self.InputLabelPassword.text! = "parent123"
                }else if(self.pickedAccount == "Accountant"){
                    self.InputLabelUsername.text = "account"
                    self.InputLabelPassword.text! = "account123"
                }
                self.loginButton!.startAnimation()
                self.makeLoginProcess()
            }
            accountPicker.itemsType = ItemsType(rawValue: 0)!
        }
    }
    
    //============================ Form manage functions =================//
    
    func createForm() {
        textFields = [InputLabelUsername, InputLabelPassword]
        for textField in textFields {
            textField.delegate = self
        }
        
        InputLabelUsername.placeholder = NSLocalizedString(
            "username",
            comment: "username"
        )
        InputLabelUsername.selectedTitle = NSLocalizedString(
            "username",
            tableName: "username",
            comment: "username"
        )
        InputLabelUsername.title = NSLocalizedString(
            "username",
            tableName: "username",
            comment: "username"
        )
        styleLabel(textField: InputLabelUsername)
        
        InputLabelPassword.placeholder = NSLocalizedString(
            "password",
            tableName: "password",
            comment: "password"
        )
        InputLabelPassword.selectedTitle = NSLocalizedString(
            "Password",
            tableName: "password",
            comment: "password"
        )
        InputLabelPassword.title = NSLocalizedString(
            "Password",
            tableName: "password",
            comment: "password"
        )
        InputLabelPassword.isSecureTextEntry = true

        styleLabel(textField: InputLabelPassword)

        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        
        
        
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
    
    func styleLabel(textField: SkyFloatingLabelTextField) {
        textField.textColor = UIColor(argb: SolColors.colorsDictionary["x_dash_main_text"]!)
        textField.selectedTitleColor = UIColor(argb: SolColors.colorsDictionary["x_dash_main_text"]!)
        
    }
    
    @IBAction func LoginProcess(_ sender: Any) {
        
        
        if(self.schoolBtn.titleLabel?.text == self.school){
            return;
        }
        
        
        self.loginButton!.startAnimation()
        self.makeLoginProcess()

    }

    @IBAction func DropDownSelect(_ sender: UIButton) {
        
        switch sender.tag {
        case 0:
            selectCityByDropdown();
            break;
        case 1:
            
            if(self.schoollistfiltered.count == 0){
                return;
            }
            selectSchoolByDropdown()
            
            break;
        default:
            break;
        }

    }
    
    func selectCityByDropdown(){
        
        let title = "";
        let rows = self.cities.map { (item) -> String in
            return item.city ?? ""
        }
        
        ActionSheetStringPicker.show(withTitle: title, rows: rows, initialSelection: selectedCity, doneBlock: { (picker, index, data) in
            
            //
            
            print(index);
            
            if(self.cities.count > 0){
                let choice = self.cities[index];
                if let id = choice.id, let name = choice.city{
                    self.selectedCity = index;
                    self.selectedCityId = id;
                    self.cityBtn.setTitle(name, for: .normal);
                    
                    self.schoollistfiltered = self.schoollist.filter({ (item) -> Bool in
                        return item.city_id == id
                    });
                    
                    
                }
            }
            
            
            
            
            
        }, cancel: { (picker) in
            //
            
        }, origin: nil)
        
    }
    
    func selectSchoolByDropdown(){
        
        let title = "";
        let rows = self.schoollistfiltered.map { (item) -> String in
            return item.name ?? ""
        }
        
        ActionSheetStringPicker.show(withTitle: title, rows: rows, initialSelection: selectedSchool, doneBlock: { (picker, index, data) in
            
            //
            
            print(index);
            
            let choice = self.schoollistfiltered[index];
            if let id = choice.id, let name = choice.name{
                self.selectedSchool = index;
                self.selectedSchoolId = id;
                self.schoolBtn.setTitle(name, for: .normal);
            }
            
            
            
        }, cancel: { (picker) in
            //
            
        }, origin: nil)
        
    }
    
    func setBaseUrl(){
        
        print(selectedSchoolId)
        print()
        
        let sch = self.schoollist.first { (item) -> Bool in
            return item.id == selectedSchoolId
        }
        
        
        
        // let choice = self.schoollistfiltered[selectedSchool];
        if let _sch = sch, let url = _sch.url{
            let defaults = UserDefaults.standard
            defaults.set(url, forKey: "base_url")
        }
    }

    
    
    func makeLoginProcess() {
        
        setBaseUrl()
        
        let url = "\(Constants.getModRewriteBaseUrl())/auth/authenticate"
        

        var paramFcmToken = ""
        if let fcmToken = InstanceID.instanceID().token() {
            paramFcmToken = fcmToken
        }

        let parameters: Parameters = [
            "username":  self.InputLabelUsername.text!,
            "password":  self.InputLabelPassword.text!,
            "android_token":  paramFcmToken
            ]

        SBFramework.request(url, method: .post,parameters: parameters).responseJSON { response in
            switch response.result {
            case .success(let value):
                
                //======== Parse result ==========//
                    let json = JSON(value)
                    if let errorOcc = json["error"].string {
                        if errorOcc == "invalid_credentials"{
                            
                            if(Utils.App_IsRtl){
                                Utils.showMsg(msg: "بيانات الاعتماد غير صالحة")
                            }else{
                                Utils.showMsg(msg: "Invalid Credentials")
                            }
                            
                            
                            
                        }else{
                            if(Utils.App_IsRtl){
                                Utils.showMsg(msg: "حدث خطأ")
                            }else{
                                Utils.showMsg(msg: "Error occurred")
                            }
                            
                        }
                        DispatchQueue.main.async(execute: { () -> Void in
                            self.loginButton!.stopAnimation(animationStyle: .shake, completion: {})
                        })
                    }else{
                        if let tokenValue = json["token"].string {
                            //======== Internal Configuration Variables ==========//
                            let defaults = UserDefaults.standard

                            Utils.AppToken = tokenValue
                            defaults.set(tokenValue, forKey: "adr")
                            SolUtils.AppToken = tokenValue
                            defaults.set(Constants.BASE_URL, forKey: "aa")
                            defaults.set(Constants.LICENCE_CODE, forKey: "ab")
                            defaults.set(Constants.VERSION_CODE, forKey: "ac")

                            Utils.loginUsername = self.InputLabelUsername.text!
                            Utils.loginPassword = self.InputLabelPassword.text!
                            
                            
                            //=================== END ===========================//
                            self.loginButton!.stopAnimation(animationStyle: .expand, completion: {
                                let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "SplashScreenStoryboard") as! SplashScreen
                                nextVC.bypassLogin = true
                                self.navigationController!.pushViewController(nextVC, animated: true)
                            })
                            
                        } else {
                            DispatchQueue.main.async(execute: { () -> Void in
                                self.loginButton!.stopAnimation(animationStyle: .shake, completion: {})
                            })
                        }
                    }
                
            case .failure(let error):
                if Int(error.localizedDescription) != nil{
                    Utils.showMsg(msg: "Error Occurred, Error code: \(error.localizedDescription)")
                }else{
                    Utils.showMsg(msg: error.localizedDescription)
                }
                
                DispatchQueue.main.async(execute: { () -> Void in
                    self.loginButton!.stopAnimation(animationStyle: .shake, completion: {})
                })
            }
            
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // When pressing return, move to the next field
        let nextTag = textField.tag + 1
        if let nextResponder = textField.superview?.viewWithTag(nextTag) as UIResponder! {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return false
    }
    
    func getSchoolsJsonSetInView(){
        
        //
        let url = "https://midad.ly/public/schoolslist/clist.json";
        loginButton.isHidden = true;
        selectedCity = 0;
        selectedSchool = 0;
        SBFramework.request(url).responseData { (response) in
            
            switch response.result {
                case .success(let value):
                    
                    self.cities = [];
                    self.schoollist = [];
                    
                     do{
                         //here dataResponse received from a network request
                         let jsonResponse = try JSONSerialization.jsonObject(with:
                                                value, options: [])
                         print(jsonResponse) //Response result
                        let res = JSON(jsonResponse);
                        
                        if let _cities = res["cities"].array{
                            for item in _cities{
                                self.cities.append(CityDataModel(
                                    id : Utils.getIntValueOf(sourceInput: item,itemKey:"id") ,
                                    city : Utils.getStringValueOf(sourceInput: item,itemKey:"city")
                                    )
                                )
                            }
                        }
                        
                        if let _schoollist = res["schoollist"].array{
                            for item in _schoollist{
                                self.schoollist.append(SchoolDataModel(
                                    id : Utils.getIntValueOf(sourceInput: item,itemKey:"id"),
                                    city_id : Utils.getIntValueOf(sourceInput: item,itemKey:"city_id"),
                                    name : Utils.getStringValueOf(sourceInput: item,itemKey:"name"),
                                    url : Utils.getStringValueOf(sourceInput: item,itemKey:"url")
                                    )
                                )
                            }
                        }
                        
                        self.loginButton.isHidden = false;
                        
                        
                        
                        
                      } catch let parsingError {
                         print("Error", parsingError)
                    }
                
                    break;
                case .failure(let error):
                
                    
                    print(error);
                    print();
                    break;
                
            }
            
        }
        
//        SBFramework.request(url).responseData { response in
//            switch response.result {
//            case .success(let value):
//
//                //======== Parse result ==========//
//                print(value);
//                let jsonData: Data = Data(value) /* get your json data */
//                let jsonDict = try JSONSerialization.jsonObject(with: jsonData) as? NSDictionary
//
//
//
//
//                print();
//
//
//                break;
//            case .failure(let error):
//
//                // handle unknown error here
//                print(error);
//                print();
//            }
//
//        }
        
    }
    
    
    
}

