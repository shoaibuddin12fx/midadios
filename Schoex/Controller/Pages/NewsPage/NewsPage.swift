//
//  NewsPage.swift
// OraSchool Suite
//
//  Created by SolutionsBricks Mobile Development Team on 9/15/17.
//  Copyright © 2019 Solutionsbricks. All rights reserved.
//

import UIKit
import SBFramework
import SwiftyJSON




class NewsPage:SolViewController,UITableViewDataSource,UITableViewDelegate,RenewTokenCallback {
    
    @IBOutlet weak var dataTableView: UITableView!
    @IBOutlet weak var navLeftButton: UIBarButtonItem!
    
    
    var page : Int = 1
    var loadMoreLocked : Bool = false

    var searchMode:Bool = false
    
    //============================ Base Functions =================//

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        initTableView(tableView: dataTableView)
        initNavBar()
        initSideMenu()
        initControllerStates()
        
        initPage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupInitialViewState()
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //============================ Init Functions =================//

    func initPage() {
        if searchAbout != nil {
            searchMode = true
            self.navLeftButton.image = Utils.getBackIcon()
        }else{
            searchMode = false
//            self.navLeftButton.image = UIImage(named: "icn_drawer")
        }
        self.loadData(isLoadMore: false)
        
        // nav bar title
        self.navigationItem.title = Utils.siteTitle
        
        
    }
    
    func initControllerStates() {
        let failureView = ErrorView(frame: view.frame)
        failureView.tapGestureRecognizer.addTarget(self, action: #selector(failLoadData))
        super.initControllerStates(failureView: failureView)
    }
    
    //==================== Load data ===================================//
    @objc func failLoadData() {
        loadData(isLoadMore : false)
    }
    @objc func loadData(isLoadMore : Bool) {
        //if (lastState == .loading) { return }
        
        if(!isLoadMore){
            self.page = 1
            self.loadMoreLocked = false
            self.dataArray.removeAll()
            self.dataTableView.reloadData()
            startLoading()
        }else{
            if(loadMoreLocked){return}
        }
        
        let url:String
        if(searchMode){
            url = "\(Constants.getModRewriteBaseUrl())/newsboard/search/\(self.searchAbout!)/\(self.page)"
        }else{
            url = "\(Constants.getModRewriteBaseUrl())/newsboard/listAll/\(self.page)"
        }
        
        SBFramework.request(url, method: .get).responseJSON { response in
            switch response.result {
            case .success(let value):
                
                //======== Parse result ==========//
                let json = JSON(value)
                if (json["error"].null == nil){
                    if(!self.tokenRenewTried){ Utils.renewToken(callback : self,navigationController : self.navigationController!) }
                }
                if let jsonArray = json["newsboard"].array {
                    
                    //================= Fill data array ==================//
                    for item in jsonArray {
                        self.dataArray.append(NewsModel(
                            id : Utils.getIntValueOf(sourceInput: item,itemKey:"id") ,
                            newsTitle : Utils.getStringValueOf(sourceInput: item,itemKey:"newsTitle") ,
                            newsText : Utils.getStringValueOf(sourceInput: item,itemKey:"newsText").html2String ,
                            newsFor : Utils.getStringValueOf(sourceInput: item,itemKey:"newsFor") ,
                            newsDate : Utils.getStringValueOf(sourceInput: item,itemKey:"newsDate")
                            )
                        )
                    }
                    if(isLoadMore){
                        if(jsonArray.count == 0){
                            self.loadMoreLocked = true
                        }
                    }
                    self.endLoading(error: nil) // Set Content
                    self.dataTableView.reloadData()
                    self.page = self.page + 1
                    
                }else{
                    self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
                }
            case .failure(let error):
                Utils.checkFailureCause(error : error, callback: self, navigationController: self.navigationController!,tokenRenewTried:self.tokenRenewTried)
                if self.tokenRenewTried {self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) }
            }
        }
    }
    var tokenRenewTried = false
    func afterRenewToken(success: Bool) {
        tokenRenewTried = true
        if success {
            loadData(isLoadMore: false)
        }else{
            self.endLoading(error: NSError(domain: "loadData", code: -1, userInfo: nil)) // Error
        }
    }
    
    //==================== Nav Bar Buttons Actions ===================================//
    @IBAction func reloadPage(_ sender: Any) {
        loadData(isLoadMore: false);
    }
    
    @IBAction func searchButtonAction(_ sender: Any) {
        showSearchDialog(targetStoryboard: "NewsPageStoryboard")
    }
    
    @IBAction func navLeftButtonAction(_ sender: Any) {
        if(self.searchMode){
            self.navigationController?.popViewController(animated: true)
        }else{
            showMenu()
        }
    }
    
    
    //==================== Table view options ===================================//
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:NewsCell = tableView.dequeueReusableCell(withIdentifier: "cellCon", for: indexPath) as! NewsCell
        cell.item = dataArray[indexPath.row] as! NewsModel
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = dataArray.count - 1
        if indexPath.row == lastElement {
            self.loadData(isLoadMore: true)
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if Utils.isUserHavePermission(checkThisModules: ["newsboard.View"]){
            let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "WebViewerPageStoryboard") as! WebViewerPage
            
            let item = self.dataArray[indexPath.row] as! NewsModel
            nextVC.pageTitleData = item.newsTitle
            nextVC.pageHtmlData = item.newsText
            
            self.navigationController!.pushViewController(nextVC, animated: true)
        }
        
    }
}
