//
//  SideMenuTableView.swift
//  SideMenu
//
//  Created by Jon Kent on 4/5/16.
//  Copyright © 2016 CocoaPods. All rights reserved.
//

import Foundation
import SBFramework
import Alamofire


class SideMenuTableView: UITableViewController {
    
    
    @IBOutlet weak var header_user_image: UIImageView!
    @IBOutlet weak var header_user_fullname: UILabel!
    @IBOutlet weak var header_user_role: UILabel!
    @IBOutlet weak var header_logout: UIView!
    var menuArray = [SideMenuReferenceModel]()
    
    var isClicked = false;
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        print("SideMenu Appearing!")
        menuArray.removeAll();
        
        
        menuArray.append(SideMenuReferenceModel(id:1,itemTitle:"Dashboard",itemImage: "dash_tab_stat_selected", itemStoryboard: "SplashScreenStoryboard"))
        
        
        if Utils.isUserHavePermission(checkThisModules: ["newsboard.list","newsboard.View"]){
            if Utils.isModuleActivated(moduleName: "newsboardAct"){
                menuArray.append(SideMenuReferenceModel(id:2,itemTitle:"News Board",itemImage: "news_g", itemStoryboard: "NewsPageStoryboard"))
            }
        }
        
        if Utils.isUserHavePermission(checkThisModules: ["events.list","events.View"]){
            if Utils.isModuleActivated(moduleName: "eventsAct"){
                menuArray.append(SideMenuReferenceModel(id:3,itemTitle:"Events",itemImage: "place", itemStoryboard: "EventsPageStoryboard"))
            }
        }
        
        if Utils.isModuleActivated(moduleName: "calendarAct"){
            menuArray.append(SideMenuReferenceModel(id:66,itemTitle:"Calender",itemImage: "calender_g", itemStoryboard: "CalendarPageStoryboard"))
        }
        
        if Utils.isModuleActivated(moduleName: "messagesAct"){
            menuArray.append(SideMenuReferenceModel(id:47,itemTitle:"Messages",itemImage: "news", itemStoryboard: "MessagesPageStoryboard"))
        }
        
        if Utils.isUserHavePermission(checkThisModules: ["staticPages.list"]){
            if Utils.isModuleActivated(moduleName: "staticPages") || Utils.isModuleActivated(moduleName: "staticpagesAct") {
                menuArray.append(SideMenuReferenceModel(id:5,itemTitle:"Static Pages",itemImage: "pages_g", itemStoryboard: "StaticPagesPageStoryboard"))
            }
        }
        
        
        if Utils.isUserHavePermission(checkThisModules: ["Homework.list", "Homework.View"]){
            if Utils.isModuleActivated(moduleName: "homeworkAct"){
                menuArray.append(SideMenuReferenceModel(id:40,itemTitle:"Homework",itemImage: "answers", itemStoryboard: "HomeworkPageStoryboard"))
            }
            
        }
        
        if Utils.isModuleActivated(moduleName: "attendanceAct"){
            
            if Utils.isUserHavePermission(checkThisModules: ["Attendance.takeAttendance"]){
                menuArray.append(SideMenuReferenceModel(id:8,itemTitle:"Attendance",itemImage: "attend", itemStoryboard: "AttendancePageStoryboard"))
            }
            if Utils.isUserHavePermission(checkThisModules: ["myAttendance.myAttendance","students.Attendance"]){
                
                if Utils.LoggedInUser.LoggedInUserRole == Utils.AppRoles.Student{
                    menuArray.append(SideMenuReferenceModel(id:6,itemTitle:"My Attendance",itemImage: "attend", itemStoryboard: "AttendanceByStudentPageStoryboard"))
                }else if Utils.LoggedInUser.LoggedInUserRole == Utils.AppRoles.Parent {
                    menuArray.append(SideMenuReferenceModel(id:7,itemTitle:"Attendance",itemImage: "attend", itemStoryboard: "AttendanceByParentChoosePageStoryboard"))
                }
            }
        }
        
        
        
        if Utils.isUserHavePermission(checkThisModules: ["staffAttendance.takeAttendance"]){
            if Utils.isModuleActivated(moduleName: "staffAttendanceAct") {
                menuArray.append(SideMenuReferenceModel(id:9,itemTitle:"Staff Attendance",itemImage: "ic_users", itemStoryboard: "StaffAttendancePageStoryboard"))
            }
        }
        
        if Utils.isUserHavePermission(checkThisModules: ["Vacation.reqVacation","Vacation.appVacation","Vacation.myVacation"]){
            if Utils.isModuleActivated(moduleName: "vacationAct"){
                menuArray.append(SideMenuReferenceModel(id:301,itemTitle:"Vacation",itemImage: "material_g", itemStoryboard: ""))
            }
        }
        
        if Utils.isUserHavePermission(checkThisModules: ["Library.list"]){
            if Utils.isModuleActivated(moduleName: "bookslibraryAct"){
                menuArray.append(SideMenuReferenceModel(id:10,itemTitle:"Books Library",itemImage: "subject", itemStoryboard: "LibraryPageStoryboard"))
            }
        }
        
        if Utils.isUserHavePermission(checkThisModules: ["classSch.list"]){
            if Utils.isModuleActivated(moduleName: "classSchAct"){
                menuArray.append(SideMenuReferenceModel(id:4,itemTitle:"Class Schedule",itemImage: "date_g", itemStoryboard: "ClassScheduleChoosePageStoryboard"))
            }
        }
        
        if Utils.isUserHavePermission(checkThisModules: ["Assignments.list"]){
            if Utils.isModuleActivated(moduleName: "assignmentsAct"){
                menuArray.append(SideMenuReferenceModel(id:11,itemTitle:"Assignments",itemImage: "answers", itemStoryboard: "AssignmentsPageStoryboard"))
            }
        }
        
        
        if Utils.isUserHavePermission(checkThisModules: ["studyMaterial.list"]){
            if Utils.isModuleActivated(moduleName: "materialsAct") {
                menuArray.append(SideMenuReferenceModel(id:12,itemTitle:"Study Material",itemImage: "material_g", itemStoryboard: "StudyMaterialsPageStoryboard"))
            }
        }
        
        if Utils.isUserHavePermission(checkThisModules: ["examsList.list","examsList.View"]){
            menuArray.append(SideMenuReferenceModel(id:13,itemTitle:"Exams List",itemImage: "exam_g", itemStoryboard: "ExamsPageStoryboard"))
        }
        
        
        if Utils.isUserHavePermission(checkThisModules: ["Marksheet.Marksheet","students.Marksheet"]){
            menuArray.append(SideMenuReferenceModel(id:14,itemTitle:"Marksheet",itemImage: "attend", itemStoryboard: "StudentsMarksheetPageStoryboard"))
        }
        
        if Utils.isUserHavePermission(checkThisModules: ["onlineExams.list"]){
            if Utils.isModuleActivated(moduleName: "onlineexamsAct"){
                menuArray.append(SideMenuReferenceModel(id:15,itemTitle:"Online Exams",itemImage: "online_exam_g", itemStoryboard: "OnlineExamsPageStoryboard"))
            }
        }
        
        
        if Utils.isUserHavePermission(checkThisModules: ["teachers.list"]){
            
            menuArray.append(SideMenuReferenceModel(id:16,itemTitle:"Teachers",itemImage: "teacher", itemStoryboard: "TeachersPageStoryboard"))
            
        }
        
        if Utils.isUserHavePermission(checkThisModules: ["students.list"]){
            menuArray.append(SideMenuReferenceModel(id:17,itemTitle:"Students",itemImage: "username", itemStoryboard: "StudentsPageStoryboard"))
        }
        
        
        if Utils.isUserHavePermission(checkThisModules: ["parents.list"]){
            
            menuArray.append(SideMenuReferenceModel(id:18,itemTitle:"Parents",itemImage: "parent_g", itemStoryboard: "ParentsPageStoryboard"))
            
        }
        
        
        if Utils.isUserHavePermission(checkThisModules: ["Invoices.list","Invoices.View"]){
            if Utils.isModuleActivated(moduleName: "paymentsAct"){
                menuArray.append(SideMenuReferenceModel(id:19,itemTitle:"Invoices",itemImage: "price", itemStoryboard: "InvoicesPageStoryboard"))
            }
        }
        if Utils.isUserHavePermission(checkThisModules: ["Invoices.dueInvoices"]){
            
            if Utils.isModuleActivated(moduleName: "paymentsAct"){
                menuArray.append(SideMenuReferenceModel(id:19,itemTitle:"Due Invoices",itemImage: "price", itemStoryboard: "DueInvoicesPageStoryboard"))
            }
        }
        
        if Utils.isUserHavePermission(checkThisModules: ["Payroll.MyPayroll"]){
            if Utils.isModuleActivated(moduleName: "payrollAct"){
                menuArray.append(SideMenuReferenceModel(id:302,itemTitle:"My Payroll",itemImage: "answers", itemStoryboard: ""))
            }
        }
        
        if Utils.isUserHavePermission(checkThisModules: ["Transportation.list"]){
            if Utils.isModuleActivated(moduleName: "transportAct") {
                menuArray.append(SideMenuReferenceModel(id:20,itemTitle:"Transportation",itemImage: "tansport_g", itemStoryboard: "TransportsPageStoryboard"))
            }
        }
        
        if Utils.isUserHavePermission(checkThisModules: ["Hostel.list"]){
            if Utils.isModuleActivated(moduleName: "hostelAct"){
                menuArray.append(SideMenuReferenceModel(id:21,itemTitle:"Hostel",itemImage: "hostel_g", itemStoryboard: "HostelsPageStoryboard"))
            }
        }
        
        if Utils.isUserHavePermission(checkThisModules: ["mediaCenter.View"]){
            if Utils.isModuleActivated(moduleName: "mediaAct") {
                menuArray.append(SideMenuReferenceModel(id:22,itemTitle:"MediaCenter",itemImage: "media_g", itemStoryboard: "MediaCenterPageStoryboard"))
            }
        }
        
        if Utils.isUserHavePermission(checkThisModules: ["classes.list"]){
            
            menuArray.append(SideMenuReferenceModel(id:23,itemTitle:"Classes",itemImage: "classes", itemStoryboard: "ClassesPageStoryboard"))
            
        }
        
        if Utils.isUserHavePermission(checkThisModules: ["Subjects.list"]){
            
            menuArray.append(SideMenuReferenceModel(id:24,itemTitle:"Subjects",itemImage: "subject", itemStoryboard: "SubjectsPageStoryboard"))
            
        }
        
        
        menuArray.append(SideMenuReferenceModel(id:25,itemTitle:"Logout",itemImage: "logout_g", itemStoryboard: "LoginPageStoryboard"))
        
        // this will be non-nil if a blur effect is applied
        guard tableView.backgroundView == nil else {
            return
        }
        
        // Set up a cool background image for demo purposes
        let imageView = UIImageView(image: UIImage(named: "saturn"))
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        tableView.backgroundView = imageView
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        
        view.backgroundColor = SolColors.getColorByName(colorName: "x_drawer_header_color")
        // Set drawer user data
        self.header_user_fullname.text = Utils.LoggedInUser.LoggedInUserFullName
        self.header_user_role.text = Utils.LoggedInUser.LoggedInUserUsername
        
        let photoManager: PhotoManager = PhotoManager()
        _ = photoManager.retrieveImage(for: SBFramework.strip(st: "\(Constants.getModRewriteBaseUrl())/dashboard/profileImage/\(Utils.LoggedInUser.LoggedInUserId ?? 0)"), imageView: self.header_user_image)
        
        self.header_logout.asCircle()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.logout))
        self.header_logout.addGestureRecognizer(tapGesture)
        
        
        
    }
    
    @objc func logout (){
        Utils.removeLoggedInUser()
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginPageStoryboard")
        self.navigationController!.pushViewController(nextVC!, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        print("SideMenu Appeared!");
        
        tableView.reloadData();
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        print("SideMenu Disappearing!")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        print("SideMenu Disappeared!")
    }
    
    //==================== Table view options ===================================//
    
    
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:SideMenuCell = tableView.dequeueReusableCell(withIdentifier: "menuCell", for: indexPath) as! SideMenuCell
        
        cell.referenceItem = menuArray[(indexPath.row)]
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        isClicked = true;
        
        let clickedItem = menuArray[indexPath.row]
     
        if clickedItem.id == 301 {
            
            self.navigationController?.pushViewController(MyVacationPage(), animated: true)
            
        }else if( clickedItem.id == 302 ){
            
            self.navigationController?.pushViewController(MyPayrollPage(), animated: true)
            
        }else{
            if clickedItem.id == 25{ Utils.removeLoggedInUser() }
            
            let nextVC = self.storyboard?.instantiateViewController(withIdentifier: (clickedItem.itemStoryboard!))
            self.navigationController!.pushViewController(nextVC!, animated: true)
        }
        
    }
    
}
